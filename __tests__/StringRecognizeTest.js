const r = require('@laboralphy/did-you-mean'); // 2.2.1

describe('basic recognition', function () {
    it('scenario du magasin de ticket', function () {
        const aList = [
            'another thing',
            'a Wutai ferry ticket to Truce',
            'a Wutai ferry ticket to Cinnabar Island'
        ];
        const sInput = 'ticket truce';
        const x1 = r.suggest(sInput, aList, { multiterm: true, full: true });
        expect(x1[0].score).toBeLessThanOrEqual(x1[1].score);
        expect(r.suggest(sInput, aList, { multiterm: true, limit: 1 }))
            .toEqual(['a Wutai ferry ticket to Truce']);
    });
    it('requete peu précise', function () {
        expect(r.suggest(
            'ferry ticket',
            [
                'another thing',
                'a Wutai ferry ticket to Truce',
                'a Wutai ferry ticket to Cinnabar Island'
            ], { multiterm: true }
        )).toEqual([
            'a Wutai ferry ticket to Truce',
            'a Wutai ferry ticket to Cinnabar Island'
        ]);
    });
    it('requete mal écrite', function () {
        expect(r.suggest(
            'ferry tocket truce',
            [
                'another thing',
                'a Wutai ferry ticket to Truce',
                'a Wutai ferry ticket to Cinnabar Island'
            ], { multiterm: true, limit: 1 }
        )).toEqual([
            'a Wutai ferry ticket to Truce'
        ]);
        expect(r.suggest(
            'fery tocket truce',
            [
                'another thing',
                'a Wutai ferry ticket to Truce',
                'a Wutai ferry ticket to Cinnabar Island'
            ], { multiterm: true, limit: 1 }
        )).toEqual([
            'a Wutai ferry ticket to Truce'
        ]);
        expect(r.suggest(
            'fery tocket cinabar',
            [
                'another thing',
                'a Wutai ferry ticket to Truce',
                'a Wutai ferry ticket to Cinnabar Island'
            ], { multiterm: true, limit: 1 }
        )).toEqual([
            'a Wutai ferry ticket to Cinnabar Island'
        ]);
        expect(r.suggest(
            'fery tocket cinabar',
            [
                'another thing',
                'a Wutai ferry ticket to Truce',
                'a Wutai ferry ticket to Cinnabar Island'
            ], { multiterm: true, limit: 1 }
        )).toEqual([
            'a Wutai ferry ticket to Cinnabar Island'
        ]);
        expect(r.suggest(
            'trève',
            [
                'un autre truc à la con',
                'un ticket pour l\'île de la trève',
                'un ticket pour l\'arène des champions'
            ], { multiterm: true, limit: 1 }
        )).toEqual([
            'un ticket pour l\'île de la trève'
        ]);
        expect(r.suggest(
            'treve',
            [
                'un autre truc à la con',
                'un ticket pour l\'île de la trève',
                'un ticket pour l\'arène des champions'
            ], { multiterm: true, limit: 1 }
        )).toEqual([
            'un ticket pour l\'île de la trève'
        ]);
    });
});

describe('bug-241023 parfois c est le nom du personnage qui est suggéré', function () {
    it('should select a ticket among "ticket" containing strings', function () {
        const a = [
            'Glouke',
            'a Wutai ferry ticket to Truce',
            'a Wutai ferry ticket to Cinnabar Island'
        ];
        const x = r.suggest('ticket', a, { multiterm: true, limit: Infinity });
        expect(x).toEqual([
            'a Wutai ferry ticket to Truce',
            'a Wutai ferry ticket to Cinnabar Island',
            'Glouke'
        ]);
    });
    it('should select a ticket among "ticket" containing strings', function () {
        const a = [
            'Glouke',
            'a Wutai ferry ticket to Truce',
            'a Wutai ferry ticket to Cinnabar Island'
        ];
        const x = r.suggest('ticket', a, { multiterm: true, limit: Infinity });
        expect(x).toEqual([
            'a Wutai ferry ticket to Truce',
            'a Wutai ferry ticket to Cinnabar Island',
            'Glouke'
        ]);
    });
});
