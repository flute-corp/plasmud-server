const MUDEngine = require('../libs/plasmud/Engine');
const MUDCONSTS = require('../libs/plasmud/consts');


function createState1 () {
    return {
        blueprints: {
            gp: {
                type: 'item',
                name: 'Pièce d\'or',
                desc: [
                    'Une pièce d\'or.'
                ],
                weight: 0.0085,
                stackable: true,
                tag: 'gp',
                inventory: false
            },
            bidule1: {
                type: 'item',
                name: 'Bidule en bois',
                desc: [
                    'Cet objet n\'a rien de particulier'
                ],
                weight: 1,
                stackable: false,
                tag: 'bidule1',
                inventory: false
            },
            bidule2: {
                type: 'item',
                name: 'Bidule en verre',
                desc: [
                    'Cet objet n\'a rien de particulier'
                ],
                weight: 1,
                stackable: false,
                tag: 'bidule2',
                inventory: false
            },
            bagwonder: {
                type: 'item',
                name: 'Sac en papier',
                desc: [
                    'Cet objet n\'a rien de particulier'
                ],
                weight: 1,
                stackable: false,
                tag: 'sacpap',
                inventory: true,
                lock: null
            },
            thing001: {
                type: 'item',
                name: 'Thing001',
                desc: [
                    'Cet objet n\'a rien de particulier'
                ],
                weight: 1,
                stackable: false,
                tag: 'thing001',
                inventory: false,
                lock: null
            }
        },
        sectors: {
            s1: {
                name: 'secteur 1',
                desc: [],
                templates: [
                    {
                        id: 'genericRoom',
                        name: 'pièce générique',
                        desc: ['description', 'de pièce', 'générique']
                    }
                ]
            }
        },
        rooms: {
            r1: {
                name: 'room 1',
                sector: 's1',
                desc: [
                    'Une pièce à une seule issue.'
                ],
                nav: {
                    s: {
                        to: 'r2',
                        name: ''
                    }
                },
                content: [
                    {
                        ref: 'gp',
                        stack: 100
                    },
                    {
                        ref: 'bidule1'
                    },
                    {
                        ref: 'bidule2'
                    },
                    {
                        ref: 'bagwonder'
                    }
                ]
            },
            rdummy: {
                name: 'dummy',
                desc: ['x'],
                sector: 's1',
                nav: {
                }
            },
            r2: {
                name: 'p2',
                desc: ['x'],
                sector: 's1',
                nav: {
                    s: {
                        to: 'rdummy',
                        name: '',
                        lock: {
                            locked: true,
                            difficulty: 11,
                            key: 'k1'
                        }
                    },
                    d: {
                        to: 'rdummy',
                        name: '',
                        secret: {
                            difficulty: 20
                        }
                    },
                    n: {
                        to: 'rdummy',
                        name: '',
                        lock: {
                            locked: true
                        }
                    }
                },
                content: [
                    {
                        ref: 'gp',
                        stack: 100
                    }
                ]
            }
        }
    };
}

function createStateSpeak () {
    return {
        blueprints: {
            speaker: {
                type: 'actor',
                name: 'speaker',
                desc: [
                    'Cet objet n\'a rien de particulier'
                ],
                weight: 1,
                tag: 'speaker',
                inventory: true
            },
            listener: {
                type: 'actor',
                name: 'listener',
                desc: [
                    'Cet objet n\'a rien de particulier'
                ],
                weight: 1,
                tag: 'listener',
                inventory: true
            }
        },
        sectors: {
            s1: {
                name: 's1',
                desc: ['x']
            }
        },
        rooms: {
            r1: {
                name: 'r1',
                desc: ['x'],
                sector: 's1',
                content: [
                    {
                        ref: 'speaker',
                        tag: 's1'
                    },
                    {
                        ref: 'listener',
                        tag: 'l0'
                    }
                ],
                nav: {
                    n: {
                        to: 'r2'
                    },
                    e: {
                        to: 'r10'
                    }
                }
            },
            r2: {
                name: 'r2',
                desc: ['x'],
                sector: 's1',
                content: [
                    {
                        ref: 'listener',
                        tag: 'l1'
                    }
                ],
                nav: {
                    n: {
                        to: 'room:r3'
                    },
                    s: {
                        to: 'room:r1'
                    }
                }
            },
            r3: {
                name: 'r3',
                desc: ['x'],
                sector: 's1',
                content: [
                    {
                        ref: 'listener',
                        tag: 'l2'
                    }
                ],
                nav: {
                    s: {
                        to: 'r2'
                    }
                }
            },
            r10: {
                name: 'r10',
                desc: ['x'],
                sector: 's1',
                content: [
                    {
                        ref: 'listener',
                        tag: 'l3'
                    }
                ],
                nav: {
                    w: {
                        to: 'r1'
                    }
                }
            }
        }
    };
}

/*
TO TEST :

getPlayerId
# createEntity
# destroyEntity
# createPlayerEntity
# _cloneEntity
getSectors
getEntities
getRooms
getBlueprint
getSector
getEntity
getRoom
isRoomExist
isEntityExist
# getLocalEntity
# getLocalEntities
# moveEntity
# getPlayerExit
# setExitLocked
# setEntityExitSpotted
# findEntitiesByTag

 */

describe('#createEntity', function () {
    it('création simple', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        expect(m.getRoom('r2')).toBeDefined();
        expect(m.getRoom('r2')).not.toBeNull();
        const oItem = m.createEntity('bidule2', m.getRoom('r2'));
        expect(oItem.tag).toBe('bidule2');
        expect(oItem.location).toBe(m.getRoom('r2'));
    });
    it('création blueprints inexistant', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        expect(() => {
            m.createEntity('bidule1000', m.getRoom('r2'));
        }).toThrow('Key "bidule1000" could not be found in collection "blueprints"');
    });
    it('ajout d\'entités dans la liste', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        expect(m.isEntityExist('entity#1')).toBeFalsy();
        const oEntity = m.createEntity('bidule1', m.getRoom('r2'));
        expect(m.isEntityExist(oEntity.id)).toBeTruthy();
    });
});

describe('#getLocalEntities', function () {
    it('basic', function () {
        const m = new MUDEngine();
        expect(m).toBeDefined();
    });
    it('lister les entités', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        const aEnt = m.getLocalEntities(m.getRoom('r1'));
        expect(aEnt).toHaveLength(4);
        expect(aEnt[0].name).toBe('Pièce d\'or');
        expect(aEnt[1].name).toBe('Bidule en bois');
        expect(aEnt[2].name).toBe('Bidule en verre');
        expect(aEnt[0].blueprint.type).toBe('item');
    });
    it('pièce bidon', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        expect(() => m.getLocalEntities(m.getRoom('r1000')))
            .toThrow('Key "r1000" could not be found in collection "rooms"');
    });
});

describe('#destroyEntity', function () {
    it('destruction d\'entity : le nombre d\'entités dans le jeu décroit', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        const r1e = m.getLocalEntities(m.getRoom('r1'));
        const r2e = m.getLocalEntities(m.getRoom('r2'));
        expect(r1e).toHaveLength(4);
        expect(r2e).toHaveLength(1);
        expect(Object.keys(m.getEntities())).toHaveLength(5);
        m.destroyEntity(r1e.shift());
        expect(Object.keys(m.getEntities())).toHaveLength(4);
        m.destroyEntity(r1e.shift());
        expect(Object.keys(m.getEntities())).toHaveLength(3);
        m.destroyEntity(r1e.shift());
        expect(Object.keys(m.getEntities())).toHaveLength(2);
        expect(() => m.destroyEntity(m.getEntity('entity#1000'))).toThrow('Key "entity#1000" could not be found in collection "entities"');
    });
});

describe('#moveEntity', function () {
    it('positionne une entité', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        expect(m.isRoom(m.getRoom('r1'))).toBeTruthy();
        expect(m.getLocalEntities(m.getRoom('r1'))).toHaveLength(4);
        expect(m.getLocalEntities(m.getRoom('r2'))).toHaveLength(1);
        const oItem3 = m.findEntitiesByTag('bidule2', m.getRoom('r1')).shift();
        expect(oItem3.location).toBe(m.getRoom('r1'));
        m.moveEntity(oItem3, m.getRoom('r2'));
        expect(m.getLocalEntities(m.getRoom('r1'))).toHaveLength(3);
        expect(m.getLocalEntities(m.getRoom('r2'))).toHaveLength(2);
        expect(oItem3.location).toBe(m.getRoom('r2'));
    });
    it('destination inexistante', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        expect(m.getLocalEntities(m.getRoom('r1'))).toHaveLength(4);
        expect(m.getLocalEntities(m.getRoom('r2'))).toHaveLength(1);
        const oItem3 = m.findEntitiesByTag('bidule2', m.getRoom('r1')).shift();
        expect(oItem3.location).toBe(m.getRoom('r1'));
        expect(() => {
            m.moveEntity(oItem3, m.getEntity('3333'));
        }).toThrow('Key "3333" could not be found in collection "entities"');
        expect(oItem3.location).toBe(m.getRoom('r1'));
    });
    it('déplacement et fusion de stack - 1', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        const oGold10 = m.findEntitiesByTag('gp', m.getRoom('r1')).shift();
        expect(oGold10).not.toBeNull();
        expect(oGold10.blueprint.type).toBe('item');
        expect(oGold10.location).toBe(m.getRoom('r1'));
        const oGold20 = m.findEntitiesByTag('gp', m.getRoom('r2')).shift();
        expect(oGold20).not.toBeNull();
        expect(oGold20.stack).toBe(100);
        m.moveEntity(oGold10, m.getRoom('r2'));
        const oGold11 = m.findEntitiesByTag('gp', m.getRoom('r1')).shift();
        expect(oGold11).toBeUndefined();
        const oGold21 = m.findEntitiesByTag('gp', m.getRoom('r2')).shift();
        expect(oGold21).not.toBeNull();
        expect(oGold21.stack).toBe(200);
    });
    it('déplacement d une partie de stack - 2', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        const oGold10 = m.findEntitiesByTag('gp', m.getRoom('r1')).shift();
        m.moveEntity(oGold10, m.getRoom('r2'), 77);
        const oGold21 = m.findEntitiesByTag('gp', m.getRoom('r2')).shift();
        expect(oGold21).not.toBeNull();
        expect(oGold21.stack).toBe(177);
        expect(oGold10.stack).toBe(23);
    });
    it('déplacement de stack dans les poches d\'un joueur', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', m.getRoom('r1'));
        const oGold10 = m.findEntitiesByTag('gp', m.getRoom('r1')).shift();
        expect(oPlayer.inventory).not.toBeNull();
        m.moveEntity(oGold10, oPlayer, 60);
        const oGoldPlayer = m.findEntitiesByTag('gp', oPlayer).shift();
        expect(oGoldPlayer.stack).toBe(60);
        expect(oGold10.stack).toBe(40);
        m.moveEntity(oGoldPlayer, m.getRoom('r1'), 15);
        expect(oGoldPlayer.stack).toBe(45);
        expect(oGold10.stack).toBe(55);
    });
});

describe('#findEntitiesByTag', function () {
    it('trouve l\'objet correspondant au tag', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        const aItemsBidule1 = m.findEntitiesByTag('bidule1', m.getRoom('r1'));
        expect(aItemsBidule1).toBeDefined();
        expect(aItemsBidule1).toBeInstanceOf(Array);
        expect(aItemsBidule1).toHaveLength(1);
        expect(aItemsBidule1[0].tag).toBe('bidule1');
        expect(aItemsBidule1[0].location).toBe(m.getRoom('r1'));
    });
    it('objet introuvable', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        const aItemsBidule9 = m.findEntitiesByTag('bidule9', m.getRoom('r1'));
        expect(aItemsBidule9).toBeDefined();
        expect(aItemsBidule9).toBeInstanceOf(Array);
        expect(aItemsBidule9).toHaveLength(0);
    });
    it('piece introuvable', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        expect(() => {
            m.findEntitiesByTag('bidule9', m.getRoom('r999'));
        }).toThrow('Key "r999" could not be found in collection "rooms"');
    });
});

describe('#getPlayerExit', function () {
    it('verification du status d\'une porte normale', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', m.getRoom('r2'));
        expect(oPlayer.location).toBe(m.getRoom('r2'));
        expect(m.getExit(oPlayer, 'x').valid).toBe(false);
        expect(m.getExit(oPlayer, 'w').valid).toBe(false);
        // s: lock d: secret n: code
        const oStatusS = m.getExit(oPlayer, 's');
        const oStatusD = m.getExit(oPlayer, 'd');
        const oStatusN = m.getExit(oPlayer, 'n');

        expect(oStatusS.valid).toBeTruthy();
        expect(oStatusS.room).toBe(m.getRoom('r2'));
        expect(oStatusS.lockable).toBeTruthy();
        expect(oStatusS.locked).toBeTruthy();
        expect(oStatusS.dcLockpick).toBe(11);
        expect(oStatusS.key).toBe('k1');
        expect(oStatusS.desc).toEqual([]);
        expect(oStatusS.secret).toBeFalsy();
        expect(oStatusS.dcSearch).toBe(0);
        expect(oStatusS.destination).toBe(m.getRoom('rdummy'));
        expect(oStatusS.discardKey).toBeFalsy();
        expect(oStatusS.name).toBe('');
        expect(oStatusS.direction).toBe('s');

        expect(oStatusN.valid).toBeTruthy();
        expect(oStatusN.room).toBe(m.getRoom('r2'));
        expect(oStatusN.lockable).toBeTruthy();
        expect(oStatusN.locked).toBeTruthy();
        expect(oStatusN.dcLockpick).toBe(Infinity);
        expect(oStatusN.key).toBe('');
        expect(oStatusN.desc).toEqual([]);
        expect(oStatusN.secret).toBeFalsy();
        expect(oStatusN.dcSearch).toBe(0);
        expect(oStatusN.destination).toBe(m.getRoom('rdummy'));
        expect(oStatusN.discardKey).toBeFalsy();
        expect(oStatusN.name).toBe('');
        expect(oStatusN.direction).toBe('n');

        expect(oStatusD.valid).toBeTruthy();
        expect(oStatusD.room).toBe(m.getRoom('r2'));
        expect(oStatusD.lockable).toBeFalsy();
        expect(oStatusD.locked).toBeFalsy();
        expect(oStatusD.dcLockpick).toBe(0);
        expect(oStatusD.key).toBe('');
        expect(oStatusD.desc).toEqual([]);
        expect(oStatusD.secret).toBeTruthy();
        expect(oStatusD.dcSearch).toBe(20);
        expect(oStatusD.destination).toBe(m.getRoom('rdummy'));
        expect(oStatusD.discardKey).toBeFalsy();
        expect(oStatusD.name).toBe('');
        expect(oStatusD.direction).toBe('d');

    });
});

describe('#setExitLocked', function () {
    it('deverrouiller une porte fermée à clé', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        expect(m.getExit(m.getRoom('r2'), 's').locked).toBeTruthy();
        m.getExit(m.getRoom('r2'), 's').locked = false;
        expect(m.getExit(m.getRoom('r2'), 's').locked).toBeFalsy();
        m.getExit(m.getRoom('r2'), 's').locked = true;
        expect(m.getExit(m.getRoom('r2'), 's').locked).toBeTruthy();
    });
    it('deverrouiller une porte inexistante', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        const oExit = m.getExit(m.getRoom('r2'), 'e');
        expect(oExit.nav).toBeUndefined();
        expect(oExit.valid).toBeFalsy();
        expect(() => {
            m.getExit(m.getRoom('r2'), 'e').locked = false;
        }).not.toThrow();
    });
});

describe('#setExitSpotted', function () {
    it('spotter une porte cachée', function () {
        const m = new MUDEngine();
        m.setAssets(createState1());
        expect(m.getExit(m.getRoom('r2'), 'd').visible).toBeFalsy();
        const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', m.getRoom('r2'));
        m.setEntityExitSpotted(oPlayer, 'd', true);
        expect(m.getExit(oPlayer, 'd').isDetectedBy(oPlayer)).toBeTruthy();
        m.setEntityExitSpotted(oPlayer, 'd', false);
        expect(m.getExit(oPlayer, 'd').isDetectedBy(oPlayer)).toBeFalsy();
    });
});

describe('#getBlueprint', function () {
    it('should not throw when blueprints has null "lock" property', function () {
        expect(() => {
            MUDEngine._checkBlueprintValid({
                type: 'item',
                name: 'Sac en papier',
                desc: [
                    'Cet objet n\'a rien de particulier'
                ],
                weight: 1,
                stackable: false,
                tag: 'sacpap',
                inventory: true,
                lock: null
            }, 'blueprints', 'x');
        }).not.toThrow();
    });
    it('should throw when "lock" property is missing"', function () {
        expect(() => {
            MUDEngine._checkBlueprintValid({
                type: 'item',
                name: 'Sac en papier',
                desc: [
                    'Cet objet n\'a rien de particulier'
                ],
                weight: 1,
                stackable: false,
                tag: 'sacpap',
                inventory: true
            }, 'blueprints');
        }).toThrow();
    });
    it('devrait renvoyer true quand le blueprints ne possède pas le champ requis "lock" mais n est pas un contenant', function () {
        expect(() => {
            MUDEngine._checkBlueprintValid({
                type: 'item',
                name: 'Sac en papier',
                desc: [
                    'Cet objet n\'a rien de particulier'
                ],
                weight: 1,
                stackable: false,
                tag: 'sacpap',
                inventory: false
            }, 'blueprints');
        }).not.toThrow();
    });
    it('verifier qu\'on puisse définir une clé', function () {
        expect(() => {
            MUDEngine._checkBlueprintValid({
                type: 'item',
                name: 'Sac en papier',
                desc: [
                    'Cet objet n\'a rien de particulier'
                ],
                weight: 1,
                stackable: false,
                tag: 'sacpap',
                inventory: false,
                lock: {
                    locked: true,
                    key: 'k'
                }
            }, 'blueprints');
        }).not.toThrow();
    });
});

describe('entity remarks', function () {
    it('creation d\'une entité avec remarque', function () {
        const m = new MUDEngine();
        m.setAssets({
            blueprints: {
                vendor: {
                    type: 'actor',
                    name: 'Vendeur à la sauvette',
                    desc: [
                        'Cet objet n\'a rien de particulier'
                    ],
                    weight: 1,
                    tag: 'v1',
                    inventory: true
                }
            },
            sectors: {
                s1: {
                    name: 's1',
                    desc: ['x']
                }
            },
            rooms: {
                r1: {
                    name: 'r1',
                    desc: ['x'],
                    sector: 's1',
                    content: [
                        {
                            ref: 'vendor',
                            remark: 'Invective les passants afin de leur proposer de voir ses marchandises.'
                        }
                    ],
                    nav: {
                    }
                }
            }
        });
        m.createPlayerEntity('x1', 'g', 'Glouke', m.getRoom('r1'));
        const [oVendor] = m.findEntitiesByTag('v1', m.getRoom('r1'));
        expect(oVendor.remark).toBe('Invective les passants afin de leur proposer de voir ses marchandises.');
    });
});

describe('parlotte', function () {
    it('4 pièces avec un parleur et 3 écouteurs', function () {
        const m = new MUDEngine();
        m.setAssets(createStateSpeak());
        m._createAllEntities();
        const [oSpeaker] = m.findEntitiesByTag('s1', m.getRoom('r1'));
        const [oListener0] = m.findEntitiesByTag('l0', m.getRoom('r1'));
        const [oListener1] = m.findEntitiesByTag('l1', m.getRoom('r2'));
        const [oListener2] = m.findEntitiesByTag('l2', m.getRoom('r3'));
        const [oListener3] = m.findEntitiesByTag('l3', m.getRoom('r10'));
        expect(oSpeaker.tag).toBe('s1');
        expect(oListener0.tag).toBe('l0');
        expect(oListener1.tag).toBe('l1');
        expect(oListener2.tag).toBe('l2');
        expect(oListener3.tag).toBe('l3');
    });
    it('le speaker emet un texte entendu par l0', function () {
        const m = new MUDEngine();
        m.setAssets(createStateSpeak());
        m._createAllEntities();
        const [oSpeaker] = m.findEntitiesByTag('s1', m.getRoom('r1'));
        const aLog = [];
        m.events.on('entity.speak', ({ entity, interlocutor, speech }) => aLog.push({ entity: entity, interlocutor: interlocutor, speech, volume: 'normal' }));
        m.events.on('entity.speak.shout', ({ entity, interlocutor, speech }) => aLog.push({ entity: entity, interlocutor: interlocutor, speech, volume: 'shout' }));
        m.events.on('entity.speak.echo', ({ entity, interlocutor, speech }) => aLog.push({ entity: entity, interlocutor: interlocutor, speech, volume: 'echo' }));
        m.speakString(oSpeaker, m.getRoom('r1'), 'test1');
        // deux entités doivent avoir recu le mesage
        expect(aLog).toHaveLength(2);
        // un des messages doit partir de s1 vers l0
        expect(aLog.find(x => x.entity.tag === 's1' && x.interlocutor.tag === 'l0' && x.speech === 'test1')).toBeDefined();
        // un des messages dois1 vers s1
        expect(aLog.find(x => x.entity.tag === 's1' && x.interlocutor.tag === 's1' && x.speech === 'test1')).toBeDefined();
    });
    it('le speaker crie un texte entendu par l0, l1, l2', function () {
        const m = new MUDEngine();
        m.setAssets(createStateSpeak());
        m._createAllEntities();
        const [oSpeaker] = m.findEntitiesByTag('s1', m.getRoom('r1'));
        const aLog = [];
        m.events.on('entity.speak', ({ entity, interlocutor, speech, volume }) =>
            aLog.push({ entity: entity, interlocutor: interlocutor, speech, volume })
        );
        m.speakString(oSpeaker, m.getRoom('r1'), 'test1', MUDCONSTS.VOLUME_SHOUT);
        // deux entités doivent avoir recu le message
        expect(aLog).toHaveLength(4);
        // un des messages doit partir de s1 vers l0
        expect(aLog.find(
            x => x.entity.tag === 's1' &&
        x.interlocutor.tag === 's1' &&
        x.volume === MUDCONSTS.VOLUME_SHOUT &&
        x.speech === 'test1')
        ).toBeDefined();
        expect(aLog.find(x => x.entity.tag === 's1' && x.interlocutor.tag === 's1' && x.speech === 'test1' && x.volume === MUDCONSTS.VOLUME_SHOUT)).toBeDefined();
        expect(aLog.find(x => x.entity.tag === 's1' && x.interlocutor.tag === 'l0' && x.speech === 'test1' && x.volume === MUDCONSTS.VOLUME_SHOUT)).toBeDefined();
        expect(aLog.find(x => x.entity.tag === 's1' && x.interlocutor.tag === 'l1' && x.speech === 'test1' && x.volume === MUDCONSTS.VOLUME_ECHO)).toBeDefined();
        expect(aLog.find(x => x.entity.tag === 's1' && x.interlocutor.tag === 'l3' && x.speech === 'test1' && x.volume === MUDCONSTS.VOLUME_ECHO)).toBeDefined();
    });
});

describe('ajustement de blueprints', function () {
    it('creation d\'une entité un poids personnalisé', function () {
        const m = new MUDEngine();
        m.events.on('asset.blueprint.hook', ({ resref, data }) => {
            if (resref === 'poids') {
                data.weight = 15;
            }
        });
        const oAssets = {
            blueprints: {
                poids: {
                    type: 'item',
                    name: 'poids',
                    desc: [],
                    weight: 0,
                    tag: 'v1',
                    inventory: false,
                    stackable: false
                }
            }
        };
        m.setAssets(oAssets);
        expect(m.getBlueprint('poids')).toEqual({
            type: 'item',
            name: 'poids',
            desc: [],
            weight: 15,
            tag: 'v1',
            inventory: false,
            stackable: false
        });
        const oPoids = m.createEntity('poids');
        expect(oAssets.blueprints.poids.weight).toBe(15);
        expect(oPoids.weight).toBe(15);
    });
});

describe('container locked at room creation', function () {
    it('creation d\'une entité avec lock structure', function () {
        const m = new MUDEngine();
        m.setAssets({
            blueprints: {
                bag: {
                    type: 'item',
                    name: 'sac',
                    desc: [
                        'Cet objet n\'a rien de particulier'
                    ],
                    weight: 1,
                    tag: 'bag',
                    stackable: false,
                    inventory: true,
                    lock: {
                        difficulty: 10,
                        locked: false
                    }
                }
            },
            sectors: {
                s1: {
                    name: 's1',
                    desc: ['x']
                }
            },
            rooms: {
                r1: {
                    name: 'r1',
                    desc: ['x'],
                    sector: 's1',
                    content: [
                        {
                            ref: 'bag',
                            locked: true
                        }
                    ],
                    nav: {
                    }
                },
                r2: {
                    name: 'r2',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        s: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                },
                r3: {
                    name: 'r3',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        n: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                }
            }
        });
        m.createPlayerEntity('x1', 'g', 'Glouke', m.getRoom('r1'));
        const [oBag] = m.findEntitiesByTag('bag', m.getRoom('r1'));
        expect(oBag.locked).toBeTruthy();
    });
    it('creation d\'une entité avec lock structure par defaut à false', function () {
        const m = new MUDEngine();
        m.setAssets({
            blueprints: {
                bag: {
                    type: 'item',
                    name: 'sac',
                    desc: [
                        'Cet objet n\'a rien de particulier'
                    ],
                    weight: 1,
                    tag: 'bag',
                    stackable: false,
                    inventory: true,
                    lock: {
                        difficulty: 10,
                        locked: false
                    }
                }
            },
            sectors: {
                s1: {
                    name: 's1',
                    desc: ['x']
                }
            },
            rooms: {
                r1: {
                    name: 'r1',
                    desc: ['x'],
                    sector: 's1',
                    content: [
                        {
                            ref: 'bag',
                            locked: false
                        }
                    ],
                    nav: {
                    }
                },
                r2: {
                    name: 'r2',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        s: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                },
                r3: {
                    name: 'r3',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        n: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                }
            }
        });
        m.createPlayerEntity('x1', 'g', 'Glouke', m.getRoom('r1'));
        const [oBag] = m.findEntitiesByTag('bag', m.getRoom('r1'));
        expect(oBag.locked).toBeFalsy();
    });
});

describe('unlocking exits', function () {
    it('should unlock exit with the right key', function () {
        const m = new MUDEngine();
        m.setAssets({
            blueprints: {
                key1: {
                    type: 'item',
                    name: 'clé',
                    desc: [
                        'Cet objet n\'a rien de particulier'
                    ],
                    weight: 1,
                    tag: 'k1',
                    stackable: false,
                    inventory: false
                }
            },
            sectors: {
                s1: {
                    name: 's1',
                    desc: ['x']
                }
            },
            rooms: {
                r1: {
                    name: 'r1',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        n: {
                            to: 'r2',
                            name: 'x',
                            lock: {
                                locked: true,
                                key: 'k1'
                            }
                        }
                    }
                },
                r2: {
                    name: 'r2',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        s: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                },
                r3: {
                    name: 'r3',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        n: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                }
            }
        });
        const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', m.getRoom('r1'));
        const oKey1 = m.createEntity('key1', oPlayer);
        const oExit1 = m.getExit(oPlayer, 'n');
        expect(oExit1.locked).toBeTruthy();
        const r1 = m.actionUnlock(oPlayer, 'n', oKey1);
        expect(r1.success).toBeTruthy();
        expect(oExit1.locked).toBeFalsy();
    });
    it('should not unlock exit with the wrong key', function () {
        const m = new MUDEngine();
        m.setAssets({
            blueprints: {
                key1: {
                    type: 'item',
                    name: 'clé',
                    desc: [
                        'Cet objet n\'a rien de particulier'
                    ],
                    weight: 1,
                    tag: 'k1',
                    stackable: false,
                    inventory: false
                },
                key2: {
                    type: 'item',
                    name: 'clé',
                    desc: [
                        'Cet objet n\'a rien de particulier'
                    ],
                    weight: 1,
                    tag: 'k2',
                    stackable: false,
                    inventory: false
                }
            },
            sectors: {
                s1: {
                    name: 's1',
                    desc: ['x']
                }
            },
            rooms: {
                r1: {
                    name: 'r1',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        n: {
                            to: 'r2',
                            name: 'x',
                            lock: {
                                locked: true,
                                key: 'k1'
                            }
                        }
                    }
                },
                r2: {
                    name: 'r2',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        s: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                },
                r3: {
                    name: 'r3',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        n: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                }
            }
        });
        const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', m.getRoom('r1'));
        const oKey1 = m.createEntity('key2', oPlayer);
        const oExit1 = m.getExit(oPlayer, 'n');
        expect(oExit1.locked).toBeTruthy();
        const r1 = m.actionUnlock(oPlayer, 'n', oKey1);
        expect(r1.success).toBeFalsy();
        expect(r1.outcome).toBe('WRONG_KEY');
        expect(oExit1.locked).toBeTruthy();
    });
    it('try to unlock door with tools', function () {
        const m = new MUDEngine();
        m.setAssets({
            blueprints: {
                tool1: {
                    type: 'item',
                    name: 'outil 1',
                    desc: [
                        'Cet objet n\'a rien de particulier'
                    ],
                    weight: 1,
                    tag: 't1',
                    stackable: true,
                    inventory: false
                },
                tool2: {
                    type: 'item',
                    name: 'outil 2',
                    desc: [
                        'Cet objet n\'a rien de particulier'
                    ],
                    weight: 1,
                    tag: 't2',
                    stackable: true,
                    inventory: false
                }
            },
            sectors: {
                s1: {
                    name: 's1',
                    desc: ['x']
                }
            },
            rooms: {
                r1: {
                    name: 'r1',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        n: {
                            to: 'r2',
                            name: 'x',
                            lock: {
                                locked: true,
                                key: 't2'
                            }
                        }
                    }
                },
                r2: {
                    name: 'r2',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        s: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                },
                r3: {
                    name: 'r3',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        n: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                }
            }
        });
        m.events.on('entity.task.attempt', oEvent => {
            if (oEvent.skill === 'unlock' && oEvent.tool.tag === 't2') {
                oEvent.success(true);
            }
        });
        const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', m.getRoom('r1'));
        const oTool1 = m.createEntity('tool1', oPlayer);
        const oTool2 = m.createEntity('tool2', oPlayer);
        const oExit1 = m.getExit(oPlayer, 'n');
        expect(oExit1.locked).toBeTruthy();
        m.actionUnlock(oPlayer, 'n', oTool1);
        expect(oExit1.locked).toBeTruthy();
        m.actionUnlock(oPlayer, 'n', oTool2);
        expect(oExit1.locked).toBeFalsy();
    });
    it('should loose key after unlocking exit', function () {
        const m = new MUDEngine();
        m.setAssets({
            blueprints: {
                key1: {
                    type: 'item',
                    name: 'clé',
                    desc: [
                        'Cet objet n\'a rien de particulier'
                    ],
                    weight: 1,
                    tag: 'k1',
                    stackable: false,
                    inventory: false
                },
                key2: {
                    type: 'item',
                    name: 'clé',
                    desc: [
                        'Cet objet n\'a rien de particulier'
                    ],
                    weight: 1,
                    tag: 'k2',
                    stackable: false,
                    inventory: false
                }
            },
            sectors: {
                s1: {
                    name: 's1',
                    desc: ['x']
                }
            },
            rooms: {
                r1: {
                    name: 'r1',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        n: {
                            to: 'r2',
                            name: 'x',
                            lock: {
                                locked: true,
                                key: 'k1',
                                discardKey: true
                            }
                        },
                        s: {
                            to: 'r3',
                            name: 'x',
                            lock: {
                                locked: true,
                                key: 'k2',
                                discardKey: false
                            }
                        }
                    }
                },
                r2: {
                    name: 'r2',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        s: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                },
                r3: {
                    name: 'r3',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        n: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                }
            }
        });
        const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', m.getRoom('r1'));
        const oKey1 = m.createEntity('key1', oPlayer);
        const oKey2 = m.createEntity('key2', oPlayer);
        expect(oKey1.location).toBe(oPlayer);
        expect(oKey2.location).toBe(oPlayer);

        m.actionUnlock(oPlayer, 'n', oKey1);
        expect(oKey1.location).not.toBe(oPlayer);

        m.actionUnlock(oPlayer, 's', oKey2);
        expect(oKey2.location).toBe(oPlayer);
    });
});

describe('search secret exit', function () {
    it('should reveal secret exit', function () {
        const m = new MUDEngine();
        m.setAssets({
            blueprints: {
            },
            sectors: {
                s1: {
                    name: 's1',
                    desc: ['x']
                }
            },
            rooms: {
                r1: {
                    name: 'r1',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        n: {
                            to: 'r2',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        },
                        s: {
                            to: 'r3',
                            name: 'x'
                        }
                    }
                },
                r2: {
                    name: 'r2',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        s: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                },
                r3: {
                    name: 'r3',
                    desc: ['x'],
                    sector: 's1',
                    content: [],
                    nav: {
                        n: {
                            to: 'r1',
                            name: 'x',
                            secret: {
                                difficulty: 10
                            }
                        }
                    }
                }
            }
        });
        m.events.on('entity.task.attempt', oEvent => {
            if (oEvent.skill === m.CONSTS.MUD_SKILL_SEARCH) {
                oEvent.success(true);
            }
        });
        const oPlayer = m.createPlayerEntity('x1', 'g', 'Glouke', m.getRoom('r1'));
        // déterminer les issues visibles
        const oNorthExit = m.getExit(oPlayer, 'n');
        expect(oNorthExit.visible).toBeFalsy();
        const f = m.actionSearchSecret(oPlayer, 'n');
        const oNorthExit2 = m.getExit(oPlayer, 'n');
        expect(f.outcome).toBe('');
        expect(f.success).toBeTruthy();
        expect(oNorthExit2.isDetectedBy(oPlayer)).toBeTruthy();
    });
});

describe('getRoomEnvironment', function () {
    it('should not show environment entities when asking for all entities in a room', function () {
        const m = new MUDEngine();
        m.setAssets({
            blueprints: {
                env1: {
                    name: 'This place is underwater.',
                    tag: '',
                    type: 'environment',
                    desc: ['gloub'],
                    environment: 'underwater'
                }
            },
            sectors: {
                s1: {
                    name: 's1',
                    desc: ['x']
                }
            },
            rooms: {
                r1: {
                    name: 'r1',
                    desc: ['x'],
                    sector: 's1',
                    content: [{
                        ref: 'env1'
                    }],
                    nav: {
                    }
                }
            }
        });
        const r1 = m.getRoom('r1');
        const a = m.getLocalEntities(r1);
        expect(a).toHaveLength(0);
    });

    it('should show environment entities when asking for environment entities in a room', function () {
        const m = new MUDEngine();
        m.setAssets({
            blueprints: {
                env1: {
                    name: 'This place is underwater.',
                    tag: '',
                    type: 'environment',
                    desc: ['it\'s underwater down here'],
                    environment: 'underwater'
                }
            },
            sectors: {
                s1: {
                    name: 's1',
                    desc: ['x']
                }
            },
            rooms: {
                r1: {
                    name: 'r1',
                    desc: ['x'],
                    sector: 's1',
                    content: [{
                        ref: 'env1'
                    }],
                    nav: {
                    }
                }
            }
        });
        const r1 = m.getRoom('r1');
        const b = m.getRoomEnvironment(r1);
        expect(b).toHaveLength(1);
        expect(b[0].ref).toBe('env1');

        const c = m.getRoomEnvironment(r1).map(e => e.name).join(' - ');
        expect(c).toBe('This place is underwater.');
    });
});

describe('Item stackable', function () {
    it('should create a room with 10 GP on floor', function () {
        const mud = new MUDEngine();
        mud.setAssets({
            blueprints: {
                gp: {
                    'type': 'item',
                    'name': 'Pièce d\'or',
                    'desc': [
                        'La pièces d\'or est une unité de négociation largement employée par les aventuriers.',
                        'Une pièce d\'or peut acheter une lance, 50 pieds de corde, 20 flèches, deux jours de rations de piste ou une chèvre. Un artisan qualifié peut gagner environ une pièce d\'or pour une journée de travail.'
                    ],
                    'weight': 0.0497,
                    'tag': 'coin-gold',
                    'stackable': true,
                    'inventory': false
                }
            },
            sectors: {
                s00: {
                    name: 's00',
                    desc: ['s00']
                }
            },
            rooms: {
                r00: {
                    'name': 'Pièce 0',
                    'desc': [
                        'description de la pièce 0'
                    ],
                    'sector': 's00',
                    'position': {
                        'x': 0,
                        'y': 0,
                        'z': 0
                    },
                    'nav': {
                    },
                    'content': [
                        {
                            'ref': 'gp',
                            'stack': 10
                        }
                    ]
                }
            }
        });
        const gp = mud.findEntitiesByTag('coin-gold', mud.getRoom('r00'));
        expect(gp).toHaveLength(1);
        expect(gp[0].stack).toBe(10);
    });
    it('should increase an item stack to 20 when adding two stacks of the same item on floor', function () {
        const mud = new MUDEngine();
        mud.setAssets({
            blueprints: {
                gp: {
                    'type': 'item',
                    'name': 'Pièce d\'or',
                    'desc': [
                        'La pièces d\'or est une unité de négociation largement employée par les aventuriers.',
                        'Une pièce d\'or peut acheter une lance, 50 pieds de corde, 20 flèches, deux jours de rations de piste ou une chèvre. Un artisan qualifié peut gagner environ une pièce d\'or pour une journée de travail.'
                    ],
                    'weight': 0.0497,
                    'tag': 'coin-gold',
                    'stackable': true,
                    'inventory': false
                }
            },
            sectors: {
                s00: {
                    name: 's00',
                    desc: ['s00']
                }
            },
            rooms: {
                r00: {
                    'name': 'Pièce 0',
                    'desc': [
                        'description de la pièce 0'
                    ],
                    'sector': 's00',
                    'position': {
                        'x': 0,
                        'y': 0,
                        'z': 0
                    },
                    'nav': {
                    },
                    'content': [
                        {
                            'ref': 'gp',
                            'stack': 10
                        }
                    ]
                }
            }
        });

        const r00 = mud.getRoom('r00');

        const gp2 = mud.createEntity('gp', null, { count: 10 });
        mud.moveEntity(gp2, r00);

        const gp = mud.findEntitiesByTag('coin-gold', r00);
        expect(gp).toHaveLength(1);
        expect(gp[0].stack).toBe(20);
    });
});

describe('roomReset', function () {
    it('should have no room entity storage when creating mud', function () {
        const mud = new MUDEngine();
        mud.setAssets(createState1());
        expect(mud.state.roomEntityStorage).toEqual({});
        const r1 = mud.getRoom('r1');
        expect(r1).toBeDefined();
        expect(mud.state.roomEntityStorage).toEqual({});
        mud._getRoomEntityStorage(r1);
        expect(mud.state.roomEntityStorage).toHaveProperty('r1');
        const t1 = mud.createEntity('thing001', r1, { id: 't1' });
        expect(t1.location).toBe(r1);
        const a1 = mud.getLocalEntities(r1);
        expect(a1.find(e => e.id === 't1')).toBeTruthy();

        mud.resetRoom(r1);
        expect(t1.location).toBeNull();
        const a2 = mud.getLocalEntities(r1);
        expect(a2.find(e => e.id === 't1')).toBeFalsy();
    });
});
