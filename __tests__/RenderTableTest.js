const TableRenderer = require('../libs/table-renderer');

describe('#renderHorizontalBorder', function () {
    it('a bunch of sizes', function () {
        const tr = new TableRenderer();
        expect(tr.renderHorizontalBorder([4], '+', '-', 'x', '+')).toBe('+------+');
        expect(tr.renderHorizontalBorder([4], '/', '-', 'x', '*')).toBe('/------*');
        expect(tr.renderHorizontalBorder([4, 1], '/', '-', 'x', '*')).toBe('/------x---*');
    });
});
