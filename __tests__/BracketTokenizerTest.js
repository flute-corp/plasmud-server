const BracketTokenier = require('../libs/bracket-tokenizer');

describe('basic', function () {
    it('is defined', function () {
        expect(() => {
            const bt = new BracketTokenier();
        }).not.toThrow();
    });
});

describe('evaluateToken', function () {
    it('devrait avoir un seul sharp x', function () {
        const bt = new BracketTokenier();
        const a = bt.evaluateToken('[#x]');
        expect(a).toEqual({ '#': ['x'] });
    });
    it('devrait avoir un sharp avec deux éléments', function () {
        const bt = new BracketTokenier();
        const a = bt.evaluateToken('[#x#y]');
        expect(a).toEqual({ '#': ['x', 'y'] });
    });
    it('devrait respecter l ordre des deux éléments', function () {
        const bt = new BracketTokenier();
        expect(bt.evaluateToken('[#x#y]'))
            .toEqual({ '#': ['x', 'y'] });
        expect(bt.evaluateToken('[#y#x]'))
            .toEqual({ '#': ['y', 'x'] });
    });
    it('penser aux éléments vides', function () {
        const bt = new BracketTokenier();
        expect(bt.evaluateToken('[##y]'))
            .toEqual({ '#': ['', 'y'] });
        expect(bt.evaluateToken('[#y#]'))
            .toEqual({ '#': ['y', ''] });
    });
});

describe('tokenize', function () {
    it('tokenization simple', function () {
        const bt = new BracketTokenier();
        expect(bt.tokenize('[#x]y'))
            .toEqual([{ tokens: {}, text: '' }, { tokens: { '#': ['x'] }, text: 'y' }]);
    });
    it('tokenization majuscule', function () {
        const bt = new BracketTokenier();
        expect(bt.tokenize('[#X]y'))
            .toEqual([{ tokens: {}, text: '' }, { tokens: { '#': ['X'] }, text: 'y' }]);
    });
    it('tokenization majuscule chiffres', function () {
        const bt = new BracketTokenier();
        expect(bt.tokenize('[#X985]y'))
            .toEqual([{ tokens: {}, text: '' }, { tokens: { '#': ['X985'] }, text: 'y' }]);
    });
    it('multiple tokens', function () {
        const bt = new BracketTokenier();
        expect(bt.tokenize('[#X985#RRR##TT]y'))
            .toEqual([{ tokens: {}, text: '' }, { tokens: { '#': ['X985', 'RRR', '', 'TT'] }, text: 'y' }]);
    });
    it('tokenier des sign avec :', function () {
        const bt = new BracketTokenier();
        expect(bt.tokenize('[:smile:]yq qsd qsdq sd'))
            .toEqual([{ tokens: {}, text: '' }, { tokens: { ':': ['smile', ''] }, text: 'yq qsd qsdq sd' }]);
    });
    it('tokenier des [ en vrac', function () {
        const bt = new BracketTokenier();
        expect(bt.tokenize('[:smile:]dsfgdfg[szerezr'))
            .toEqual([{ tokens: {}, text: '' }, { tokens: { ':': ['smile', ''] }, text: 'dsfgdfg[szerezr' }]);
        expect(bt.tokenize('[:smile:]dsfgdfg[ok]ezr'))
            .toEqual([{ tokens: {}, text: '' }, { tokens: { ':': ['smile', ''] }, text: 'dsfgdfg[ok]ezr' }]);
        expect(bt.tokenize('eeee[:smile:]dsfgdfg[ok]ezr'))
            .toEqual([{ tokens: {}, text: 'eeee' }, { tokens: { ':': ['smile', ''] }, text: 'dsfgdfg[ok]ezr' }]);
    });
    it('traitment de chaine vide', function () {
        const bt = new BracketTokenier();
        expect(bt.tokenize('[:smile:]'))
            .toEqual([{ tokens: {}, text: '' }, { tokens: { ':': ['smile', ''] }, text: '' }]);
    });
});

describe('init', function () {
    it('changement de tokens', function () {
        const bt = new BracketTokenier();
        bt.tokens = '*';
        expect(bt.tokens).toBe('*');
        expect(bt.tokenize('aaze[*777]dfg'))
            .toEqual([{ tokens: {}, text: 'aaze' }, { tokens: { '*': ['777'] }, text: 'dfg' }]);
    });
});
