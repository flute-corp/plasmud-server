const SpellSlotManager = require('../libs/spell-slot-manager');

describe('constructor', function () {
    it('should throw type error when param is not array', function () {
        expect(() => {
            new SpellSlotManager({ timers: 1 });
        }).toThrow(TypeError);
    });
    it('should throw type error when param is array with non-number', function () {
        expect(() => {
            new SpellSlotManager({ timers: [2, 4, 6, 8, 'r'] });
        }).toThrow(TypeError);
    });
    it('should not throw type error when param is array filled with number', function () {
        let x;
        expect(() => {
            x = new SpellSlotManager({ timers: [2, 4, 6, 8, 10, 12 ] });
        }).not.toThrow(TypeError);
        expect(x.timers[0]).toBe(2);
        expect(x.timers[2]).toBe(6);
        expect(x.timers[3]).toBe(8);
    });
});

describe('getSpellSlotData', function () {
    it('should create structure when asking slots for a new character', function () {
        const oNewChar = { data: {} };
        const ssm = new SpellSlotManager();
        const x = ssm.getSpellSlotData(oNewChar);
        expect(x).toEqual({ slots: [[], [], [], [], [], []]});
        expect(oNewChar.data).toHaveProperty(SpellSlotManager.NAMESPACE);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE]).toHaveProperty('slots');
    });
    it('should not recreate structure when asking slots twice for a new character', function () {
        const oNewChar = { data: {} };
        const ssm = new SpellSlotManager();
        const x = ssm.getSpellSlotData(oNewChar);
        const x2 = ssm.getSpellSlotData(oNewChar);
        expect(x2).toBe(x);
    });
});

describe('getSpellSlotLevelData', function () {
    it('should return empty array when asking for a specific slot level on new char', function () {
        const oNewChar = { data: {} };
        const ssm = new SpellSlotManager();
        const x = ssm.getSpellSlotLevelData(oNewChar, 4);
        expect(x).toEqual([]);
    });
    it('should throw an error asking for an invalid slot level on new char', function () {
        const oNewChar = { data: {} };
        const ssm = new SpellSlotManager();
        expect(() => {
            const x = ssm.getSpellSlotLevelData(oNewChar, 16);
        }).toThrow();
    });
});

describe('consumeSpellSlot', function () {
    it('should add a consumption structure when consuming spell slot', function () {
        const oNewChar = { data: {} };
        const ssm = new SpellSlotManager({ timers: [10, 11, 12, 13, 14, 15]});
        ssm.consumeSpellSlot(oNewChar, 1);
        expect(ssm.getSpellSlotData(oNewChar)).toEqual({
            slots: [[10], [], [], [], [], []]
        });
        ssm.consumeSpellSlot(oNewChar, 2);
        expect(ssm.getSpellSlotData(oNewChar)).toEqual({
            slots: [[10], [11], [], [], [], []]
        });
        ssm.consumeSpellSlot(oNewChar, 3);
        ssm.consumeSpellSlot(oNewChar, 4);
        ssm.consumeSpellSlot(oNewChar, 5);
        ssm.consumeSpellSlot(oNewChar, 6);
        expect(ssm.getSpellSlotData(oNewChar)).toEqual({
            slots: [[10], [11], [12], [13], [14], [15]]
        });
        ssm.consumeSpellSlot(oNewChar, 2);
        expect(ssm.getSpellSlotData(oNewChar)).toEqual({
            slots: [[10], [11, 11], [12], [13], [14], [15]]
        });
    });
});

describe('rechargeSpellSlot', function () {
    it('should remove value from slots[0] when recharging level 1 spell', function () {
        const oNewChar = {
            data: {
                [SpellSlotManager.NAMESPACE]: {
                    slots: [[5], [], [], [], [], []]
                }
            }
        };
        const ssm = new SpellSlotManager({ timers: [10, 11, 12, 13, 14, 15]});
        ssm.rechargeSpellSlot(oNewChar, 1);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[], [], [], [], [], []]);
    });
    it('should remove one value from slots[2] when recharging level 3 spell', function () {
        const oNewChar = {
            data: {
                [SpellSlotManager.NAMESPACE]: {
                    slots: [[5, 6, 7], [8, 9, 10], [11, 12, 13], [14, 15], [16], [17]]
                }
            }
        };
        const ssm = new SpellSlotManager({ timers: [10, 11, 12, 13, 14, 15]});
        ssm.rechargeSpellSlot(oNewChar, 3);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[5, 6, 7], [8, 9, 10], [12, 13], [14, 15], [16], [17]]);
    });
});

describe('rechargeSpellSlotLevel', function () {
    it('should remove all values from slots[0] when recharging level 1 spell', function () {
        const oNewChar = {
            data: {
                [SpellSlotManager.NAMESPACE]: {
                    slots: [[5, 6, 8, 9, 10], [], [], [], [], []]
                }
            }
        };
        const ssm = new SpellSlotManager({ timers: [10, 11, 12, 13, 14, 15]});
        ssm.rechargeSpellSlotLevel(oNewChar, 1);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[], [], [], [], [], []]);
    });
});

describe('rechargeAllSpellSlots', function () {
    it('should reset slot structure when recharging all slots', function () {
        const oNewChar = {
            data: {
                [SpellSlotManager.NAMESPACE]: {
                    slots: [[5, 6, 7], [8, 9, 10], [11, 12, 13], [14, 15], [16], [17]]
                }
            }
        };
        const ssm = new SpellSlotManager({ timers: [10, 11, 12, 13, 14, 15]});
        ssm.rechargeAllSpellSlots(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[], [], [], [], [], []]);
    });
});

describe('getConsumedSpellSlotLevelCount / getConsumedSpellSlots', function () {
    const oNewChar = {
        data: {
            [SpellSlotManager.NAMESPACE]: {
                slots: [[5, 6, 7], [8, 9, 10], [11, 12, 13], [14, 15], [16], [17]]
            }
        }
    };
    const ssm = new SpellSlotManager({ timers: [10, 11, 12, 13, 14, 15]});
    it('should return 3 when asking for consummed slot count level 1, 2, 3', function () {
        expect(ssm.getConsumedSpellSlotLevelCount(oNewChar, 1)).toBe(3);
        expect(ssm.getConsumedSpellSlotLevelCount(oNewChar, 2)).toBe(3);
        expect(ssm.getConsumedSpellSlotLevelCount(oNewChar, 3)).toBe(3);
    });
    it('should return 2 when asking for consummed slot count level 4', function () {
        expect(ssm.getConsumedSpellSlotLevelCount(oNewChar, 4)).toBe(2);
    });
    it('should return 1 when asking for consummed slot count level 5, 6', function () {
        expect(ssm.getConsumedSpellSlotLevelCount(oNewChar, 5)).toBe(1);
        expect(ssm.getConsumedSpellSlotLevelCount(oNewChar, 6)).toBe(1);
    });
    it('should return [3, 3, 3, 2, 1, 1]', function () {
        expect(ssm.getConsumedSpellSlots(oNewChar)).toEqual([3, 3, 3, 2, 1, 1]);
    });
});

describe('process', function () {
    it('should lower all timers', function () {
        const oNewChar = {
            data: {
                [SpellSlotManager.NAMESPACE]: {
                    slots: [[5, 6, 7], [8, 9, 10], [11, 12, 13], [14, 15], [16], [17]]
                }
            }
        };
        const ssm = new SpellSlotManager({ timers: [10, 11, 12, 13, 14, 15]});
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[5, 6, 7], [8, 9, 10], [11, 12, 13], [14, 15], [16], [17]]);
        ssm.restore(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[4, 5, 6], [7, 8, 9], [10, 11, 12], [13, 14], [15], [16]]);
        ssm.restore(oNewChar);
        ssm.restore(oNewChar);
        ssm.restore(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11], [12], [13]]);
        ssm.restore(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[1, 2], [3, 4, 5], [6, 7, 8], [9, 10], [11], [12]]);
        ssm.restore(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[1], [2, 3, 4], [5, 6, 7], [8, 9], [10], [11]]);
        ssm.restore(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[], [1, 2, 3], [4, 5, 6], [7, 8], [9], [10]]);
        ssm.restore(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[], [1, 2], [3, 4, 5], [6, 7], [8], [9]]);
        ssm.restore(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[], [1], [2, 3, 4], [5, 6], [7], [8]]);
        ssm.restore(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[], [], [1, 2, 3], [4, 5], [6], [7]]);
        ssm.restore(oNewChar);
        ssm.restore(oNewChar);
        ssm.restore(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[], [], [], [1, 2], [3], [4]]);
        ssm.restore(oNewChar);
        ssm.restore(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[], [], [], [], [1], [2]]);
        ssm.restore(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[], [], [], [], [], [1]]);
        ssm.restore(oNewChar);
        expect(oNewChar.data[SpellSlotManager.NAMESPACE].slots).toEqual([[], [], [], [], [], []]);
    });
});


