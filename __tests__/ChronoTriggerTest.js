const ChronoTrigger = require('../libs/chrono-trigger');

describe('ChronoTrigger', function () {
    it('should give time 0 when no stats enterd', function () {
        const ct = new ChronoTrigger();
        expect(ct.time).toBe(0);
    });
    it('should give time 1 when one stats is { start: 0, end: 1 }', function () {
        const ct = new ChronoTrigger();
        ct.begin(0);
        ct.commit(1);
        expect(ct.time).toBe(1);
    });
    it('should give avg time 1.5 when stats are (0, 1), (5, 7)', function () {
        const ct = new ChronoTrigger();
        ct.begin(0);
        ct.commit(1);
        ct.begin(5);
        ct.commit(7);
        expect(ct.time).toBe(1.5);
    });
    it('should give avg time 1.5 when stats are (0, 1), (5, 7)', function () {
        const ct = new ChronoTrigger();
        ct.begin(0);
        ct.commit(1);
        ct.begin(5);
        ct.commit(7);
        expect(ct.time).toBe(1.5);
    });
});
