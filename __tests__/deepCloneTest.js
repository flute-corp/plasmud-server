const { deepClone } = require('@laboralphy/object-fusion');

describe('#deepClone', function () {
    it('les objets doivent être identique', function () {
        const oSource = {
            alpha: {
                x: 20,
                nom: 'zeratul',
                liste: ['jan', 'fev', 'avr', { f: 3, d: 9.9, e: Infinity, tuple: [true, false] }]
            },
            beta: [9, 5, 1.1],
            gamma: 'zozozo'
        };
        const oTarget = deepClone(oSource);
        expect(oTarget).toEqual(oSource);
        expect(oTarget).not.toBe(oSource);
        expect(oTarget.alpha).not.toBe(oSource.alpha);
        expect(oTarget.alpha.liste).not.toBe(oSource.alpha.liste);
        expect(oTarget.alpha.liste[3]).not.toBe(oSource.alpha.liste[3]);
    });
});
