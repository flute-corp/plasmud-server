const MUDEngine = require('../libs/plasmud/Engine');

describe('buildItemState', function () {
    it('should return a valid item state when building key state', function () {
        const mud = new MUDEngine();
        mud.setAssets({
            'blueprints': {
                'key-bronze': {
                    'type': 'item',
                    'name': 'Clé de bronze',
                    'desc': [
                        'Une petite clé faite d\'un matériaux qui semble être du bronze. Elle ouvre les portes dont la serrure est en bronze.'
                    ],
                    'weight': 0.1,
                    'tag': 'key-bronze',
                    'stackable': false,
                    'inventory': false
                }
            },
            'sectors': {
                's00': {
                    'name': 's00',
                    'desc': ['s00']
                }
            },
            'rooms': {
                'r00': {
                    'name': 'Pièce 0',
                    'desc': [
                        'description de la pièce 0'
                    ],
                    'sector': 's00',
                    'position': {
                        'x': 0,
                        'y': 0,
                        'z': 0
                    },
                    'nav': {
                    },
                    'content': [
                    ]
                }
            }
        });
        const key = mud.createEntity('key-bronze', mud.getRoom('r00'), { id: 'e0' });
        expect(mud.buildItemState(key)).toEqual({
            _state: true,
            type: 'item',
            id: 'e0',
            ref: 'key-bronze',
            name: 'Clé de bronze',
            location: 'r00',
            stack: 1,
            remark: '',
            inventory: null,
            locked: false,
            data: {}
        });
    });
    it('item state should have property data.test.ok = 1 when handler item.save uses save({test:{ok:1}})', function () {
        const mud = new MUDEngine();
        mud.setAssets({
            'blueprints': {
                'key-bronze': {
                    'type': 'item',
                    'name': 'Clé de bronze',
                    'desc': [
                        'Une petite clé faite d\'un matériaux qui semble être du bronze. Elle ouvre les portes dont la serrure est en bronze.'
                    ],
                    'weight': 0.1,
                    'tag': 'key-bronze',
                    'stackable': false,
                    'inventory': false
                }
            },
            'sectors': {
                's00': {
                    'name': 's00',
                    'desc': ['s00']
                }
            },
            'rooms': {
                'r00': {
                    'name': 'Pièce 0',
                    'desc': [
                        'description de la pièce 0'
                    ],
                    'sector': 's00',
                    'position': {
                        'x': 0,
                        'y': 0,
                        'z': 0
                    },
                    'nav': {
                    },
                    'content': [
                    ]
                }
            }
        });
        mud.events.on('entity.save', ({ save }) => {
            save({ test: { ok: 1 }});
        });
        const key = mud.createEntity('key-bronze', mud.getRoom('r00'), { id: 'e0' });
        expect(mud.buildItemState(key)).toEqual({
            _state: true,
            type: 'item',
            id: 'e0',
            inventory: null,
            ref: 'key-bronze',
            name: 'Clé de bronze',
            location: 'r00',
            stack: 1,
            remark: '',
            locked: false,
            data: {
                test: {
                    ok: 1
                }
            }
        });
    });
    it('should update the state when several event handlers item.save are declared', function () {
        const mud = new MUDEngine();
        mud.setAssets({
            'blueprints': {
                'key-bronze': {
                    'type': 'item',
                    'name': 'Clé de bronze',
                    'desc': [
                        'Une petite clé faite d\'un matériaux qui semble être du bronze. Elle ouvre les portes dont la serrure est en bronze.'
                    ],
                    'weight': 0.1,
                    'tag': 'key-bronze',
                    'stackable': false,
                    'inventory': false
                }
            },
            'sectors': {
                's00': {
                    'name': 's00',
                    'desc': ['s00']
                }
            },
            'rooms': {
                'r00': {
                    'name': 'Pièce 0',
                    'desc': [
                        'description de la pièce 0'
                    ],
                    'sector': 's00',
                    'position': {
                        'x': 0,
                        'y': 0,
                        'z': 0
                    },
                    'nav': {
                    },
                    'content': [
                    ]
                }
            }
        });
        mud.events.on('entity.save', ({ save }) => {
            save({ test: { ok: 1 }});
        });
        mud.events.on('entity.save', ({ save }) => {
            save({ test2: { ok2: 2 }, test: { okayou: 3 }});
        });
        const key = mud.createEntity('key-bronze', mud.getRoom('r00'), { id: 'e0' });
        expect(mud.buildItemState(key)).toEqual({
            _state: true,
            type: 'item',
            id: 'e0',
            inventory: null,
            name: 'Clé de bronze',
            ref: 'key-bronze',
            location: 'r00',
            stack: 1,
            remark: '',
            locked: false,
            data: {
                test: {
                    ok: 1,
                    okayou: 3
                },
                test2: {
                    ok2: 2
                }
            }
        });
    });
    it('should build a valid player state', function () {
        const mud = new MUDEngine();
        mud.setAssets({
            'blueprints': {
                'key-bronze': {
                    'type': 'item',
                    'name': 'Clé de bronze',
                    'desc': [
                        'Une petite clé faite d\'un matériaux qui semble être du bronze. Elle ouvre les portes dont la serrure est en bronze.'
                    ],
                    'weight': 0.1,
                    'tag': 'key-bronze',
                    'stackable': false,
                    'inventory': false
                },
                'sacoche': {
                    'type': 'item',
                    'name': 'sacoche',
                    'desc': [
                        'Petite sacoche de cuire.'
                    ],
                    'weight': 1.0,
                    'tag': '',
                    'stackable': false,
                    'inventory': true,
                    'lock': null
                },
                'filer': {
                    'type': 'item',
                    'name': 'Dossier',
                    'desc': [
                        'Dossier contenant des documents.'
                    ],
                    'weight': 0.1,
                    'tag': '',
                    'stackable': false,
                    'inventory': true,
                    'lock': null
                },
                'document-1': {
                    'type': 'item',
                    'name': 'Document 1',
                    'desc': [
                        'Document 1'
                    ],
                    'weight': 0.1,
                    'tag': 'd1',
                    'stackable': true,
                    'inventory': false
                },
                'document-2': {
                    'type': 'item',
                    'name': 'Document 2',
                    'desc': [
                        'Document 2'
                    ],
                    'weight': 0.1,
                    'tag': 'd2',
                    'stackable': true,
                    'inventory': false
                },
                'document-3': {
                    'type': 'item',
                    'name': 'Document 3',
                    'desc': [
                        'Document 2'
                    ],
                    'weight': 0.1,
                    'tag': 'd3',
                    'stackable': true,
                    'inventory': false
                }
            },
            'sectors': {
                's00': {
                    'name': 's00',
                    'desc': ['s00']
                }
            },
            'rooms': {
                'r00': {
                    'name': 'Pièce 0',
                    'desc': [
                        'description de la pièce 0'
                    ],
                    'sector': 's00',
                    'position': {
                        'x': 0,
                        'y': 0,
                        'z': 0
                    },
                    'nav': {
                    },
                    'content': [
                    ]
                }
            }
        });
        const r00 = mud.getRoom('r00');
        const player = mud.createPlayerEntity('p1', 'c1', 'test', r00);
        const dateCreation = player.creation;
        const sacoche = mud.createEntity('sacoche', player, { id: 'sacoche' });
        const dossier1 = mud.createEntity('filer', sacoche, { id: 'f1' });
        const dossier2 = mud.createEntity('filer', sacoche, { id: 'f2' });
        mud.createEntity('document-1', dossier1, { id: 'd1', count: 3 });
        mud.createEntity('document-2', dossier1, { id: 'd2', count: 2 });
        mud.createEntity('document-3', dossier2, { id: 'd3', count: 5 });
        const aLosDosItemos = [
            {
                '_state': true,
                'data': {},
                'id': 'd1',
                'inventory': null,
                'location': 'f1',
                'locked': false,
                'name': 'Document 1',
                'ref': 'document-1',
                'remark': '',
                'stack': 3,
                'type': 'item'
            },
            {
                '_state': true,
                'data': {},
                'id': 'd2',
                'inventory': null,
                'location': 'f1',
                'locked': false,
                'name': 'Document 2',
                'ref': 'document-2',
                'remark': '',
                'stack': 2,
                'type': 'item'
            },
            {
                '_state': true,
                'data': {},
                'id': 'f1',
                'inventory': [
                    'd1',
                    'd2'
                ],
                'location': 'sacoche',
                'locked': false,
                'name': 'Dossier',
                'ref': 'filer',
                'remark': '',
                'stack': 1,
                'type': 'item'
            },
            {
                '_state': true,
                'data': {},
                'id': 'd3',
                'inventory': null,
                'location': 'f2',
                'locked': false,
                'name': 'Document 3',
                'ref': 'document-3',
                'remark': '',
                'stack': 5,
                'type': 'item'
            },
            {
                '_state': true,
                'data': {},
                'id': 'f2',
                'inventory': [
                    'd3'
                ],
                'location': 'sacoche',
                'locked': false,
                'name': 'Dossier',
                'ref': 'filer',
                'remark': '',
                'stack': 1,
                'type': 'item'
            },
            {
                '_state': true,
                'data': {},
                'id': 'sacoche',
                'inventory': [
                    'f1',
                    'f2'
                ],
                'location': 'c1',
                'locked': false,
                'name': 'sacoche',
                'ref': 'sacoche',
                'remark': '',
                'stack': 1,
                'type': 'item'
            }
        ];
        expect(mud.buildPlayerState(player)).toEqual({
            _state: true,
            type: 'player',
            id: 'c1',
            uid: 'p1',
            name: 'test',
            location: 'r00',
            remark: '',
            creation: dateCreation,
            data: {},
            inventory: [
                'sacoche'
            ],
            items: aLosDosItemos
        });
    });
    it('should load a valid player state', function () {
        const dNow = 1690975546227;
        const mud = new MUDEngine();
        mud.setAssets({
            'blueprints': {
                'key-bronze': {
                    'type': 'item',
                    'name': 'Clé de bronze',
                    'desc': [
                        'Une petite clé faite d\'un matériaux qui semble être du bronze. Elle ouvre les portes dont la serrure est en bronze.'
                    ],
                    'weight': 0.1,
                    'tag': 'key-bronze',
                    'stackable': false,
                    'inventory': false
                },
                'sacoche': {
                    'type': 'item',
                    'name': 'sacoche',
                    'desc': [
                        'Petite sacoche de cuire.'
                    ],
                    'weight': 1.0,
                    'tag': '',
                    'stackable': false,
                    'inventory': true,
                    'lock': null
                },
                'filer': {
                    'type': 'item',
                    'name': 'Dossier',
                    'desc': [
                        'Dossier contenant des documents.'
                    ],
                    'weight': 0.1,
                    'tag': '',
                    'stackable': false,
                    'inventory': true,
                    'lock': null
                },
                'document-1': {
                    'type': 'item',
                    'name': 'Document 1',
                    'desc': [
                        'Document 1'
                    ],
                    'weight': 0.1,
                    'tag': 'd1',
                    'stackable': true,
                    'inventory': false
                },
                'document-2': {
                    'type': 'item',
                    'name': 'Document 2',
                    'desc': [
                        'Document 2'
                    ],
                    'weight': 0.1,
                    'tag': 'd2',
                    'stackable': true,
                    'inventory': false
                },
                'document-3': {
                    'type': 'item',
                    'name': 'Document 3',
                    'desc': [
                        'Document 2'
                    ],
                    'weight': 0.1,
                    'tag': 'd3',
                    'stackable': true,
                    'inventory': false
                }
            },
            'sectors': {
                's00': {
                    'name': 's00',
                    'desc': ['s00']
                }
            },
            'rooms': {
                'r00': {
                    'name': 'Pièce 0',
                    'desc': [
                        'description de la pièce 0'
                    ],
                    'sector': 's00',
                    'position': {
                        'x': 0,
                        'y': 0,
                        'z': 0
                    },
                    'nav': {
                    },
                    'content': [
                    ]
                }
            }
        });
        const oState = {
            _state: true,
            type: 'player',
            id: 'c1',
            uid: 'p1',
            name: 'test',
            location: 'r00',
            remark: '',
            creation: dNow,
            data: {},
            inventory: [
                'sacoche'
            ],
            items: [
                {
                    '_state': true,
                    'data': {},
                    'id': 'd1',
                    'inventory': null,
                    'location': 'f1',
                    'locked': false,
                    'name': 'Document 1',
                    'ref': 'document-1',
                    'remark': '',
                    'stack': 3,
                    'type': 'item'
                },
                {
                    '_state': true,
                    'data': {},
                    'id': 'd2',
                    'inventory': null,
                    'location': 'f1',
                    'locked': false,
                    'name': 'Document 2',
                    'ref': 'document-2',
                    'remark': '',
                    'stack': 2,
                    'type': 'item'
                },
                {
                    '_state': true,
                    'data': {},
                    'id': 'f1',
                    'inventory': [
                        'd1',
                        'd2'
                    ],
                    'location': 'sacoche',
                    'locked': false,
                    'name': 'Dossier',
                    'ref': 'filer',
                    'remark': '',
                    'stack': 1,
                    'type': 'item'
                },
                {
                    '_state': true,
                    'data': {},
                    'id': 'd3',
                    'inventory': null,
                    'location': 'f2',
                    'locked': false,
                    'name': 'Document 3',
                    'ref': 'document-3',
                    'remark': '',
                    'stack': 5,
                    'type': 'item'
                },
                {
                    '_state': true,
                    'data': {},
                    'id': 'f2',
                    'inventory': [
                        'd3'
                    ],
                    'location': 'sacoche',
                    'locked': false,
                    'name': 'Dossier',
                    'ref': 'filer',
                    'remark': '',
                    'stack': 1,
                    'type': 'item'
                },
                {
                    '_state': true,
                    'data': {},
                    'id': 'sacoche',
                    'inventory': [
                        'f1',
                        'f2'
                    ],
                    'location': 'c1',
                    'locked': false,
                    'name': 'sacoche',
                    'ref': 'sacoche',
                    'remark': '',
                    'stack': 1,
                    'type': 'item'
                }
            ]
        };
        const aRegEntitiesSet = new Set();
        const aRegDestroyedEntitiesSet = new Set();
        mud.events.on('entity.register', ev => {
            aRegEntitiesSet.add(ev.entity.id);
        });
        mud.events.on('entity.destroy', ev => {
            aRegDestroyedEntitiesSet.add(ev.entity.id);
            aRegEntitiesSet.delete(ev.entity.id);
        });
        const p = mud.restorePlayerState(oState);
        expect(aRegEntitiesSet.has('d1')).toBeTruthy();
        expect(aRegEntitiesSet.has('d2')).toBeTruthy();
        expect(aRegEntitiesSet.has('d3')).toBeTruthy();
        expect(aRegEntitiesSet.has('f1')).toBeTruthy();
        expect(aRegEntitiesSet.has('f2')).toBeTruthy();
        expect(aRegEntitiesSet.has('sacoche'));
        expect(mud.isEntityExist('sacoche')).toBeTruthy();
        expect(mud.isEntityExist('d2')).toBeTruthy();
        expect(mud.isEntityExist('d3')).toBeTruthy();
        expect(mud.isEntityExist('f1')).toBeTruthy();
        expect(mud.isEntityExist('f2')).toBeTruthy();
        expect(mud.isEntityExist('d1')).toBeTruthy();
        const sacoche = mud.getEntity('sacoche');
        const d1 = mud.getEntity('d1');
        const d2 = mud.getEntity('d2');
        const d3 = mud.getEntity('d3');
        const f1 = mud.getEntity('f1');
        const f2 = mud.getEntity('f2');
        expect(sacoche.location).toBe(p);
        expect(d1.location).toBe(f1);
        expect(d2.location).toBe(f1);
        expect(d3.location).toBe(f2);
        expect(f1.location).toBe(sacoche);
        expect(f2.location).toBe(sacoche);
    });
});
