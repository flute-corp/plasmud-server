const System = require('../libs/plasmud/System');
const CustomTextRenderer = require('../libs/text-renderer');
const DummyTextProducer = require('../libs/text-renderer/TextProducerDummy');

const EXTREMLY_SIMPLE_ADVENTURE = {
    sectors: {
        s00: {
            name: 's00'
        },
        desc: ['description s00']
    },
    rooms: {
        r00: {
            sector: 's00',
            name: 'room 0',
            desc: ['description room 00'],
            nav: {
                n: {
                    to: 'r01',
                    lock: {
                        locked: true,
                        difficulty: 10
                    }
                },
                s: {
                    to: 'r02'
                }
            }
        },
        r01: {
            sector: 's00',
            name: 'room 1',
            desc: ['description room 01'],
            nav: {
                s: {
                    to: 'r00'
                }
            }
        },
        r02: {
            sector: 's00',
            name: 'room 2',
            desc: ['description room 02'],
            nav: {
                n: {
                    to: 'r00'
                }
            }
        }
    }
};

let DummyCustomTextRenderer;
beforeAll(() => {
    DummyCustomTextRenderer = new CustomTextRenderer();
    DummyCustomTextRenderer.addProducer('strings', new DummyTextProducer());
    DummyCustomTextRenderer.addProducer('templates', new DummyTextProducer());
});

describe('system instanciation', function () {
    it('should instanciate without error when there is no module', function () {
        expect(() => {
            const s = new System();
            s.textRenderer = DummyCustomTextRenderer;
            s.config = {
                modules: [],
                language: 'fr',
                styles: {}
            };
            return s.init();
        }).not.toThrow();
    });
    it('should instanciate without error when there are exploration, dialog, and quests modules', async function () {
        const s = new System();
        s.textRenderer = DummyCustomTextRenderer;
        s.config = {
            modules: [
                'libs/plasmud/modules/exploration',
                'libs/plasmud/modules/dialogs',
                'libs/plasmud/modules/quests'
            ],
            language: 'fr',
            styles: {}
        };
        await s.init();
        expect([...s._loadedModules]).toEqual(['exploration', 'dialogs', 'quests']);
    });
});

describe('création d\'un module avec une pièce', function () {
    it('should create a module with one room and log a player into it', async function () {
        const s = new System();
        s.textRenderer = DummyCustomTextRenderer;
        const oModule = {
            sectors: {
                s00: {
                    name: 's00'
                },
                desc: ['description s00']
            },
            rooms: {
                r00: {
                    sector: 's00',
                    name: 'room 0',
                    desc: ['description room 00'],
                    nav: {}
                }
            }
        };
        s.config = {
            modules: [
                'libs/plasmud/modules/exploration',
                'libs/plasmud/modules/dialogs',
                'libs/plasmud/modules/quests'
            ],
            language: 'fr',
            styles: {}
        };
        await s.init();
        s.engine.setAssets(oModule);
        s.registerPlayer('p1');
        s.engine.createPlayerEntity('p1', 'c1', 'ralphy', s.engine.getRoom('r00'));
        expect(s.engine.getEntity('c1')).toBeDefined();
    });
});

describe('crochetage de serrure', function () {
    it('should not be able to unlock door twice', async function () {
        const s = new System();
        s.textRenderer = DummyCustomTextRenderer;
        const oModule = EXTREMLY_SIMPLE_ADVENTURE;
        s.config = {
            modules: [
                'libs/plasmud/modules/exploration',
                'libs/plasmud/modules/dialogs',
                'libs/plasmud/modules/quests'
            ],
            language: 'fr',
            styles: {}
        };
        await s.init();
        s.engine.setAssets(oModule);
        s.registerPlayer('p1');
        s.engine.createPlayerEntity('p1', 'c1', 'ralphy', s.engine.getRoom('r00'));
        const c1 = s.engine.getEntity('c1');
        const oExitTest = s.engine.getExit(c1, 'n');
        expect(oExitTest.valid).toBeTruthy();
        expect(oExitTest.secret).toBeFalsy();
        expect(oExitTest.isDetectedBy(c1)).toBeTruthy();
        expect(oExitTest.lockable).toBeTruthy();
        expect(oExitTest.nav.lock).toBeDefined();
        expect(oExitTest.nav.lock.locked).toBeTruthy();
        expect(oExitTest.locked).toBeTruthy();
        const oc = s.engine._unlockExit(c1, 'n', null);
        expect(oc).toEqual({
            success: false, outcome: 'KEY_INVALID'
        });
        const oc2 = s.engine._unlockExit(c1, 'n', null);
        expect(oc2).toEqual({
            success: false, outcome: 'COOLDOWN'
        });
    });
});


describe('altered perception', function () {
    it('c2 should see c1 when c1 and c2 are both visible', async function () {
        const s = new System();
        s.textRenderer = DummyCustomTextRenderer;
        const oModule = EXTREMLY_SIMPLE_ADVENTURE;
        s.config = {
            modules: [
                'libs/plasmud/modules/exploration',
                'libs/plasmud/modules/dialogs',
                'libs/plasmud/modules/quests'
            ],
            language: 'fr',
            styles: {}
        };
        await s.init();
        s.engine.setAssets(oModule);
        s.engine.events.on('resolve.entity.visibility', ev => {
            if (ev.target.data.invisible) {
                ev.visibility = s.engine.CONSTS.VISIBILITY_INVISIBLE;
            }
        });
        s.registerPlayer('p1');
        s.engine.createPlayerEntity('p1', 'c1', 'ralphy', s.engine.getRoom('r00'));
        s.registerPlayer('p2');
        s.engine.createPlayerEntity('p2', 'c2', 'bob', s.engine.getRoom('r00'));


        const c1 = s.engine.getEntity('c1');
        const c2 = s.engine.getEntity('c2');

        expect(c1).not.toBeNull();
        expect(c2).not.toBeNull();
        expect(c1).toBeDefined();
        expect(c2).toBeDefined();

        c1.data.invisible = false;
        c2.data.invisible = false;

        expect(s.engine.getEntityVisibility(c1, c2)).toBe(s.engine.CONSTS.VISIBILITY_VISIBLE);
        expect(s.engine.getEntityVisibility(c2, c1)).toBe(s.engine.CONSTS.VISIBILITY_VISIBLE);
    });

    it('c2 should not be able to see c1 when c1 is flagged as invisible', async function () {
        const s = new System();
        s.textRenderer = DummyCustomTextRenderer;
        const oModule = EXTREMLY_SIMPLE_ADVENTURE;
        s.config = {
            modules: [
                'libs/plasmud/modules/exploration',
                'libs/plasmud/modules/dialogs',
                'libs/plasmud/modules/quests'
            ],
            language: 'fr',
            styles: {}
        };
        await s.init();
        s.engine.setAssets(oModule);
        s.engine.events.on('resolve.entity.visibility', ev => {
            if (ev.target.data.invisible) {
                ev.visibility = s.engine.CONSTS.VISIBILITY_INVISIBLE;
            }
        });
        s.registerPlayer('p1');
        s.engine.createPlayerEntity('p1', 'c1', 'ralphy', s.engine.getRoom('r00'));
        s.registerPlayer('p2');
        s.engine.createPlayerEntity('p2', 'c2', 'bob', s.engine.getRoom('r00'));


        const c1 = s.engine.getEntity('c1');
        const c2 = s.engine.getEntity('c2');

        expect(c1).not.toBeNull();
        expect(c2).not.toBeNull();
        expect(c1).toBeDefined();
        expect(c2).toBeDefined();

        c1.data.invisible = true;
        c2.data.invisible = false;

        expect(s.engine.getEntityVisibility(c1, c2)).toBe(s.engine.CONSTS.VISIBILITY_INVISIBLE);
        expect(s.engine.getEntityVisibility(c2, c1)).toBe(s.engine.CONSTS.VISIBILITY_VISIBLE);
    });


    it('c2 should not be see c1 print.room when c1 is flagged as invisible', async function () {
        const s = new System();
        s.textRenderer = DummyCustomTextRenderer;
        const oModule = EXTREMLY_SIMPLE_ADVENTURE;
        s.config = {
            modules: [
                'libs/plasmud/modules/exploration',
                'libs/plasmud/modules/dialogs',
                'libs/plasmud/modules/quests'
            ],
            language: 'fr',
            styles: {}
        };

        const aOutput = [];

        await s.init();
        s.events.on('output', ({ id, content }) => {
            aOutput.push({ id, content });
        });

        s.engine.setAssets(oModule);
        s.engine.events.on('resolve.entity.visibility', ev => {
            if (ev.target.data.invisible) {
                ev.visibility = s.engine.CONSTS.VISIBILITY_INVISIBLE;
            }
        });
        s.registerPlayer('p1');
        s.engine.createPlayerEntity('p1', 'c1', 'ralphy', s.engine.getRoom('r00'));
        s.registerPlayer('p2');
        s.engine.createPlayerEntity('p2', 'c2', 'bob', s.engine.getRoom('r00'));


        const c1 = s.engine.getEntity('c1');
        const c2 = s.engine.getEntity('c2');

        c1.data.invisible = false;
        c2.data.invisible = false;

        const { context: ctx1 } = s.players[c1.uid];
        expect(ctx1).toBeDefined();
        expect(ctx1.print).toBeDefined();

        ctx1.print.room('test1');
        expect(aOutput.pop()).toEqual({ id: 'p2', content: 'test1 - {}' });

        aOutput.splice(0, aOutput.length);

        c1.data.invisible = true;
        ctx1.print.room('test2 - you can see through my invisibility');
        ctx1.print.room.unaware('test - you cannot see me');
        expect(aOutput).toEqual([
            { id: 'p2', content: 'test - you cannot see me - {}'}
        ]);
    });

    it('c2 should not determine who is talking, when c1/invisible is speaking string', async function () {
        const s = new System();
        s.textRenderer = DummyCustomTextRenderer;
        const oModule = EXTREMLY_SIMPLE_ADVENTURE;
        s.config = {
            modules: [
                'libs/plasmud/modules/exploration',
                'libs/plasmud/modules/dialogs',
                'libs/plasmud/modules/quests'
            ],
            language: 'fr',
            styles: {}
        };

        const aOutput = [];

        await s.init();
        s.events.on('output', ({ id, content }) => {
            aOutput.push({ id, content });
        });

        s.engine.setAssets(oModule);
        s.engine.events.on('resolve.entity.visibility', ev => {
            if (ev.target.data.invisible) {
                ev.visibility = s.engine.CONSTS.VISIBILITY_INVISIBLE;
            }
        });
        s.registerPlayer('p1');
        s.engine.createPlayerEntity('p1', 'c1', 'ralphy', s.engine.getRoom('r00'));
        s.registerPlayer('p2');
        s.engine.createPlayerEntity('p2', 'c2', 'bob', s.engine.getRoom('r00'));


        const c1 = s.engine.getEntity('c1');
        const c2 = s.engine.getEntity('c2');

        c1.data.invisible = true;
        c2.data.invisible = false;

        aOutput.splice(0, aOutput.length);

        s.engine.speakString(c1, c1.location, 'Bla');
        expect(aOutput).toEqual([
            { id: 'p1', content: 'speakstrings.normal - {"name":"ralphy","speech":"Bla"}' },
            { id: 'p2', content: 'speakstrings.normal - {"name":"say.invisibleEntityName - {}","speech":"Bla"}' }
        ]);
    });
});
