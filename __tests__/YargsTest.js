const yargs = require('yargs');

describe('tester les options', function () {
    it('should parse i switch', function () {
        const y = yargs.option('items', {
            alias: 'i'
        });
        const x1 = y.parse('-i');
        expect(x1.items).toBeTruthy();
    });
});
