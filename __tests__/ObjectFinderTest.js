const {
    findDirection,
    findLidOrObject,
    findObject,
    CONTAINER_TYPES
} = require('../libs/plasmud/helpers/object-finder');

function getRoomLocalEntities () {
    return [
        {
            id: 'id1',
            name: 'Flêche acide de Melf',
            blueprint: {
                stackable: true
            }
        },
        {
            id: 'id2',
            name: 'Flétrissure horrible d\'Abi-Dalsim'
        },
        {
            id: 'id3',
            name: 'Sphère résiliente d\'Otiluke'
        },
        {
            id: 'id4',
            name: 'Main impérieuse de Bigby'
        },
        {
            id: 'id5',
            name: 'Coffre en bois ouvragé'
        },
        {
            id: 'id16',
            name: 'Chest you can open with a key'
        },
        {
            id: 'id6',
            name: 'Coffre en métal rouillé'
        },
        {
            id: 'id7',
            name: 'Sacoche en cuir véritable'
        },
        {
            id: 'id20',
            name: 'Potion de rapidité'
        }
    ];
}

function getInvLocalEntities () {
    return [
        {
            id: 'id8',
            name: 'Clé de coffre'
        },
        {
            id: 'id9',
            name: 'Clé fantôme'
        },
        {
            id: 'id10',
            name: 'Pièces d\'or'
        }
    ];
}

const CONTEXT = {
    engine: {
        isItemStackable (oItem) {
            return true;
        },
        getLocalEntities (id) {
            if (id === 'r1') {
                return getRoomLocalEntities();
            } else {
                return getInvLocalEntities();
            }
        },
        getEntity () {
            return {
                location: 'r1'
            };
        },
        text (sResRef) {
            const R = {
                'dir.n': 'north',
                'dir.e': 'east',
                'dir.w': 'west',
                'dir.s': 'south',
                'dir.ne': 'north',
                'dir.nw': 'north',
                'dir.se': 'south',
                'dir.sw': 'south',
                'dir.u': 'up',
                'dir.d': 'down'
            };
            return R[sResRef];
        }
    },
    pid: 'player::p1',
    text: s => {
        switch (s) {
        case 'dir.n':
            return 'north';

        case 'dir.e':
            return 'east';

        case 'dir.w':
            return 'west';

        case 'dir.s':
            return 'south';

        case 'dir.ne':
            return 'north-east';

        case 'dir.nw':
            return 'north-west';

        case 'dir.se':
            return 'south-east';

        case 'dir.sw':
            return 'south-west';

        case 'dir.u':
            return 'up';

        case 'dir.d':
            return 'down';
        }
    }
};

describe('basic', function () {
    it('les directions', function () {
        expect(findDirection(CONTEXT.engine, 'n')).toHaveProperty('direction', 'n');
        expect(findDirection(CONTEXT.engine, 'north')).toHaveProperty('direction', 'n');
        expect(findDirection(CONTEXT.engine, 'east')).toHaveProperty('direction', 'e');
        expect(findDirection(CONTEXT.engine, 'sw')).toHaveProperty('direction', 'sw');
        expect(findDirection(CONTEXT.engine, 'south west')).toHaveProperty('direction', 'sw');
        expect(findDirection(CONTEXT.engine, 'south-west')).toHaveProperty('direction', 'sw');
    });

    it('objets 1', function () {
        const oEnt1 = findObject(CONTEXT.engine, 'flech acid melf', ['r1']);
        expect(oEnt1.entity.name).toBe('Flêche acide de Melf');
        const oEnt2 = findObject(CONTEXT.engine, 'bigby', ['r1']);
        expect(oEnt2.entity.name).toBe('Main impérieuse de Bigby');
        expect(oEnt2.entity.id).toBe('id4');
        const oEnt4 = findObject(CONTEXT.engine, '4 flech acid melf', ['r1']);
        expect(oEnt4.entity.id).toBe('id1');
        expect(oEnt4.count).toBe(4);
    });

    it('objets trop éloignés', function () {
        const oEnt1 = findObject(CONTEXT.engine, ['tarte', 'tatin'], ['r1']);
        expect(oEnt1).toBeNull();
    });

    it('objet avec numérateur', function () {
        const oEnt1 = findObject(CONTEXT.engine, ['4', 'melf'], ['r1']);
        expect(oEnt1.entity.name).toBe('Flêche acide de Melf');
        expect(oEnt1.count).toBe(4);
    });
});
