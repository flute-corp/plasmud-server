const Quest = require('../libs/questlib');

describe('basic', function () {
    it('should not throw constructor error', function () {
        expect(() => {
            return new Quest.Manager();
        }).not.toThrow();
    });
});

describe('#QuestBlueprint.load', function () {
    it('chargement quete simple', function () {
        const qd = {
            id: 'q1',
            state: { x: 0 },
            title: '',
            chapters: [
                {
                    ref: 'start',
                    text: 'Allez chercher des clopes.',
                    watchers: {},
                    status: Quest.CONSTS.QUEST_STATUS.ACCEPTED
                },
                {
                    ref: 'done',
                    text: 'Vous avez trouvé les clopes.',
                    watchers: {},
                    status: Quest.CONSTS.QUEST_STATUS.COMPLETED
                }
            ]
        };
        const q1 = new Quest.Blueprint();
        expect(() => q1.load(qd)).not.toThrow();
        expect(q1.getStateClone()).toEqual({ x: 0 });
        expect(q1.chapters).toHaveLength(2);
        expect(q1.chapters[0].ref).toBe('start');
        expect(q1.chapters[1].ref).toBe('done');
    });
});

describe('QuestManager', function () {
    const qd = {
        id: 'q1',
        state: { clopes: 0 },
        title: 'Mes clopes !',
        chapters: [
            {
                ref: 'start',
                text: 'Allez chercher 10 clopes.',
                watchers: {
                    'item.acquire': 'checkClopes'
                },
                status: Quest.CONSTS.QUEST_STATUS.ACCEPTED
            },
            {
                text: 'Vous avez trouvé les 10 clopes.',
                watchers: {},
                status: Quest.CONSTS.QUEST_STATUS.COMPLETED
            }
        ]
    };
    it('#defineQuest', function () {
        const qm = new Quest.Manager();
        expect(qm.quests).not.toHaveProperty('q1');
        qm.defineQuest(qd);
        expect(qm.quests).toHaveProperty('q1');
    });
    it('#addQuest', function () {
        const qm = new Quest.Manager();
        const oPlayer = { data: { quests: {} } };
        qm.defineQuest(qd);
        expect(oPlayer.data.quests).not.toHaveProperty('q1');
        qm.addQuest(oPlayer, qd.id);
        expect(oPlayer.data.quests).toHaveProperty('q1');
    });
    it('#getEntityQuestReport', function () {
        const qm = new Quest.Manager();
        const oPlayer = { data: { quests: {} } };
        qm.defineQuest(qd);
        qm.addQuest(oPlayer, qd.id);
        expect(qm.getEntityQuestReport(oPlayer, 'q1')).toEqual({
            qid: 'q1',
            title: 'Mes clopes !',
            chapter: {
                title: '',
                text: 'Allez chercher 10 clopes.'
            },
            state: { clopes: 0 },
            status: 'QUEST_STATUS_ACCEPTED',
            date: 0
        });
    });
    it('#advanceEntityQuest', function () {
        const qm = new Quest.Manager();
        const oPlayer = { data: { quests: {} } };
        qm.defineQuest(qd);
        qm.addQuest(oPlayer, qd.id);
        expect(qm.getEntityQuestReport(oPlayer, 'q1')).toEqual({
            qid: 'q1',
            title: 'Mes clopes !',
            chapter: {
                title: '',
                text: 'Allez chercher 10 clopes.'
            },
            state: { clopes: 0 },
            status: 'QUEST_STATUS_ACCEPTED',
            date: 0
        });
        qm.advanceEntityQuest(oPlayer, 'q1');
        expect(qm.getEntityQuestReport(oPlayer, 'q1')).toEqual({
            qid: 'q1',
            title: 'Mes clopes !',
            chapter: {
                title: '',
                text: 'Vous avez trouvé les 10 clopes.'
            },
            state: { clopes: 0 },
            status: 'QUEST_STATUS_COMPLETED',
            date: 0
        });
    });
    it('quest avancement events', function () {
        const qm = new Quest.Manager();
        const oPlayer = { id: 111, data: { quests: {} } };
        qm.defineQuest(qd);
        qm.addQuest(oPlayer, qd.id);
        let sLastEvent = '';
        qm.events.on('quest.script', ({ parameters, script, state, advance }) => {
            if (script === 'checkClopes') {
                // on rammasse une clope, si on arrive à 10 on avance la quete
                if (parameters.item.tag === 'CLOPE') {
                    state.clopes += parameters.stack;
                    sLastEvent = 'got ' + parameters.stack + ' clope(s) - total : ' + state.clopes;
                    if (state.clopes >= 10) {
                        sLastEvent += ' - advancing';
                        advance();
                    }
                } else {
                    sLastEvent = 'bad item';
                }
            }
        });
        expect(qm.getEntityQuestReport(oPlayer, 'q1').status).toBe('QUEST_STATUS_ACCEPTED');
        expect(sLastEvent).toBe('');

        const pickup = (nCount, sItem) => {
            qm.eventItemAcquired(oPlayer, { tag: sItem }, nCount);
        };

        // acquerir 1 clope
        pickup(1, 'CLOPE');
        expect(sLastEvent).toBe('got 1 clope(s) - total : 1');
        expect(qm.getEntityQuestReport(oPlayer, 'q1').status).toBe('QUEST_STATUS_ACCEPTED');

        // acquerir 3 clopes d'un coup
        pickup(3, 'CLOPE');
        expect(sLastEvent).toBe('got 3 clope(s) - total : 4');
        expect(qm.getEntityQuestReport(oPlayer, 'q1').status).toBe('QUEST_STATUS_ACCEPTED');

        // acquerir 2 bières
        pickup(2, 'ALE');
        expect(sLastEvent).toBe('bad item');
        expect(qm.getEntityQuestReport(oPlayer, 'q1').status).toBe('QUEST_STATUS_ACCEPTED');

        // acquerir 6 clopes d'un coup
        pickup(6, 'CLOPE');
        expect(sLastEvent).toBe('got 6 clope(s) - total : 10 - advancing');
        expect(qm.getEntityQuestReport(oPlayer, 'q1').status).toBe('QUEST_STATUS_COMPLETED');
    });
});
