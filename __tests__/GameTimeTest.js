const GameTime = require('../libs/game-time');
const Month = require('../libs/game-time/Month');

describe('setTime', function () {
    it('should display 0-01-01 00:00 when initializing', function () {
        const gt = new GameTime();
        expect(gt._months).toBeInstanceOf(Array);
        expect(gt._months).toHaveLength(12);
        expect(gt._date.month).toBe(0);
        expect(gt._months[0]).toBeDefined();
        expect(gt._months[gt._date.month]).toBeDefined();
        expect(gt._months[gt._date.month]).toBeInstanceOf(Month);
        expect(gt.month).toBeDefined();
        expect(gt.month).toBeInstanceOf(Month);
        expect(gt.month.id).toBe(1);
        expect(gt.toShortString()).toBe('0-01-01 00:00');
    });

    it('should display 1970-01-01 00:00 when setting year to 1970, month 1, day 1', function () {
        const gt = new GameTime();
        gt.setDate(1970, 1, 1);
        expect(gt.toShortString()).toBe('1970-01-01 00:00');
    });

    it('should display 1970-01-01 19:28 when setting year to 1970, month 1, day 1, time hour 19 minute 28', function () {
        const gt = new GameTime();
        gt.setDate(1970, 1, 1);
        gt.setTime(19, 28);
        expect(gt.toShortString()).toBe('1970-01-01 19:28');
    });

    it('should pad hour and minutes when setting less than 10', function () {
        const gt = new GameTime();
        gt.setDate(1970, 1, 1);
        gt.setTime(9, 8);
        expect(gt.toShortString()).toBe('1970-01-01 09:08');
    });

    it('should advance to next month when advancing 30 days', function () {
        const gt = new GameTime();
        gt.setDate(1970, 1, 1);
        gt.advanceDay(30);
        expect(gt.toShortString()).toBe('1970-02-01 00:00');
    });

    it('should advance to next month when advancing 30 days', function () {
        const gt = new GameTime();
        gt.setDate(1000, 1, 1);
        gt.advanceDay(473541);
        gt.advanceMinute(22541);
        expect(gt.toShortString()).toBe('2315-06-07 15:41');
    });
});
