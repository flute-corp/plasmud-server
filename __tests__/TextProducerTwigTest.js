const TPTwig = require('../libs/text-renderer/TextProducerTwig');

describe('isResourceDefined', function () {
    it ('should say true when asking for a defined resource', async function () {
        const m = new TPTwig();
        m.init({ styles: {} });
        m.defineResource('temp1', 'template content 1');
        await m.initDone();
        expect(m.isResourceDefined('temp1')).toBeTruthy();
    });
    it ('should say false when asking for a undefined resource', async function () {
        const m = new TPTwig();
        m.init({ styles: {} });
        await m.initDone();
        expect(m.isResourceDefined('temp1000')).toBeFalsy();
    });
});

describe('renderSync', function () {
    it ('should render a resource with substitution', async function () {
        const m = new TPTwig();
        m.init({ styles: {} });
        m.defineResource('temp2', 'template content {{ name }}.');
        await m.initDone();
        expect(m.renderSync('temp2', { name: 'ralphy' }))
            .toBe('template content ralphy.');
    });
});

describe('renderAsync', function () {
    it ('should render a async resource with substitution', async function () {
        const m = new TPTwig();
        m.init({ styles: {} });
        m.defineResource('temp3', 'template content {{ name }}.');
        await m.initDone();
        expect(await m.renderAsync('temp3', { name: 'ralphy' })).toBe('template content ralphy.');
    });
});

describe('render special', function () {
    it ('should respect the newline char', async function () {
        const m = new TPTwig();
        m.init({ styles: {} });
        m.defineResource('temp40', 'XXXX\n...DDDDD');
        await m.initDone();
        expect(await m.renderAsync('temp40', {})).toBe('XXXX\n...DDDDD');
    });
    it ('should not respect the newline char (but its a twig.js bug)', async function () {
        /*
        Apres un tag twig, les éventuels \n sont ignorés

        {% apply packline %}
        {% if ddddd %}

            111
        {% endif %}

        222

        111
               222
        {% endapply %}\n

         */
        const m = new TPTwig();
        m.init({ styles: {} });
        m.defineResource('temp41', '{% if name == \'x\' %}XXXX{% else %}YYYY{% endif %}\n...DDDDD');
        await m.initDone();
        expect(await m.renderAsync('temp41', { name: 'x' })).toBe('XXXX\n...DDDDD');
        expect(await m.renderAsync('temp41', { name: 'zzz' })).toBe('YYYY\n...DDDDD');
    });
});
