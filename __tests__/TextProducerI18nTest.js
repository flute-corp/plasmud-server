const TPI18n = require('../libs/text-renderer/TextProducerI18next');

describe('isResourceDefined', function () {
    it('should not be able to tell if resource is defined before init', function () {
        const m = new TPI18n();
        m.defineResource('fr', {
            s1: 'text 1'
        });
        expect(m.isResourceDefined('s1')).toBeFalsy();
    });
    it('should be able to tell if resource is defined after initDone', async function () {
        const m = new TPI18n();
        await m.init({ strings: {}, lang: 'fr', styles: {} });
        m.defineResource('fr', {
            s1: 'text 1'
        });
        await m.initDone();
        expect(m.isResourceDefined('s1')).toBeTruthy();
    });
});

describe('render', function () {
    it('should render string when invoking render', async function () {
        const m = new TPI18n();
        await m.init({ strings: {}, lang: 'fr', styles: {} });
        m.defineResource('fr', {
            s1: 'text 1'
        });
        await m.initDone();
        expect(m.renderSync('s1')).toBe('text 1');
    });
    it('should render async string when invoking render', async function () {
        const m = new TPI18n();
        await m.init({ strings: {}, lang: 'fr', styles: {} });
        m.defineResource('fr', {
            s1: 'text 2'
        });
        await m.initDone();
        expect(await m.renderAsync('s1')).toBe('text 2');
    });
});

describe('defineResourcePath', function () {
    const promfs = {
        tree: async function () {
            return [
                'test1.json',
                'dummy.txt',
                'test2.json'
            ];
        },
        read: async function (s) {
            switch (s) {
            case 'fr/assets/strings/test1.json': {
                return JSON.stringify({
                    x10: 'file 1 x10',
                    x11: 'file 1 x11',
                    sub: {
                        x120: 'file 1 x120'
                    }
                });
            }
            case 'fr/assets/strings/test2.json': {
                return JSON.stringify({
                    x20: 'file 2 x20',
                    x21: 'file 2 x21',
                    sub: {
                        x220: 'file 2 x220'
                    }
                });
            }
            }
        }
    };
    it ('should load additional strings', async function () {
        const m = new TPI18n();
        m._promfs = promfs;
        await m.init({ strings: {}, lang: 'fr', styles: {} });
        m.defineResource('fr', { r1: 'imm res 1' });
        await m.defineResourcePath('fr/assets/strings/');
        await m.initDone();
        expect(m.renderSync('x20', {})).toBe('file 2 x20');
        expect(m.renderSync('sub.x120', {})).toBe('file 1 x120');
        expect(m.renderSync('sub.x220', {})).toBe('file 2 x220');
        expect(m.renderSync('r1', {})).toBe('imm res 1');
    });
});
