const c = require('../libs/ansi-256-colors/escape-codes');
const A = require('../libs/ansi-256-colors/index');
const Tokenizer = require('../libs/bracket-tokenizer');

describe('basic', function () {
    it('is defined', function () {
        expect(c).toBeDefined();
    });
});

describe('getColorCode', function () {
    it('couleur de base', function () {
        expect(c.getColorCode(0, 0, 0)).toBe('0');
        expect(c.getColorCode(255, 255, 255)).toBe('15');
        expect(c.getColorCode(100, 100, 100)).toBe('241');
        expect(c.getColorCode(17, 17, 17)).toBe('233');
        expect(c.getColorCode(249, 249, 249)).toBe('255');
        expect(c.getColorCode(239, 239, 239)).toBe('254');
    });
});

describe('parseRGB', function () {
    it('0x111 doit etre gris foncé', function () {
        const { r, g, b } = c.parseRGB('#111');
        expect(r).toBe(17);
        expect(g).toBe(17);
        expect(b).toBe(17);
    });
});

describe('getAnsi256Code', function () {
    it('0x111 devrait donner du gris foncé', function () {
        expect(c.getAnsi256Code('#111')).toBe('233');
        expect(c.getAnsi256Code('9')).toBe('9');
        expect(c.getAnsi256Code('009')).toBe('019');
        expect(c.getAnsi256Code('#009')).toBe('019');
        expect(c.getAnsi256Code('#418')).toBe('055');
    });
});

describe('getAnsi256Code - bug #800', function () {
    it('#800 devrait donner rouge foncé', function () {
        expect(c.parseRGB('#800000')).toEqual({ r: 128, g: 0, b: 0 });
        expect(c.parseRGB('#800')).toEqual({ r: 136, g: 0, b: 0 });
        expect(c.getColorCode(136, 0, 0)).toEqual('124');
    });
});

describe('color incorect #2ff', function () {
    it('should be like (34, 255, 255)', function () {
        expect(c.parseRGB('#2ff')).toEqual({ r: 34, g: 255, b: 255 });
        expect(c.getAnsi256Code('#2ff')).toBe('087');
    });
    it('rendering token should be like (34, 255, 255)', function () {
        expect(c.parseRGB('#2ff')).toEqual({ r: 34, g: 255, b: 255 });
        expect(c.getAnsi256Code('#2ff')).toBe('087');
    });
    it('voyons avec le tag', function () {
        const a = new A();
        // color : 0x2ff -> 0x22 (34d) 0xff 0xff
        const nExpectedCode = 87; // 1 * 36 + 5 * 6 + 5 + 16
        expect(c.getAnsi256Code('#2ff')).toBe('087');
        expect(c.fg256('087')).toBe(String.fromCharCode(27) + '[38;5;087m');
        expect(c.fg256(c.getAnsi256Code('#2ff'))).toBe(String.fromCharCode(27) + '[38;5;087m');
        const ESC = '\u001b';
        // TODO réparer ce test
        expect(a.render('[#2FF]x')).toBe(ESC + '[38;5;087mx');
    });
});

describe('bug color+bold not ended', function () {
    it('should return to previous color weight state when using [#:]', function () {
        const s = '[#f20]Vous subissez des dégâts : [#fff:b]{{ damageAmount }}[#:] [{{ damages }}] infligés par {{ attacker }}.';
        const a = new A();
        expect(a.render(s).replaceAll(String.fromCharCode(27), 'ESC'))
            .toBe('ESC[38;5;202mVous subissez des dégâts : ESC[38;5;15;1m{{ damageAmount }}ESC[38;5;202;22m [{{ damages }}] infligés par {{ attacker }}.');
    });
});
