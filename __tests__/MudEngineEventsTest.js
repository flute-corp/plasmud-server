const MUDEngine = require('../libs/plasmud/Engine');
const MUDCONSTS = require('../libs/plasmud/consts');

let mud = '';

function createContainerBlueprint (name) {
    return {
        type: MUDCONSTS.ENTITY_TYPES.PLACEABLE,
        name: name,
        desc: [
            'Le coffre de test.'
        ],
        weight: 1,
        stackable: false,
        inventory: true,
        tag: name
    };
}

function createLockedContainerBlueprint (name) {
    const oContainer = createContainerBlueprint(name);
    return {
        ...oContainer,
        tag: name,
        lock: {
            locked: true,
            difficulty: 1
        }
    };
}

function createKeyLockedContainerBlueprint (name) {
    const oContainer = createLockedContainerBlueprint(name);
    oContainer.lock.key = name + 'key';
    return oContainer;
}

function createActorBlueprint (name) {
    return {
        type: MUDCONSTS.ENTITY_TYPES.ACTOR,
        name: name,
        desc: [
            'Une créature.'
        ],
        weight: 1,
        inventory: true,
        tag: name
    };
}

function createMiscItemBlueprint (name) {
    return {
        type: MUDCONSTS.ENTITY_TYPES.ITEM,
        name: name,
        desc: [
            'Il existe de nombreux objets inhabituel dans le jeu. En voici un.'
        ],
        weight: 1,
        stackable: false,
        inventory: false,
        tag: name
    };
}

function createBagItemBlueprint (name) {
    return {
        type: MUDCONSTS.ENTITY_TYPES.ITEM,
        name: name,
        desc: [
            'Il existe de nombreux objets inhabituel dans le jeu. En voici un.'
        ],
        weight: 1,
        stackable: false,
        inventory: true,
        lock: null,
        tag: name
    };
}

function createMudInstance () {
    mud = new MUDEngine();
    const assets = {
        blueprints: {
            coffre: createContainerBlueprint('coffre'),
            coffre2: createLockedContainerBlueprint('coffre2'),
            coffre3: createKeyLockedContainerBlueprint('coffre3'),
            pomme: createMiscItemBlueprint('pomme'),
            orange: createMiscItemBlueprint('orange'),
            acteur: createActorBlueprint('acteur'),
            acteur2: createActorBlueprint('acteur'),
            coffre3key: createMiscItemBlueprint('coffre3key'),
            sacoche: createBagItemBlueprint('sacoche')
        },
        sectors: {
            s00: {
                desc: ['un secteur']
            }
        },
        rooms: {
            r00: {
                name: 'une pièce',
                desc: ['une pièce'],
                sector: 's00',
                nav: {
                    n: {
                        to: 'r01',
                        lock: {
                            locked: true,
                            difficulty: 1
                        }
                    }
                },
                content: [
                    {
                        ref: 'coffre',
                        content: [
                            {
                                ref: 'pomme'
                            }
                        ]
                    },
                    {
                        ref: 'coffre2',
                        content: [
                            {
                                ref: 'pomme'
                            }
                        ]
                    },
                    {
                        ref: 'acteur',
                        content: [
                            {
                                ref: 'orange'
                            }
                        ]
                    },
                    {
                        ref: 'acteur2',
                        tag: 'acteur2'
                    },
                    {
                        ref: 'coffre3key'
                    },
                    {
                        ref: 'coffre3'
                    }
                ]
            },
            r01: {
                name: 'une autre pièce',
                desc: ['une autre pièce'],
                sector: 's00',
                nav: {
                    s: {
                        to: 'r00'
                    }
                }
            }
        }
    };
    mud.setAssets(assets);
    return mud;
}

function getEntitiesByTagInRoom (engine, aTags, sLocation) {
    const oEntitites = {};
    aTags.forEach(t => {
        const aFound = engine.findEntitiesByTag(t, sLocation, true);
        if (aFound.length > 0) {
            oEntitites[t] = aFound.shift();
        } else {
            throw new Error('Not found : ' + t);
        }
    });
    return oEntitites;
}

function getAllTestableEntities (engine) {
    const {
        coffre,
        coffre2,
        acteur,
        acteur2,
        coffre3,
        coffre3key
    } = getEntitiesByTagInRoom(engine, [
        'coffre',
        'coffre2',
        'acteur',
        'acteur2',
        'coffre3',
        'coffre3key'
    ], engine.getRoom('r00'));
    const { pomme } = getEntitiesByTagInRoom(engine, ['pomme'], coffre);
    const { orange } = getEntitiesByTagInRoom(engine, ['orange'], acteur);
    return {
        coffre,
        coffre2,
        acteur,
        acteur2,
        pomme,
        orange,
        coffre3,
        coffre3key
    };
}

beforeEach(() => {
    createMudInstance();
});

afterEach(() => {
    mud = null;
});

describe('item.acquire event', () => {
    const aEventLog = [];
    beforeEach(() => {
        mud.events.on('item.acquire', ({ acquiredBy, acquiredFrom, entity }) => {
            aEventLog.push({
                event: 'item.acquire',
                acqby: acquiredBy?.tag,
                acqFrom: acquiredFrom ? acquiredFrom.tag : '',
                item: entity.tag
            });
        });
    });
    afterEach(() => {
        aEventLog.splice(0, aEventLog.length);
    });
    it('container should fire event "item.acquire" when actor gets an item from this container', () => {
    // déplacer l'item depuis le coffre vers la poche de l'acteur
        const { coffre, acteur, pomme } = getAllTestableEntities(mud);
        expect(mud.isEntityExist(pomme.id)).toBeTruthy();
        expect(pomme.location).toBe(coffre);
        mud.moveEntity(pomme, acteur);
        expect(aEventLog.pop()).toEqual({
            event: 'item.acquire',
            acqby: 'acteur',
            acqFrom: 'coffre',
            item: 'pomme'
        });
    });
    it('container should fire event "item.acquire" when actor puts an item in this container', () => {
        const { coffre, acteur, orange } = getAllTestableEntities(mud);
        expect(mud.isEntityExist(orange.id)).toBeTruthy();
        expect(orange.location).toBe(acteur);
        // Mettre la pomme dans l'inventaire de l'acteur
        mud.moveEntity(orange, acteur);
        // mettre la pomme dans le coffre
        mud.moveEntity(orange, coffre);
        expect(aEventLog.pop()).toEqual({
            event: 'item.acquire',
            acqby: 'coffre',
            acqFrom: 'acteur',
            item: 'orange'
        });
    });
    it('container should fire event "item.acquire" when actor receive an item from another actor', () => {
        const { orange, acteur2 } = getAllTestableEntities(mud);
        mud.moveEntity(orange, acteur2);
        expect(aEventLog.pop()).toEqual({
            event: 'item.acquire',
            acqby: 'acteur2',
            acqFrom: 'acteur',
            item: 'orange'
        });
    });
    it('actor should fire "item.aquired" event when item is created inside a container in his inventory', function () {
        const { coffre, acteur, orange, pomme } = getAllTestableEntities(mud);
        const oRoom = acteur.location;
        expect(oRoom.id).toBe('r00');
        const oSacoche1 = mud.createEntity('sacoche', oRoom, { id: 'sacoche1' });
        expect(oSacoche1.location.id).toBe('r00');
        const oSacoche2 = mud.createEntity('sacoche', oSacoche1, { id: 'sacoche2' });
        expect(oSacoche2.location.id).toBe('sacoche1');
        const oSacoche3 = mud.createEntity('sacoche', oSacoche2, { id: 'sacoche3' });
        expect(oSacoche3.location.id).toBe('sacoche2');

        mud.moveEntity(oSacoche1, acteur);

        expect(oSacoche3.location).toBe(oSacoche2);
        expect(oSacoche1.location).toBe(acteur);

        expect(aEventLog.pop()).toEqual({
            event: 'item.acquire',
            acqby: 'acteur',
            acqFrom: '',
            item: 'sacoche'
        });

        // mettre l'orange dans la sacoche 3 et constaté que l'acteur est notifié

        aEventLog.splice(0, aEventLog.length);

        mud.moveEntity(oSacoche3, acteur);
        mud.moveEntity(orange, oSacoche3);

        expect(aEventLog).toHaveLength(0);
        expect(pomme.location.name).toBe('coffre');
        mud.moveEntity(pomme, oSacoche3);
        expect(aEventLog.pop()).toEqual({
            'acqFrom': 'coffre',
            'acqby': 'acteur',
            'event': 'item.acquire',
            'item': 'pomme',
        });
        mud.moveEntity(pomme, coffre);
        expect(aEventLog.pop()).toEqual({
            'acqFrom': 'acteur',
            'acqby': 'coffre',
            'event': 'item.acquire',
            'item': 'pomme',
        });
    });
});

describe('item.drop event', () => {
    it('should fire item.drop event when actor drop an item on floor', () => {
        const { acteur, orange } = getAllTestableEntities(mud);
        const aEventLog = [];
        mud.events.on('item.drop', ({ droppedBy, location, entity }) => {
            aEventLog.push({
                event: 'item.drop',
                by: droppedBy.tag,
                room: location,
                item: entity.tag
            });
        });
        expect(orange.location).toBe(acteur);
        mud.moveEntity(orange, mud.getRoom('r00'));
        expect(aEventLog.pop()).toEqual({
            event: 'item.drop',
            by: 'acteur',
            room: mud.getRoom('r00'),
            item: 'orange'
        });
    });
});

describe('item.lose event', () => {
    const aEventLog = [];
    beforeEach(() => {
        mud.events.on('item.lose', ({ lostBy, acquiredBy, entity }) => {
            aEventLog.push({
                event: 'item.lose',
                lostBy: lostBy.tag,
                acquiredBy: acquiredBy.tag,
                item: entity.tag
            });
        });
    });
    afterEach(() => {
        aEventLog.splice(0, aEventLog.length);
    });
    it('should fire item.lose event when actor puts an item to chest', () => {
        const { coffre, acteur, orange } = getAllTestableEntities(mud);
        expect(orange.location).toBe(acteur);
        mud.moveEntity(orange, coffre);
        expect(aEventLog.pop()).toEqual({
            event: 'item.lose',
            lostBy: 'acteur',
            item: 'orange',
            acquiredBy: 'coffre'
        });
    });
    it('should fire item.lose event when actor gives an item to another actor', () => {
        const { acteur, acteur2, orange } = getAllTestableEntities(mud);
        expect(orange.location).toBe(acteur);
        mud.moveEntity(orange, acteur2);
        expect(aEventLog.pop()).toEqual({
            event: 'item.lose',
            lostBy: 'acteur',
            item: 'orange',
            acquiredBy: 'acteur2'
        });
    });
    it('should fire item.lose event when actor picks up an item from container', () => {
        const { acteur, pomme } = getAllTestableEntities(mud);
        mud.moveEntity(pomme, acteur);
        expect(aEventLog.pop()).toEqual({
            event: 'item.lose',
            lostBy: 'coffre',
            item: 'pomme',
            acquiredBy: 'acteur'
        });
    });
});

describe('container.unlock event', () => {
    const aEventLog = [];
    beforeEach(() => {
        mud.events.on('container.unlock', ({ container, actor }) => {
            aEventLog.push({
                event: 'container.unlock',
                actor: actor.tag,
                container: container.tag
            });
        });
        mud.events.on('entity.task.attempt', oEvt => {
            oEvt.success();
        });
    });
    afterEach(() => {
        aEventLog.splice(0, aEventLog.length);
    });
    it('should fire container.unlock when an actor unlocks a chest', () => {
        const { coffre2, acteur } = getAllTestableEntities(mud);
        expect(coffre2.locked).toBeTruthy();
        mud.actionUnlock(acteur, coffre2);
        expect(aEventLog.pop()).toEqual({
            event: 'container.unlock',
            actor: 'acteur',
            container: 'coffre2'
        });
    });
});

describe('container.unlock.failure event', () => {
    const aEventLog = [];
    beforeEach(() => {
        mud.events.on('container.unlock.failure', ({ container, actor }) => {
            aEventLog.push({
                event: 'container.unlock.failure',
                actor: actor.tag,
                container: container.tag
            });
        });
        mud.events.on('entity.task.attempt', oEvt => {
            oEvt.success(false);
        });
    });
    afterEach(() => {
        aEventLog.splice(0, aEventLog.length);
    });
    it('should fire container.unlock.failure when an actor fails to unlock a chest', () => {
        const { coffre2, acteur } = getAllTestableEntities(mud);
        expect(coffre2.locked).toBeTruthy();
        mud.actionUnlock(acteur, coffre2);
        expect(aEventLog.pop()).toEqual({
            event: 'container.unlock.failure',
            actor: 'acteur',
            container: 'coffre2'
        });
    });
});

describe('exit.unlock event', () => {
    const aEventLog = [];
    beforeEach(() => {
        mud.events.on('exit.unlock', ({ room, direction, actor }) => {
            aEventLog.push({
                event: 'exit.unlock',
                actor: actor.tag,
                room,
                direction
            });
        });
        mud.events.on('entity.task.attempt', oEvt => {
            oEvt.success();
        });
    });
    afterEach(() => {
        aEventLog.splice(0, aEventLog.length);
    });
    it('should fire exit.unlock, (and should set an exit lock flag to false) when an actor attempts to unlock an exit', () => {
        const { acteur } = getAllTestableEntities(mud);
        // door is initially locked
        expect(mud.getExit(mud.getRoom('r00'), 'n').locked).toBeTruthy();
        mud.actionUnlock(acteur, 'n');
        // the event exit.unlock has been fired
        expect(aEventLog.pop()).toEqual({
            event: 'exit.unlock',
            actor: 'acteur',
            room: mud.getRoom('r00'),
            direction: 'n'
        });
        // the exit r00.n is now unlocked
        expect(mud.getExit(mud.getRoom('r00'), 'n').locked).toBeFalsy();
    });
});

describe('exit.unlock.failure event', () => {
    const aEventLog = [];
    beforeEach(() => {
        mud.events.on('exit.unlock.failure', ({ room, direction, actor }) => {
            aEventLog.push({
                event: 'exit.unlock.failure',
                actor: actor.tag,
                room,
                direction
            });
        });
        mud.events.on('entity.task.attempt', oEvt => {
            oEvt.success(false);
        });
    });
    afterEach(() => {
        aEventLog.splice(0, aEventLog.length);
    });
    it('should fire exit.unlock.failure when an actor attempts to unlock exit (and fails)', () => {
        const { acteur } = getAllTestableEntities(mud);
        expect(mud.getExit(mud.getRoom('r00'), 'n').locked).toBeTruthy();
        mud.actionUnlock(acteur, 'n');
        expect(aEventLog.pop()).toEqual({
            event: 'exit.unlock.failure',
            actor: 'acteur',
            room: mud.getRoom('r00'),
            direction: 'n'
        });
        expect(mud.getExit(mud.getRoom('r00'), 'n').locked).toBeTruthy();
    });
});

describe('entity.room.enter event', () => {
    const aEventLog = [];
    beforeEach(() => {
        mud.events.on('entity.room.enter', ({ entity, from, to }) => {
            aEventLog.push({
                event: 'entity.room.enter',
                entity: entity.tag,
                from,
                to
            });
        });
    });
    afterEach(() => {
        aEventLog.splice(0, aEventLog.length);
    });
    it('should fire entity.room.enter event when actor moves from r00 to r01', () => {
        const { acteur } = getAllTestableEntities(mud);
        mud.moveEntity(acteur, mud.getRoom('r01'));
        expect(aEventLog.pop()).toEqual({
            event: 'entity.room.enter',
            entity: 'acteur',
            from: mud.getRoom('r00'),
            to: mud.getRoom('r01')
        });
    });
});

describe('entity.destroy event', () => {
    const aEventLog = [];
    beforeEach(() => {
        mud.events.on('entity.destroy', ({ entity }) => {
            aEventLog.push({
                event: 'entity.destroy',
                entity: entity.tag
            });
        });
    });
    afterEach(() => {
        aEventLog.splice(0, aEventLog.length);
    });
    it('should fire entity.Destroy event when an entity is destroyed', () => {
        const { pomme } = getAllTestableEntities(mud);
        mud.destroyEntity(pomme);
        expect(aEventLog.pop()).toEqual({
            event: 'entity.destroy',
            entity: 'pomme'
        });
    });
});

describe('entity.create event', () => {
    const aEventLog = [];
    beforeEach(() => {
        mud.events.on('entity.create', ({ entity, location }) => {
            aEventLog.push({
                event: 'entity.create',
                entity: entity.tag,
                location
            });
        });
    });
    afterEach(() => {
        aEventLog.splice(0, aEventLog.length);
    });
    it('should fire entity.create event when an entity is created', () => {
        mud.createEntity('orange', mud.getRoom('r01'));
        expect(aEventLog.pop()).toEqual({
            event: 'entity.create',
            entity: 'orange',
            location: mud.getRoom('r01')
        });
    });
});

describe('entity.speak event', () => {
    const aEventLog = [];
    beforeEach(() => {
        mud.events.on('entity.speak', ({ entity, speech }) => {
            aEventLog.push({
                event: 'entity.speak',
                entity: entity.tag,
                speech
            });
        });
    });
    afterEach(() => {
        aEventLog.splice(0, aEventLog.length);
    });
    it('should fire entity.speak event when an actor is speaking', () => {
        const { acteur } = getAllTestableEntities(mud);
        mud.speakString(acteur, mud.getRoom('r00'), 'speech');
        expect(aEventLog.pop()).toEqual({
            event: 'entity.speak',
            entity: 'acteur',
            speech: 'speech'
        });
    });
});

describe('container.lock event', () => {
    const aEventLog = [];
    beforeEach(() => {
        mud.events.on('container.lock', ({ container, actor }) => {
            aEventLog.push({
                event: 'container.lock',
                container,
                actor
            });
        });
    });
    afterEach(() => {
        aEventLog.splice(0, aEventLog.length);
    });
    it('should fire container.lock event when actor locks container', () => {
        const { acteur, coffre3key, coffre3 } = getAllTestableEntities(mud);
        const oCoffre = coffre3;
        const oKey = coffre3key;
        expect(oCoffre.blueprint.lock.key).toBe('coffre3key');
        expect(oKey.tag).toBe('coffre3key');
        oCoffre.locked = false;
        expect(aEventLog).toHaveLength(0);
        mud.moveEntity(coffre3key, acteur);
        const { outcome, success } = mud.actionLock(acteur, coffre3, coffre3key);
        expect(outcome).toBe(MUDCONSTS.LOCK_OUTCOMES.SUCCESS);
        expect(success).toBeTruthy();
        expect(aEventLog).toHaveLength(1);
        expect(aEventLog[0]).toEqual({
            event: 'container.lock',
            container: coffre3,
            actor: acteur
        });
    });
});
