const Cooldown = require('../libs/plasmud/helpers/Cooldown');

describe('isCooldown', function () {
    it('should return true when not initialized', function () {
        const cd = new Cooldown();
        const data = {};
        const TS = 1000;
        cd.dateNowMethod = () => TS;
        expect(cd.now).toBe(1000);
        expect(cd.isCooldown(data, 'test', 6)).toBeTruthy();
        expect(cd.getCooldownRegistry(data, 'test').init).toBeTruthy();
        expect(cd.getCooldownRegistry(data, 'test').ts).toBe(1000);
        expect(cd.getCooldownRegistry(data, 'test').delay).toBe(6000);
    });
    it('should return false when cooldown is not done', function () {
        const cd = new Cooldown();
        const data = {};
        let TS = 1000;
        cd.dateNowMethod = () => TS;
        expect(cd.isCooldown(data, 'test', 6)).toBeTruthy();
        TS = 2000;
        expect(cd.isCooldown(data, 'test', 6)).toBeFalsy();
    });
    it('should return false, then true when cooldown is not done, then done', function () {
        const cd = new Cooldown();
        const data = {};
        let TS = 1000;
        cd.dateNowMethod = () => TS;
        expect(cd.isCooldown(data, 'test', 6)).toBeTruthy();
        TS = 2000;
        expect(cd.isCooldown(data, 'test', 6)).toBeFalsy();
        TS = 6999;
        expect(cd.isCooldown(data, 'test', 6)).toBeFalsy();
        TS = 7000;
        expect(cd.isCooldown(data, 'test', 6)).toBeTruthy();
    });
});
