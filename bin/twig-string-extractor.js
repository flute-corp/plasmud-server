const fs = require('fs');
const TWIG = require('twig');
const util = require('util');

function displayToken (token, aAggregator) {
    if (!aAggregator) {
        throw new Error('Missing parameter : aggregator');
    }
    if (token === undefined || token === null) {
        return;
    }
    if (typeof token !== 'object') {
        return;
    }
    if (Array.isArray(token)) {
        token.forEach(t => displayToken(t, aAggregator));
        return;
    }
    switch (token.type) {
    case 'raw':
    case 'Twig.expression.type.string': {
        aAggregator.push(token.value);
        break;
    }
    default: {
        for (const value of Object.values(token)) {
            displayToken(value, aAggregator);
        }
    }
    }
}

function go1 () {
    const text1 = fs.readFileSync('../libs/plasmud/modules/addv/assets/fr/templates/stats.twig').toString();
    const template1 = TWIG.twig({
        id: 'text1',
        allowInlineIncludes: true,
        data: text1
    });
    const aOutput = [];
    template1.tokens.forEach(token => {
        if ('token' in token) {
            displayToken(token.token, aOutput);
        }
    });
    console.log(aOutput);
}

go1();
