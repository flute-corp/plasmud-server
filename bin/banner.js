#!/usr/bin/env node

const Banner = require('../libs/block-banner/BlockBanner');

const a = process.argv.slice(2);

if (a.length > 0) {
    let bComments = false;
    if (a[0] === '//') {
        bComments = true;
        a.shift();
    }
    const sInput = a.join(' ');
    const sOutput = bComments
        ? Banner
            .renderString(sInput)
            .split('\n')
            .map(s => '// ' + s)
            .join('\n')
        : Banner
            .renderString(sInput);
    console.log(sOutput);
}
