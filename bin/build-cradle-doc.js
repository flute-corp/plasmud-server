const { createContainer } = require('../srv/config/dependencies');
const fs = require('node:fs');

function getSource () {
    const aOutput = [];
    const container = createContainer();
    const aMembers = Object.keys(container.cradle);
    const sList = aMembers
        .map(s => '  ' + s)
        .join(',\n');
    const sObject = '{\n' + sList + '\n}';
    const sProperties = aMembers
        .map(s => ' * @property ' + s)
        .join('\n');
    const sDoc = '/**\n' + ' * @typedef ApplicationCradle {object}\n' + sProperties + '\n */';
    aOutput.push('// THIS FILE IS AUTO GENERATED - DO NOT MODIFY');
    aOutput.push('// ' + new Date().toString());
    aOutput.push(sDoc);
    aOutput.push('/**');
    aOutput.push(' * @return {ApplicationCradle}');
    aOutput.push(' */');
    aOutput.push('function getCradleMembers (' + sObject + ') {');
    aOutput.push('  return ' + sObject);
    aOutput.push('}');
    aOutput.push('');
    aOutput.push('module.exports = { getCradleMembers }');
    return aOutput.join('\n');
}

function main () {
    const sSource = getSource();
    fs.writeFileSync('srv/config/cradle.members.js', sSource);
}

main();
