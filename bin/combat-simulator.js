const Addv = require('@laboralphy/addv-srd');
const addvConfig = require('@laboralphy/addv-srd/src/config');

addvConfig.setModuleActive('classic', true);

class Runner {
    constructor () {
        this._addv = new Addv();
    }

    init () {
        this._addv.init();
    }

    startFight (c1, c2, nRoundCount = 100) {
        const f1 = this._addv.createEntity(c1);
        const f2 = this._addv.createEntity(c2);
        while (f1.store.getters.getHitPoints > 0 && f2.store.getters.getHitPoints > 0 && nRoundCount > 0) {
            const a1 = f1.store.getters.getAttackCount;
            const a2 = f2.store.getters.getAttackCount;
            while (a1 > 0 && a2 > 0) {

            }
            --nRoundCount;
        }
    }
}
