const { exec } = require('child_process');

function translate (sString, sFrom, sTo) {
    return new Promise((resolve, reject) => {
        exec(`trans -b -s ${sFrom} -t ${sTo} "${sString}"`, function (err, stdout, stderr) {
            if (err) {
                reject(new Error('Command error: ' + err.message), { cause: err });
            }
            resolve(stdout.trim());
        });
    });
}

function isContainingTags (sString) {
    return sString.includes('{{') || sString.includes('$t(');
}


function translateAllValues (oJSON, sFrom, sTo) {
    const oJSONTarget = {};
}

async function main() {
    console.log(await translate('damage immunity', 'en', 'fr'));
}

main();
