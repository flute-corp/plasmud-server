module.exports = function main ({ engine, self, interactor }) {
    engine.speakString(self, self.location, engine.text('events.unlockingDoor'), engine.CONSTS.VOLUME_NOTIFY);
    engine.getExit(self.location, 'n').locked = false;
};
