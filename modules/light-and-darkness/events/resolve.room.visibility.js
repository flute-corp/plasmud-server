function getInventoryItemsByTag (engine, actor, tag) {
    return engine.getLocalEntities(actor) // getLocalEntities: ok
        .filter(entity => engine.isEntityTagged(entity, tag));
}

function getRoomActors (engine, room) {
    return engine.getLocalEntities(room) // getLocalEntities: ok
        .filter(entity => engine.isActor(entity));
}

module.exports = (oEvent) => {
    const { engine, room, watcher } = oEvent;
    if (oEvent.visibility === engine.CONSTS.VISIBILITY_DARKNESS) {
        if (getRoomActors(engine, room)
            .some(actor => getInventoryItemsByTag(engine, actor, 'effect-light').length > 0)) {
            oEvent.visibility = engine.CONSTS.VISIBILITY_VISIBLE;
            return;
        }
        if (getInventoryItemsByTag(engine, watcher, 'effect-darkvision').length > 0) {
            console.log('effect-darkvision');
            oEvent.visibility = engine.CONSTS.VISIBILITY_VISIBLE;
        }
    }
};
