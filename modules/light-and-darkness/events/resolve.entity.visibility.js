module.exports = (oEvent) => {
    const { engine, room, target, watcher } = oEvent;
    // déterminer is la cible est invisible
    const bTargetInvisible = engine.findEntitiesByTag('effect-invisibility', target).length > 0;
    const bWatcherCanSeeInvisibility = engine.findEntitiesByTag('effect-see-invisibility', watcher).length > 0;
    if (bTargetInvisible && !bWatcherCanSeeInvisibility) {
        oEvent.visibility = engine.CONSTS.VISIBILITY_INVISIBLE;
    }
};
