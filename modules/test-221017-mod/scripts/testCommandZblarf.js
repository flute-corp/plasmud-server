/**
 * Affiche juste "ZBLARF"
 * @param context {MUDContext}
 * @param parameters {string[]}
 * @param preventDefault {Function}
 */
module.exports = function main (context, parameters, preventDefault) {
    context.print('ZBLARF');
    preventDefault();
};
