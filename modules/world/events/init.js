const debug = require('debug');

const log = debug('mod:world');

module.exports = function main (evt) {
    log('Development world ! v0.0.1');
};
