module.exports = function main ({ engine, self: oSelf, interactor: oPlayer, item: oItem, type }) {
    if (type === engine.CONSTS.DISTURBANCE_ITEM_ADDED) {
        if (engine.isEntityTagged(oItem, 'wpn-dagger')) {
            engine.speakString(self, oPlayer, engine.text('gobelinQuest.accepteCadeau', { giver: oPlayer.name, item: oItem.name, count: oItem.stack }));
            oSelf.remark = engine.text('gobelinQuest.happy', { giver: oPlayer.name });
        } else {
            engine.speakString(self, oPlayer, engine.text('gobelinQuest.noThx'));
            engine.moveEntity(oItem, oSelf.location);
        }
    }
};
