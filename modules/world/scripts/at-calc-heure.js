module.exports = function main (evt) {
    const d = new Date();
    const h = d.getHours();
    const m = d.getMinutes();
    evt.variables.serverTime = h.toString() + ' heure' + (h > 1 ? 's' : '') + ' ' + m.toString();
};
