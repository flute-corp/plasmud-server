module.exports = function main ({ engine, self: oGob, entity: oEntity, types }) {
    const { heard, seen, vanished, inaubible } = types;
    const sRoom = oGob.location;
    console.log('event perception fired');
    if (seen) {
        engine.speakString(self, sRoom, 'Moi, ' + oGob.name + ' voit apparaitre : ' + oEntity.name);
    }
};
