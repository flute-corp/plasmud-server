/**
 *
 * @param engine {Engine}
 * @param interactor
 */
module.exports = function main ({ engine, interactor: oEnterer }) {
    if (engine.isPlayer(oEnterer)) {
        console.log(oEnterer.name, 'vient d\'entrer dans R00');
    }
};
