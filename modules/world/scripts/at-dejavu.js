// Action taken: at-dejavu
//
// Ce script permet de stocker une variable dejavu sur le joueur
// pour l'interlocuteur du dialogue, ainsi lorsque le joueur
// reparle au NPC la phrase d'introduction va changer pour rappeler
// au joueur qu'il a déja visité ce NPC.
//
// auteur : Ralphy
// last update : 2023-07-29

module.exports = function main ({ player, actor }) {
    player.data['dialog-' + actor.id];
};
