module.exports = function main (evt) {
    const {
        engine,
        entity,
        skill,
        difficulty,
        success,
        value,
        tool
    } = evt;
    switch (skill) {
    case engine.CONSTS.MUD_SKILL_SEARCH: {
        // player is trying to discover secret exits
        success(Math.random() >= 0.5); // Let's assume it's a success 50% of the time
        break;
    }

    case engine.CONSTS.MUD_SKILL_UNLOCK: {
        // player is trying to unlock a locked exit or container
        success(Math.random() >= 0.75); // Let's assume it's a success 75% of the time
        break;
    }

    default: {
        // Another skill is at work
        success(false); // unknow skill -> failure by default
        break;
    }
    }
};
