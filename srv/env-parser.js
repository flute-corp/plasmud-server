const envValidator = require('./config/env-validator');
const processEnvMan = require('../libs/process-env-man');

let _bEnvParsed = false;

function getEnv() {
    if (!_bEnvParsed) {
        processEnvMan.checkAll(envValidator);
        _bEnvParsed = true;
    }
    return processEnvMan.getEnv();
}


module.exports = {
    getEnv
};
