const VirtualSocketAbstract = require('../abstract/VirtualSocketAbstract');

class VirtualTelnetSocket extends VirtualSocketAbstract {
    constructor () {
        super();
        this._socket = null;
    }

    get socket () {
        return this._socket;
    }

    set socket (value) {
        this._socket = value;
    }

    close () {
        this._socket.emit('end');
        this._socket.destroy();
    }

    send (content) {
        this._socket.write(content + '\n');
        return Promise.resolve();
    }
}

module.exports = VirtualTelnetSocket;
