const VirtualSocketAbstract = require('../abstract/VirtualSocketAbstract');

class VirtualWebSocket extends VirtualSocketAbstract {
    constructor () {
        super();
        /**
         * @type {WebSocket}
         * @private
         */
        this._socket = null;
    }

    /**
     * @returns {WebSocket}
     */
    get socket () {
        return this._socket;
    }

    /**
     * @param value {WebSocket}
     */
    set socket (value) {
        this._socket = value;
    }

    close () {
        this._socket.close();
    }

    send (content) {
        const socket = this._socket;
        return new Promise((resolve, reject) => {
            if (socket.readyState === 1) {
                socket.send(content, err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            }
        });
    }
}

module.exports = VirtualWebSocket;
