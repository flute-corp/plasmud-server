const debug = require('debug');
const CommControllerAbstract = require('./abstract/CommControllerAbstract');
const crypto = require('crypto');
const VirtualTelnetSocket = require('./sockets-implementations/VirtualTelnetSocket');

const logCtrl = debug('serv:telnetctl');

const TELNET_PHASE = {
    LOGIN: 0,
    PASSWORD: 1,
    LOGGED_IN: 2
};


class TelnetController extends CommControllerAbstract {
    constructor (cradle) {
        super(cradle);
        this.PROTOCOL = 'telnet';
        this.initHandlers();
    }

    log (...args) {
        logCtrl(...args);
    }

    closeSocket (socket) {
        socket.emit('end');
        socket.destroy();
    }

    /**
   * Initialisation du dialogue de login
   * @param context {SRVClientContext}
   */
    telnetPhaseInit (context) {
        context.data.telnetSession = {
            loginPhase: TELNET_PHASE.LOGIN,
            login: '',
            password: ''
        };
        context.client.socket.socket.write('Login:');
    }

    /**
   * Considère que le message telnet reçu est un nom de login. Passage à la phase suivante
   * @param context {SRVClientContext}
   * @param sMessage {string}
   */
    telnetPhaseLogin (context, sMessage) {
    // On attend le username
        const oTelnetSession = context.data.telnetSession;
        oTelnetSession.login = sMessage.trim();
        oTelnetSession.loginPhase = TELNET_PHASE.PASSWORD;
        const socket = context.client.socket.socket;
        socket.write('Password:');
    // utilisé si on ne veut pas voir le mot de passe affiché
    // socket._setRawMode(true)
    }

    /**
   * Considère que le message telnet reçu est le mot de passe
   * @param context {SRVClientContext}
   * @param sMessage {string}
   * @returns {Promise<void>}
   */
    async telnetPhasePassword (context, sMessage) {
    // On attend le mot de passe
        const oTelnetSession = context.data.telnetSession;
        const socket = context.client.socket.socket;
        oTelnetSession.password = sMessage.trim();
        oTelnetSession.password = crypto
            .createHash('sha256')
            .update(oTelnetSession.password)
            .digest('hex');
        await this.processContextMessage(context, 'login ' + oTelnetSession.login + ' ' + oTelnetSession.password);
        oTelnetSession.loginPhase = TELNET_PHASE.LOGGED_IN;
    }

    /**
   * Ecoute la connexion telnet afin de collecter les caractères composant le mot de passe
   * ce handler est utilisé pour proposer une saisie sans echo du mot de passe.
   * @param context {SRVClientContext}
   * @param sMessage {string}
   * @returns {Promise<void>}
   */
    async telnetPhasePasswordRaw (context, sMessage) {
    // On attend le mot de passe
        const oTelnetSession = context.data.telnetSession;
        const socket = context.client.socket.socket;
        if (sMessage.charCodeAt(0) === 127) {
            // backspace
            oTelnetSession.password = oTelnetSession.password.substring(0, oTelnetSession.password.length - 1);
        } else if (sMessage === '\r') {
            // return
            socket.write('\r\n');
            oTelnetSession.password = crypto
                .createHash('sha256')
                .update(oTelnetSession.password)
                .digest('hex');
            await this.processContextMessage(context, 'login ' + oTelnetSession.login + ' ' + oTelnetSession.password);
            oTelnetSession.loginPhase = TELNET_PHASE.LOGGED_IN;
            socket._setRawMode(false);
        } else {
            oTelnetSession.password += sMessage;
        }
    }

    /**
   * Handler de connexion client telnet
   * @param socket {*}
   * @returns {Promise<*>}
   */
    async connectClient (socket) {
        const vs = new VirtualTelnetSocket();
        vs.socket = socket;
        const { client, context } = await super.connectClient(vs);
        // handler de messages websocket
        socket.on('window size', function (e) {
            if (e.command === 'sb') {
                // telnet window resized to e.width, e.height
            }
        });
        const pRuntimePhase = async sMessage => {
            await this.processContextMessage(context, sMessage.toString().trim());
        };
        if (!('telnetSession' in context.data)) {
            this.telnetPhaseInit(context);
        }
        const pLoginPhase = async sMessage => {
            sMessage = sMessage.toString();
            switch (context.data.telnetSession.loginPhase) {
            case TELNET_PHASE.LOGIN: {
                this.telnetPhaseLogin(context, sMessage);
                break;
            }
            case TELNET_PHASE.PASSWORD: {
                await this.telnetPhasePassword(context, sMessage);
                // TODO ici on a eu une erreur : telnetSession est parfois undefined, et donc provoque une erreur
                // Cela intervient lorsqu'on a voulu lancer une commande pendant la phase de login
                // Faire attention à ne pas taper de commande avant le mot de passe
                if (context.data.telnetSession.loginPhase === TELNET_PHASE.LOGGED_IN) {
                    socket.off('data', pLoginPhase);
                    socket.on('data', pRuntimePhase);
                    delete context.data.telnetSession;
                }
                break;
            }
            }
        };
        socket.on('data', pLoginPhase);
        socket.on('end', () => {
            this.log('user::' + client.uid + ' has ended telnet session');
            this.disconnectClient(client);
        });
        return { client, context };
    }
}

module.exports = TelnetController;
