class ApiController {
    constructor ({
        AdminController
    }) {
        this.cradle = {
            AdminController
        };
    }

    async postCreateUser (req, res) {
        return this.cradle.AdminController.createUser(req, res);
    }
}

module.exports = ApiController;
