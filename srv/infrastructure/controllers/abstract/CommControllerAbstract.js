const debug = require('debug');

const logCtrl = debug('serv:comctl');

class CommControllerAbstract {
    constructor (cradle) {
        const {
            FindUserClient,
            ParseMessage,
            ExecuteCommand,
            DisconnectClient,
            ConnectClient,
            GameInteractor,
            ChatInteractor,
            CLIENT_STATES,
            ACL
        } = cradle;
        this.game = GameInteractor;
        this.txat = ChatInteractor;
        this.PROTOCOL = '';
        this.cradle = {
            FindUserClient,
            ParseMessage,
            ExecuteCommand,
            DisconnectClient,
            ConnectClient,
            GameInteractor,
            ChatInteractor
        };
        /**
     * @type ClientStates
     */
        this.CLIENT_STATES = CLIENT_STATES;
        this.ACL = ACL;
        this.contextUseCases = cradle;
    }

    log (...args) {
        logCtrl(...args);
    }

    /**
   * Méthode d'envoie au user spécifié
   * @param uid {string} identifiant utilisateur
   * @param content {string} contenu du message
   * @returns {Promise<void>}
   */
    async sendMessageToUser (uid, content) {
        const client = await this.cradle.FindUserClient.execute(uid);
        if (client.type !== this.PROTOCOL) {
            // This client is using another protocol, ignore request
            return;
        }
        if (client && client.state !== this.CLIENT_STATES.CLIENT_STATE_DISCONNECTED) {
            return client.socket.send(content);
        }
    }

    /**
   * procède à l'analyse et l'exécution de l'ordre issu du client.
   * @param context {*}
   * @param sMessage {string}
   */
    async processContextMessage (context, sMessage) {
        const client = context.client;
        try {
            const { message } = await this.cradle.ParseMessage.execute(sMessage);
            if (!message.valid) {
                this.log('client::%s sent an invalid command', client.id);
                return;
            }
            this.log('@%s > %s [%s]',
                client.uname,
                message.opcode,
                message.arguments.join(' ')
            );
            switch (client.state) {
            case this.CLIENT_STATES.CLIENT_STATE_CONNECTED: {
                // le client n'est pas encore associé à un utilisateur authentifié
                // le message sera le username
                // seule commande autorisée : login
                await this.cradle.ExecuteCommand.execute(context, message);
                break;
            }
            case this.CLIENT_STATES.CLIENT_STATE_AUTHENTICATED: {
                // le client est associé à un utilisateur authentifié
                await this.cradle.ExecuteCommand.execute(context, message);
                break;
            }
            case this.CLIENT_STATES.CLIENT_STATE_IN_GAME: {
                // le client est associé à un utilisateur authentifié qui est actuellement en jeu.
                await this.cradle.ExecuteCommand.execute(context, message);
                break;
            }
            case this.CLIENT_STATES.CLIENT_STATE_DISCONNECTED: {
                // le client est déconnecté : ignorer tous les messages
                break;
            }
            }
        } catch (e) {
            try {
                if (e.message.startsWith('ERR_TXAT')) {
                    console.error('Chat error', client.id, e.message);
                    return;
                } else if (e.message.startsWith('ERR_COMMAND_NOT_FOUND')) {
                    const command = e.message.substring(23).trim();
                    this.log('@%s is invoking an invalid command : %s.', client.uname, command);
                    context.print.text('generic.error.commandNotFound', { command });
                    return;
                }
                switch (e.message) {
                case 'ERR_NOT_IN_GAME': {
                    this.log('@%s is using game-only command without entering game.', client.uname);
                    context.print.text('generic.error.commandNotInMud');
                    break;
                }
                case 'ERR_ACCESS_DENIED': {
                    this.log('@%s is not allowed to use this command.', client.uname);
                    context.print.text('generic.error.commandAccessDenied');
                    break;
                }
                case 'ERR_AUTH_FAILED': {
                    this.log('@%s authentication failed.', client.uname);
                    client.socket.close();
                    break;
                }
                default: {
                    console.error(e);
                    this.log('@%s command error : %s', client.uname, e.message);
                    context.print.text('generic.error.command', { error: e.message });
                    break;
                }
                }
            } catch (e) {
                console.error(e);
                this.log('@%s : an error occurred while processing a client message : %s', client.uname, e.message);
                client.socket.close();
            }
        }
    }

    async disconnectClient (client) {
        this.log('@%s disconnecting...', client.uname);
        await this.cradle.DisconnectClient.execute(client.id);
        this.log('@%s disconnected.', client.uname);
    }

    /**
   * @typedef VSocket {object}
   * @property send {function(content)} envoie de données à la socket vers le client
   * @property close {function()} fermeture de la socket et déconnection du client.
   */

    /**
   *
   * @param vsocket {VirtualSocketAbstract}
   * @returns {Promise<*>}
   */
    async connectClient (vsocket) {
        try {
            this.log('incoming client connection');
            // connexion du client
            const { client, context } = await this.cradle.ConnectClient.execute(vsocket, this.PROTOCOL);
            /**
       * @type {ApplicationCradle}
       */
            context.uc = this.contextUseCases;
            if (!client) {
                vsocket.close();
                return;
            }
            this.log('client::%s is now connected', client.id);

            return {
                client,
                context
            };
        } catch (e) {
            await vsocket.send(e.message);
            vsocket.close();
            throw e;
        }
    }

    /**
   * Méthode d'envoie de données au client spécifié
   * @param socket {*} instance du client
   * @param content {string} contenue du message
   * @returns {Promise}
   */
    sendToSocket (socket, content) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    closeSocket (socket) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    clientTransmitter (client, message) {
        return this.sendMessageToUser(client.uid, message);
    }

    initHandlers () {
        this.game.events.on('output', ({ id, content }) => {
            if (content instanceof Promise) {
                return content.then(s => this.sendMessageToUser(id, s));
            } else {
                return this.sendMessageToUser(id, content);
            }
        });
        const ts = this.txat;
        // prise en charge des évènements du txat
        ts.events.on(ts.TXAT_EVENT_TYPES.OPEN_CHANNEL, ({ client, channel }) => {
            return this.sendMessageToUser(client.uid, 'CHAT:JOIN ' + channel);
        });
        ts.events.on(ts.TXAT_EVENT_TYPES.CLOSE_CHANNEL, ({ client, channel }) => {
            return this.sendMessageToUser(client.uid, 'CHAT:LEAVE ' + channel);
        });
        ts.events.on(ts.TXAT_EVENT_TYPES.MESSAGE, ({ client, channel, content }) => {
            return this.sendMessageToUser(client.uid, 'CHAT:MSG ' + channel + ' ' + content);
        });
    }
}

module.exports = CommControllerAbstract;
