class VirtualSocketAbstract {
    constructor () {

    }

    get socket () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    set socket (value) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    close () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    send (message) {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }
}

module.exports = VirtualSocketAbstract;
