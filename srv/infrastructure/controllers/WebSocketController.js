const debug = require('debug');
const CommControllerAbstract = require('./abstract/CommControllerAbstract');
const logCtrl = debug('serv:wsctl');
const VirtualWebSocket = require('./sockets-implementations/VirtualWebSocket');

class WebSocketController extends CommControllerAbstract {
    constructor (cradle) {
        super(cradle);
        this.PROTOCOL = 'ws';
        this.initHandlers();
    }

    log (...args) {
        logCtrl(...args);
    }

    /**
   *
   * @param socket {*}
   * @returns {Promise<*>}
   */
    async connectClient (socket) {
        try {
            const vs = new VirtualWebSocket();
            vs.socket = socket;
            const { client, context } = await super.connectClient(vs);
            // handler de messages websocket
            socket.on('message', message => {
                this.processContextMessage(context, message.toString());
            });
            socket.on('close', () => {
                this.disconnectClient(client);
            });
            return { client, context };
        } catch (e) {
            logCtrl('error while client attempting to connect : %s', e.message);
            throw e;
        }
    }
}

module.exports = WebSocketController;
