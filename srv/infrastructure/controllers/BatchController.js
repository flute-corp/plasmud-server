module.exports = class BatchController {
    constructor ({
        FindUser,
        AdminGetInformation
    }) {
        this.cradle = {
            FindUser,
            AdminGetInformation
        };
    }

    async viewAdminSwitches () {
        const { switches: oSwitches } = await this.cradle.AdminGetInformation.execute();
        console.log(oSwitches);
    }

    async viewUserInfo (argv) {
        const sUserName = argv[0];
        try {
            const user = await this.cradle.FindUser.execute(sUserName, true);
            console.log(user);
        } catch (e) {
            switch (e.message) {
            case 'ERR_USER_NOT_FOUND': {
                console.warn('user not found %s', sUserName);
                break;
            }

            default: {
                console.error(e);
                break;
            }
            }
        }
    }
};
