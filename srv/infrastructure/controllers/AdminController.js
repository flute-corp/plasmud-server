const JsonMutationManager = require('../../../libs/json-mutation-manager');
const debug = require('debug');
const logAdmin = debug('serv:admin');
const crypto = require('crypto');

class AdminController {
    constructor ({
        AdminGetInformation,
        CreateUser,
        GetUserList,
        FindUser,
        SetUserBanishment,
        RemoveUserBanishment,
        AddUserRole,
        RemoveUserRole,
        AdminSetSwitch,
        ShutdownService,
        AdminBroadcastMessage,
        PersistCharacterState,
        FindCharacter,
        FindUserClient,
        RestoreCharacterState,
        ADMIN_SWITCHES
    }) {
        this.cradle = {
            AdminGetInformation,
            CreateUser,
            GetUserList,
            FindUser,
            SetUserBanishment,
            RemoveUserBanishment,
            AddUserRole,
            RemoveUserRole,
            AdminSetSwitch,
            ShutdownService,
            AdminBroadcastMessage,
            PersistCharacterState,
            FindCharacter,
            FindUserClient,
            RestoreCharacterState,
            ADMIN_SWITCHES
        };
        this.ADMIN_SWITCHES = ADMIN_SWITCHES;
    }

    /**
   * Liste des switches d'administration
   * @param req {*}
   * @param res {*}
   * @returns {Promise<void>}
   */
    async getSwitches (req, res) {
        const oAdminInfo = await this.cradle.AdminGetInformation.execute();
        res.send.ok(oAdminInfo.switches);
    }

    /**
   * Obtention de la valeur d'un switch d'administration
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
    async getSwitch (req, res) {
        const name = req.params.name;
        const { switches: oSwitches } = await this.cradle.AdminGetInformation.execute();
        const { value } = oSwitches[name];
        res.send.ok({
            result: { [name]: value }
        });
    }

    async putSwitch (req, res) {
        try {
            const name = req.params.name;
            const { value } = req.body;
            logAdmin('setting admin switch %s to value %s', name, value.toString());
            switch (name) {
            case 'open': {
                // value:
                // 1 = Les clients peuvent se connecter
                // 0 = Les clients ne peuvent pas se connecter - vire les clients deja connectés.
                const { switches: oSwitches } = await this.cradle.AdminGetInformation.execute();
                const { value } = oSwitches[this.ADMIN_SWITCHES.OPEN];
                const bOpen = Boolean(value | 0);
                const sState = (value ? 'o' : 'c') + (bOpen ? 'o' : 'c');
                switch (sState) {
                case 'oo':
                case 'cc': {
                    // L'état de OPEN ne change pas
                    res.send.ok({ result: { [name]: value } });
                    break;
                }
                case 'oc': {
                    // C'était ouvert on veut fermer
                    logAdmin('closing service');
                    await this.shutdownService(req, res);
                    break;
                }
                case 'co': {
                    // C'était fermé on veut ouvrir
                    logAdmin('opening service');
                    await this.openService();
                    break;
                }
                }
                break;
            }

            default: {
                res.send.badRequest('admin switch or command "' + name + '" invalid.');
                return;
            }
            }
        } catch (e) {
            console.error(e);
            res.send.internalError(e);
        }
    }

    /**
   * Information concernant le service
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
    async getInformation (req, res) {
        const info = await this.cradle.AdminGetInformation.execute();
        res.send.ok({
            result: info
        });
    }


    // ▗▖▗▖
    // ▐▌▐▌▗▛▀▘▗▛▜▖▐▛▜▖▗▛▀▘
    // ▐▌▐▌ ▀▜▖▐▛▀▘▐▌   ▀▜▖
    // ▝▀▀▘▝▀▀  ▀▀ ▝▘  ▝▀▀

    async createUser (req, res) {
        try {
            const { name, password = null, email } = req.body;
            if (password === null) {
                res.send.badRequest('password missing');
            }
            const sPasswrodSha256 = crypto
                .createHash('sha256')
                .update(password)
                .digest('hex');
            const { user } = await this.cradle.CreateUser.execute(name, email, sPasswrodSha256);
            res.send.ok(user);
        } catch (e) {
            switch (e.message) {
            case 'ERR_USER_NAME_ALREADY_TAKEN': {
                res.send.conflict();
                break;
            }

            case 'ERR_USER_NAME_INVALID': {
                res.send.badRequest('username');
                break;
            }

            case 'ERR_USER_EMAIL_INVALID': {
                res.send.badRequest('email');
                break;
            }

            default: {
                console.error(e);
                res.send.internalError(e.message);
                break;
            }
            }
        }
    }

    async deleteUser (req, res) {
        const sUserName = req.params.user;

    }

    /**
   * Liste des utilisateurs enregistrés
   * Avec filtre
   * @returns {Promise<void>}
   */
    async findUsers (req, res) {
        const sFilter = req.params.filter;
        const nCount = (req.params.count | 0) || Infinity;
        logAdmin('finding users - filter: %s - count: %d', sFilter, nCount);
        res.send.ok({
            result: await this.cradle.GetUserList.execute(sFilter, nCount)
        });
    }

    /**
   * Information concernant un utilisateur
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
    async getUser (req, res) {
        const sUser = req.params.user;
        logAdmin('getting user by name: %s', sUser);
        try {
            const user = await this.cradle.FindUser.execute(sUser, true);
            res.send.ok({
                result: user
            });
        } catch (e) {
            switch (e.message) {
            case 'ERR_USER_NOT_FOUND': {
                res.send.notFound();
                break;
            }

            default: {
                console.error(e);
                res.send.internalError(e.message);
                break;
            }
            }
        }
    }

    /**
   * Liste des utilisateur enregistrés
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
    async getUserList (req, res) {
        const nCount = (req.params.count | 0) || Infinity;
        logAdmin('getting user list - count: %d', nCount);
        res.send.ok({
            result: await this.cradle.GetUserList.execute('', nCount)
        });
    }

    async banUser (req, res) {
        const user = req.params.user;
        try {
            const { duration = '0', reason = '' } = req.body;
            const nDuration = parseInt(duration) || 0;
            const nDateEnd = nDuration === 0 ? 0 : (Date.now() + nDuration);
            const oBannedUser = await this.cradle.FindUser.execute(user);
            const { untilStr } = await this.cradle.SetUserBanishment.execute(oBannedUser.id, nDateEnd, reason, '');
            logAdmin('ban user %s until: %s - reason: "%s"', oBannedUser.name, untilStr, reason);
            res.send.ok({
                user: oBannedUser.name,
                reason,
                until: untilStr
            });
        } catch (e) {
            switch (e.message) {
            case 'ERR_USER_NOT_FOUND': {
                res.send.badRequest('User not found : ' + user);
                break;
            }

            default: {
                console.error(e);
                res.send.internalError(e.message);
            }
            }
        }
    }

    async unbanUser (req, res) {
        const user = req.params.user;
        logAdmin('unban user %s', user);
        try {
            const oBannedUser = await this.cradle.FindUser.execute(user);
            const { success } = await this.cradle.RemoveUserBanishment.execute(oBannedUser.id);
            res.send.ok({
                success
            });
        } catch (e) {
            switch (e.message) {
            case 'ERR_USER_NOT_FOUND': {
                res.send.badRequest('User not found : ' + user);
                break;
            }

            default: {
                console.error(e);
                res.send.internalError(e.message);
            }
            }
        }
    }

    async addUserRoles (req, res) {
        const { user, role } = req.params;
        logAdmin('adding role %s to user %s', role, user);
        try {
            const oUser = await this.cradle.FindUser.execute(user);
            const { success, roles } = await this.cradle.AddUserRole.execute(oUser.id, role);
            res.send.ok({
                success,
                roles
            });
        } catch (e) {
            switch (e.message) {
            case 'ERR_USER_NOT_FOUND': {
                res.send.badRequest('User not found : ' + user);
                break;
            }

            default: {
                console.error(e);
                res.send.internalError(e.message);
            }
            }
        }
    }

    async removeUserRoles (req, res) {
        const { user, role } = req.params;
        logAdmin('remove role %s from user %s', role, user);
        try {
            const oUser = await this.cradle.FindUser.execute(user);
            const { success, roles } = await this.cradle.RemoveUserRole.execute(oUser.id, role);
            res.send.ok({
                success,
                roles
            });
        } catch (e) {
            switch (e.message) {
            case 'ERR_USER_NOT_FOUND': {
                res.send.badRequest('User not found : ' + user);
                break;
            }

            case 'ERR_INVALID_ROLE': {
                res.send.badRequest('Invalid role : ' + role);
                break;
            }

            default: {
                console.error(e);
                res.send.internalError(e.message);
            }
            }
        }
    }

    //  ▄▄              ▗▖
    // ▝▙▄ ▗▛▜▖▐▛▜▖▐▌▐▌ ▄▖ ▗▛▀ ▗▛▜▖
    //   ▐▌▐▛▀▘▐▌  ▝▙▟▘ ▐▌ ▐▌  ▐▛▀▘
    //  ▀▀  ▀▀ ▝▘   ▝▘  ▀▀  ▀▀  ▀▀

    async openService (req, res) {
        logAdmin('opening service');
        res.send.ok({
            result: await this.cradle.AdminSetSwitch.execute(this.ADMIN_SWITCHES.OPEN, true)
        });
    }

    async cancelShutdowService (req, res) {
        logAdmin('canceling service shutdown');
        res.send.ok({
            result: await this.cradle.ShutdownService.execute({ cancel: true })
        });
    }

    async shutdownService (req, res) {
        const { cancel = false, message = '', delay = 0 } = req.body;
        if (cancel) {
            return this.cancelShutdowService(req, res);
        } else {
            logAdmin('closing service to incoming client connections.');
            res.send.ok({
                result: await this.cradle.ShutdownService.execute({ cancel, delay, message })
            });
        }
    }


    async broadcastMessage (req, res) {
        const sMessage = req.body.message || '';
        if (sMessage) {
            return this.cradle.AdminBroadcastMessage.execute(sMessage);
        }
    }

    //  ▄▄ ▗▖                   ▗▖
    // ▐▌▝▘▐▙▄  ▀▜▖▐▛▜▖ ▀▜▖▗▛▀ ▝▜▛▘▗▛▜▖▐▛▜▖▗▛▀▘
    // ▐▌▗▖▐▌▐▌▗▛▜▌▐▌  ▗▛▜▌▐▌   ▐▌ ▐▛▀▘▐▌   ▀▜▖
    //  ▀▀ ▝▘▝▘ ▀▀▘▝▘   ▀▀▘ ▀▀   ▀▘ ▀▀ ▝▘  ▝▀▀

    async getCharacterInformation (req, res) {
        try {
            const sCharName = req.params.name;
            if (sCharName) {
                const character = await this.cradle.FindCharacter.execute(sCharName, true);
                const rawCharacter = await this.cradle.FindCharacter.execute(sCharName, false);
                const client = await this.cradle.FindUserClient.execute(rawCharacter.owner); // ok 1 client
                const bUserConnected = !!client;
                const { data } = await this.cradle.RestoreCharacterState.execute(character.id);
                res.send.ok({ character, live: bUserConnected && character.selected , ...data });
            } else {
                res.send.badRequest('ERR_UNDEFINED_CHAR_NAME');
            }
        } catch (e) {
            res.send.badRequest(e.message);
        }
    }

    async modifyCharacterInformation (req, res) {
        try {
            const sCharName = req.params.name;
            if (sCharName) {
                const tryChange = (obj, prop, val) => {
                    try {
                        const j = new JsonMutationManager(obj);
                        j.changeProperty(prop, val);
                        return true;
                    } catch (e) {
                        return false;
                    }
                };
                const { property: sVariable } = req.body;
                let { value } = req.body;
                const fValue = parseFloat(value);
                // number
                if (!isNaN(fValue)) {
                    value = fValue;
                } else if (value === 'true' || value === 'false') {
                    value = value === true;
                } else if (value === 'null') {
                    value = null;
                }
                const character = await this.cradle.FindCharacter.execute(sCharName, true);
                const oState = await this.cradle.RestoreCharacterState.execute(character.id);
                if (tryChange(oState.data, sVariable, value)) {
                    // save new state json
                    await this.cradle.PersistCharacterState.execute(oState);
                    res.send.ok({ success: true });
                } else if (tryChange(character, sVariable, value)) {
                    // save new character json
                    res.send.badRequest('Character core data cannot be changed, only state data can be altered');
                } else {
                    res.send.badRequest('Cannot find property ' + sVariable + ' in character state');
                }
            }
        } catch (e) {
            res.send.badRequest(e.message);
        }
    }


    // ▗▖ ▄      ▗▖     ▄▖
    // ▐█▟█▗▛▜▖ ▄▟▌▐▌▐▌ ▐▌ ▗▛▜▖▗▛▀▘
    // ▐▌▘█▐▌▐▌▐▌▐▌▐▌▐▌ ▐▌ ▐▛▀▘ ▀▜▖
    // ▝▘ ▀ ▀▀  ▀▀▘ ▀▀▘ ▀▀  ▀▀ ▝▀▀

    /**
   * Renvoie la liste des modules actuellement chargé dans le serveur
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
    async listModules (req, res) {

    }
}

module.exports = AdminController;
