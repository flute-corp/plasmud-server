const ClientRepository = require('../../../application/interfaces/repositories/ClientRepository');
const InMemoryImplementationAutoInc = require('./implementations/InMemoryImplementationAutoInc');
const Client = require('../../../entities/Client');
const CacheHelper = require('../../common/cache-helper');

class InMemoryClientRepository extends ClientRepository {
    constructor () {
        super();
        this._implementation = new InMemoryImplementationAutoInc();
        this._lastId = 0;
        this._userCache = new CacheHelper();
    }

    /**
   * Renvoie la valeur d'un nouvel identifiant
   * @return {Promise<number|string>}
   */
    getNewId () {
        return Promise.resolve(++this._lastId);
    }

    /**
   * Sauvegarde de l'entité, si l'identifiant de l'entité est null ou undefined un nouvel identifiant est attribué
   * @param entity {object}
   * @returns {Promise<never>}
   */
    persist (entity) {
        if (!(entity instanceof Client)) {
            throw new TypeError('Instance of Client expected ; got ' + typeof (entity));
        }
        if (entity.uid) {
            this._userCache.storeEntity(entity, entity.uid);
        }
        return this._implementation.persist(entity);
    }

    /**
   * Suppression de l'entité, l'identifiant de l'entité doit être spécifié.
   * @param entity
   * @returns {Promise<never>}
   */
    remove (entity) {
        if (!(entity instanceof Client)) {
            throw new TypeError('Instance of Client expected ; got ' + typeof (entity));
        }
        if (entity.uid) {
            this._userCache.removeEntity(entity, entity.uid);
        }
        return this._implementation.remove(entity);
    }

    /**
   * Récupération d'une entité à partir de son identifiant
   * @param id {string|number}
   * @returns {Promise<Client>}
   */
    get (id) {
        return this._implementation.get(id);
    }

    /**
   * Récupération de toutes les entités
   * @returns {Promise<Client[]>}
   */
    getAll () {
        return this._implementation.getAll();
    }

    /**
   * Renvoie une liste d'entité satisfaisant le prédicat spécifié
   * @param pFunction {function}
   * @returns {Promise<Client|undefined>}
   */
    find (pFunction) {
        return this._implementation.find(pFunction);
    }

    /**
   *
   * @param idUser
   * @returns {Promise<Client[]>}
   */
    async findByUser (idUser) {
        const a = this._userCache.loadEntities(idUser);
        return Array.isArray(a)
            ? a.slice(0)
            : [];
    }
}
module.exports = InMemoryClientRepository;
