const InMemoryImplementationAbstract = require('./InMemoryImplementationAbstract');

class InMemoryImplementation extends InMemoryImplementationAbstract {
    getNewId () {
        return Promise.reject(new Error('Entity must have "id" property defined'));
    }
}

module.exports = InMemoryImplementation;
