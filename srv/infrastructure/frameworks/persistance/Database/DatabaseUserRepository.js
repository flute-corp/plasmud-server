const UserRepository = require('../../../application/interfaces/repositories/UserRepository');
const DatabaseImplementationUuid = require('./implementations/DatabaseImplementationUuid');

class DatabaseUserRepository extends UserRepository {
    constructor ({ DatabaseInteractor }) {
        super();
        this._users = {};
        this._implementation = new DatabaseImplementationUuid('users', DatabaseInteractor);
    }

    /**
   * Sauvegarde de l'entité, si l'identifiant de l'entité est null ou undefined un nouvel identifiant est attribué
   * @param entity {object}
   * @returns {Promise<never>}
   */
    persist (entity) {
        this._users[entity.id] = entity;
        return this._implementation.persist(entity);
    }

    /**
   * Suppression de l'entité, l'identifiant de l'entité doit être spécifié.
   * @param entity
   * @returns {Promise<never>}
   */
    remove (entity) {
        delete this._users[entity.id];
        return this._implementation.remove(entity);
    }

    /**
   * Récupération d'une entité à partir de son identifiant
   * @param id {string|number}
   * @returns {Promise<never>}
   */
    async get (id) {
        if (!(id in this._users)) {
            this._users[id] = await this._implementation.get(id);
        }
        return this._users[id];
    }

    /**
   * Recherche un utilisateur par son nom
   * @param name {string}
   * @return {Promise<User>}
   */
    async findByName (name) {
        const oUsers = await this._implementation.find({ name: { $eq: name } });
        return oUsers.first();
    }

    /**
   * @param pattern
   * @returns {Promise<Cursor>}
   */
    findByNameLike (pattern) {
        const r = new RegExp(pattern, 'i');
        return this._implementation.find({ name: { $match: new RegExp(pattern, 'i') } });
    }

    getAll () {
        return this._implementation.getAll();
    }

    async getCount () {
        return this._implementation.getCount();
    }
}

module.exports = DatabaseUserRepository;
