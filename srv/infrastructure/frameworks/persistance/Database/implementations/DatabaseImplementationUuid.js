const DatabaseImplementationAbstract = require('./DatabaseImplementationAbstract');
const { generate: smalluuid } = require('../../../../../../libs/small-uuid');
const ShortUniqueId = require('short-unique-id');

class DatabaseImplementationUuid extends DatabaseImplementationAbstract {
    getNewId () {
        return Promise.resolve(smalluuid());
    }
}

module.exports = DatabaseImplementationUuid;
