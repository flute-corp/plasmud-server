const PlayerCharacterSaveRepository = require('../../../application/interfaces/repositories/PlayerCharacterSaveRepository');
const DatabaseImplementationAbstract = require('./implementations/DatabaseImplementationAbstract');

class DatabasePlayerCharacterSaveRepository extends PlayerCharacterSaveRepository {
    constructor ({ DatabaseInteractor }) {
        super();
        this._implementation = new DatabaseImplementationAbstract('player-character-save', DatabaseInteractor);
    }

    /**
   * Sauvegarde de l'entité, si l'identifiant de l'entité est null ou undefined un nouvel identifiant est attribué
   * @param entity {object}
   * @returns {Promise<never>}
   */
    persist (entity) {
        return this._implementation.persist(entity);
    }

    /**
   * Suppression de l'entité, l'identifiant de l'entité doit être spécifié.
   * @param entity
   * @returns {Promise}
   */
    remove (entity) {
        return this._implementation.remove(entity);
    }

    /**
   * Récupération d'une entité à partir de son identifiant
   * @param id {string|number}
   * @returns {Promise<Character>}
   */
    get (id) {
        return this._implementation.get(id);
    }
}

module.exports = DatabasePlayerCharacterSaveRepository;
