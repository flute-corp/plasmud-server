const CharacterRepository = require('../../../application/interfaces/repositories/CharacterRepository');
const DatabaseImplementationUuid = require('./implementations/DatabaseImplementationUuid');

class DatabaseCharacterRepository extends CharacterRepository {
    constructor ({ DatabaseInteractor }) {
        super();
        this._implementation = new DatabaseImplementationUuid('characters', DatabaseInteractor);
    }

    /**
   * Sauvegarde de l'entité, si l'identifiant de l'entité est null ou undefined un nouvel identifiant est attribué
   * @param entity {object}
   * @returns {Promise<Character>}
   */
    persist (entity) {
        return this._implementation.persist(entity);
    }

    /**
   * Suppression de l'entité, l'identifiant de l'entité doit être spécifié.
   * @param entity
   * @returns {Promise}
   */
    remove (entity) {
        return this._implementation.remove(entity);
    }

    /**
   * Récupération d'une entité à partir de son identifiant
   * @param id {string|number}
   * @returns {Promise<Character>}
   */
    get (id) {
        return this._implementation.get(id);
    }

    /**
   * Recherche un personnage par son nom
   * @param name {string} nom du personnage
   * @return {Promise<Character>} personnage trouvé
   */
    async findByName (name) {
        const aCharacters = await this._implementation.find({ name: { $eq: name } });
        return aCharacters.first();
    }

    /**
   * Recherche les personnages de l'utilisateur spécifié
   * @param idUser {string} identifiant de l'utilisateur dont on cherche les personnages
   * @returns {Promise<Character[]>} liste des personnages trouvés
   */
    findByOwner (idUser) {
        return this._implementation.find({ owner: idUser }).then(c => c.toArray());
    }

    /**
   * Definit ce personnage en tant que personnage courant (celui utilisé dans le jeu)
   * @param id
   * @returns {Promise<Character[]>}
   */
    async setOwnerCharacterAsCurrent (id) {
        const oChar = await this.get(id);
        // liste des autres personnages du meme auteur
        const aChars = await this.findByOwner(oChar.owner);
        const aPromMutChars = aChars
            .map(ch => {
                const bOld = ch.selected;
                ch.selected = id === ch.id;
                return bOld !== ch.selected ? ch : false;
            })
            .filter(ch => !!ch)
            .map(ch => this.persist(ch));
        return Promise.all(aPromMutChars);
    }

    /**
   *
   * @param owner
   * @returns {Promise<Character>}
   */
    async getOwnerCurrentCharacter (owner) {
        const aChars = await this.findByOwner(owner);
        return aChars.filter(c => c.selected).shift();
    }
}

module.exports = DatabaseCharacterRepository;
