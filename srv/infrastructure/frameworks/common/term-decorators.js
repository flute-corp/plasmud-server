const Ansi256 = require('../../../../libs/ansi-256-colors');
const c = new Ansi256();
const STYLES = require('../../../config/styles.json');

module.exports = {
    error: s => c.render(STYLES.NEG + '\uD83D\uDCA5 ' + s),
    warn: s => c.render(STYLES.WARN + '\u26a0 ' + s),
    danger: s => c.render(STYLES.DANGER + '💀 ' + s),
    info: s => c.render(STYLES.INFO + '[:b]ⓘ[:] ' + s),
    room: s => c.render('[#ca1:b]ⓘ[:] ' + s),
    fail: s => c.render(STYLES.NEG + '\u2716 ' + s),
    succ: s => c.render(STYLES.POS + '✔ ' + s),
    hint: s => c.render(STYLES.INFO + '(?) ' + s),
    speak: s => c.render('[#BB0]💬 ' + s),
    shout: s => c.render('[#FF2]\uD83D\uDCE3 ' + s),
    opt: s => {
        const r = s.match(/(\[[0-9]+]) +(.*)$/);
        if (r) {
            return c.render('[#2F4]' + r[1]) + ' ' + c.render('[#1a2]' + r[2]);
        } else {
            return c.render('[#2F4]' + c.render('[#1a2]' + s));
        }
    }
};
