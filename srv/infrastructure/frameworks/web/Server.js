const express = require('express');
const { WebSocket } = require('ws');
const telnet = require('telnet');
const http = require('http');
const debug = require('debug');
const rp = require('../../../../libs/resolve-path');
// web server sub components
const { SOCKET_PATH } = require('../../../../libs/socket-path');
const httpPresentedResponse = require('./middlewares/httpPresentedResponse');
const apiRouter = require('./routes/api-routes');
const adminRouter = require('./routes/admin-routes');
const { getEnv } = require('../../../env-parser');

const logServ = debug('serv:main');

class Server {
    constructor () {
        this._servers = {
            exp: {},
            http: null,
            httpAdmin: null,
            ws: null,
            telnet: null,
            app: null,
            appAdmin: null
        };
        this._container = null;
        this._webClientDocumentRoot = '';
        this._timeHandler = 0;
    }

    /**
   * Avancée par le time interval du server toutes les 1000ms
   * @returns {*}
   */
    doomloop () {
        const ac = this._container.resolve('AdvanceClock');
        return ac.execute({ logFunc: logServ });
    }

    checkDirectoryAccess (sDir, bWritable = false) {
        const oDocRoot = rp(sDir);
        if (oDocRoot.path === undefined) {
            throw new Error('Directory does not exist: ' + sDir);
        }
        if (bWritable && !oDocRoot.write) {
            throw new Error('Directory is not readable and/or not writable: ' + sDir);
        }
        if (!oDocRoot.read) {
            throw new Error('Directory is not readable: ' + sDir);
        }
        return oDocRoot.path;
    }

    getWebApp (port, name) {
        const sPort = port.toString();
        if (!(sPort in this._servers.exp)) {
            const app = express();
            app.use(express.json());
            app.use(httpPresentedResponse);
            const serv = http.createServer(app);
            this._servers.exp[sPort] = {
                app,
                serv,
                port,
                names: new Set()
            };
        }
        const s = this._servers.exp[sPort];
        if (name) {
            s.names.add(name);
        }
        return s;
    }

    createTelnetServer (cnxHandler) {
        return telnet.createServer(function (socket) {
            // make unicode characters work properly
            socket.do.transmit_binary();

            // make the client emit 'window size' events
            socket.do.window_size();

            cnxHandler(socket);
        });
    }

    /**
   * Création des instances des différents servers (express, http, ws)
   */
    async init ({
        dependencies,
        ports,
        env,
        autorun = true
    }) {
        try {
            // Awilix : contruction des dépendences
            logServ('building dependencies');
            const container = dependencies.createContainer(); // TODO async ou pas ?
            this._container = container;

            // Express : instancier le service http d'administration (qui sera sur un autre port)
            const cwd = process.cwd();
            logServ('current working directory : %s', cwd);

            const PATH_BASE_MODULE = this.checkDirectoryAccess('./srv/base');
            logServ('base module path : %s', PATH_BASE_MODULE);

            const PATH_DATA = this.checkDirectoryAccess(getEnv().PLASMUD_DIRECTORY_DATA);
            logServ('data path : %s', PATH_DATA);

            const InitApplication = container.resolve('InitApplication');
            await InitApplication.execute({
                moduleLocation: PATH_BASE_MODULE,
                logFunc: debug('serv:init'),
                env
            });

            const oUserCount = await container.resolve('GetUserCount').execute();
            logServ('registered user count : %d', oUserCount.count);

            this._webClientDocumentRoot = env.PLASMUD_DIRECTORY_WEB_CLIENT;
            if (autorun) {
                logServ('starting listening services');
                await this.listen(ports);
            }

            logServ('starting doom loop');
            this._timeHandler = setInterval(() => {
                this.doomloop();
            }, 1000);
        } catch (e) {
            logServ('server initialization failed');
            throw e;
        }
    }

    async shutdown () {
    // no more game loops
        if (this._timeHandler) {
            logServ('shutting down time loop...');
            clearInterval(this._timeHandler);
            this._timeHandler = 0;
        }
        // kick all clients
        const oShutdownService = this._container.resolve('ShutdownService');
        logServ('disconnecting all clients...');
        await oShutdownService.execute();
        const aExpServ = Object.values(this._servers.exp);
        if (aExpServ.length > 0) {
            logServ('shutting down %d express service(s)...', aExpServ.length);
            aExpServ.forEach(({ serv, port, names }) => {
                logServ('- shutting down express service at port %d : %s', port, [...names].join(', '));
                serv.close();
            });
        }
        const game = this._container.resolve('GameInteractor');
        logServ('canceling jobs and shutting down job scheduler...');
        await game.shutdown();
        logServ('shutdown complete.');
    }

    defineAdminWebApp (port) {
        logServ('creating admin http service');
        const { app } = this.getWebApp(port, 'admin');
        logServ('declaring admin application routes and middlewares');
        app.use('/admin', adminRouter(this._container));
    }

    defineBaseWebApp (port) {
        logServ('creating base http service');
        const { app } = this.getWebApp(port, 'api');

        logServ('declaring base application routes and middlewares');
        app.use('/api', apiRouter(this._container));

        const sDocumentRoot = this.checkDirectoryAccess(this._webClientDocumentRoot);
        logServ('spa location : %s', sDocumentRoot);
        app.use('/', express.static(sDocumentRoot));
    }

    /**
   * Mise à l'écoute du serveur sur le port spécifié en paramètre
   * @param port {number}
   */
    listenWS (port) {
    // Creation serveur HTTP
        logServ('creating websocket service');
        // SOCKET_PATH est la route dédiée aux websockets
        if (SOCKET_PATH) {
            logServ('socket path : %s', SOCKET_PATH);
        }

        const { serv } = this.getWebApp(port, 'ws');

        // Creation server WebSocket
        logServ('building websocket server');
        const wss = new WebSocket.Server({
            server: serv,
            path: SOCKET_PATH || null
        });

        logServ('declaring websocket client connection handler');

        // Controleur WebSocket : initialisation
        const oWSController = this._container.resolve('WebSocketController');
        wss.on('connection', ws => oWSController
            .connectClient(ws)
            .catch(e => {
                switch (e.message) {
                case 'ERR_SERVICE_DOWN': {
                    break;
                }
                default: {
                    logServ(e.message);
                    break;
                }
                }
            }));
        this._servers.ws = wss;
    }

    listenTelnet (port) {
        port = parseInt(port);
        if (port) {
            logServ('creating telnet service');
            // Creation serveur Telnet
            logServ('declaring telnet client connection handler');
            const oTelnetController = this._container.resolve('TelnetController');
            this._servers.telnet = this.createTelnetServer(socket => oTelnetController.connectClient(socket));
            this._servers.telnet.listen(port);
            this._servers.telnet.on('close', () => {
                logServ('telnet : server closed');
            });
            logServ('telnet : now listening on port ' + port);
        }
    }

    openPorts () {
        const proms = Object.values(this
            ._servers
            .exp)
            .map(({ serv, port, names }) =>
                new Promise(resolve => {
                    return serv.listen(port, '0.0.0.0', () => {
                        const sNames = [...names].join(', ');
                        logServ('%s : now listening on port %d', sNames, port);
                        resolve();
                    });
                })
            );
        return Promise.all(proms);
    }

    /**
   *
   * @param ports {{admin: number, ws: number, telnet: number}}
   * @returns {Promise<void>}
   */
    listen (ports) {
        if (ports.admin) {
            this.defineAdminWebApp(ports.admin);
        }
        if (ports.ws) {
            this.defineBaseWebApp(ports.ws);
            this.listenWS(ports.ws);
        }
        if (ports.telnet) {
            this.listenTelnet(ports.telnet);
        }
        logServ('opening http ports');
        return this.openPorts();
    }
}

module.exports = Server;
