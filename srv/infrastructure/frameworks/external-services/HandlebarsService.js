const TemplateInteractor = require('../../application/interfaces/interactors/TemplateInteractor');
const PromFS = require('../../../../libs/prom-fs');
const path = require('path');
const HandlebarManager = require('../../../../libs/handlebars-manager');

class HandlebarsService extends TemplateInteractor {
    constructor () {
        super();
        this._handlebarsManager = new HandlebarManager();
    }

    /**
   * Indexe un ensemble de template dans un emplacement donné.
   * @param sBasePath {string} emplacement des templates
   * @returns {Promise<void>}
   */
    async index (sBasePath) {
        const t1 = await PromFS.tree(sBasePath);
        const t2 = t1
            .filter(x => x.endsWith('.hb'))
            .map(x => this.loadTemplate(sBasePath, x));
        await Promise.all(t2);
    }

    /**
   * Chargement d'un template
   * @param filename {string} emplacement du fichier contenant le template (pour générer l'identifiant)
   * @param sBasePath {string} emplacement de base
   */
    loadTemplate (sBasePath, filename) {
        return PromFS.read(path.resolve(sBasePath, filename)).then(sTemplate => {
            this._handlebarsManager.defineTemplate(filename, sTemplate);
        });
    }

    get count () {
        return this._handlebarsManager.count;
    }

    hasTemplate (key) {
        return this._handlebarsManager.hasTemplate(key);
    }

    init (sPath) {
        if (typeof sPath !== 'string') {
            throw new TypeError('HandlebarsService.init : Path must be a string.');
        }
        return this
            .index(sPath)
            .then(() => this._handlebarsManager.init());
    }

    render (sTpl, oVariables) {
        return this._handlebarsManager.render(sTpl, oVariables);
    }
}

module.exports = HandlebarsService;
