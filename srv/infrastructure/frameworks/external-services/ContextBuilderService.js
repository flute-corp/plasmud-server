const ContextInteractor = require('../../application/interfaces/interactors/ContextBuilderInteractor');
const debug = require('debug');
const ArgumentTypeValidator = require('../../../../libs/validate-argument-type');
const System = require('../../../../libs/plasmud/System');

const logCommand = debug('serv:command');

class ContextBuilderService extends ContextInteractor {
    constructor ({ TextRendererInteractor }) {
        super();
        this._services = {
            textRenderer: TextRendererInteractor
        };
    }

    init () {
    }

    static check (print, sTypes, ...params) {
        try {
            sTypes.split('').forEach((t, i) => {
                const p = params[i];
                switch (t) {
                case 'S': { // string mandatory
                    ArgumentTypeValidator.checkString(p);
                    break;
                }

                case 'N': { // number mandatory
                    ArgumentTypeValidator.checkNumeric(p);
                    break;
                }

                case 's': { // string optional
                    if (ArgumentTypeValidator.isDefined(p)) {
                        ArgumentTypeValidator.checkString(p);
                    }
                    break;
                }

                case 'n': { // string optional
                    if (ArgumentTypeValidator.isDefined(p)) {
                        ArgumentTypeValidator.checkNumeric(p);
                    }
                    break;
                }
                }
            });
            return true;
        } catch (e) {
            switch (e.message) {
            case 'ERR_VALUE_UNDEFINED': {
                print.text('generic.error.missingParam');
                break;
            }

            case 'ERR_TYPE_MISMATCH': {
                print.text('generic.error.typeMismatch');
                break;
            }
            }
            return false;
        }
    }

    /**
   * @typedef SRVClientContext {object}
   * @property client {Client}
   * @property check {function (string, string[]): boolean}
   * @property text {function (sString:string, oVariables: object): string}
   * @property print {object}
   * @property data {object}
   * @property uc {object}
   *
   * Création d'un contexte pour le client
   * @param client {Client}
   * @returns {SRVClientContext}
   */
    buildClientContext (client) {
        const text = (sString, oVariables) => {
            return this
                ._services
                .textRenderer
                .renderSync(sString, oVariables) + '(buildClientContext text)';
        };

        const print = (sString, oVariables) => this
            ._services
            .textRenderer
            .renderAsync(sString, oVariables)
            .then(s => s
                .split('\n')
                .forEach(l => print.to(client, System.getResRefPrefixCode(sString).style + l))
            );
        print.to = function (client, sString) {
            throw new Error('ERR_PRINT_TO_UNDEFINED');
        };

        print.log = (...aArguments) => {
            logCommand(...aArguments);
        };
        print.room = () => print.to(client, 'print.room function is not available in this context');
        print.text = (sString, oVariables) =>
            text(sString, oVariables)
                .split('\n')
                .map(s => print.to(client, s));
        /**
     * Check est en outil permetant de tester si les paramètre correspondent aux types spécifiés
     * @example check('SSNn', param1, param2, param3, param4)
     * va verifier que param1 soint bien une chaine de caractère (pas undefined), param2 pareil, param3 doite etre un nombre
     * et param4 s'il n'est pas undefined doit être un nombre.
     * S: Chaine de caractère non-undefined
     * s: Chaine de caracter optionnel (undefined possible)
     * N: Nombre
     * n: Nombre ou undefined
     * @param sType
     * @param params
     * @returns {boolean}
     */
        const check = (sType, ...params) => ContextBuilderService.check(print, sType, ...params);
        return {
            contextType: 'ClientContext',
            client,
            check,
            text,
            print,
            data: {},
            uc: {}
        };
    }
}

/*

CONTEXT DE SYSTEM

text
print
print.to
print.log
print.room
print.room.all
print.room.except
engine
abort
services
command
pc
lastCommand.command
lastCommand.parameters
parse
PARSER_PATTERNS

CONTEXT DE SRV

text
print
print.to <abstract>
print.log
print.room <not available>
print.text
contextType
client
check
data
uc


 */
module.exports = ContextBuilderService;
