const path = require('path');
const ResourceLoader = require('../../../../libs/resource-loader');
const DialogInteractor = require('../../application/interfaces/interactors/DialogInteractor');
const dialog = require('../../../../libs/dialogs');
const CONTEXT_DATA_NAMESPACE = require('../../application/types/context-data-namespaces');
const A256 = require('../../../../libs/ansi-256-colors');

class DialogManager extends DialogInteractor {
    constructor () {
        super();
        this.DIALOG_PATH = path.resolve('srv/base/assets/fr/dialogs');
        this._resourceLoader = new ResourceLoader();
        this._dialogs = null;
        this._a256 = new A256();
    }

    async load () {
        this._dialogs = await this._resourceLoader.load(this.DIALOG_PATH);
    }

    createDialog (sDialog, context) {
        const sFile = sDialog + '.json';
        if (sFile in this._dialogs) {
            const oDialog = new dialog.Context();
            oDialog.load(this._dialogs[sFile], {});
            context.data[CONTEXT_DATA_NAMESPACE.DIALOG] = oDialog;
            delete context.data[CONTEXT_DATA_NAMESPACE.MENU_OPTIONS];
            oDialog.events.on('dialog.end', () => {
                delete context.data[CONTEXT_DATA_NAMESPACE.DIALOG];
                delete context.data[CONTEXT_DATA_NAMESPACE.MENU_OPTIONS];
            });
            oDialog.events.on('dialog.screen', async ({ text, options }) => {
                await context.print(text);
                for (let i = 0, l = options.length; i < l; ++i) {
                    const option = options[i];
                    const nDialogOption = i + 1;
                    await context.print(this._a256.render('[#03f][' + nDialogOption + '][#] ') + option.text);
                }
            });
            return oDialog;
        } else {
            throw new Error('Dialog not found');
        }
    }
}

module.exports = DialogManager;
