const StyleBuilderInteractor = require('../../application/interfaces/interactors/StyleBuilderInteractor');
const STYLES = require('../../../config/styles.json');

class StyleBuilderService extends StyleBuilderInteractor {

    constructor () {
        super();
        this._styles = STYLES;
    }

    get styles () {
        return this._styles;
    }

    build () {
        return this.styles;
    }
}

module.exports = StyleBuilderService;
