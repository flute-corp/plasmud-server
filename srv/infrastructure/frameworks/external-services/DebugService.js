const debug = require('debug');

const logDebug = debug('serv:app');

class DebugService {
    log (sString, ...aVariables) {
        logDebug(sString, ...aVariables);
    }
}

module.exports = DebugService;
