const Scriptorium = require('../../../../libs/scriptorium');
const CommandRunnerInteractor = require('../../application/interfaces/interactors/CommandRunnerInteractor');

class CommandRunnerService extends CommandRunnerInteractor {
    constructor ({ TextRendererInteractor }) {
        super();
        this._scriptorium = new Scriptorium();
        this._textRenderer = TextRendererInteractor;
    }

    get scriptorium () {
        return this._scriptorium;
    }

    get events () {
        return this._scriptorium.events;
    }

    /**
   * Défini le chemin où se trouve les events de commandes
   * @param sPath {string}
   * @returns {Promise<{count: number}>}
   */
    async loadScripts (sPath) {
        return {
            count: await this._scriptorium.loadScripts(sPath)
        };
    }

    /**
   * Lance un script.
   * Les scripts sont des mini programmes qui correspondent à des commandes tapées par l'utilisateur.
   * @param sScript {string}
   * @param context {object}
   * @param params {string}
   */
    runScript (sScript, context, params) {
        if (this._scriptorium.scriptExists(sScript)) {
            return this._scriptorium.runScript(sScript, context, params);
        } else {
            throw new Error('ERR_COMMAND_NOT_FOUND: ' + sScript);
        }
    }

    /**
   * @return {{ name: string, description: string }[]}
   */
    getCommandList () {
        return Object
            .keys(this._scriptorium.routes)
            .filter(s => !s.startsWith('@') && !s.includes('/'))
            .map(s => {
                const name = s;
                const description = this._textRenderer.renderSync('commands.' + name, {});
                return {
                    name,
                    description
                };
            });
    }
}

module.exports = CommandRunnerService;
