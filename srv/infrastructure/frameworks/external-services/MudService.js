const EventManager = require('events');
const GameInteractor = require('../../application/interfaces/interactors/GameInteractor');
const MUD = require('../../../../libs/plasmud');
const config = require('../../../config/modules');
const PromFS = require('../../../../libs/prom-fs');
const path = require('path');
const debug = require('debug');
const logGame = debug('serv:game');
const { getEnv } = require('../../../env-parser');
const { modulesFolderList } = require('../../../config/modules');

class MudService extends GameInteractor {
    constructor () {
        super();
        this.PLAYER_ID_PREFIX = '#';
        this._events = new EventManager();
        this._system = new MUD.System();
        this._system.events.on('output', ({ id, content }) => {
            this.events.emit('output', { id, content });
        });
        this._system.events.on('player.character.export', ({ state }) => {
            logGame('saving character %s (with %d items).', state.name, state.items.length);
            this.events.emit('player.character.export', { state });
        });
        this._information = {};
    }

    async init ({
        moduleList,
        language,
        styles
    }) {
    /**
     * Vérifie l'existence d'un module
     * @param sModule {string}
     * @returns {Promise<{filename:string, access:boolean}>}
     */
        const moduleExists = sModule => new Promise(resolve =>
            PromFS
                .access(sModule, 'r')
                .then(bAccess => {
                    resolve({
                        filename: sModule,
                        access: bAccess
                    });
                })
        );

        /**
     * Déterminer le nom complet d'un module susceptible de se trouver dans la liste
     * des dossiers spécifiés
     * @param sModule {string}
     * @param aPaths {string[]}
     * @returns {Promise<{filename:string, access:boolean}>[]}
     */
        const seekModuleFullPath = (sModule, aPaths) =>
            aPaths.map((p) => {
                const sFull = path.resolve(p, sModule);
                return moduleExists(sFull);
            });

        // Rechercher les modules
        logGame('resolving plasmud module list');
        const aResolvedModules = await Promise.all(moduleList
            .map(m => seekModuleFullPath(m, modulesFolderList))
            .flat()
        );
        const aAccessibleModules = aResolvedModules
            .filter(m => m.access)
            .map(m => m.filename);
        const aInaccessibleModules = new Set(moduleList);
        aAccessibleModules
            .forEach(m => {
                aInaccessibleModules.delete(path.basename(m));
            });
        if (aInaccessibleModules.size > 0) {
            throw new Error('Module(s) not found : ' + aInaccessibleModules.size + ' module(s) could not be located : ' + [...aInaccessibleModules].join(', '));
        }
        this._information.modules = aResolvedModules.reduce((prev, m) => {
            const moduleName = path.basename(m.filename);
            prev[moduleName] = !!prev[moduleName] || m.access;
            return prev;
        }, {});
        this._system.config = {
            modules: aAccessibleModules,
            language,
            styles
        };
        logGame('initializing plasmud lib system');
        return this._system.init();
    }

    get events () {
        return this._events;
    }

    registerPlayer (uid, client, oCharacter, oState = null) {
        if (oState) {
            logGame('@%s\'s character "%s" is registered. Now restoring player character state.', client.uname, oCharacter.name);
        } else {
            logGame('@%s\'s character "%s" is now registered. No previous state found for this character.', client.uname, oCharacter.name);
        }
        const oPlayer = this._system.registerPlayer(uid); // MUD: REGISTER CLIENT
        oPlayer.client = client;
        const oEntity = oState
            ? this._system.importPlayerCharacter(oState)
            : this._system.engine.createPlayerEntity(uid, this.PLAYER_ID_PREFIX + oCharacter.id, oCharacter.name);
        if (!oPlayer.context.pc) {
            oPlayer.context.pc = oEntity;
        } else if (oPlayer.context.pc !== oEntity) {
            throw new Error('incoherences when creating player character entity...');
        }
        return {
            player: oPlayer,
            entity: oEntity
        };
    }

    unregisterPlayer (uid) {
        if (this._system.isPlayerRegistered(uid)) {
            logGame('unregistering user::%s from plasmud', uid);
            this._system.unregisterPlayer(uid); // MUD: UNREGISTER CLIENT
        }
    }

    processCommand (uid, sOpcode, args) {
        return this._system.command(uid, sOpcode, args);
    }

    isCommandExist (sOpcode) {
        return this._system.isCommandExist(sOpcode);
    }

    getInformation() {
        return this._information;
    }

    getCommandList() {
        return this._system.getCommandList();
    }

    handleClock () {
        this._system.handleClock();
    }

    shutdown() {
        return this._system.shutdown();
    }
}

module.exports = MudService;
