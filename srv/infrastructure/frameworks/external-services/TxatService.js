const ChatInteractor = require('../../application/interfaces/interactors/ChatInteractor');
const TinyTxat = require('../../../../libs/tiny-txat');
const debug = require('debug');
const EventEmitter = require('events');

const log = debug('serv:txat');

const TXAT_EVENT_TYPES = {
    OPEN_CHANNEL: 'OPEN_CHANNEL', // demande au client d'ouvrir un canal de son coté afin de pouvoir en recevoir les messages
    CLOSE_CHANNEL: 'CLOSE_CHANNEL', // demande au client de fermer un canal précédemment ouvert de son coté : il ne recevra plus de message sur ce canal
    MESSAGE: 'MESSAGE' // indique au client qu'il a reçu un message sur un canal
};

class TxatService extends ChatInteractor {
    constructor ({ TextRendererInteractor }) {
        super();
        this._txat = null;
        this._lobbyName = 'lobby';
        this._lobbyChannel = null;
        this._eventEmitter = new EventEmitter();
        this._services = {
            textRenderer: TextRendererInteractor
        };
    }

    get TXAT_EVENT_TYPES () {
        return TXAT_EVENT_TYPES;
    }

    get events () {
        return this._eventEmitter;
    }

    /**
   * Envoie une chaine de caractère au client en passant par le service i18n
   * @param client {Client} client
   * @param cid {string} identifiant canal
   * @param sKey {string} clé i18n
   * @param oContext {object} contexte pour i18n
   */
    sendI18nToClient (client, cid, sKey, oContext) {
        const sText = this._services.textRenderer.renderSync(sKey, oContext);
        this.triggerClientEvent(client, TXAT_EVENT_TYPES.MESSAGE, {
            channel: cid,
            content: sText
        });
    }

    /**
   * Envoie central d'évènement au client
   * @param client {Client}
   * @param sEventType {string} type d'évènement TXAT_EVENT_TYPES
   * @param data {object}
   */
    triggerClientEvent (client, sEventType, data) {
        this.events.emit(sEventType, {
            ...data,
            client
        });
    }

    /**
   * Initialisation du service
   */
    init () {
        log('initialization');
        const oTxat = new TinyTxat.System();
        this._txat = oTxat;
        oTxat.on('log', ({ message }) => log(message));
        this._lobbyChannel = this.createChannel(this._lobbyName);
        // un utilisateur rejoin un canal
        oTxat.on('user-joined', async ({ to, uid, cid }) => {
            const uTo = oTxat.getUser(to);
            const c = oTxat.getChannel(cid);
            const u = oTxat.getUser(uid);
            if (to === uid) {
                // pour le cas ou le txat-user vient d'etre créé : il n'a pas encore le client ws renseigné.
                if (uTo.data.client) {
                    log('sending %s panel creation to %s', c.display, uTo.display);
                    this.triggerClientEvent(uTo.data.client, TXAT_EVENT_TYPES.OPEN_CHANNEL, {
                        channel: cid,
                        name: c.name
                    });
                }
            } else {
                await this.sendI18nToClient(uTo.data.client, cid, 'chat.info.eventJoin', { name: u.name });
            }
        });
        // un utilisateur quitte un canal
        oTxat.on('user-left', async ({ to, uid, uname, cid }) => {
            const uTo = oTxat.getUser(to);
            const c = oTxat.getChannel(cid);
            if (to === uid) {
                this.triggerClientEvent(uTo.data.client, TXAT_EVENT_TYPES.CLOSE_CHANNEL, {
                    channel: cid,
                    name: c.name
                });
            } else {
                await this.sendI18nToClient(uTo.data.client, cid, 'chat.info.eventLeave', { name: uname });
            }
        });
        // un utilisateur envoie un message soit à un autre utilisateur soit à un canal entier
        oTxat.on('user-message', async ({ to, uid, cid, message }) => {
            const uTo = oTxat.getUser(to);
            const u = oTxat.getUser(uid);
            if (cid) {
                // message venant d'un canal
                this.triggerClientEvent(uTo.data.client, TXAT_EVENT_TYPES.MESSAGE, {
                    channel: cid,
                    content: u.name + ': ' + message
                });
            } else if (uid) {
                // message venant d'un utilisateur
                const sMessageFromClient = this._services.textRenderer.renderSync('chat.from', { name: u.name, message });
                const sMessageToInterlocutor = this._services.textRenderer.renderSync('chat.to', { name: uTo.name, message });
                this.triggerClientEvent(uTo.data.client, TXAT_EVENT_TYPES.MESSAGE, {
                    channel: '*',
                    content: sMessageFromClient
                });
                this.triggerClientEvent(u.data.client, TXAT_EVENT_TYPES.MESSAGE, {
                    channel: '*',
                    content: sMessageToInterlocutor
                });
            }
        });
    }

    /**
   * Création d'un canal
   * @param sChannel {string}
   * @returns {*|Channel}
   */
    createChannel (sChannel) {
        const t = this._txat;
        sChannel = sChannel.toLowerCase();
        if (!sChannel.match(/^[\x21-\x7F]+$/)) {
            throw new Error('ERR_INVALID_CHANNEL_NAME');
        }
        let c = t.searchChannel(sChannel);
        if (!c) {
            // création du canal
            c = t.createChannel({ id: sChannel, name: sChannel });
            t.addChannel(c);
        }
        return c;
    }

    /**
   * L'utilisateur veut acceder à un nouveau canal
   * @param uid {string} identifiant utilisateur
   * @param sChannel {string} nom du canal
   */
    join (uid, sChannel) {
        const t = this._txat;
        const u = t.getUser(uid);
        try {
            const c = this.createChannel(sChannel);
            c.addUser(u);
        } catch (e) {
            const u = t.getUser(uid);
            this.sendI18nToClient(u.data.client, null, 'chat.error.invalidChannelName');
        }
    }

    /**
   * L'utilisateur veut quitter un canal
   * @param uid {string} identifiant utilisateur
   * @param sChannel {string} nom du canal
   */
    leave (uid, sChannel) {
        const t = this._txat;
        const u = t.getUser(uid);
        const c = t.searchChannel(sChannel);
        if (c && c.userPresent(u)) {
            // Le canal existe et l'utilisateur est bien présent dedans
            c.dropUser(u);
        }
    }

    /**
   * Envoie d'un message publi à un canal
   * @param uid {string} utilisateur emmettant le message
   * @param sChannel {string} nom du canal
   * @param sMessage {string} contenu du message
   */
    sendPublicMessage (uid, sChannel, sMessage) {
        const t = this._txat;
        const u = t.getUser(uid);
        const c = t.searchChannel(sChannel);
        if (c) {
            c.transmitMessage(u, sMessage);
        }
    }

    /**
   * L'utilisateur envoie un message public au canal spécifié
   * @param uid {string} identifiant utilisateur
   * @param duid {string} identifiant de l'utilisateur destinataire
   * @param sMessage {string} contenu du message
   */
    sendPrivateMessage (uid, duid, sMessage) {
        const t = this._txat;
        const u = t.getUser(uid); // expéditeur
        const ud = t.getUser(duid); // destinataire
        ud.transmitMessage(u, sMessage);
    }

    /**
   * Connection d'un utilisateur
   * @param uid {string} identifiant de l'utilisateur qui se connecte
   * @param sName {string} nom de l'utilisateur
   * @returns {User} instance utilisateur créé par le txat système
   */
    connectUser (uid, sName) {
        const t = this._txat;
        // créer l'utilisateur
        const u = t.createUser({ id: uid, name: sName });
        // l'intégrer dans le système
        t.addUser(u);
        return u;
    }

    /**
   * Déconnection d'un utilisateur
   * @param uid {string} identifiant utilisateur
   */
    disconnectUser (uid) {
        const t = this._txat;
        const u = t.getUser(uid);
        t.dropUser(u);
    }
}

module.exports = TxatService;
