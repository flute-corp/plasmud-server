const StringUtilsInteractor = require('../../application/interfaces/interactors/StringUtilsInteractor');
const quoteSplit = require('../../../../libs/quote-split/node');
const { isSafeName } = require('../../../../libs/is-safe-name');

class StringUtilsService extends StringUtilsInteractor {
    /**
     * découpe une chaine en éléments séparés par des espaces. Les portions délimitées par des " constituent un seul élément
     * @param s {string}
     * @returns {string[]}
     */
    quoteSplit(s) {
        return quoteSplit(s);
    }

    /**
     * Indique si une chaine est sûre pour un nom de personnage
     * @param s {string}
     * @return {boolean}
     */
    isSafeName (s) {
        return isSafeName(s);
    }

    isValidEMailAddress(s) {
        return !!s.match(/^([-.a-zA-Z0-9_]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([-a-zA-Z0-9]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(]?)$/);
    }
}

module.exports = StringUtilsService;
