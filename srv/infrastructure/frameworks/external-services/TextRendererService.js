const TextRenderer = require('../../../../libs/text-renderer');
const TPIntl = require('../../../../libs/text-renderer/TextProducerI18next');
const TPTmpl = require('../../../../libs/text-renderer/TextProducerTwig');
const TextRendererInteractor = require('../../application/interfaces/interactors/TextRendererInteractor');
// const A256 = require('../../../libs/ansi-256-colors')

class TextRendererService extends TextRendererInteractor {
    constructor() {
        super();
        // this._a256 = new A256()
        this._textRenderer = new TextRenderer();
        this._textRenderer.addProducer('strings', new TPIntl());
        this._textRenderer.addProducer('templates', new TPTmpl());
    }

    get producers () {
        return this._textRenderer.producers;
    }

    /**
     * Initialisation du service
     * @param config {*} chemin d'accès aux templates
     */
    async init ({ strings, templates, styles }) {
        const tr = this._textRenderer;
        await this._textRenderer.init({ strings, templates, styles });
        const trp = tr.producers;
        const trpStrings = trp.get('strings');
        const trpTemplates = trp.get('templates');
        if (templates.paths) {
            templates.paths.forEach(p => trpTemplates.defineResourcePath(p));
        }
        trpStrings.defineResource('helpers', strings);
    }

    /**
     * Termine de charger tous les template defini avant de pouvoir les utiliser
     * @returns {Promise<void>}
     */
    initDone () {
        return this._textRenderer.initDone();
    }

    /**
     * @param sKey
     * @param oVariables
     * @returns {string|null}
     */
    renderSync (sKey, oVariables) {
        return this
            ._textRenderer
            .renderSync(sKey, oVariables);
        // .split('\n')
        // // .map(s => this._a256.render(s))
        // .join('\n')
    }

    /**
     *
     * @param sKey
     * @param oVariables
     * @returns {Promise<string>}
     */
    renderAsync (sKey, oVariables) {
        return this
            ._textRenderer
            .renderAsync(sKey, oVariables);
        // .then(sRenderedText => sRenderedText
        //     .split('\n')
        //     .map(s => this._a256.render(s))
        //     .join('\n')
        // )
    }
}

module.exports = TextRendererService;
