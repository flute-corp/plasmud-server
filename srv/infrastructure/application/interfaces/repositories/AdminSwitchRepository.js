const Repository = require('./Repository');
/**
 * Opérations de base de tout repository proposant de la persistance
 */
class AdminSwitchRepository extends Repository {
    setSwitch (prop, value) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    getSwitch (prop, sDefault) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    getSwitchList () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }
}

module.exports = AdminSwitchRepository;
