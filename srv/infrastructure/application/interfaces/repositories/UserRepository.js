const Repository = require('./Repository');
/**
 * Opérations de base de tout repository proposant de la persistance
 */
class UserRepository extends Repository {
    /**
   * Recherche un utilisateur par son nom
   * @param name {string}
   * @returns {Promise<never>}
   */
    async findByName (name) {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    /**
   * Recherche d'un utilisateur à partir d'un pattern de texte (comme dan sun RLIKE)
   * @param pattern {string} chaine représentant une expression régulière
   * @returns {Promise<never>}
   */
    async findByNameLike (pattern) {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    /**
   * Récupération de tous les utilisateurs, à des fins de listage administratif.
   * @returns {Promise<never>}
   */
    async getAll () {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    /**
   * Renvoie le nombre d'utilisateurs enregistrés
   * @returns {Promise<number>}
   */
    async getCount () {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }
}

module.exports = UserRepository;
