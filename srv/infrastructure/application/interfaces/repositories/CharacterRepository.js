const Repository = require('./Repository');
/**
 * Opérations de base de tout repository proposant de la persistance
 */
class CharacterRepository extends Repository {
    /**
   * Recherche un personnage par son nom
   * @param name {string}
   * @return {Promise<Character>}
   */
    findByName (name) {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    /**
   * Recherche les personnages de l'utilisateur spécifié
   * @param owner {string} identifiant de l'utilisateur dont on cherche les personnages
   * @return {Promise<Character[]>}
   */
    findByOwner (owner) {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    /**
   * Défini le personnage spécifié comme étant le personnage par défaut
   * @param id {string} identifiant du personnage
   * @return {Promise<Character>}
   */
    setOwnerCharacterAsCurrent (id) {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    /**
   * Renvoie le personnage par défault de l'utilisateur spécifié
   * @param owner {string} identifiant utilisateur
   * @return {Promise<Character>}
   */
    getOwnerCurrentCharacter (owner) {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }
}

module.exports = CharacterRepository;
