const Repository = require('./Repository');
/**
 * Opérations de base de tout repository proposant de la persistance
 */
class ItemSaveRepository extends Repository {
}

module.exports = ItemSaveRepository;
