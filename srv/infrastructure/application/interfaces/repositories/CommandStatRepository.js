const Repository = require('./Repository');

class CommandStatRepository extends Repository {
    addCommandStat (opcode, time) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    /**
   * Récupération de toutes les entités
   * @returns {Promise<Client[]>}
   */
    getAll () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }}

module.exports = CommandStatRepository;
