const Repository = require('./Repository');
/**
 * Opérations de base de tout repository proposant de la persistance
 */
class PlayerCharacterSaveRepository extends Repository {
}

module.exports = PlayerCharacterSaveRepository;
