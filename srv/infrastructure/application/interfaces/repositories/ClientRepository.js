const Repository = require('./Repository');
/**
 * Opérations de base de tout repository proposant de la persistance
 */
class ClientRepository extends Repository {
    removeUserDeadClients (idUser) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    findByUser (idUser) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    getConnectedClients () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    getAll () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }
}

module.exports = ClientRepository;
