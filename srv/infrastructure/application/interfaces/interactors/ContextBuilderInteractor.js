class ContextBuilderInteractor {
    init () {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    /**
   * Construit le contexte d'exécution des commandes d'un client
   * Les "events" sont des programmes associés à une commande que les clients envoient au serveur.
   * Le contexte transmit aux events permettent à ces dernier d'accéder aux ressources du serveur
   * (plasmud, print, strings, template etc...)
   * @param client {Client}
   * @returns {*}
   */
    buildClientContext (client) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }
}

module.exports = ContextBuilderInteractor;
