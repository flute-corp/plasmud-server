class TextRendererInteractor {
    get producers () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    /**
   * Initialisation du service
   * @param config {string} chemin d'accès aux templates
   */
    async init (config) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    /**
   * Termine de charger tous les template defini avant de pouvoir les utiliser
   * @returns {Promise<void>}
   */
    initDone () {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    renderSync (sKey, oVariables) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    renderAsync (sKey, oVariables) {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }
}

module.exports = TextRendererInteractor;
