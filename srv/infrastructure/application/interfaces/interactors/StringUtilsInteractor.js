class StringUtilsInteractor {
    quoteSplit (s) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    /**
     * REnvoie true si le nom est valide
     * @param s
     */
    isSafeName (s) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    /**
     * Renvoie true si la chaine est un emiale valide
     * @param s {string}
     */
    isValidEMailAddress (s) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }
}

module.exports = StringUtilsInteractor;
