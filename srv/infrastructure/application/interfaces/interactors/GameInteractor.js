class GameInteractor {

    init (config) {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    get events () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    registerPlayer (uid) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    unregisterPlayer (uid) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    isCommandExist (sOpcode) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    getCommandList () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    processCommand (uid, sOpcode, args) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    getInformation () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    handleClock () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    shutdown () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }
}

module.exports = GameInteractor;
