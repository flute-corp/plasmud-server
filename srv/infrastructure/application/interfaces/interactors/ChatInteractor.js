class ChatInteractor {
    /**
   * L'utilisateur s'enregistre aupres du service de chat
   * @param uid
   * @param name
   */
    connectUser (uid, name) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    disconnectUser (uid, name) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    /**
   * L'utilisateur rejoin le canal
   * @param uid {string} identifiant utilisateur
   * @param sChannel {string} nom du canal
   */
    join (uid, sChannel) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    /**
   * L'utilisateur quitte le canal
   * @param uid {string} identifiant utilisateur
   * @param sChannel {string} nom du canal
   */
    leave (uid, sChannel) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    /**
   * L'utilisateur envoie un message public au canal spécifié
   * @param uid {string} identifiant utilisateur
   * @param sChannel {string} nom du canal
   * @param sMessage {string} contenu du message
   */
    sendPublicMessage (uid, sChannel, sMessage) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    /**
   * L'utilisateur envoie un message privé a l'utilisateur spécifié
   * @param uid {string} identifiant utilisateur
   * @param duid {string} identifiant de l'utilisateur destinataire
   * @param sMessage {string} contenu du message
   */
    sendPrivateMessage (uid, duid, sMessage) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }
}

module.exports = ChatInteractor;
