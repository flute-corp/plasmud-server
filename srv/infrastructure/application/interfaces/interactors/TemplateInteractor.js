class TemplateInteractor {
    /**
   * Initialisation du service
   * @param sPath {string} chemin d'accès aux templates
   */
    async init (sPath) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    /**
   * Termine de charger tous les template defini avant de pouvoir les utiliser
   * @returns {Promise<void>}
   */
    initDone () {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    hasTemplate (sKey) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    render (sKey, oVariables) {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }
}

module.exports = TemplateInteractor;
