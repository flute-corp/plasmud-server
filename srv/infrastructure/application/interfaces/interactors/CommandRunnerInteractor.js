class CommandRunnerInteractor {
    get events () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }

    /**
   * Défini le chemin ou se trouve les events de commandes
   * @param sPath {string}
   * @return {Promise<never>}
   */
    async loadScripts (sPath) {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    /**
   * Lance un script.
   * Les script son des mini programme qui correspondent à des commandes tapées par l'utilisateur.
   * @param sScript {string} nom du script
   * @param context {MUDContext} contexte du script (outils, variables...}
   * @param params {string} paramètre tapés à la suite de la commande, et transmises au script sous
   * forme de tableau de chaine
   * @return {Promise<never>}
   */
    runScript (sScript, context, ...params) {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    /**
   * Lance une commande numérique système
   * Typiquement utilisée dans un menu.
   * @param nNum {number}
   * @param context {object}
   * @returns {Promise<never>}
   */
    runNumericOption (nNum, context) {
        return Promise.reject(new Error('ERR_NOT_IMPLEMENTED'));
    }

    /**
   * Renvoi la liste des commandes système disponnibles
   * @return {{ name: string, description: string }[]}
   */
    getCommandList () {
        throw new Error('ERR_NOT_IMPLEMENTED');
    }
}

module.exports = CommandRunnerInteractor;
