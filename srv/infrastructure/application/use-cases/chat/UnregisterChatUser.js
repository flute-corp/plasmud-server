class UnregisterChatUser {
    constructor ({ ChatInteractor }) {
        this.chat = ChatInteractor;
    }

    async execute (uid) {
        const oTxatUser = this.chat.getUser(uid);
        this.chat.dropUser(oTxatUser);
        return {
            success: true
        };
    }
}

module.exports = UnregisterChatUser;
