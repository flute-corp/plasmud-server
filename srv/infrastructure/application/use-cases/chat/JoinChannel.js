class JoinChannel {
    constructor ({ ChatInteractor }) {
        this.chat = ChatInteractor;
    }

    async execute (uid, sChannel) {
        return this.chat.join(uid, sChannel);
    }
}

module.exports = JoinChannel;
