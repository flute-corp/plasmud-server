class SendPublicMessage {
    constructor ({ ChatInteractor }) {
        this.chat = ChatInteractor;
    }

    async execute (uid, sChannel, sMessage) {
        return this.chat.sendPublicMessage(uid, sChannel, sMessage);
    }
}

module.exports = SendPublicMessage;
