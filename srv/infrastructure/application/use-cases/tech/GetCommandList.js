class GetCommandList {
    constructor ({ GameInteractor, CommandRunnerInteractor }) {
        this.GameInteractor = GameInteractor;
        this.CommandRunnerInteractor = CommandRunnerInteractor;
    }

    async execute () {
        return [
            ...this.GameInteractor.getCommandList(),
            ...this.CommandRunnerInteractor.getCommandList()
        ].sort((a, b) => a.name.localeCompare(b.name));
    }
}

module.exports = GetCommandList;
