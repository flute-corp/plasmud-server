const CONTEXT_DATA_NS = require('../../types/context-data-namespaces');

class DefineContextMenuOptions {
    constructor ({ DialogInteractor }) {
        this.DialogInteractor = DialogInteractor;
    }

    async execute (context, sDialog) {
        this.DialogInteractor.createDialog(sDialog, context).displayCurrentScreen();
    }
}

module.exports = DefineContextMenuOptions;
