const Message = require('../../../entities/Message');

class ParseMessage {
    constructor ({ StringUtilsInteractor }) {
        this.StringUtilsInteractor = StringUtilsInteractor;
    }

    parse (sMessage) {
        sMessage = sMessage.trim();
        if (sMessage === '') {
            return {
                valid: false,
                opcode: '',
                numeric: NaN,
                arguments: [],
                unparsed: sMessage
            };
        }
        const aMessage = this.StringUtilsInteractor.quoteSplit(sMessage);
        const opcode = aMessage.shift();
        const nNum = parseInt(opcode);
        return new Message({
            valid: true,
            opcode,
            numeric: nNum,
            arguments: aMessage,
            unparsed: sMessage
        });
    }

    /**
   * Transforme une chaine de caractère en entité message
   * @return {{success: boolean, error: string, message: Message}}
   */
    async execute (sMessage) {
        const t = typeof sMessage;
        if (t !== 'string') {
            throw new TypeError('ParseMessage message should be a string... ' + t + 'given');
        }
        if (sMessage.startsWith('{')) {
            throw new Error('ERR_BAD_FORMAT');
        }
        return { message: this.parse(sMessage) };
    }
}

module.exports = ParseMessage;
