const v8 = require('v8');

/**
 * Obtenir des info sur la Heap
 */
class GetMemoryUsage {
    async execute () {
        const h = v8.getHeapStatistics();
        // heap_size_limit : La limite maximale de mémoire qui peut être allouée pour le heap (en bytes).
        // total_available_size : La quantité de mémoire disponible pour des allocations supplémentaires dans le heap (en bytes).
        // total_physical_size : représente la quantité de mémoire physique utilisée par le heap de V8.
        //          Cette valeur comprend la mémoire effectivement utilisée par les objets JavaScript ainsi que la
        //          mémoire qui a été allouée mais n'est pas encore utilisée (fragmentation). Elle peut être plus élevée
        //          que used_heap_size à cause de la fragmentation de la mémoire et des allocations préventives pour
        //          optimiser les performances.
        // total_heap_size : La quantité de mémoire allouée pour le heap (en bytes).
        // used_heap_size : La quantité de mémoire utilisée par les objets dans le heap (en bytes).
        // total_heap_size_executable : La quantité de mémoire allouée pour des espaces de code exécutable (en bytes).
        // malloced_memory : La quantité de mémoire allouée via des appels malloc (en bytes).
        // peak_malloced_memory : Le pic de mémoire allouée via des appels malloc (en bytes).
        // does_zap_garbage : Un indicateur si V8 remplit les zones libérées de la mémoire avec des zéros pour faciliter le débogage (1 si vrai, 0 si faux).
        return {
            maximum: h.heap_size_limit,
            available: h.total_available_size,
            allocated: h.used_heap_size,
            free: 100 - 100 * h.used_heap_size / h.heap_size_limit,
            program: h.total_heap_size_executable,
        };
    }
}

module.exports = GetMemoryUsage;
