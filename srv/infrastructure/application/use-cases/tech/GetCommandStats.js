class GetCommandStats {
    constructor ({ CommandStatRepository }) {
        this.commandStatRepository = CommandStatRepository;
    }

    async execute () {
        const m = await this.commandStatRepository.getAll();
        return m.map(({ command, count, time }) => ({
            command,
            count,
            time
        }));
    }
}

module.exports = GetCommandStats;
