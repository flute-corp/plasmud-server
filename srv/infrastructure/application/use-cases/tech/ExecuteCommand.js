const CONTEXT_DATA_NAMESPACES = require('../../types/context-data-namespaces');
const CONTEXT_DATA_NAMESPACE = require('../../types/context-data-namespaces');

class ExecuteCommand {
    constructor ({ DefineContextMenuOptions, ParseMessage, CommandStatRepository, CommandRunnerInteractor, GameInteractor, CLIENT_STATES }) {
        this.commandRunner = CommandRunnerInteractor;
        this.game = GameInteractor;
        this.commandStatRepository = CommandStatRepository;
        this.CLIENT_STATES = CLIENT_STATES;
        this.parseMessage = ParseMessage;
        this.defineMenuOptions = DefineContextMenuOptions;
    }

    hasContextDialog (context) {
        return CONTEXT_DATA_NAMESPACE.DIALOG in context.data;
    }

    clearDialogFromContext (context) {
        if (this.hasContextDialog(context)) {
            context.data[CONTEXT_DATA_NAMESPACE.DIALOG].triggerEnd();
        }
    }

    async runNumericCommand (n, context) {
        if (CONTEXT_DATA_NAMESPACES.MENU_OPTIONS in context.data && Array.isArray(context.data[CONTEXT_DATA_NAMESPACES.MENU_OPTIONS])) {
            const option = context.data[CONTEXT_DATA_NAMESPACES.MENU_OPTIONS].find(o => o.id === n);
            delete context.data[CONTEXT_DATA_NAMESPACES.MENU_OPTIONS];
            // clear menu options
            await this.defineMenuOptions.execute(context, []);
            if (option) {
                const { command } = option;
                const { message } = await this.parseMessage.execute(command);
                return this.commandRunner.runScript(message.opcode, context, message.arguments);
            } else {
                throw new Error('Undefined menu option');
            }
        } else if (CONTEXT_DATA_NAMESPACES.DIALOG in context.data) {
            const oDialogCtx = context.data[CONTEXT_DATA_NAMESPACES.DIALOG];
            oDialogCtx.selectOption(n - 1);
        } else {
            throw new Error('Undefined/unavailable or invalid menu');
        }
    }

    async commitTime (m, t) {
        const [s, ns] = process.hrtime(t);
        return this.commandStatRepository.addCommandStat(m, s + ns / 1e9);
    }

    async execute (context, message) {
        const client = context.client;

        // vérifier que le client soit bien dans le MUD
        const aTime1 = process.hrtime();

        let r;

        const bInGame = client.state === this.CLIENT_STATES.CLIENT_STATE_IN_GAME;
        const bGameCommand = this.game.isCommandExist(message.opcode);
        const bNumeric = !isNaN(message.numeric);
        const nCase = (bInGame ? 100 : 0) + (bGameCommand ? 10 : 0) + (bNumeric ? 1 : 0);

        switch (nCase) {
        // COMMANDES NUMÉRIQUES
        case 1:
        case 101: {
        // Pas une commande de jeu, c'est une commande numérique
        // Ce cas ne devrait pas exister, car toute commande numérique est considérée comme une command ede jeu
            throw new Error('ERR_NUMERIC_COMMAND_INCOHERENT');
        }
        case 11: {
        // Commande système numérique
            r = await this.runNumericCommand(message.numeric, context);
            break;
        }

        case 111: {
        // Commande de jeu numérique ou commande système numérique
            if (this.hasContextDialog(context)) {
                r = await this.runNumericCommand(message.numeric, context);
            } else {
                r = this.game.processCommand(client.uid, message.opcode, message.arguments);
            }
            break;
        }

        // COMMANDES ALPHABETIQUES
        case 0: {
        // Pas en jeu, pas une commande de jeu, pas une commande numérique
        // Il s'agit donc d'une commande système pendant qu'on est en phase système
            this.clearDialogFromContext(context);
            r = await this.commandRunner.runScript(message.opcode, context, message.arguments);
            break;
        }

        case 10: {
        // Pas en jeu et pourtant, c'est commande de jeu
            throw new Error('ERR_NOT_IN_GAME');
        }

        case 100: {
        // En jeu, mais ce n'est pas une commande de jeu
        // Les commandes système sont accessibles en jeu également
            this.clearDialogFromContext(context);
            r = await this.commandRunner.runScript(message.opcode, context, message.arguments);
            break;
        }

        case 110: {
        // En jeu et c'est une commande de jeu
        // C'est une commande de jeu plasmud, qu'on utilise en cours de partie
            this.clearDialogFromContext(context);
            r = this.game.processCommand(client.uid, message.opcode, message.arguments);
            break;
        }
        }
        if (!bNumeric) {
            await this.commitTime(message.opcode, aTime1);
        }
        return r;
    }
}

module.exports = ExecuteCommand;
