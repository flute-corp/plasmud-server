const CONTEXT_DATA_NS = require('../../types/context-data-namespaces');

class DefineContextMenuOptions {
    async execute (context, aOptions) {
        // vérifier
        if (aOptions.every(o =>
            o &&
            (typeof o === 'object') &&
            'id' in o &&
            'command' in o
        )) {
            context.data[CONTEXT_DATA_NS.MENU_OPTIONS] = aOptions;
        } else {
            throw new TypeError('DefineContextMenuOptions : Expected options are invalid');
        }
    }
}

module.exports = DefineContextMenuOptions;
