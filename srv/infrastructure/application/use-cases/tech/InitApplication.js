const path = require('path');

/**
 * Obtenir le fichier d'aide correspondant
 */
class InitApplication {
    constructor ({
        DatabaseInteractor,
        DialogInteractor,
        ChatInteractor,
        GameInteractor,
        TextRendererInteractor,
        CommandRunnerInteractor,
        ContextBuilderInteractor,
        StyleBuilderInteractor,
        AdminSwitchRepository,
        PersistCharacterState,
        ADMIN_SWITCHES
    }) {
        this.ADMIN_SWITCHES = ADMIN_SWITCHES;
        this._useCases = {
            PersistCharacterState
        };
        this._interactors = {
            DatabaseInteractor,
            DialogInteractor,
            ChatInteractor,
            GameInteractor,
            CommandRunnerInteractor,
            TextRendererInteractor,
            ContextBuilderInteractor,
            StyleBuilderInteractor
        };
        this._repositories = {
            AdminSwitchRepository
        };
    }

    async execute ({
        moduleLocation,
        logFunc,
        env
    }) {
        const PATH_STRINGS = path.join(moduleLocation, 'assets', env.PLASMUD_LANG, 'strings');
        const STRINGS = require(PATH_STRINGS);

        const int = this._interactors;
        const rep = this._repositories;

        logFunc('database - starting service');
        await int.DatabaseInteractor.init();

        logFunc('text renderer - starting service');
        const oStyles = int.StyleBuilderInteractor.build();
        await int.TextRendererInteractor.init({
            strings: STRINGS,
            templates: {
                paths: [path.join(moduleLocation, 'assets', env.PLASMUD_LANG, 'templates')],
                resetEachLine: true
            },
            styles: oStyles
        });
        await int.TextRendererInteractor.initDone();

        logFunc('chat - starting service');
        int.ChatInteractor.init();

        logFunc('plasmud - starting service');
        await int.GameInteractor.init({
            moduleList: env.PLASMUD_MODULES.split(',').map(s => s.trim()),
            language: env.PLASMUD_LANG,
            styles: oStyles
        });
        int.GameInteractor.events.on('player.character.export', ({ state }) => {
            return this._useCases.PersistCharacterState.execute(state);
        });

        logFunc('command interpreter - starting service');
        await int.CommandRunnerInteractor.loadScripts(path.join(moduleLocation, 'commands'));

        logFunc('client context builder - starting service');
        int.ContextBuilderInteractor.init();

        logFunc('opening service for clients');
        await rep.AdminSwitchRepository.setSwitch(this.ADMIN_SWITCHES.OPEN, true);

        await this._interactors.DialogInteractor.load();
    }
}

module.exports = InitApplication;
