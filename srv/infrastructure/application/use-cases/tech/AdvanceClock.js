class AdvanceClock {
    constructor ({
        GameInteractor,
        GetMemoryUsage
    }) {
        this._game = GameInteractor;
        this._getMemoryUsage = GetMemoryUsage;
        this._time = 0;
        this._lastMemoryUsage = null;
    }

    formatMemorySize(bytes) {
        const units = ['B', 'K', 'M', 'G', 'T', 'P'];
        let unitIndex = 0;

        while (bytes >= 1024 && unitIndex < units.length - 1) {
            bytes /= 1024;
            unitIndex++;
        }

        const nFix = bytes < 10
            ? 2
            : bytes < 100
                ? 1
                : 0;

        return `${bytes.toFixed(nFix)} ${units[unitIndex]}`;
    }

    /**
     * This should be executed each second
     * @returns {Promise<void>}
     */
    async execute ({
        logFunc
    }) {
        this._game.handleClock();
        if ((this._time % 60) === 0) { // each minute
            const m = await this._getMemoryUsage.execute();
            let bPrint = false;
            if (!this._lastMemoryUsage) {
                this._lastMemoryUsage = m;
                bPrint = true;
            }
            const lmu = this._lastMemoryUsage;
            if (Math.abs(m.free - lmu.free) >= 1) {
                this._lastMemoryUsage = m;
                bPrint = true;
            }
            if (bPrint) {
                logFunc(
                    'memory - used %s / free %s (%d %%)',
                    this.formatMemorySize(m.allocated),
                    this.formatMemorySize(m.available),
                    Math.round(m.free * 10) / 10
                );
            }
        }
        ++this._time;
    }
}

module.exports = AdvanceClock;
