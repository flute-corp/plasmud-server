class GetClientList {
    constructor ({ ClientRepository }) {
        this.clientRepository = ClientRepository;
    }

    /**
     * Récupérer l'instance Client à partir de son identifiant
     */
    async execute () {
        return this.clientRepository.getAll();
    }
}

module.exports = GetClientList;
