class FindUserClient {
    constructor ({ ClientRepository }) {
        this.clientRepository = ClientRepository;
    }

    /**
   * Trouver les clients que le user utilise pour se connecter
   * @param idUser {string} identifiant utilisateur
   */
    async execute (idUser) {
        const clients = await this.clientRepository.findByUser(idUser);
        return clients.shift();
    }
}

module.exports = FindUserClient;
