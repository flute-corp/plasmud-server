class GetClient {
    constructor ({ ClientRepository }) {
        this.clientRepository = ClientRepository;
    }

    /**
   * Récupérer l'instance Client à partir de son identifiant
   */
    async execute (clientId) {
        return this.clientRepository.get(clientId);
    }
}

module.exports = GetClient;
