class KickAllClients {
    constructor ({ ClientRepository, CLIENT_STATES, LogInteractor, GameInteractor }) {
        this.logInteractor = LogInteractor;
        this.clientRepository = ClientRepository;
        this.game = GameInteractor;
        this.CLIENT_STATES = CLIENT_STATES;
    }

    /**
   * Creation d'une instance client à partir d'une socket de connexion
   */
    async execute () {
        await this.logInteractor.log('disconnecting all clients.');
        const clients = await this.clientRepository.getAll();
        clients.forEach(client => {
            client.socket.close();
        });
    }
}

module.exports = KickAllClients;
