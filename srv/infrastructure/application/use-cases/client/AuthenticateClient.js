/**
 * @class AuthenticateClient
 */
class AuthenticateClient {
    constructor ({ GetUserBanishment, UserRepository, ClientRepository, CLIENT_STATES }) {
        this.userRepository = UserRepository;
        this.clientRepository = ClientRepository;
        this.getUserBanishment = GetUserBanishment;
        this.CLIENT_STATES = CLIENT_STATES;
    }

    /**
   * L'Authentification d'un utilisateur
   */
    async execute (clientId, name, password) {
    // 1 trouver un utilisateur en base avec le nom spécifié
        const oUser = await this.userRepository.findByName(name);
        if (!oUser) {
            throw new Error('ERR_AUTH_FAILED');
        }
        const aOtherClients = await this.clientRepository.findByUser(oUser.id);
        if (aOtherClients.length > 0) {
            throw new Error('ERR_ALREADY_CONNECTED');
        }
        const oClient = await this.clientRepository.get(clientId);
        if (oUser) {
            if (oUser.password === '') {
                oUser.password = password;
            }
            if (password === oUser.password) {
                const banishment = await this.getUserBanishment.execute(oUser.id);
                if (banishment.banned) {
                    return {
                        success: false,
                        user: oUser,
                        banishment
                    };
                }
                oClient.uid = oUser.id;
                oClient.uname = oUser.name;
                oClient.state = this.CLIENT_STATES.CLIENT_STATE_AUTHENTICATED;
                await this.clientRepository.persist(oClient);
                oUser.dateLastUsed = Date.now();
                await this.userRepository.persist(oUser);
                return {
                    success: true,
                    user: oUser
                };
            }
        }
        // echec authentification
        return {
            success: false,
            user: null
        };
    }
}

module.exports = AuthenticateClient;
