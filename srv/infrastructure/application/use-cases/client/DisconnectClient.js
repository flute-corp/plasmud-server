class DisconnectClient {
    constructor ({ ClientRepository, CLIENT_STATES, ChatInteractor, GameInteractor }) {
        this.clientRepository = ClientRepository;
        this.chat = ChatInteractor;
        this.game = GameInteractor;
        this.CLIENT_STATES = CLIENT_STATES;
    }

    /**
   * Le client s'est déconnecté : nettoyer les instance enregistrée
   */
    async execute (clientId) {
        const client = await this.clientRepository.get(clientId);
        if (client.uid) {
            this.game.unregisterPlayer(client.uid);
            this.chat.disconnectUser(client.uid);
        }
        client.state = this.CLIENT_STATES.CLIENT_STATE_DISCONNECTED;
        await this.clientRepository.remove(client);
    }
}

module.exports = DisconnectClient;
