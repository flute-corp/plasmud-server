class FindCharacter {
    constructor ({ CharacterRepository, UserRepository }) {
        this.characterRepository = CharacterRepository;
        this.userRepository = UserRepository;
    }

    getDateDisplayString (timestamp) {
        const oDate = new Date(timestamp);
        return oDate.toJSON();
    }

    async execute (sName, bFull = false) {
        const oChar = await this.characterRepository.findByName(sName);
        if (oChar) {
            if (bFull) {
                const oUser = await this.userRepository.get(oChar.owner);
                // Reformater pour affichage
                return {
                    id: oChar.id,
                    name: oChar.name,
                    owner: oUser.name,
                    dateCreation: this.getDateDisplayString(oChar.dateCreation),
                    dateLastUsed: this.getDateDisplayString(oChar.dateLastUsed),
                    selected: oChar.selected
                };
            } else {
                return oChar;
            }
        } else {
            return null;
        }
    }
}

module.exports = FindCharacter;
