class RestoreCharacterState {
    constructor ({ PlayerCharacterSaveRepository }) {
        this.playerCharacterSaveRepository = PlayerCharacterSaveRepository;
    }

    typeState (payload, sType) {
        return {
            _state: true,
            type: sType,
            ...payload
        };
    }

    async execute (idCharacter) {
        try {
            return await this
                .playerCharacterSaveRepository
                .get('#' + idCharacter)
                .then(c => this.typeState(c, 'player'));
        } catch (e) {
            return null;
        }
    }
}

module.exports = RestoreCharacterState;
