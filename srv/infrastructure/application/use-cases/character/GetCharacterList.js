class GetCharacterList {
    constructor ({ ClientRepository, CharacterRepository }) {
        this.clientRepository = ClientRepository;
        this.characterRepository = CharacterRepository;
    }

    async execute (uid) {
        return this.characterRepository.findByOwner(uid);
    }
}

module.exports = GetCharacterList;
