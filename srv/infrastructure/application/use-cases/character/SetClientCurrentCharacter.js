class SetClientCurrentCharacter {
    constructor ({ ClientRepository, CharacterRepository }) {
        this.clientRepository = ClientRepository;
        this.characterRepository = CharacterRepository;
    }

    /**
   * Définit le personnage courant du user.
   * Le personnage courant est celui que l'utilisateur va utiliser quand il rejoindra le jeu.
   * @param uid {number}
   * @param name {string}
   * @returns {Promise<{character}>}
   */
    async execute (uid, name) {
        name = name.toLowerCase();
        const oChar = await this.characterRepository.findByName(name);
        // verifier que le personnage appartienne bien au client
        if (oChar && oChar.owner === uid) {
            // Personnage éligible
            await this.characterRepository.setOwnerCharacterAsCurrent(oChar.id);
            return {
                character: oChar
            };
        } else {
            throw new Error('ERR_CHARACTER_NOT_FOUND');
        }
    }
}

module.exports = SetClientCurrentCharacter;
