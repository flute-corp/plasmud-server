const Character = require('../../../entities/Character');

class CreateCharacter {
    constructor ({ ClientRepository, CharacterRepository, StringUtilsInteractor }) {
        this.clientRepository = ClientRepository;
        this.characterRepository = CharacterRepository;
        this.stringUtils = StringUtilsInteractor;
    }

    /**
   * Efface un personnage d'un joueur
   * @param uid {number}
   * @param name {string}
   * @returns {Promise<{character}>}
   */
    async execute (uid, name) {
        const oChar = await this.characterRepository.findByName(name);
        if (oChar) {
            throw new Error('ERR_CHARACTER_NAME_ALREADY_USED');
        }
        if (!this.stringUtils.isSafeName(name)) {
            throw new Error('ERR_CHARACTER_NAME_INVALID');
        }
        const d = Date.now();
        const c = new Character({
            name,
            owner: uid,
            dateCreation: d
        });
        this.characterRepository.persist(c);
        return {
            character: c
        };
    }
}

module.exports = CreateCharacter;
