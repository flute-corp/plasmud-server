const PlayerCharacterSave = require('../../../entities/PlayerCharacterSave');
const ItemSave = require('../../../entities/ItemSave');

class DeleteCharacter {
    constructor ({ CharacterRepository, DeleteCharacterState }) {
        this.characterRepository = CharacterRepository;
        this.deleteCharacterState = DeleteCharacterState;
    }

    /**
   * Efface un personnage d'un joueur
   * @param cid {number} identifiant du personnage à supprimer
   * @returns {Promise<{character}>}
   */
    async execute (cid) {
        const oChar = await this.characterRepository.get(cid);
        if (oChar) {
            const {
                success: bCharacterStateDeleteSuccess,
                reason: sCharacterStateDeleteReason = ''
            } = await this.deleteCharacterState.execute(cid);
            // TODO Peut etre faudrait il archiver le personnage
            await this.characterRepository.remove(oChar);
            return {
                success: true,
                state: {
                    success: bCharacterStateDeleteSuccess,
                    reason: sCharacterStateDeleteReason
                }
            };
        } else {
            return {
                success: false,
                reason: 'Character not found'
            };
        }
    }
}

module.exports = DeleteCharacter;
