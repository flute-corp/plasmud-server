const PlayerCharacterSave = require('../../../entities/PlayerCharacterSave');

class PersistCharacterState {
    constructor ({ PlayerCharacterSaveRepository }) {
        this.playerCharacterSaveRepository = PlayerCharacterSaveRepository;
    }

    async execute (oCharState) {
        // Interroger le game service pour obtenir un export du personnage et tous ses objets.
        // Sauvegarder tous les items
        // Sauvegarder le personnage
        const oNewState = new PlayerCharacterSave(oCharState);
        return this.playerCharacterSaveRepository.persist(oNewState);
    }
}

module.exports = PersistCharacterState;
