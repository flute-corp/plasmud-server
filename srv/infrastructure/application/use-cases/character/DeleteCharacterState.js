const ItemSave = require('../../../entities/ItemSave');
const PlayerCharacterSave = require('../../../entities/PlayerCharacterSave');

class DeleteCharacterState {
    constructor ({ PlayerCharacterSaveRepository }) {
        this.playerCharacterSaveRepository = PlayerCharacterSaveRepository;
    }



    async execute (cid) {
        try {
            await this.playerCharacterSaveRepository.remove({ id: cid });
            return {
                success: true
            };
        } catch (e) {
            return {
                success: false,
                reason: e.message
            };
        }
    }
}

module.exports = DeleteCharacterState;
