/**
 * @classdesc Get a list of all admin switches and game information
 */
class AdminGetInformation {
    constructor ({ AdminSwitchRepository, GameInteractor }) {
        this.adminSwitchRepository = AdminSwitchRepository;
        this.gameInteractor = GameInteractor;
    }

    async execute () {
        const aSwitches = await this.adminSwitchRepository.getSwitchList();
        const oSwitches = {};
        aSwitches.forEach(({ id, value }) => {
            oSwitches[id] = value;
        });
        const oInformation = this.gameInteractor.getInformation();
        return {
            switches: oSwitches,
            game: oInformation
        };
    }
}

module.exports = AdminGetInformation;
