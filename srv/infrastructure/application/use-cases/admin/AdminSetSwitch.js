class AdminSetSwitch {
    constructor ({ AdminSwitchRepository, ADMIN_SWITCHES }) {
        this.adminSwitchRepository = AdminSwitchRepository;
        this.ADMIN_SWITCHES = ADMIN_SWITCHES;
    }

    async execute (sSwitch, value) {
        const asw = this.adminSwitchRepository;
        const ASW = this.ADMIN_SWITCHES;
        if (Object.values(ASW).includes(sSwitch)) {
            await asw.setSwitch(sSwitch, value);
            return {
                [sSwitch]: value
            };
        } else {
            throw new Error('invalid admin switch : "' + sSwitch + '" - Allowed switches are : ' + Object.values(ASW).join(', ') + '.');
        }
    }
}

module.exports = AdminSetSwitch;
