/**
 * @classdesc Used to broadcast a message among all connected clients
 * These are maintenance important messages
 */
class AdminBroadcastMessage {
    constructor ({ ClientRepository, LogInteractor }) {
    /**
     * @type {ClientRepository}
     */
        this.clientRepository = ClientRepository;
        /**
     * @type {LogInteractor}
     */
        this.logInteractor = LogInteractor;
    }

    /**
   * @param message {string} message to be sent to all connected users
   * @returns {Promise<Awaited<unknown>[]>}
   */
    async execute (message) {
        await this.logInteractor.log('broadcasting admin message : %s', message);
        const aClients = await this.clientRepository.getAll();
        const aPromBC = aClients.map(
            /**
       * @param c {Client}
       */
            c => c.socket.send('📣 ' + message)
        );
        return Promise.all(aPromBC);
    }
}

module.exports = AdminBroadcastMessage;
