const User = require('../../../entities/User');

class CreateUser {
    constructor ({ UserRepository, FindUserClient, USER_ROLES, StringUtilsInteractor }) {
        this._userRepository = UserRepository;
        this._findUserClients = FindUserClient;
        this.USER_ROLES = USER_ROLES;
        this.stringUtils = StringUtilsInteractor;
    }

    async execute (sName) {
        const oUser = await this._userRepository.findByName(sName);
        if (oUser) {
            // on ne peut pas effacer un utilisateur inexistant
            throw new Error('ERR_USER_NOT_FOUND');
        }
        const aClients = await this._findUserClients.execute(oUser.id);
        if (aClients.length) {
            throw new Error('ERR_USER_LOGGED_IN');
        }
        await this._userRepository.remove(oUser);
        return {
            user: oUser
        };
    }
}

module.exports = CreateUser;
