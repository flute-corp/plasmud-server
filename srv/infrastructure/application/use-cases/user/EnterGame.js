class EnterGame {
    constructor ({
        GameInteractor,
        ClientRepository,
        CharacterRepository,
        PlayerCharacterSaveRepository,
        RestoreCharacterState,
        CLIENT_STATES
    }) {
        this.game = GameInteractor;
        this.clientRepository = ClientRepository;
        this.characterRepository = CharacterRepository;
        this.restoreCharacterState = RestoreCharacterState;
        this.CLIENT_STATES = CLIENT_STATES;
    }

    async execute (uid) {
    // trouver le personnage actuellement selectionné
        const oClient = (await this.clientRepository.findByUser(uid)).shift();
        // client introuvable dans le repository (bizarre)
        if (!oClient) {
            throw new Error('ERR_CLIENT_NOT_AUTHENTICATED');
        }
        // Déja en jeu, cette commande ne sert donc pas ici
        if (oClient.state === this.CLIENT_STATES.CLIENT_STATE_IN_GAME) {
            throw new Error('ERR_CLIENT_ALREADY_IN_GAME');
        }
        // client non encore authentifié
        if (oClient.state !== this.CLIENT_STATES.CLIENT_STATE_AUTHENTICATED) {
            throw new Error('ERR_CLIENT_NOT_AUTHENTICATED');
        }
        const oCharacter = await this.characterRepository.getOwnerCurrentCharacter(uid);
        // client sans personnage défini
        if (!oCharacter) {
            throw new Error('ERR_NO_SELECTED_CHARACTER');
        }
        // client avec un personnage ne lui appartenant pas (bizarre)
        if (oCharacter.owner !== uid) {
            throw new Error('ERR_ILLEGAL_CURRENT_CHARACTER');
        }
        oClient.state = this.CLIENT_STATES.CLIENT_STATE_IN_GAME;
        const oState = await this.restoreCharacterState.execute(oCharacter.id);
        const oPlayer = this.game.registerPlayer(uid, oClient, oCharacter, oState);
        oClient.character = oCharacter.id;
        oClient.dateConnection = Date.now();
        this.clientRepository.persist(oClient);
        return {
            success: true,
            player: oPlayer
        };
    }
}

module.exports = EnterGame;
