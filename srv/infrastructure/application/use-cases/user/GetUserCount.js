class GetUserCount {
    constructor ({ UserRepository }) {
        this.userRepository = UserRepository;
    }

    async execute () {
        const nCount = await this.userRepository.getCount();
        return {
            count: nCount
        };
    }
}

module.exports = GetUserCount;
