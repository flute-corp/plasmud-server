class FindUser {
    constructor ({ UserRepository, GetUserBanishment, GetCharacterList }) {
        this.userRepository = UserRepository;
        this.getUserBanishment = GetUserBanishment;
        this.getCharacterList = GetCharacterList;
    }

    getDateDisplayString (timestamp) {
        const oDate = new Date(timestamp);
        return oDate.toJSON();
    }

    async execute (sName, bFull = false) {
        const oUser = await this.userRepository.findByName(sName);
        if (oUser) {
            if (bFull) {
                // Les personnages
                const charList = await this.getCharacterList.execute(oUser.id);
                const aCharDisp = charList.map(c => c.name);
                // Reformater pour affichage
                const oUserDisp = {
                    id: oUser.id,
                    name: oUser.name,
                    email: oUser.email,
                    dateCreation: this.getDateDisplayString(oUser.dateCreation),
                    dateLastUsed: this.getDateDisplayString(oUser.dateLastUsed),
                    roles: oUser.roles,
                    characters: aCharDisp
                };
                const oBanishment = await this.getUserBanishment.execute(oUser.id);
                if (oBanishment.banned) {
                    oUserDisp.banishment = {
                        banner: oBanishment.banner,
                        when: this.getDateDisplayString(oBanishment.when),
                        until: oBanishment.forever ? 'forever' : this.getDateDisplayString(oBanishment.until),
                        reason: oBanishment.reason
                    };
                }
                return oUserDisp;
            } else {
                return oUser;
            }
        } else {
            throw new Error('ERR_USER_NOT_FOUND: ' + sName);
        }
    }
}

module.exports = FindUser;
