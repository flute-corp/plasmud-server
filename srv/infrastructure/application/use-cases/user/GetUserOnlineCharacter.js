class GetUserOnlineCharacter {
    constructor ({ GetCharacterList, FindUserClient }) {
        this.getCharacterList = GetCharacterList;
        this.findUserClient = FindUserClient;
    }

    /**
     * Récupérer l'instance Client à partir de son identifiant
     */
    async execute (uid) {
        const aCharacterList = await this.getCharacterList.execute(uid);
        const client = await this.findUserClient.execute(uid);
        const idOnlineCharacter = client.character;
        return aCharacterList.find(c => c.id === idOnlineCharacter);
    }
}

module.exports = GetUserOnlineCharacter;
