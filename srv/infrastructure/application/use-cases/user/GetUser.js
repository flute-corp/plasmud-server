class GetUser {
    constructor ({ UserRepository }) {
        this.userRepository = UserRepository;
    }

    async execute (uid) {
        const oUser = await this.userRepository.get(uid);
        if (oUser) {
            return oUser;
        } else {
            throw new Error('ERR_USER_NOT_FOUND');
        }
    }
}

module.exports = GetUser;
