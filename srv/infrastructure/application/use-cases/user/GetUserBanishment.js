class GetUserBanishment {
    constructor ({ UserRepository, BanishmentRepository, DateInteractor }) {
        this._userRepository = UserRepository;
        this._banishmentRepository = BanishmentRepository;
        this._dateInteractor = DateInteractor;
    }

    async execute (idUser) {
        const dWhen = Date.now();
        const oUser = await this._userRepository.get(idUser);
        const idBanishment = oUser.banishment;
        if (idBanishment) {
            const oBanishment = await this._banishmentRepository.get(idBanishment);
            if ((oBanishment.dateBegin <= dWhen && oBanishment.dateEnd > dWhen) || oBanishment.forever) {
                /**
         * @type {User}
         */
                const promBanner = oBanishment.banner
                    ? this._userRepository.get(oBanishment.banner)
                    : Promise.resolve({ name: '' });
                const oBanner = await promBanner;
                return {
                    banned: true,
                    banner: oBanner.name,
                    when: oBanishment.dateBegin,
                    until: oBanishment.dateEnd,
                    forever: oBanishment.forever,
                    reason: oBanishment.reason,
                    untilStr: this._dateInteractor.getDateTimeString(new Date(oBanishment.dateEnd)),
                    whenStr: this._dateInteractor.getDateTimeString(new Date(oBanishment.dateBegin)),
                    durationStr: oBanishment.forever ? 'forever' : this._dateInteractor.getDurationString(oBanishment.dateEnd - oBanishment.dateBegin)
                };
            } else {
                return {
                    banned: false
                };
            }
        }
        return {
            banned: false
        };
    }
}

module.exports = GetUserBanishment;
