class GetUserList {
    constructor ({ UserRepository, DateInteractor, ClientRepository }) {
        this.userRepository = UserRepository;
        this.dateInteractor = DateInteractor;
        this.clientRepository = ClientRepository;
    }

    async execute (sFilter = '', nMaxCount = 100) {
        const oUserCursor = sFilter === ''
            ? await this.userRepository.getAll()
            : await this.userRepository.findByNameLike(sFilter);
        const aUsers = await oUserCursor.toArray(0, nMaxCount);
        const aPromShownUsers = aUsers
            .map(u => this
                .clientRepository
                .findByUser(u.id)
                .then(c => {
                    const oUserData = {
                        id: u.id,
                        name: u.name,
                        date: this.dateInteractor.getDateTimeString(u.dateLastUsed),
                        roles: u.roles,
                        type: '',
                        state: '',
                        banned: !!u.banishment
                    };
                    if (c) {
                        oUserData.state = c
                            .map(c1 => c1.state)
                            .join(' ');
                        oUserData.type = c
                            .map(c1 => c1.type)
                            .join(' ');
                    }
                    return oUserData;
                })
            );
        const aShownUsers = await Promise.all(aPromShownUsers);
        const nShownCount = aShownUsers.length;
        const nUnlistedCount = aUsers.length - aShownUsers.length;
        return {
            filter: sFilter,
            count: nShownCount,
            unlisted: nUnlistedCount,
            users: aShownUsers
        };
    }
}

module.exports = GetUserList;
