class RemoveUserRole {
    constructor ({ UserRepository, USER_ROLES }) {
        this._userRepository = UserRepository;
        this.USER_ROLES = Object.values(USER_ROLES);
    }

    async execute (idUser, sRole) {
        if (!this.USER_ROLES.includes(sRole)) {
            throw new Error('ERR_INVALID_ROLE');
        }
        const oUser = await this._userRepository.get(idUser);
        const oRoleSet = new Set(oUser.roles);
        oRoleSet.delete(sRole);
        oUser.roles = [...oRoleSet];
        await this._userRepository.persist(oUser);
        return {
            success: true,
            roles: oUser.roles
        };
    }
}

module.exports = RemoveUserRole;
