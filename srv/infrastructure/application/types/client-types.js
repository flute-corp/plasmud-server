module.exports = {
    CLIENT_TYPE_WEBSOCKET: 'ws',
    CLIENT_TYPE_TELNET: 'telnet',
    CLIENT_TYPE_SSH: 'ssh'
};
