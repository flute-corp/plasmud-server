/**
 * Etats possibles pour un client
 * @typedef ClientStates {object}
 * @property CLIENT_STATE_AUTHENTICATED {string} le client est authentifié
 * @property CLIENT_STATE_IN_GAME {string} le client est entré dans le jeu
 * @property CLIENT_STATE_DISCONNECTED {string} le client est déconnecté
 * @property CLIENT_STATE_CONNECTED {string} le client est connecté mais pas encore authentifié
 */
module.exports = {
    CLIENT_STATE_CONNECTED: 'CLIENT_STATE_CONNECTED', // le client est juste connecté
    CLIENT_STATE_AUTHENTICATED: 'CLIENT_STATE_AUTHENTICATED', // le client a entré un mot de passe valide
    CLIENT_STATE_IN_GAME: 'CLIENT_STATE_IN_GAME', // le client est entré dans le jeu, certaines fonctions ne sont plus disponibles
    CLIENT_STATE_DISCONNECTED: 'CLIENT_STATE_DISCONNECTED' // le client n'est plus connecté
};
