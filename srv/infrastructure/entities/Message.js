/**
 * @class Message
 * @property opcode {string}
 * @property valid {boolean}
 * @property numerci {number}
 * @property arguments {string[]}
 * @property unparsed {string}
 */
class Message {
    constructor (payload) {
        this.opcode = payload.opcode;
        this.valid = payload.valid;
        this.numeric = payload.numeric;
        this.arguments = payload.arguments;
        this.unparsed = payload.unparsed;
    }
}

module.exports = Message;
