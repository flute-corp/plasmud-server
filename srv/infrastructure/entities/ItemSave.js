/**
 * @class ItemSave
 */
class ItemSave {
    constructor (payload) {
        this.id = payload.id;
        this.ref = payload.ref;
        this.location = payload.location;
        this.name = payload.name;
        this.remark = payload.remark;
        this.stack = payload.stack;
        this.locked = payload.locked;
        this.data = payload.data;
        this.inventory = payload.inventory;
    }
}

module.exports = ItemSave;
