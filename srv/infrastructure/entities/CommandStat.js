class CommandStat {
    constructor (payload) {
        this.id = payload.id;
        this.command = payload.command;
        this.count = payload.count;
        this.time = payload.time;
    }
}

module.exports = CommandStat;
