/**
 * @class Client
 * @property id {number} identifiant
 * @property state {string} état de la connection
 * @property type {string} ws http ssh telnet
 * @property socket {VSocket} ou autre selon methode de connexion du client
 * @property uid {string} identifiant de l'utilisateur
 * @property uname {string} nom de l'utilisateur
 */
class Client {
    constructor (payload) {
        this.id = payload.id;
        this.state = payload.state;
        this.type = payload.type;
        this.socket = payload.socket;
        this.uid = payload.uid;
        this.uname = payload.uname;
        this.character = payload.cid;
        this.dateConnection = payload.dateConnection;
    }
}

module.exports = Client;
