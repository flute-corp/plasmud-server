class AdminSwitch {
    constructor (payload) {
        this.id = payload.id;
        this.value = payload.value;
        this.dateChanged = payload.dateChanged || Date.now();
    }
}

module.exports = AdminSwitch;
