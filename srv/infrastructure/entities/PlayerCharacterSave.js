/**
 * @class PlayerCharacterSave
 *
 * Cette classe est la sauvegarde de l'état d'un personnage joueur.
 */
class PlayerCharacterSave {
    constructor (payload) {
        this.id = payload.id;
        this.uid = payload.uid;
        this.name = payload.name;
        this.remark = payload.remark;
        this.location = payload.location;
        this.creation = payload.creation;
        this.inventory = payload.inventory;
        this.data = payload.data;
        this.items = payload.items;
    }
}

module.exports = PlayerCharacterSave;
