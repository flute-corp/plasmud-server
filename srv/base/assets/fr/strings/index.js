const oStrings = require('./strings.json');
const oCommands = require('./commands.json');
module.exports = {
    ...oStrings,
    ...oCommands
};
