/**
 * Description : Quitter le jeu et revenir à un mode hors jeu (sorte de menu principal)
 * Cas d'utilisation : Quitter le jeu en sauvegardant son personnage
 * Syntaxe : leave
 */

async function main (context) {
    const { uc, client } = context;
    if (client) {
        uc.ExitGame.execute(client.id);
    }
}

module.exports = main;
