/**
 * La commande d'aide permet d'affiche un fichier d'aide
 * exemple : help char create -> va lire templates/help/char/create
 */
async function main (context, aArguments) {
    if (aArguments.length > 0) {
        const sWhat = aArguments[0];
        switch (sWhat) {
        case 'commands': {
            const aCommands = await context.uc.GetCommandList.execute();
            aCommands.sort((a, b) => a.name.localeCompare(b.name));
            context.print('tech/command-list', { commands: aCommands });
            break;
        }

        case 'guide': {
            await context.uc.StartDialog.execute(context, 'dlg-guide');
            break;
        }

        default: {
            const sHelpFile = [
                'help',
                ...aArguments
            ].join('/');
            context.print(sHelpFile);
            break;
        }
        }
    } else {
        await context.uc.StartDialog.execute(context, 'dlg-guide');
    }
}

module.exports = main;
