/**
 * Description : Gestion des utilisateurs
 */

/**
 * Commande de gestion des utilisateurs
 * @param context {SRVClientContext}
 * @param aArguments {string}
 * @returns {Promise<*>}
 */
async function main (context, aArguments) {
    const { command, print } = context;
    // le premier argument est le nom de la sous-commande
    // ici : "user xxx" est équivalent à la commande "user-xxx"
    const sSubOpcode = aArguments.shift();
    if (!sSubOpcode) {
        print('help/user');
        return;
    }
    // composer le nom complet du script
    const sCommand = 'user/' + sSubOpcode + '';
    if (command.exists(sCommand)) {
    // la commande existe, on peut la lancer
        return command(sCommand, aArguments);
    } else {
    // la commande n'existe pas
        print('generic.error.unknownCommand', { command: sSubOpcode });
        print('user.info.subCommandList');
    }
}

module.exports = main;
