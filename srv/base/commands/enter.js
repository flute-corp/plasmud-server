async function main ({ uc, client, print }) {
    try {
    // s'enregistrer en tant que joueur dans le plasmud
        await uc.EnterGame.execute(client.uid);
        print.log('@%s has enter plasmud', client.uname);
    } catch (e) {
        switch (e.message) {
        case 'ERR_NO_SELECTED_CHARACTER':
            print('enter.error.noChar');
            break;

        case 'ERR_ILLEGAL_CHARACTER':
            print('enter.error.illegalChar');
            break;

        default:
            console.error(e);
            print('generic.error.command', { error: e.message });
            break;
        }
    }
}

module.exports = main;
