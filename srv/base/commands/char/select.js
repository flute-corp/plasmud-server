/**
 * Description : Selection du personnage courant
 * Cas d'utilisation : Les client utilisent cette commande pour selectionner le personnage avec lequel ils vont jouer
 * Syntaxe : char select <nom-du-personnage>
 */

const { suggest } = require('@laboralphy/did-you-mean'); // 2.2.1

async function getCharacterNameSuggest (uc, client, name) {
    const r = await uc.GetCharacterList.execute(client.uid);
    return suggest(name, r.map(c => c.name), { limit: 0, threshold: 0.25 });
}

async function main (context, [name]) {
    const { client, uc, print } = context;
    try {
        if (name === undefined) {
            print('char.error.noParam');
            return;
        }
        const r = await uc.SetClientCurrentCharacter.execute(client.uid, name);
        print('char.info.selected', { name: r.character.name });
    } catch (e) {
        if (e.message === 'ERR_CHARACTER_NOT_FOUND') {
            print('char.error.notFound', { name, suggest: await getCharacterNameSuggest(uc, client, name) });
        } else {
            console.error(e);
            print('generic.error.command', { error: e.message });
        }
    }
}

module.exports = main;
