/**
 * Description : Création d'un nouveau personnage
 * Cas d'utilisation : Lors qu'un utilisateur souhaite créer un nouveau personage
 * Syntaxe :
 */

/**
 * Fonction principale de la commande
 * @param context {MUDContext}
 * @param name {string}
 * @returns {Promise<void>}
 */
async function main ({ check, uc, client, print }, [name]) {
    // arguments : name
    if (!check('S', name)) {
        return;
    }
    try {
        await uc.CreateCharacter.execute(client.uid, name);
        print('char.info.created', { name });
    } catch (e) {
        switch (e.message) {
        case 'ERR_CHARACTER_NAME_ALREADY_USED': {
            print('char.error.nameUsed');
            break;
        }

        case 'ERR_CHARACTER_NAME_INVALID': {
            print('char.error.nameInvalid');
            break;
        }

        default: {
            console.error(e);
            print('generic.error.command', { error: e.message });
            break;
        }
        }
    }
}

module.exports = main;
