/**
 * Description : Affiche la liste des personnages d'un client
 * Syntaxe : char list
 */

/**
 * @param context {MUDContext}
 */
async function main (context) {
    const { uc, client, print } = context;
    try {
        const r = await uc.GetCharacterList.execute(client.uid);
        const characters = r.map(c => ({
            name: c.name,
            current: c.selected,
            dates: {
                creation: c.dateCreation,
                lastUsed: c.dateLastUsed
            }
        }));
        if (characters.length > 0) {
            print('char/list', { characters });
        } else {
            print('char.warn.noCharacter', { characters });
        }
    } catch (e) {
        console.error(e);
        print('generic.error.command', { error: e.message });
    }
}

module.exports = main;
