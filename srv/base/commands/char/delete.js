/**
 * Description : Suppression de l'un de ses propres personnages
 * Cas d'utilisation : Quand un client veut effacer l'un de ces personnages, il fait appel à cette commande
 * Syntaxe : char delete <nom-du-personnage>
 */

const { suggest } = require('@laboralphy/did-you-mean'); // 2.2.1

/**
 * Dans le cas où l'on orthographie mal un nom de personnage, suggérer un nom valide proche
 * @param uc {object} les use cases
 * @param client {Client}
 * @param name {string} nom du personnage mal orthographié
 * @returns {Promise<string[]>} tableau de noms proches
 */
async function getCharacterNameSuggest (uc, client, name) {
    const r = await uc.GetCharacterList.execute(client.uid);
    return suggest(name, r.map(c => c.name), { limit: 0, threshold: 0.25 });
}

/**
 * Commande de suppression de personnage.
 * @param context {MUDContext}
 * @param name {string} nom du personnage
 * @returns {Promise<void>}
 */
async function main (context, [name]) {
    const { client, uc, print, check } = context;
    try {
        if (name === undefined) {
            print('char.error.noParam');
            return;
        }
        const c = await uc.FindCharacter.execute(name);
        if (!c) {
            print('char.error.notFound', { name, suggest: await getCharacterNameSuggest(uc, client, name) });
            return;
        }
        if (c.owner === client.uid) {
            await uc.DeleteCharacter.execute(c.id);
            print('char.info.deleted', { name: c.name });
        } else {
            print('char.error.notFound', { name, suggest: await getCharacterNameSuggest(uc, client, name) });
        }
    } catch (e) {
        if (e.message === 'ERR_CHARACTER_NOT_FOUND') {
            print('char.error.notFound', { name, suggest: await getCharacterNameSuggest(uc, client, name) });
        } else {
            console.error(e);
            print('generic.error.command', { error: e.message });
        }
    }
}

module.exports = main;
