/**
 * Description : Gestion par les clients de leurs propres personnages. La commande fait appel a des sous-commandes
 * Cas d'utilisation : Quand un client gérer ses personnage : listing, suppression, création etc...
 */

/**
 * Commande de gestion des personnages-joueurs
 * @param context {MUDContext}
 * @param aArguments {string}
 * @returns {Promise<*>}
 */
async function main (context, aArguments) {
    const { command, print } = context;
    // le premier argument est le nom de la sous-commande
    // ici : "char xxx" est équivalent à la commande "char-xxx"
    const sSubOpcode = aArguments.shift();
    if (!sSubOpcode) {
        print('help/char');
        return;
    }
    // composer le nom complet du script
    const sCommand = 'char/' + sSubOpcode;
    if (command.exists(sCommand)) {
    // la commande existe, on peut la lancer
        return command(sCommand, aArguments);
    } else {
    // la commande n'existe pas
        print('generic.error.unknownCommand', { command: sSubOpcode });
        print('char.info.subCommandList');
    }
}

module.exports = main;
