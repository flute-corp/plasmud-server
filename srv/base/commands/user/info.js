/**
 * Description : Affichage des information d'un utilisateur.
 * Syntaxe : user-info <nom-d-utilisateur>
 */

async function main (context, [name]) {
    const { uc, print, check } = context;
    if (!check('S', name)) {
        return;
    }
    let user;
    try {
        user = await uc.FindUser.execute(name);
    } catch (e) {
        print('user.error.notFound', { name });
        return;
    }
    try {
        const client = await uc.FindUserClient.execute(user.id); // ok 1 client
        const connected = !!client;
        const banishment = await uc.GetUserBanishment.execute(user.id);
        const aCharacterList = await uc.GetCharacterList.execute(user.id);
        const oOnlineCharacter = await uc.GetUserOnlineCharacter.execute(user.id);
        const p = {
            name,
            connected,
            email: user.email,
            dateCreation: user.dateCreation,
            dateLogin: user.dateLastUsed,
            charCount: aCharacterList.length,
            characters: aCharacterList.map(c => c.name),
            currentCharacter: oOnlineCharacter.name,
            banishment,
            roles: user.roles
        };
        print('user/info', p);
    } catch (e) {
    }
}

module.exports = main;
