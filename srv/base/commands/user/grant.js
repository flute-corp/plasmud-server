/**
 * Description : Ajout d'un role à un utilisateur
 * Syntaxe : user-grant <user> <role>
 */
const { roleNameToConst, roleConstToName } = require('../../../infrastructure/frameworks/common/role-strings');

async function main (context, [name, role]) {
    const { check, uc, print } = context;
    try {
        if (!check('SS', name, role)) {
            return;
        }
        role = roleNameToConst(role);
        const user = await uc.FindUser.execute(name);
        const { roles } = await uc.AddUserRole.execute(user.id, role);
        print('user.info.roles', { name, roles: roles.map(roleConstToName).join(', ') });
    } catch (e) {
        switch (e.message) {
        case 'ERR_USER_NOT_FOUND':
            print('user.error.notFound', { name });
            break;

        case 'ERR_INVALID_ROLE':
            print('user.error.badRole');
            break;

        default:
            console.error(e);
            print('generic.error.command', { error: e.message });
            break;
        }
    }
}

module.exports = main;
