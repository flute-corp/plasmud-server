/**
 * Description : Affichage des utilisateurs connectés avec leurs personnages.
 * Syntaxe : user/list
 */

async function main (context) {
    const { client, uc, print, check } = context;
    try {
        const aOutput = [];
        // Liste des clients
        const aClientList = await uc.GetClientList.execute();
        for (const client of aClientList) {
            const character = await uc.GetUserOnlineCharacter.execute(client.uid);
            const oUserLine = {
                name: client.uname,
                character: character.name,
                dateConnection: client.dateConnection
            };
            aOutput.push(oUserLine);
        }
        print('user/list', { users: aOutput });
    } catch (e) {
        console.error(e);
    }
}

module.exports = main;
