/**
 * Description : permet à un administrateur de changer le mot de passe d'un utilisateur
 */
const crypto = require('crypto');

async function main ({ check, print, uc, client }, [name, password]) {
    try {
        if (!check('SS', name, password)) {
            return;
        }
        password = crypto
            .createHash('sha256')
            .update(password)
            .digest('hex');

        const user = await uc.FindUser.execute(name);
        await uc.ChangeUserPassword.execute(user.id, password);
        print('passwd.info.success');
    } catch (e) {
        console.error(e);
        print('generic.error.command', { error: e.message });
    }
}

module.exports = main;
