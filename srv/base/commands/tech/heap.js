/**
 * Description : de la mémoire utilisée
 */

async function main (context) {
    const h = await context.uc.GetMemoryUsage.execute();
    const heap = {
        maximum: Math.ceil(h.maximum / 1024),
        available: Math.ceil(h.available / 1024),
        allocated: Math.ceil(h.allocated / 1024),
        free: h.free.toFixed(2),
        program: Math.ceil(h.program / 1024)
    };
    context.print('tech/memory-usage', { heap });
}

module.exports = main;
