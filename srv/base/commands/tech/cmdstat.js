async function main ({ print, uc }) {
    const aStats = await uc.GetCommandStats.execute();
    aStats.sort((a, b) => b.count - a.count);
    print('tech/cmdstat', { stats: aStats });
}

module.exports = main;
