/**
 * Description : Affichage de l'heure.
 * Cas d'utilisation : utilisée chaque fois qu'on vue t savoir l'heure
 * Syntaxe : time
 */

function main (context) {
    const d = new Date();
    context.print('tech/time', {
        date: d.toLocaleDateString(),
        time: d.toLocaleTimeString(),
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone
    });
}

module.exports = main;
