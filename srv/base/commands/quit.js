/**
 * Description : Déconnexion du client
 * Cas d'utilisation : Permet de se déconnecter
 * Syntaxe : logout
 */

async function main (context) {
    const { uc, client } = context;
    if (client) {
        client.socket.close();
    }
}

module.exports = main;
