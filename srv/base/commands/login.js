/**
 * Description : Authentifiacation du client
 * Cas d'utilisation : Utilisée automatiquement lors de la saisie login/mot de passe
 * Syntaxe : login <nom-d'utilisateur> <mot-de-passe>
 */

const pjson = require('../../../package.json');
const { motd } = require('../../config/motd');

/**
 * @param context {SRVClientContext}
 * @param name
 * @param password
 * @returns {Promise<void>}
 */
async function main (context, [name, password]) {
    const { uc, client, print, check } = context;
    if (!check('SS', name, password)) {
        return;
    }
    const idClient = client.id;
    try {
        const oResult = await uc.AuthenticateClient.execute(idClient, name, password);
        if (oResult.success) {
            const oUser = oResult.user;
            // controllers.plasmud.registerPlayer(idClient)
            print.log('@%s is now authenticated', oUser.name);
            // TODO Enregistrer le user dans le txat
            const { user } = await uc.RegisterChatUser.execute(oUser.id, oUser.name, idClient);
            user.data.client = client;
            print('login.info.success', { name });
            print('menu/main', {
                version: pjson.version,
                description: pjson.description,
                motd
            });
            await uc.JoinChannel.execute(user.id, 'lobby');
        } else {
            print.log('@%s could not authenticate', name);
            if (oResult.banishment) {
                print('user-banned', {
                    forever: oResult.banishment.forever,
                    reason: oResult.banishment.reason,
                    until: oResult.banishment.until
                });
                client.socket.close();
            } else {
                print('login.error.failure');
            }
        }
    } catch (e) {
        switch (e.message) {
        case 'ERR_AUTH_FAILED':
            print('login.error.failure');
            break;

        case 'ERR_ALREADY_CONNECTED':
            print('login.error.already');
            break;

        default:
            console.error(e);
            print('generic.error.command', { error: e.message });
            break;
        }
        if (client) {
            client.socket.close();
        }
    }
}

module.exports = main;
