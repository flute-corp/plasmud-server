module.exports = async function main ({ uc, client, txat }, [cid, ...message]) {
    // front : say "text" -> say channel "text"
    await uc.SendPublicMessage.execute(client.uid, cid, message.join(' '));
};
