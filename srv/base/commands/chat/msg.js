module.exports = async function main ({ uc, client, txat }, [name, ...message]) {
    // front : msg user "text" -> msg user "text"
    // rechercher user par son nom
    const user = await uc.FindUser.execute(name);
    if (user) {
        await uc.SendPrivateMessage.execute(client.uid, user.id, message.join(' '));
    } else {
        throw new Error('ERR_TXAT_USER_NOT_FOUND: ' + name);
    }
};
