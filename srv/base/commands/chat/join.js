module.exports = async function main ({ uc, client, print }, [cid]) {
    // front : join channel -> join channel
    print.log('got user join channel ' + cid + ' request');
    await uc.JoinChannel.execute(client.uid, cid);
};
