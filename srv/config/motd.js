const fs = require('fs');
const path = require('path');
const rp = require('../../libs/resolve-path');
const { getEnv } = require('../env-parser');


function getMOTD () {
    const dataDir = rp(getEnv().PLASMUD_DIRECTORY_DATA);
    const sFilename = path.join(dataDir.path, 'motd.txt');
    try {
        return fs.readFileSync(sFilename).toString();
    } catch (e) {
        return '';
    }
}

module.exports = {
    motd: getMOTD()
};
