// THIS FILE IS AUTO GENERATED - DO NOT MODIFY
// Wed Nov 13 2024 18:43:04 GMT+0100 (heure normale d’Europe centrale)
/**
 * @typedef ApplicationCradle {object}
 * @property AdminBroadcastMessage
 * @property AdminGetInformation
 * @property AdminSetSwitch
 * @property CreateCharacter
 * @property DeleteCharacter
 * @property DeleteCharacterState
 * @property FindCharacter
 * @property GetCharacterList
 * @property PersistCharacterState
 * @property RestoreCharacterState
 * @property SetClientCurrentCharacter
 * @property JoinChannel
 * @property LeaveChannel
 * @property RegisterChatUser
 * @property SendPrivateMessage
 * @property SendPublicMessage
 * @property UnregisterChatUser
 * @property AuthenticateClient
 * @property ConnectClient
 * @property DisconnectClient
 * @property FindUserClient
 * @property GetClient
 * @property GetClientList
 * @property KickAllClients
 * @property AdvanceClock
 * @property DefineContextMenuOptions
 * @property ExecuteCommand
 * @property GetCommandList
 * @property GetCommandStats
 * @property GetMemoryUsage
 * @property InitApplication
 * @property OpenService
 * @property ParseMessage
 * @property ShutdownService
 * @property StartDialog
 * @property AddUserRole
 * @property ChangeUserPassword
 * @property CreateUser
 * @property DeleteUser
 * @property EnterGame
 * @property ExitGame
 * @property FindUser
 * @property GetUser
 * @property GetUserBanishment
 * @property GetUserCount
 * @property GetUserList
 * @property GetUserOnlineCharacter
 * @property RemoveUserBanishment
 * @property RemoveUserRole
 * @property SetUserBanishment
 * @property AclController
 * @property AdminController
 * @property ApiController
 * @property BatchController
 * @property TelnetController
 * @property WebSocketController
 * @property InMemoryAdminSwitchRepository
 * @property InMemoryClientRepository
 * @property InMemoryCommandStatRepository
 * @property InMemoryUserRepository
 * @property DatabaseBanishmentRepository
 * @property DatabaseCharacterRepository
 * @property DatabasePlayerCharacterSaveRepository
 * @property DatabaseUserRepository
 * @property CommandRunnerService
 * @property ContextBuilderService
 * @property DatabaseService
 * @property DateService
 * @property DebugService
 * @property DialogManager
 * @property HandlebarsService
 * @property MudService
 * @property StringUtilsService
 * @property StyleBuilderService
 * @property TextRendererService
 * @property TxatService
 * @property CLIENT_STATES
 * @property USER_ROLES
 * @property ADMIN_SWITCHES
 * @property ACL
 * @property PATH_BASE_MODULE
 * @property ClientRepository
 * @property CharacterRepository
 * @property UserRepository
 * @property BanishmentRepository
 * @property CommandStatRepository
 * @property AdminSwitchRepository
 * @property PlayerCharacterSaveRepository
 * @property CommandRunnerInteractor
 * @property ContextBuilderInteractor
 * @property DateInteractor
 * @property DatabaseInteractor
 * @property DialogInteractor
 * @property ChatInteractor
 * @property GameInteractor
 * @property LogInteractor
 * @property TextRendererInteractor
 * @property StringUtilsInteractor
 * @property StyleBuilderInteractor
 */
/**
 * @return {ApplicationCradle}
 */
function getCradleMembers ({
    AdminBroadcastMessage,
    AdminGetInformation,
    AdminSetSwitch,
    CreateCharacter,
    DeleteCharacter,
    DeleteCharacterState,
    FindCharacter,
    GetCharacterList,
    PersistCharacterState,
    RestoreCharacterState,
    SetClientCurrentCharacter,
    JoinChannel,
    LeaveChannel,
    RegisterChatUser,
    SendPrivateMessage,
    SendPublicMessage,
    UnregisterChatUser,
    AuthenticateClient,
    ConnectClient,
    DisconnectClient,
    FindUserClient,
    GetClient,
    GetClientList,
    KickAllClients,
    AdvanceClock,
    DefineContextMenuOptions,
    ExecuteCommand,
    GetCommandList,
    GetCommandStats,
    GetMemoryUsage,
    InitApplication,
    OpenService,
    ParseMessage,
    ShutdownService,
    StartDialog,
    AddUserRole,
    ChangeUserPassword,
    CreateUser,
    DeleteUser,
    EnterGame,
    ExitGame,
    FindUser,
    GetUser,
    GetUserBanishment,
    GetUserCount,
    GetUserList,
    GetUserOnlineCharacter,
    RemoveUserBanishment,
    RemoveUserRole,
    SetUserBanishment,
    AclController,
    AdminController,
    ApiController,
    BatchController,
    TelnetController,
    WebSocketController,
    InMemoryAdminSwitchRepository,
    InMemoryClientRepository,
    InMemoryCommandStatRepository,
    InMemoryUserRepository,
    DatabaseBanishmentRepository,
    DatabaseCharacterRepository,
    DatabasePlayerCharacterSaveRepository,
    DatabaseUserRepository,
    CommandRunnerService,
    ContextBuilderService,
    DatabaseService,
    DateService,
    DebugService,
    DialogManager,
    HandlebarsService,
    MudService,
    StringUtilsService,
    StyleBuilderService,
    TextRendererService,
    TxatService,
    CLIENT_STATES,
    USER_ROLES,
    ADMIN_SWITCHES,
    ACL,
    PATH_BASE_MODULE,
    ClientRepository,
    CharacterRepository,
    UserRepository,
    BanishmentRepository,
    CommandStatRepository,
    AdminSwitchRepository,
    PlayerCharacterSaveRepository,
    CommandRunnerInteractor,
    ContextBuilderInteractor,
    DateInteractor,
    DatabaseInteractor,
    DialogInteractor,
    ChatInteractor,
    GameInteractor,
    LogInteractor,
    TextRendererInteractor,
    StringUtilsInteractor,
    StyleBuilderInteractor
}) {
    return {
        AdminBroadcastMessage,
        AdminGetInformation,
        AdminSetSwitch,
        CreateCharacter,
        DeleteCharacter,
        DeleteCharacterState,
        FindCharacter,
        GetCharacterList,
        PersistCharacterState,
        RestoreCharacterState,
        SetClientCurrentCharacter,
        JoinChannel,
        LeaveChannel,
        RegisterChatUser,
        SendPrivateMessage,
        SendPublicMessage,
        UnregisterChatUser,
        AuthenticateClient,
        ConnectClient,
        DisconnectClient,
        FindUserClient,
        GetClient,
        GetClientList,
        KickAllClients,
        AdvanceClock,
        DefineContextMenuOptions,
        ExecuteCommand,
        GetCommandList,
        GetCommandStats,
        GetMemoryUsage,
        InitApplication,
        OpenService,
        ParseMessage,
        ShutdownService,
        StartDialog,
        AddUserRole,
        ChangeUserPassword,
        CreateUser,
        DeleteUser,
        EnterGame,
        ExitGame,
        FindUser,
        GetUser,
        GetUserBanishment,
        GetUserCount,
        GetUserList,
        GetUserOnlineCharacter,
        RemoveUserBanishment,
        RemoveUserRole,
        SetUserBanishment,
        AclController,
        AdminController,
        ApiController,
        BatchController,
        TelnetController,
        WebSocketController,
        InMemoryAdminSwitchRepository,
        InMemoryClientRepository,
        InMemoryCommandStatRepository,
        InMemoryUserRepository,
        DatabaseBanishmentRepository,
        DatabaseCharacterRepository,
        DatabasePlayerCharacterSaveRepository,
        DatabaseUserRepository,
        CommandRunnerService,
        ContextBuilderService,
        DatabaseService,
        DateService,
        DebugService,
        DialogManager,
        HandlebarsService,
        MudService,
        StringUtilsService,
        StyleBuilderService,
        TextRendererService,
        TxatService,
        CLIENT_STATES,
        USER_ROLES,
        ADMIN_SWITCHES,
        ACL,
        PATH_BASE_MODULE,
        ClientRepository,
        CharacterRepository,
        UserRepository,
        BanishmentRepository,
        CommandStatRepository,
        AdminSwitchRepository,
        PlayerCharacterSaveRepository,
        CommandRunnerInteractor,
        ContextBuilderInteractor,
        DateInteractor,
        DatabaseInteractor,
        DialogInteractor,
        ChatInteractor,
        GameInteractor,
        LogInteractor,
        TextRendererInteractor,
        StringUtilsInteractor,
        StyleBuilderInteractor
    };
}

module.exports = { getCradleMembers };
