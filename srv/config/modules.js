const rp = require('../../libs/resolve-path');
const { getEnv } = require('../env-parser');

/**
 * These are the locations where the server will scan for modules.
 * Any module may be located at any location, there is no special location for special type of module.
 * The ideal location for a module to be placed is in the PLASMUD_DATA_DIRECTORY folder
 *
 * location $CWD/libs/plasmud/modules : these are plasmud specific module library
 * location $CWD/modules : these are modules that can be helpers throughout many adventures
 * location PLASMUD_DATA_DIRECTORY : this is where adventures usually go
 */

module.exports = {
    modulesFolderList: [
        rp('./libs/plasmud/modules').path, // plasmud library built-in modules
        rp('./modules').path, // suggestion : server built-in module (combat, magic, ...)
        rp(getEnv().PLASMUD_DIRECTORY_DATA, 'modules').path // suggestion : modularized adventure, map and resources
    ]
};
