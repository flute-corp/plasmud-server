const awilix = require('awilix');
const CLIENT_STATES = require('../infrastructure/application/types/client-states');
const ADMIN_SWITCHES = require('../infrastructure/application/types/admin-switches');
const USER_ROLES = require('../infrastructure/frameworks/common/user-roles');
const ACL = require('./acl.json');
const implementations = require('./implementations.json');
const path = require('path');

function createContainer () {
    const container = awilix.createContainer();

    container.loadModules(
        [
            'srv/infrastructure/application/use-cases/**/*.js',
            'srv/infrastructure/controllers/*.js',
            'srv/infrastructure/frameworks/persistance/InMemory/*.js',
            'srv/infrastructure/frameworks/persistance/Database/*.js',
            'srv/infrastructure/frameworks/external-services/*.js'
        ],
        {
            resolverOptions: {
                register: awilix.asClass,
                lifetime: awilix.Lifetime.SINGLETON
            }
        }
    );
    // Définir les alias utilisés dans l'application
    // Les alias sont des pointeurs vers différentes implémentations
    const oAliases = {};
    for (const key in implementations) {
        oAliases[key] = awilix.aliasTo(implementations[key]);
    }
    container.register({
        CLIENT_STATES: awilix.asValue(CLIENT_STATES),
        USER_ROLES: awilix.asValue(USER_ROLES),
        ADMIN_SWITCHES: awilix.asValue(ADMIN_SWITCHES),
        ACL: awilix.asValue(ACL),
        PATH_BASE_MODULE: awilix.asValue(path.join(__dirname, '../base')),
        ...oAliases
    });
    return container;
}

module.exports = {
    createContainer
};
