const path = require('path');
const os = require('os');
const sHomeDir = path.resolve(os.homedir());

module.exports = {
    PLASMUD_DIRECTORY_DATA: {
        required: false,
        type: 'string',
        default: path.join(sHomeDir, '.plasmud')
    },
    PLASMUD_DIRECTORY_WEB_CLIENT: {
        required: false,
        type: 'string',
        default: path.join(__dirname, '../../dist/spa')
    },
    PLASMUD_LANG: {
        require: true,
        type: 'string'
    },
    PLASMUD_MODULES: {
        required: true,
        type: 'string'
    },
    PLASMUD_MOTD: {
        require: false,
        type: 'string',
        default: ''
    },
    PLASMUD_SERVER_PORT_ADMIN: {
        required: false,
        type: 'integer',
        default: 8084
    },
    PLASMUD_SERVER_PORT_TELNET: {
        required: false,
        type: 'integer',
        default: 8023
    },
    PLASMUD_SERVER_PORT_WEB: {
        required: false,
        type: 'integer',
        default: 8082
    }
};
