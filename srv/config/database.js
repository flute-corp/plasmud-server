const rp = require('../../libs/resolve-path');
const { getEnv } = require('../env-parser');
const { io } = require('../../libs/o876-db');

module.exports = {
    root: {
        name: 'root',
        email: ''
    },
    path: rp(getEnv().PLASMUD_DIRECTORY_DATA, 'db').path, // will be created if non existant
    collections: ['users', 'characters', 'banishments', 'player-character-save', 'item-save'],
    io: new io.fs(),
    indexes: {
        users: {
            name: {
                caseInsensitive: true,
                unique: true
            }
        },
        characters: {
            name: {
                caseInsensitive: true,
                unique: true
            }
        },
        banishments: {
            banner: {}
        },
        'player-character-save': {
            uid: {}
        }
    }
};
