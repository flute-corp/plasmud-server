const { getEnv } = require('./env-parser');

const oPackage = require('../package.json');
const Server = require('./infrastructure/frameworks/web/Server');
const dependencies = require('./config/dependencies');
const debug = require('debug');

const logServ = debug('serv:main');

logServ('%s v%s - %s', oPackage.name, oPackage.version, oPackage.description);

async function run () {
    const env = getEnv();
    const oServer = new Server();

    const ports = {
        ws: env.PLASMUD_SERVER_PORT_WEB,
        admin: env.PLASMUD_SERVER_PORT_ADMIN,
        telnet: env.PLASMUD_SERVER_PORT_TELNET
    };

    await oServer.init({
        dependencies,
        ports,
        env
    });

    logServ('server initialization complete : clients welcome !');

    // déclaring sigint sigterm handler

    async function gracefulShutdown () {
        return oServer.shutdown();
    }

    process.on('SIGTERM', async () => {
        logServ('receiving SIGTERM');
        await gracefulShutdown();
    });

    process.on('SIGINT', async () => {
        logServ('receiving SIGINT');
        await gracefulShutdown();
        process.exit();
    });

    return oServer;
}

run()
    .catch(e => console.error(e));
