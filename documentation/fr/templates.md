# Création de templates

## C'est du twig

La création de templates se fait en twig. 
Ce langage de templating super cool adapté pour node.js.

### Tested filters

#### round

Arrodissement d'une valeur numérique.

```
{{ 5.25 | round }} --> 5
{{ 5.75 | round }} --> 6
{{ 5.25 | round(1) }} --> 5.3
{{ 5.25 | round(0, 'floor') }} --> 5
{{ 5.25 | round(0, 'ceil') }} --> 6
{{ 5.25 | round(0, 'common') }} --> 5 (arrondissement normal)

```

### Custom filters

Ces filtres ne sont pas des filtres twig standards.

#### packline
Remplace les sauts line par des espaces. S'utilise principalement avec {% apply %} 
pour les blocs multilignes.

exemple :

```twig
{% apply packline %}
Ceci
Sur
Une
Seule
Ligne
{% endapply %}
```

donne :

```
"Ceci Sur Une Seule Ligne"
```

#### rpad

Bourre une chaine de caractères à droite jusqu'à ce qu'elle atteigne
la longueur désirée.

```
{{ 'abc' | rpad(7, '*') }}

outputs : 'abc****'
```

Si la chaîne est plus longue que la longueur du bourrage, elle reste inchangée.

```
{{ 'abcdefghijklm' | rpad(7, '*') }}

outputs : 'abcdefghijklm'
```

#### lpad

Semblable à __rpad__. Bourre une chaine de caractères à gauche jusqu'à ce qu'elle atteigne
la longueur désirée.

```
{{ 'abc' | lpad(7, '*') }}

outputs : '****abc'
```

Comme pour __rpad__ la chaîne est plus longue que la longueur du bourrage, elle reste inchangée.

#### bpad

Bourre une chaîne de caractères des deux côtés, de manière à ce que le texte
original soit sensiblement au centre de la nouvelle chaîne.

```
{{ 'abc' | pad(7, '*') }}

outputs : '**abc**'
```

Comme pour les autres fonctions de padding, si la chaîne est plus longue que la longueur du bourrage,
elle reste inchangée.

#### pre

Transforme tous les espaces d'un texte, en espaces non-sécables.

exemple :

```twig
{% apply pre %}
  Ce            texte
  est de plus en plus
       indenté
          n'importe comment
{% endapply %}
```

donne : 

```
"  Ce            texte"
"  est de plus en plus"
"       indenté"
"          n'importe comment"
```

