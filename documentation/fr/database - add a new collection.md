# Ajout d'une collection
Lorsqu'on souhaite ajouter une collection, penser à :
- Déclarer la collection dans srv/config/database.js
- Déclarer le repository correspondant dans srv/config/implementations.json
- Créer un repository dans les interfaces
- Créer une implementation de ce repository

