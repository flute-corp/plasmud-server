# Scripts de commandes

Les scripts de commandes ajoutent de nouvelles commandes à PLASMUD.

Il y a deux sortent de script de commandes :
- Les scripts de commandes de base du serveur qui ont accès à certaines fonctionnalités (use cases) du service, et qui servent à effectuer des opérations système ou de gestion. Ces commandes peuvent être utilisées en dehors des parties de jeu. On ne peut pas en définir dans les __modules de jeu__.
- Les scripts de commandes MUD qui ajoutent des fonctionnalités au jeu ; et qui ont donc accès au moteur de jeu. Ils servent à progresser dans la partie, et permettent d'interagir avec le monde du jeu. Ces scripts sont nécessairement inclus dans des __modules de jeu__.

## Les scripts de commandes de base du serveur

### Paramètre 1 : le contexte

C'est un contexte qui peut être considéré comme une boîte à outil contextualisée
à l'utilisateur qui a lancé la commande.

#### Propriété : client

Instance du client qui a lancé la commande.

Un client est un utilisateur connecté au service à travers un mode de transport (websocket, socket telnet, socket ssh...).
Le client est une structure de donnée contenant les propriétés suivantes :

| propriété | description                                                                                                                                                                                                                                             |
|-----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| id        | Identifiant technique du client (ne pas confondre avec l'uid).                                                                                                                                                                                          |
| state     | Etat de connexion du client. Voir le fichier _srv/infrastructure/application/types/client-states.js_ pour connaitre les différentes valeurs possibles de cette propriété.                                                                               |
| type      | Type de client, correspondant au mode de transport utilisé par l'utilisateur pour se connecter au service. Voir le fichier _srv/infrastructure/application/types/client-states.js_ pour connaitre les différentes valeurs possibles de cette propriété. |
| socket    | Instance de la socket de connexion du client. Peut dépendre du type de client                                                                                                                                                                           |
| uid       | Identifiant de l'utilisateur. C'est un identifiant de base de données, unique pour chaque compte.                                                                                                                                                       |
| uname     | Nom de l'utilisateur                                                                                                                                                                                                                                    |


#### Propriété : print

Un ensemble de fonctions qui permettent d'envoyer un message au client, ce message est généralement affiché dans sa console.

| fonction      | paramètres                  | effet                                                                                   |
|---------------|-----------------------------|-----------------------------------------------------------------------------------------|
| print.to      | idUser, sString             | Envoie un message __sString__ à l'utilisateur dont on spécifie l'identifiant __idUser__ |
| print.log     | ...aStrings                 | Affiche un ensemble de chaines caractères dans la console du serveur.                   |
| print.text    | sStrRef, oVariables         | Affiche un texte dont la référence et les variables sont spécifié en paramètre          |
| print.text.to | idUser, sStrRef, oVariables | Comme _print.text_, mais envoie la chaîne à un utilisateur particulier                  |

#### Propriété : uc

Un ensemble de use-cases qui permet d'accéder aux fonctionnalités techniques du service.

_(L'ensemble des use-cases n'est pas détaillé dans ce fichier)_

### Paramètres 2+

À partir du deuxième argument, on accède aux arguments tapés en commande.



## Script de commande PLASMUD

Ces scripts ne sont accessibles qu'une fois qui l'utilisateur ait rejoin la partie.
Ils sont exécuté lorsque l'utilisateur tape la commande correspondante.
Ces commandes ont pour but de progresser dans le jeu.

### Variable globale $context : le contexte

Ce contexte est un objet contextualisé à l'utilisateur qui a tapé la commande.

| propriété | description                                                                                                                                          |
|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------|
| system    | Instance du système                                                                                                                                  |
| engine    | Instance du moteur du jeu                                                                                                                            |
| pc        | Entité (personnage) controlée par le joueur. Notons qu'un joueur peut disposer de plusieurs personnages dans la même partie (mais pas simultanément) |
| print     | permet d'envoyer des messages                                                                                                                        |
| text      | permet de composer une chaine de caractère à partir d'une référence et de  variables                                                                 |
| abort     | fonction qui permet à l'utilisateur de quitter la partie                                                                                             |

### Variable global : $parameters

Avec cette variable, on accède aux arguments tapés en commande. C'est une simple liste [] de chaînes caractère composée des différent mots tapé à la suite de la commande.

# Scripts d'évènements

Les scripts d'évènement sont lancés lorsque certains évènements se produisent.
Le nom du script permet de déterminer l'évènement auquel il est rattaché.

$event
