# Wizard commands

Parfois, il faut intervenir dans le déroulement de la partie 
en créant supprimant ou modifiant des objets.


### gm/spawn

Création d'un nouvel objet, d'un nouveau plaçable, ou nouvelle créature (PNJ).

### gm/destroy

Destruction d'un objet, plaçable, ou d'une créature

### gm/move

Déplacement (changement de pièce ou d'inventaire) d'un item, plaçable, ou créature.

### gm/examine

Examen minutieux d'un objet
