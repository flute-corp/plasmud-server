# Mise en route du serveur.

Après téléchargement du serveur...

## Variables d'environnement

Les variables d'environnement permettent de modifier le comportement du serveur.
On peut les définir dans un fichier ".env" ou bien par définition d'environnement classique.

### PLASMUD_SERVER_PORT_HTTP
Numéro de port du service de jeu HTTP. Ce service permet de servir les pages web constituant
l'application cliente minimale d'accès au service. Les websockets passent par ce port.

### PLASMUD_ADMIN_SERVER_PORT_HTTP
Numéro de port du service d'administration. Ce service permet de consulter ou modifier
l'état du serveur.

### PLASMUD_SERVER_PORT_TELNET
Numéro de port du service TELNET. Ce service est une alternative aux websocket et est
utilisé par des clients TELNET ou client MUD riches (comme Mudlet).

### DEBUG
Variable utilisée par le système d'affichage de logs. La valeur de cette variable ne devrait
pas être modifiée.

### PLASMUD_DATA_DIRECTORY
Emplacement du dossier dans lequel toutes les données vivantes du serveur sont placées.
- Sous-dossier des Modules
- Base de données des personnages
- Base de données des utilisateurs
- Sous-dossier des sauvegardes

### PLASMUD_SAVE_SLOT
Variable permettant de définir le nom du sous-dossier des sauvegardes.
Ce sous-dossiers est relatif au dossier des données vivantes défini par __PLASMUD_DATA_DIRECTORY__.

### PLASMUD_MODULES
Liste des modules à charger au démarrage du serveur.
Les modules sont organisé en liste de noms séparés par des virgules.

Exemple :
```dotenv
PLASMUD_MODULES="exploration, dialogs, quests, test-221017-mod"
```

### PLASMUD_LANG
Défini la langue utilisée dans plasmud. Cela exclut la langue des modules (les modules ne sont pas multilangues).

Les valeurs sont des chaines de caractères comme "fr", "en"...

### PLASMUD_MOTD
Défini le "Moto of the day", la citation du jour, qui sera affichée
dans l'écran d'accueil des clients lorsqu'il se connectent.

### PLASMUD_WEB_CLIENT_DIRECTORY
Defini le dossier dans lequel se trouvent les distribuables du client web.


## Commande de démarrage

Lancer la commande suivante :

```
npm start
```