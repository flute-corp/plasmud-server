# Light and darkness

## Niveau de luminosité dans les pièces

Dans une pièce sombre il n'est pas possible de voir clairement.
Cela ne veut pas dire qu'on est aveugle.
On peut distinguer les sortie, le contour de l'ameublement, voire même le nombre le la taille des créature qui nous entoure.
Mais on ne peut pas reconnaitre les trait distinctif des créatures.

### Effet sur la commande look
- Dans une pièce sombre, le nom des entités (acteurs) est changé par un nom générique "présence invisible".
- 