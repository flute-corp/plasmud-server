# dev log

## 2024-01-01

### Création du mage Curwen
```
train
```
Il y a deux feat qui n'ont pas de i18n
Trouver les deux feats du passage de Mage entre level 0 et 1
- Incantation
- Restauration arcanique

Problème réglé

### Erreur classe Mage passage niveau 2

Quand on fait passer le mage du niveau 1 à 2 on a une erreur de dialogue
ERR_EVOL_GROUP_FEAT_NOT_SELECTED

Ca doit être un problème dans le template : il faut sélectionner : tradition arcanique évocation.

On change la gestion des template : maintenant les template sont défini dans ADDV.
Le connecteur addv du plasmud reconstitue ses données depuis les template défini dans Addv
Cela permet d'éviter de copier-coller les données d'évolution.

Ca fonctionne ! Le personnage peut atteindre le niveau 2

### Sort sans script associé

Passer le niveau 2 - choisir MAge niv. 2
Apprendre les sorts armure de mage, projectile magique, rayon de givre
Lancer armure de mage

Le script n'existant pas il faut arranger le message d'erreur

Un message d'erreur explicite sera transmis au joueur

### Une IA pour les monstres
Il faudrait s'atteler à une IA ; s'assurer que les messages de magie arrivent bien jusqu'au plasmud pour pouvoir les exploiter.
Si un PJ lance un sort hostile sur un PNJ, il faut que celui-ci riposte comme pour une attaque.
Il faut se rappeler comment un PNJ prend l'initiatique de contre attaquer

On a attaché un message de débug au spellcast-at event qui devrait lancer un script MUD.
On va tester avec un projectile

### La sauvegarde de personnage ne fonctionne pas
On entraine un mage niveau 2 celui-ci retourne touriste niveau 1 après rechargement
Le fichier de sauvegarde a l'air correct, et fait référence aux sorts et même à leur préparation.
Les data ne semblent pas être restaurées.

Un mauvais format dans addv-srd qui stipulait que
- la propriété skills devait être présente dans le state restauré. Mais on les a enlevés car les skills sont des proficiencies.
- les propriétés de la section counters devaient être des number ; mais ce sont des { value: number, max: number }

Un petit bug dans la mutation de store resetCharacter construisait mal les extraAttacks
Les personnages sont correctement chargés

### Tentative de lancer mains brulantes sur le gobelin

Il y a un defaut dans la valeur de la propriété target du spell data
TARGET_TYPE_HOSTILES au lieu de l'attendu TARGET_TYPE_HOSTILE (sans S )

Les sorts de la base de données sont maintenant verifier dans les tests unitaires de addv
On ne peut pas lancer main brulante, erreur possible dans le getter de castabilité

L'erreur était dûe à une faute de frappe. On utilisait la prop getSpellSlotStatus.uses" au lieu de "used" dans getSpellCastability

On constate que les évènements spellcast arrive jusqu'au plasmud, car le message s'affiche dans le log.

### Gobelin désavantagé lors du jet de sauvegarde dex face à un sort

Le gobelin est désavantagé dans son jet de sauvegarde, car il a un shield.
Sa proficiency est mal foutue : les monstres ont une proficiency PROFICIENCY_ARMOR_SHIELD.
Il faut la changer en PROFICIENCY_SHIELD.

C'est fait.

### Look devrai indiquer l'état physiologique des pnj

A part la description du type d'entité il faudrait une description de
l'état de la créature.
- HP en 100%

De plus la commande __consider__ ne fonctionne plus : ca marque : 
```
key 'consider (fr)' returned an object instead of string.
```

On a réglé cela et on conseille d'utiliser des noms composés pour les templates avec des dash - dedans.

Pour l'instant, c'est la commande __consider__ qui permettra de bien déterminer les
données physiologiques d'un adversaire en vue d'un combat.

### Le lancement des sorts ne consomme pas de slots

On a beau lancer "mains brûlantes", le slot de niveau 1 ne se consomme pas.

Quelques tests unitaires vont permettre de résoudre ce problème, lié à la réactivité
des tableau de valeur scalaire. Dans un state changer les valeurs scalire d'un tableau ne garantit
pas que les getters qui y accèdent seront mis à jours.
- on créé array-mutation pour centraliser les mutation de tableau avec des fonction qui respectent la réactivité

### Pour les effets imbriqué

Actuellement les effets de type concentration, group, end_on_attack contiennent
des références vers des effets sur lequels ils agissent. 
Par exemple l'effet group est un effet qui, lorsqu'il est dissipé, dissipe en
cascade tous ses enfants.
L'effet concentration ressemble à l'effet group sauf qu'il s'auto dissipe en cas de dégâts
L'effet end-on-attack termine un effet si on attaque.

Certains effets-enfants peuvent être posés sur des créatures différentes.
Par exemple lors du lancement du sort d'invisibilité, plusieurs créaatures
peuvent être affectées si le sort est lancé suffisament haut (+1 cible par niveau supplémentaire)
Pourtant ces effets disparaitront tous si le caster prend des dégâts.
Cela pose problème si le caster est sérialisé.
