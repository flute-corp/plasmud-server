const { User, System, Channel } = require('../index');

describe('User', function () {
    describe('setters/getters', function () {
        it('should set name properly', function () {
            const u = new User({ id: 1, name: 'johnson' });
            expect(u.name).toBe('johnson');
            u.name = 'joe';
            expect(u.name).toBe('joe');
        });
        it('should set id properly', function () {
            const u = new User({ id: 12, name: 'x' });
            expect(u.id).toBe(12);
            u.id = 13;
            expect(u.id).toBe(13);
        });
        it('should display properly', function () {
            const u = new User({ id: 12, name: 'johnson' });
            expect(u.display).toBe('@johnson');
        });
    });

    describe('message transmission', function () {
        it('should transmit a message', function () {
            const u = new User({});
            const uSender = new User({});
            u.name = 'johnson';
            u.id = 12;
            uSender.name = 'joe gillian';
            uSender.id = 13;
            const aLogEvent = [];
            u.on('message-received', event => aLogEvent.push(event.from.name + ':' + event.message));
            u.transmitMessage(uSender, 'let\'s play rugball !', null);
            expect(aLogEvent[0]).toBe('joe gillian:let\'s play rugball !');
        });
    });
});

describe('Channel', function () {
    describe('setters/getters', function () {
        it('should set name properly', function () {
            const c = new Channel();
            c.name = 'lobby';
            expect(c.name).toBe('lobby');
        });
        it('should set id properly', function () {
            const c = new Channel();
            c.id = '12';
            expect(c.id).toBe('12');
        });
        it('should display properly', function () {
            const c = new Channel();
            c.name = 'lobby';
            c.id = '12';
            expect(c.display).toBe('#lobby');
        });
    });

    describe('user present', function () {
        it('should test if user is present', function () {
            const c = new Channel();
            c.name = 'lobby';
            c.id = '12';
            const users = [
                new User({}),
                new User({}),
                new User({})
            ];
            users.forEach((u, i) => {
                u.id = i + 1;
                u.name = 'name' + i;
            });
            c.addUser(users[0]);
            c.addUser(users[2]);
            expect(c.userPresent(users[0])).toBeTruthy();
            expect(c.userPresent(users[1])).toBeFalsy();
            expect(c.userPresent(users[2])).toBeTruthy();
            expect(c.userPresent(users[2])).toBeTruthy();
            c.dropUser(users[2]);
            expect(c.userPresent(users[0])).toBeTruthy();
            expect(c.userPresent(users[1])).toBeFalsy();
            expect(c.userPresent(users[2])).toBeFalsy();
            c.dropUser(users[0]);
            expect(c.userPresent(users[0])).toBeFalsy();
            expect(c.userPresent(users[1])).toBeFalsy();
            expect(c.userPresent(users[2])).toBeFalsy();
        });
    });

    describe('purge', function () {
        it('should purge the channel from all users', function () {
            const c = new Channel();
            c.name = 'lobby';
            c.id = '12';
            const users = [
                new User({}),
                new User({}),
                new User({}),
                new User({}),
                new User({}),
                new User({}),
                new User({}),
                new User({}),
                new User({}),
                new User({})
            ];
            users.forEach((u, i) => {
                u.id = i + 1;
                u.name = 'name' + i;
                c.addUser(u);
            });
            expect(users.every(u => c.userPresent(u))).toBeTruthy();
            c.purge();
            expect(users.some(u => c.userPresent(u))).toBeFalsy();
            expect(c.users).toHaveLength(0);
        });
    });

    describe('events join/leave', function () {
        it('should trigger leave/join events', function () {
            const c = new Channel();
            c.name = 'lobby';
            c.id = '12';
            const aLog = [];
            const users = [
                new User({}),
                new User({}),
                new User({})
            ];
            function evJoin (event) {
                aLog.push('join ' + event.user.display);
            }
            function evLeave (event) {
                aLog.push('leave ' + event.user.display);
            }
            c.on('user-added', evJoin);
            c.on('user-dropped', evLeave);
            users.forEach((u, i) => {
                u.id = i + 1;
                u.name = 'name' + i;
                c.addUser(u);
            });
            c.dropUser(users[1]);
            expect(aLog).toEqual([
                'join @name0',
                'join @name1',
                'join @name2',
                'leave @name1'
            ]);
        });
    });

    describe('events message', function () {
        it('should send messages', function () {
            const c = new Channel();
            c.name = 'lobby';
            c.id = '12';
            const aLog = [];
            const users = [
                new User({}),
                new User({}),
                new User({})
            ];
            function evMsg (event) {
                aLog.push('message from:' + event.from.display + ' to:' + event.to.display + ' channel:' + event.channel.display + ' ' + event.message);
            }
            users.forEach((u, i) => {
                u.id = i + 1;
                u.name = 'name' + i;
                u.on('message-received', evMsg);
                c.addUser(u);
            });
            users[0].sendMessage(c, 'hello world !');
            expect(aLog).toEqual([
                'message from:@name0 to:@name0 channel:#lobby hello world !',
                'message from:@name0 to:@name1 channel:#lobby hello world !',
                'message from:@name0 to:@name2 channel:#lobby hello world !'
            ]);
        });
    });

    describe('channel drop', function () {
        it('should send messages', function () {
            const c = new Channel();
            c.name = 'lobby';
            c.id = '12';
            function evMsg ({ channel, user }) {
                aLog.push('dropped user:' + user.display + ' from channel:' + channel.display);
            }
            c.on('user-dropped', evMsg);
            const aLog = [];
            const users = [
                new User({}),
                new User({}),
                new User({})
            ];
            users.forEach((u, i) => {
                u.id = i + 1;
                u.name = 'name' + i;
                c.addUser(u);
            });
            c.purge();
            expect(aLog).toEqual([
                'dropped user:@name0 from channel:#lobby',
                'dropped user:@name1 from channel:#lobby',
                'dropped user:@name2 from channel:#lobby'
            ]);
        });
    });

    describe('find channel', function () {
        it('should find channel', function () {
            const c = new Channel();
            c.name = 'lobby';
            c.id = '12';
            const s = new System();
            s.addChannel(c);
            expect(Object.values(s.channels).length).toBe(1);
            expect(Object.values(s.channels)[0]).toBe(c);
            expect(Object.values(s.channels).find(x => x.id === '12')).toBe(c);
            expect(s.getChannel('12')).toBeDefined();
            expect(() => s.getChannel('13')).toThrow();
        });
    });
});
