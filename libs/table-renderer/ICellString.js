class ICellString {
    constructor () {
    }

    /**
     * the number of printable characters
     */
    get length () {
    }

    /**
     * The final output text, that will be render.
     * @return {string}
     */
    toString () {
    }
}

module.exports = ICellString;
