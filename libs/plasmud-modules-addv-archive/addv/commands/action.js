// Script action
//
// Lancement d'une action.
//
// @author Ralphy
// @date 2023-12-06

/**
 *
 * @param context
 * @param sAction
 * @returns {{innate: boolean, script: string, canUse: boolean, uses: {max: number, value: number}, action: string, clearUses: function, incUses: function, run: function }[]}
 */
function getActionData (context, sAction) {
    const { pc, services: { addv } } = context;
    const oAddvPlayer = addv.getAddvEntity(pc);
    const aActions = addv.getAvailableActions(oAddvPlayer);
    return aActions.find(a => a.action === sAction);
}

function doAction (context, sAction) {
    const oAction = getActionData(context, sAction);
    if (!!oAction && oAction.canUse) {
        context.print('cmdAction.action.runOnSelf', { action: sAction });
        oAction.run();
        oAction.incUses();
    }
}

function doActionOnActor (context, sAction, oActor) {
    const { services: { addv } } = context;
    const oAction = getActionData(context, sAction);
    if (!!oAction && oAction.canUse) {
        context.print('cmdAction.action.runOnTarget', { action: sAction, target: oActor.name });
        oAction.run(addv.getAddvEntity(oActor));
        oAction.incUses();
    }
}

module.exports = function main (context, aParams) {
    try {
        context.parse(aParams, {
            [context.PARSER_PATTERNS.STRING_ON_ACTOR]: p => doActionOnActor(context, p.string, p.actor),
            [context.PARSER_PATTERNS.NONE]: () => context.print('generic.error.needArgument'),
            [context.PARSER_PATTERNS.DEFAULT]: () => doAction(context, aParams.join(' '))
        });
    } catch (e) {
        console.error(e);
    }
    /*
    const { engine, print, pc, services: { addv } } = context
    const oAddvPlayer = addv.getAddvEntity(pc)
    const aActions = addv.getAvailableActions(oAddvPlayer)
    const a = aActions.map(oAction => {
        return {
            name: context.text('addvFeatDescription.' + oAction.action + '.name'),
            uses: oAction.uses.value
        }
    })
     */
};
