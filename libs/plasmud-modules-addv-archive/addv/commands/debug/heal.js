module.exports = function main(context) {
    const { engine, print, pc, services: { addv } } = context;
    const player = addv.getAddvEntity(pc);
    pc.dead = false;
    player.store.mutations.heal({ amount: Infinity });
    print('debug.heal', {
        name: pc.name
    });
};
