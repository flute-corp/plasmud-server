module.exports = function main(context) {
    const { engine, print, pc, services: { addv } } = context;
    const player = addv.getAddvEntity(pc);
    player.store.mutations.addClass({ ref: 'tourist', levels: 1 });
    print('debug.levelup', {
        level: player.store.getters.getLevel,
        name: pc.name
    });
};
