function printPCStats (context) {
    const { pc, text, print, services: { addv } } = context;
    const oAddvPC = addv.getAddvEntity(pc);
    const g = oAddvPC.store.getters;
    const abilityBaseValues = g.getAbilityBaseValues;
    const abilityValues = g.getAbilityValues;
    const classes = Object
        .entries(g.getLevelByClass)
        .map(([sClass, nLevel]) => ({
            class: sClass,
            levels: nLevel
        }));
    const aWarnings = [];
    const nExhaustionLevel = g.getExhaustionLevel;
    const sWeapon = g.getSelectedWeapon?.name || '';
    if (!sWeapon) {
        aWarnings.push(text('score.moreinfo.noWeapon'));
    }
    if (nExhaustionLevel > 0) {
        aWarnings.push(text('score.moreinfo.exhaustionLevel', { value: nExhaustionLevel }));
    }
    if (!g.isProficientSelectedWeapon) {
        aWarnings.push(text('score.moreinfo.notProficientWeapon'));
    }
    if (!g.isProficientArmorAndShield) {
        aWarnings.push(text('score.moreinfo.notProficientArmor'));
    }
    if (g.isEquippedWithRangedWeapon && !g.isRangedWeaponProperlyLoaded) {
        aWarnings.push(text('score.moreinfo.rangedWeaponNotLoaded'));
    }
    g.getConditions.forEach(condition => {
        aWarnings.push(text('score.moreinfo.condition', { condition }));
    });

    print('stats', {
        name: oAddvPC.name,
        classes,
        level: g.getLevel,
        ac: g.getArmorClass,
        atk: g.getAttackBonus,
        atkCount: g.getAttackCount,
        hp: {
            max: g.getMaxHitPoints,
            current: g.getHitPoints
        },
        warnings: aWarnings,
        attributes: {
            base: {
                strength: abilityBaseValues[addv.CONSTS.ABILITY_STRENGTH],
                dexterity: abilityBaseValues[addv.CONSTS.ABILITY_DEXTERITY],
                constitution: abilityBaseValues[addv.CONSTS.ABILITY_CONSTITUTION],
                intelligence: abilityBaseValues[addv.CONSTS.ABILITY_INTELLIGENCE],
                wisdom: abilityBaseValues[addv.CONSTS.ABILITY_WISDOM],
                charisma: abilityBaseValues[addv.CONSTS.ABILITY_CHARISMA]
            },
            values: {
                strength: abilityValues[addv.CONSTS.ABILITY_STRENGTH],
                dexterity: abilityValues[addv.CONSTS.ABILITY_DEXTERITY],
                constitution: abilityValues[addv.CONSTS.ABILITY_CONSTITUTION],
                intelligence: abilityValues[addv.CONSTS.ABILITY_INTELLIGENCE],
                wisdom: abilityValues[addv.CONSTS.ABILITY_WISDOM],
                charisma: abilityValues[addv.CONSTS.ABILITY_CHARISMA]
            },
        }
    });
}

module.exports = function main (context) {
    printPCStats(context);
};
