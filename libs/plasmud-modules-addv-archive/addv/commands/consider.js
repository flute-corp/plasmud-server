function considerEntity (context, actor) {
    const { engine, print, services: { addv } } = context;
    if (engine.isActor(actor)) {
        const oAddvTarget = addv.getAddvEntity(actor);
        const oAddvMe = addv.getAddvEntity(context.pc);
        const c = oAddvMe.compare(oAddvTarget);
        context.print('consider-screen', {
            adv: {
                hp100: Math.ceil(100 * oAddvTarget.store.getters.getHitPoints / oAddvTarget.store.getters.getMaxHitPoints)
            },
            melee: c.melee,
            ranged: c.ranged,
            target: actor.name,
        });
    } else {
        context.print('generic.error.argumentInaccurate');
    }
}

module.exports = function main(context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.ENTITY]: p => considerEntity(context, p.entity),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};
