function describeWeapon (context, oItem) {
    const info = {
        damage: oItem.damage,
        weaponType: context.text('inventory.weaponType', oItem),
        material: context.text('inventory.weaponMaterial', oItem),
        damageType: context.text('inventory.weaponDamageType', oItem),
        versatile: oItem.attributes.includes('WEAPON_ATTRIBUTE_VERSATILE'),
        versatileDamage: oItem.versatileDamage
    };
    context.print('look/weapon', info);
}

/**
 * Description des propriétés de l'item spécifié
 * @param context {MUDContext}
 * @param oItem {MUDEntity}
 */
function describeItem (context, oItem) {
    const addv = context.services.addv;
    const oAddvEntity = addv.getAddvEntity(oItem);
    if (!oAddvEntity) {
        // cet objet n'est pas référencé par le système addv
        return;
    }
    const { properties } = oAddvEntity;
    if (oAddvEntity.itemType === addv.CONSTS.ITEM_TYPE_WEAPON) {
        describeWeapon(context, oAddvEntity);
    }
    if (oAddvEntity.properties.find(ip => ip.property === addv.CONSTS.ITEM_PROPERTY_UNIDENTIFIED)) {
        context.print('inventory.itemUnidentified');
    } else {
        for (const [, oProp] of Object.entries(properties)) {
            context.print('inventory.itemPropertyList', oProp);
        }
    }
}

module.exports = function main(context, parameters) {
    const { engine, command } = context;
    command('@exploration/look', parameters);
    context.parse(parameters, {
        [context.PARSER_PATTERNS.ITEM]: p => describeItem(context, p.item),
        [context.PARSER_PATTERNS.ENTITY]: p => {
            if (engine.isItem(p.entity)) {
                describeItem(context, p.entity);
            }
        }
    });
};
