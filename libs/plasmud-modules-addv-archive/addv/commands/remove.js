/**
 * Modification de l'équipement
 */
function unequipItem (context, item) {
    const addv = context.services.addv;
    const slot = addv.getItemEquipmentSlot(context.pc, item);
    if (slot) {
        const oAddvCreature = addv.getAddvEntity(context.pc);
        // on ne pourra pas retirer l'item si il est cursed
        const { cursed } = oAddvCreature.unequipItem(slot);
        if (cursed) {
            context.print('unequip.failure.cursedItem', {
                item: item.name
            });
        } else {
            context.print('unequip.action.unequipped', { item: item.name });
            context.print.room('unequip.room.unequipped', { name: context.pc.name, item: item.name });
        }
    } else {
        context.print('unequip.failure.unequipped', { item: item.name });
    }
}

module.exports = function main (context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.ITEM]: p => unequipItem(context, p.item),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};
