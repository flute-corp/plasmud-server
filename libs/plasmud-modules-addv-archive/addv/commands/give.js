const { isItemCursed } = require('../scripts/helpers/is-item-cursed');

module.exports = function main(context, parameters) {
    let bTransmit = true;
    let oCursedItem = null;
    const ifCursedAndEquippedThenCancel = oItem => {
        if (isItemCursed(context, oItem)) {
            bTransmit = false;
            oCursedItem = oItem;
        }
    };
    context.parse(parameters, {
        [context.PARSER_PATTERNS.ITEM_TO_CONTAINER]: p => ifCursedAndEquippedThenCancel(p.item),
        [context.PARSER_PATTERNS.ITEM]: p => ifCursedAndEquippedThenCancel(p.item)
    });
    if (bTransmit) {
        context.command('@exploration/give', parameters);
    } else {
        // indiquer que l'objet est maudit et qu'on ne peut pas le donner
        context.print('give.failure.cursedItem', { item: oCursedItem.name });
    }
};
