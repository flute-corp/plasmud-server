/**
 * INV
 * @author ralphy
 * @date 2022-03-03
 * Cette commande visualise le contenu de l'inventaire du PJ
 * Syntaxe : INV
 */

function displayInventory (context) {
    const { engine, print, pc, services: { addv } } = context;
    const oCreature = addv.getAddvEntity(pc);
    // Déterminer l'équipement identifié
    const aEqIds = new Set();
    const aEquipment = Object.entries(oCreature
        .store
        .getters
        .getEquippedItems
    )
        .map(([slot, ei]) => [slot, ei?.id])
        .filter(([, id]) => !!id)
        .map(([slot, id]) => {
            const oItem = engine.getEntity(id);
            aEqIds.add(id);
            return {
                slot: context.text('inventory.equipmentSlot', { slot }),
                item: {
                    id: oItem.id,
                    name: oItem.name,
                    stack: oItem.stack,
                    remark: oItem.remark
                },
            };
        });
    const aEntities = engine
        .map(({ entity }) => entity)
        .getLocalEntities(pc) // getLocalEntities: ok
        .filter(entity => !aEqIds.has(entity.id));
    // Déterminer le poids
    const weight = pc.weight;
    const encumbrance = oCreature.store.getters.getEncumbranceLevel;
    const carryingCapacity = oCreature.store.getters.getCarryingCapacity;
    // déterminer les items équipés et les non-équipé
    // trier par types
    print('equip-inventory', {
        entities: aEntities,
        equipment: aEquipment,
        weight,
        encumbrance,
        carryingCapacity
    });
}

module.exports = function main (context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.NONE]: () => displayInventory(context),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.command('@exploration/inv', aArguments)
    });
};
