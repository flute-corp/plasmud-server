// Script actions
//
// Affiche la liste des actions disponible pour le personnage
// Ces actions incluent les actions de feat principalement pour le combat.
//
// @author Ralphy
// @date 2023-11-02

module.exports = function main (context, aParams) {
    const { engine, print, pc, services: { addv } } = context;
    const oAddvPlayer = addv.getAddvEntity(pc);
    const aActions = addv.getAvailableActions(oAddvPlayer);
    const a = aActions.map(oAction => {
        return {
            name: context.text('addvFeatDescription.' + oAction.action + '.name'),
            uses: oAction.uses.value,
            maxUses: oAction.uses.max,
            description: context.text('addvFeatDescription.' + oAction.action + '.description')
        };
    });
    aActions.forEach(oAction => {
        context.print('actions', {
            name: pc.name,
            actions: a
        });
    });
};
