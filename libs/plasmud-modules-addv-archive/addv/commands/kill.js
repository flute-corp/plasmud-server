function attackEntity (context, target) {
    const { print, services: { addv } } = context;
    print('combat.action.start', {
        target: target.name
    });
    const combat = addv.doAttack(context.pc, target);
    if (!combat) {
        context.print('kill.failure.invalidTarget', { name: target.name });
    }
}

module.exports = function main (context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.ACTOR]: p => attackEntity(context, p.actor),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};
