/**
 * Modification de l'équipement
 */
function equipItem ({ pc, print, services: { addv }}, item) {
    const oAddvItem = addv.getAddvEntity(item);
    const oAddvCreature = addv.getAddvEntity(pc);
    // Ici, on va empêcher l'équipement de l'item selon certain critère
    // notamment si l'objet est maudit
    const { previousItem, cursed } = oAddvCreature.equipItem(oAddvItem);
    if (cursed) {
        const oPrevItem = addv._getMudEntity(previousItem);
        print('equip.failure.cursedItem', {
            item: item.name,
            cursedItem: oPrevItem.name
        });
    } else {
        // Objet précédemment equippé = non maudit
        print('equip.action.equipped', { item: item.name });
        print.room('equip.room.equipped', { name: pc.name, item: item.name });
    }
}

module.exports = function main (context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.ITEM]: p => equipItem(context, p.item),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};
