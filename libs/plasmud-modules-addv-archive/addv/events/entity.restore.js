/**
 * ADDV entity.restore event
 *
 * Ce script commande à addv la restoration de l'état d'une entité mud
 * qui vient d'etre rechargé à partir d'une sauvegarde
 *
 * @author ralphy
 * last update : 2023-07-28
 */
module.exports = function main ({ entity, system: { services: { addv } } }) {
    addv.importEntity(entity);
};
