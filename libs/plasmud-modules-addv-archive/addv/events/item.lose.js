/**
 * ADDV item.lose event
 *
 * Ce script s'assure que lorsqu'un actor perd un item, celui ci est déséquippé le cas échéant.
 * Il faut aussi mettre à jour l'encombrement.
 *
 * @author ralphy
 * Création : 2023-07-28
 * Mise à jour : 2023-08-06
 */
module.exports = function main ({ entity: item, lostBy: actor, engine, system: { services: { addv } } }) {
    if (engine.isActor(actor)) {
        const slot = addv.getItemEquipmentSlot(actor, item);
        const oAddvCreature = addv.getAddvEntity(actor);
        if (slot) {
            // On bypass le curse car l'item est perdu d'une manière ou d'une autre
            oAddvCreature.unequipItem(slot, true);
        }
        oAddvCreature.store.mutations.setEncumbrance({ value: actor.weight });
    }
};
