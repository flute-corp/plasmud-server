module.exports = function main (evt) {
    const addv = evt.system.services?.addv;
    if (addv) {
        addv.processMutableEffects();
        const combatManager = addv.combatManager;
        if (combatManager) {
            combatManager.loop();
        }
    }
};
