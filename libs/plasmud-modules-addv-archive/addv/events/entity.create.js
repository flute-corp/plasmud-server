/**
 * ADDV entity.create event
 *
 * Ce script enregistre toute entité-mud nouvellement créée dans le système addv
 *
 * @author ralphy
 * last update : 2023-07-28
 */
module.exports = function main ({ entity, system: { services: { addv } } }) {
    addv.generateAddvEntityFromMud(entity);
};
