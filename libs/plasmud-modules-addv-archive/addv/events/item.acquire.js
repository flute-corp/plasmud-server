/**
 * ADDV item.acquire event
 *
 * Ce script s'assure l'encombrance est bien mise à jour lorsqu'un acteur obtient un nouvel objet
 *
 * @author ralphy
 * last update : 2023-08-06
 */
module.exports = function main ({ engine, entity: item, droppedBy: actor, system: { services: { addv } } }) {
    if (engine.isActor(actor)) {
        const oAddvCreature = addv.getAddvEntity(actor);
        oAddvCreature.store.mutations.setEncumbrance({ value: actor.weight });
    }
};
