/**
 * ADDV entity.save event
 *
 * Ce script enregistre toute entité-mud nouvellement créée dans le système addv
 *
 * @author ralphy
 * last update : 2023-07-28
 */
module.exports = function main ({ entity, save, system: { services: { addv } } }) {
    const oPayload = addv.exportEntity(entity);
    save({ [addv.namespace]: { entity: oPayload } });
};
