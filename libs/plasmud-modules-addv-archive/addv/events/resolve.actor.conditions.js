module.exports = function main ({ engine, actor, conditions, system }) {
    const oAddvActor = system.services.addv.getAddvEntity(actor);
    if (oAddvActor) {
        oAddvActor.store.getters.getConditions.forEach(c => conditions.push(c));
    }
};
