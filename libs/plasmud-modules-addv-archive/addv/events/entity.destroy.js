/**
 * ADDV entity.destroy event
 *
 * Ce script désenregistre toute entité-mud qui vien d'être supprimée par le mud.
 *
 * @author ralphy
 * last update : 2023-07-28
 */
module.exports = function main ({ entity, system: { services: { addv } } }) {
    addv.unregisterEntity(entity);
};
