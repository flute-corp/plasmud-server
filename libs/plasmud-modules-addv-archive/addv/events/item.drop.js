/**
 * ADDV item.drop event
 *
 * Ce script s'assure que lorsqu'un actor abandonne un item, celui-ci est déséquippé le cas échéant.
 *
 * @author ralphy
 * Création : 2023-07-28
 * Mise à jour : 2023-08-06
 */
module.exports = function main ({ engine, entity: item, droppedBy: actor, system: { services: { addv } } }) {
    if (engine.isActor(actor)) {
        const slot = addv.getItemEquipmentSlot(actor, item);
        const oAddvCreature = addv.getAddvEntity(actor);
        if (slot) {
            // Même si l'objet est maudit on autorise sont déséquipement car c'est un
            // changement de location décidé par un script
            oAddvCreature.unequipItem(slot, true);
        }
        oAddvCreature.store.mutations.setEncumbrance({ value: actor.weight });
    }
};
