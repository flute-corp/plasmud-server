/**
 * ADDV init event
 *
 * Ce script initialise le système ADDV et l'installe en tant que service du système mud.
 * Les autres scripts d'évènement peuvent y accéder via $event.system.services.addv
 * Les scripts de commandes peuvent y accéder via context.services.addv
 *
 * @author ralphy
 * last update : 2023-07-28
 */

const AddvConnector = require('../scripts/services/addv-connector');
const CombatManager = require('../scripts/services/combat-manager');

const TURN_DURATION = 6;

const STRINGS = {
    fr: require('@laboralphy/addv-srd/src/public-assets/public-assets.fr.json')
};


module.exports = function main ({ system, engine }) {
    const addv = new AddvConnector();
    const lang = system.config.language;
    system
        .textRenderer
        .producers
        .get('strings')
        .defineResource(lang, {
            addv: lang in STRINGS ? STRINGS[lang].strings : {}
        });
    addv.init({ system, engine });
    system.addContextService('addv', addv);
    const cm = new CombatManager();

    cm.useInternalClock = false;
    cm.turnDuration = TURN_DURATION * 1000;
    cm._turnTickCount = TURN_DURATION;
    cm.reverseRound = true;
    addv.combatManager = cm;
};
