/**
 * renvoie true si l'item est maudit
 * @param context {*}
 * @param oMudItem {D20Item}
 * @returns {boolean}
 */
function isItemCursed (context, oMudItem) {
    const addv = context.services.addv;
    const oAddvItem = addv.getAddvEntity(oMudItem);
    const oPlayer = addv.getAddvEntity(context.pc);
    const aEquippedItems = oPlayer.store.getters.getEquipmentList;
    const bIsEquipped = aEquippedItems.find(item => item.id === oAddvItem.id);
    return bIsEquipped && !!oAddvItem.properties.find(ip => ip.property === addv.CONSTS.ITEM_PROPERTY_CURSED);
}

module.exports = {
    isItemCursed
};
