const containers = require('../../../../helpers/containers');

function getEntityList (context, roomOrEntity) {
    const aList = containers.getEntityList(context, roomOrEntity);
    const { system: { services: { addv } }, engine, pc } = context;
    aList.forEach(entity => {
        entity.itemType = '';
        if (entity.is.item) {
            const oItem = engine.getEntity(entity.id);
            const oAddvItem = addv.getAddvEntity(oItem);
            entity.itemType = oAddvItem ? oAddvItem.itemType : '';
        }
    });
    return aList;
}

module.exports = {
    getEntityList
};
