const EventEmitter = require('events');

/**
 * @typedef AddvCombat {object}
 * @property fighter {Creature}
 * @property target {Creature}
 * @property attackCount {number}
 * @property attackPerTurn {number}
 * @property isNew {boolean}
 * @property plan {number[]}
 * @property initiative {number[]}
 * @property turn {number}
 * @property action {string} action validée prete à etre lancée
 * @property actionTaken {boolean} action effectuée ce tour
 * @property bonusAction {string}
 * @property bonusActionTaken {boolean} action bonus effectuée ce tour
 * @property data {{}}
 */

class CombatManager {
    constructor () {
        this._combatIndex = {};
        this._combatList = [];
        this._invalidList = false;
        this._events = new EventEmitter;
        this._turn = 0; // current turn
        this._turnTick = 0; // current tick in turn
        this._turnTickCount = 6; // number of ticks in a turn
        this._turnDuration = 6000; // duration of a turn
        this._interval = null;
        this._useInternalClock = true;
        this._reverseRound = false; // Place les attaques en fin de round afin de coller à l'affichage du status de tour
    }

    set reverseRound (value) {
        this._reverseRound = value;
    }

    get reverseRound () {
        return this._reverseRound;
    }

    get combats () {
        return this._combatList;
    }

    get useInternalClock () {
        return this._useInternalClock;
    }

    set useInternalClock (value) {
        if (this.started && !value) {
            this.stop();
        }
        this._useInternalClock = value;
    }

    /**
     * durée du tour en millisecondes
     * @returns {number}
     */
    get turnDuration () {
        return this._turnDuration;
    }

    /**
     * Définition de la durée du tour en millisecondes
     * @param value {number}
     */
    set turnDuration (value) {
        if (value !== this._turnDuration) {
            this._turnDuration = value;
            this.start();
        }
    }

    /**
     * Nombre de fois qu'il faut jouer les attack pendant 1 tour
     * @returns {number}
     */
    get turnTickCount () {
        return this._turnTickCount;
    }

    set turnTickCount (value) {
        if (this._turnTickCount !== value) {
            this._turnTickCount = value;
            this.start();
        }
    }

    get tick () {
        return this._turnTick;
    }

    /**
     * Renvoie true si la combat loop a démarré
     * @returns {boolean}
     */
    get started () {
        return !!this._interval;
    }

    set started (value) {
        const n = (this.started ? 2 : 0) | (value ? 1 : 0);
        switch (n) {
        case 1: {
            // pas démarré, on veut démarrer
            this.start();
            break;
        }

        case 2: {
            // démarré, on veut arreter
            this.stop();
            break;
        }

        default: {
            // pas de changement
            break;
        }
        }
    }

    /**
     * Démarrage de la boucle de combat
     */
    start () {
        if (!this._useInternalClock) {
            return;
        }
        if (this._turnTickCount === 0) {
            throw new Error('Cannot start combat loop : turnTick is 0');
        }
        this.stop();
        this._interval = setInterval(() => {
            this.loop();
        }, Math.floor(1000 * this._turnDuration / this._turnTickCount));
    }

    /**
     * Arret de la boucle de combat
     */
    stop () {
        if (!this._useInternalClock) {
            return;
        }
        if (this.started) {
            clearInterval(this._interval);
            this._interval = null;
        }
    }

    /**
     * boucle de combat
     */
    loop () {
        this.play();
    }

    get events () {
        return this._events;
    }

    /**
     * Renvoie true si la créature spécifiée est en train de combattre
     * @param oFighter {Creature}
     * @return {boolean}
     */
    isCreatureFighting (oFighter) {
        return oFighter.id in this._combatIndex;
    }

    /**
     * Désenregistre une créature du système de combat
     * @param oFighter {Creature}
     */
    removeFighter (oFighter) {
        if (this.isCreatureFighting(oFighter)) {
            const combat = this._combatIndex[oFighter.id];
            const i = this._combatList.indexOf(combat);
            if (i >= 0) {
                this._combatList.splice(i, 1);
            }
            this._events.emit('combat.quit', { combat });
            delete this._combatIndex[oFighter.id];
        }
    }

    /**
     * Ajoute un combattant et sa cible.
     * Une fois ajouté, le combattant attaquera sa cible tant qu'il peut.
     * @param oFighter
     * @param oTarget
     */
    addFighter (oFighter, oTarget) {
        // déterminer si cette creature est déjà en combat
        // si oui, alors on la désenregistre de son ancien combat
        if (this.isCreatureFighting(oFighter)) {
            this.removeFighter(oFighter);
        }
        const combat = {
            fighter: oFighter,
            target: oTarget,
            turn: 0,
            attackCount: 0,
            attackPerTurn: 0,
            isNew: true,
            initiative: [],
            plan: [],
            data: {},
            action: '',
            actionTaken: false,
            bonusActionTaken: false
        };
        this._combatIndex[oFighter.id] = combat;
        this._combatList.push(combat);
        this._events.emit('combat.created', { combat });
        this._invalidList = true;
        return combat;
    }

    declareDeadFighter (fighter) {
        if (this.isCreatureFighting(fighter)) {
            Object
                .values(this._combatIndex)
                .filter(combat => combat.target === fighter)
                .forEach(combat => this.removeFighter(combat.fighter));
            this.removeFighter(fighter);
        }
    }

    /**
     * Renvoie le nombre d'attaques qu'il reste au combatant pour ce tour
     * Fait appel à l'évènement 'combat.requestAttackCount'
     * @param oFighter
     */
    getFighterAttackCount (oFighter) {
        let nCount = null;
        const response = {
            fighter: oFighter,
            result: n => {
                nCount = n;
            }
        };
        this._events.emit('combat.requestAttackCount', response);
        if (nCount === null) {
            throw new Error('getFighterAttackCount: handler did not call result() / or no handler defined.');
        }
        return nCount;
    }

    getFighterCombat (oFighter) {
        return this._combatIndex[oFighter.id];
    }

    computePlanning (nAttackPerTurn, nTurnTickCount) {
        const aPlan = new Array(nTurnTickCount);
        aPlan.fill(0);
        for (let i = 0; i < nAttackPerTurn; ++i) {
            const iRank = Math.floor(aPlan.length * i / nAttackPerTurn);
            const nIndex = this._reverseRound
                ? nTurnTickCount - 1 - iRank
                : iRank;
            ++aPlan[nIndex];
        }
        return aPlan;
    }

    updateAttackCount (combat) {
        const nAttackPerTurn = this.getFighterAttackCount(combat.fighter);
        if (nAttackPerTurn !== combat.attackPerTurn) {
            combat.attackPerTurn = nAttackPerTurn;
            combat.plan = this.computePlanning(nAttackPerTurn, this._turnTickCount);
        }
    }

    /**
     *
     * @param ai {number[]}
     * @param bi {number[]}
     * @returns {number}
     * @private
     */
    _compareInitiative (ai, bi) {
        for (let i = 0, l = Math.min(ai.length, bi.length); i < l; ++i) {
            const n = bi[i] - ai[i];
            if (n !== 0) {
                return n;
            }
        }
        return bi.length - ai.length;
    }

    requestBonusAction (combat, sAction) {
        combat.bonusAction = sAction;
    }

    playAction (combat, bonus = false) {
        const key = bonus ? 'bonusAction' : 'action';
        const keyTaken = key + 'Taken';

        if (!combat[key]) {
            return;
        }
        if (combat[keyTaken]) {
            return;
        }
        // il y a une action en cour
        // vidange de l'action
        const sAction = combat[key];
        combat[key] = '';
        combat[keyTaken] = true; // ne plus accepter d'action ce tour
        // lancement de l'action
        this._events.emit('combat.action', {
            combat,
            turn: combat.turn,
            tick: this._turnTick,
            action: sAction
        });
    }

    playAttack (combat) {
        let bHasAttacked = true;
        const oEvent = {
            combat,
            turn: combat.turn,
            tick: this._turnTick,
            cancel: () => {
                // Permet d'annuler l'attaque en cas d'incapacité par exemple
                bHasAttacked = true;
            }
        };
        this._events.emit('combat.attack', oEvent);
        if (bHasAttacked) {
            ++combat.attackCount;
        }
    }

    /**
     * Effectue un tour de combat.
     * On ne sélectionne que les créature qui peuvent attaquer, c'est à dire celles qui n'ont pas épuisé leurs
     * attaque par tour. On lance l'évènement 'combat.attack' pour chaque créature.
     * Cet évènement doit être écouté et le gestionnaire de l'évènement doit écrire dans la variable
     * hasAttacked true pour signifier qu'une attaque a été consommée.
     */
    play () {
        // faire avancer l'horloge
        const tick = this._turnTick;
        const turn = this._turn;
        const bStartTick = tick === 0;
        if (this._invalidList) {
            this._combatList.sort((a, b) => this._compareInitiative(a.initiative, b.initiative));
            this._invalidList = false;
        }
        // fraction du tour écoulé
        // Déterminer les combattants qui peuvent jouer
        this._combatList.forEach(c => {
            if (bStartTick) {
                ++c.turn;
                c.attackCount = 0;
                c.actionTaken = false;
                c.bonusActionTaken = false;
                if (c.isNew) {
                    c.isNew = false;
                }
                this.updateAttackCount(c);
            }
            let bCancelCombat = false;
            this._events.emit('combat.loop', {
                turn: c.turn,
                tick,
                combat: c,
                cancel: (b = true) => {
                    bCancelCombat = b;
                }
            });
            if (bCancelCombat) {
                this.declareDeadFighter(c.fighter);
            }
        });
        this._combatList.forEach(c => {
            if (c.isNew) {
                // c'est un nouveau combat pris en cours de tour
                // il faut attendre le debut du tour suivant
                return;
            }
            // Seules les combattants à qui ils restent des attaques peuvent agir
            // Si une action est programmée elle se fera au dépent de l'une des attaques
            if (c.plan[tick] > 0) {
                // on peut agir, c'est soit une action normale soit une attaque
                // les action en priorité
                if (c.action && !c.actionTaken) {
                    this.playAction(c);
                } else {
                    // pas d'action en cours : on fait l'attaque
                    this.playAttack(c);
                }
            }
            if (c.bonusAction && !c.bonusActionTaken) {
                this.playAction(c, true);
            }
        });
        this._events.emit('combat.tick.end', {
            turn,
            tick
        });
        ++this._turnTick;
        if (this._turnTick >= this._turnTickCount) {
            this._events.emit('combat.turn.end', {
                turn,
                tick
            });
            this._turnTick = 0;
            ++this._turn;
        }
    }
}

module.exports = CombatManager;
