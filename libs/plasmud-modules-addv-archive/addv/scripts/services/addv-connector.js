const { Manager: ADDVManager, Creature, CONSTS: ADDV_CONSTS } = require('@laboralphy/addv-srd');
const Evolution = require('@laboralphy/addv-srd/src/Evolution');
const ADDV_NS = 'addv';
const debug = require('debug');
const log = debug('mod:addv');
const logDebug = debug('mod:addv:debug');
const EventEmitter = require('events');
const {shallowMap} = require('@laboralphy/object-fusion');


const COMBAT_DATA_VAR_USED_ACTIONS = 'usedActions'; // un Set qui mémorise les actions déjà lancées par l'adversaire ou le fighter

/**
 * Ce type permet de remplir un context i18n
 * @typedef BroadcastI18nContext {object}
 * @property attacker {string}
 * @property target {string}
 */

/**
 * @class AddvConnector
 */
class AddvConnector {
    constructor () {
        this.logDebug('addv-connector constructor.');

        this._addv = new ADDVManager();
        this._evolution = new Evolution();
        /**
         * @type {Object<string, Creature>}
         * @private
         */
        this._entities = {};
        this._system = null;
        this._engine = null;
        this._verbose = 1;
        this._events = new EventEmitter();
        this._combatManager = null;
        this._postponedBroadcasts = [];
        this._ewme = new Set(); // Entities with mutable effects

        // 'death', 'attack', 'saving-throw', 'damaged', 'target-distance'
        // 'action', 'check-skill'
        this._addv.events.on('offensive-slot', ev => {
            this.addvCreatureOffensiveSlot(ev);
        });

        this._addv.events.on('death', ev => {
            this.addvCreatureDeath(ev);
        });

        this._addv.events.on('attack', ev => {
            this.addvCreatureAttack(ev);
        });

        this._addv.events.on('saving-throw', ev => {
            this.addvCreatureSavingThrow(ev);
        });

        this._addv.events.on('damaged', ev => {
            this.addvCreatureDamaged(ev);
        });

        this._addv.events.on('spellcast-at', ev => {
            this.addvEntitySpellcastAt(ev);
        });
        this._addv.events.on('effect-applied', ev => {
            this.addvCreatureEffectApplied(ev);
        });
    }

    get data () {
        return this._addv.assetManager.data;
    }

    get publicAssets () {
        return this._addv.assetManager.publicAssets;
    }

    get manager () {
        return this._addv;
    }

    get evolution () {
        return this._evolution;
    }

    /**
     * Envoie un texte à un joueur
     * @param oMUDPlayer {MUDEntity}
     * @param sResRef {string}
     * @param oContext {Object}
     * @returns {Promise<void>}
     */
    sendText (oMUDPlayer, sResRef, oContext) {
        if (this._engine.isPlayer(oMUDPlayer)) {
            return this._system.sendTextToUser(oMUDPlayer.uid, sResRef, oContext);
        }
    }

    /**
     * Renvoie le nom d'un item générique qui n'a pas d'entité MUD associée
     * @param item {D20Item}
     */
    getGenericItemName (item) {
        let sDataField = '';
        switch (item.itemType) {
        case this.CONSTS.ITEM_TYPE_NATURAL_WEAPON:
        case this.CONSTS.ITEM_TYPE_NATURAL_ARMOR: {
            return '';
        }
        case this.CONSTS.ITEM_TYPE_WEAPON: {
            sDataField = 'weaponType';
            break;
        }
        case this.CONSTS.ITEM_TYPE_ARMOR: {
            sDataField = 'armorType';
            break;
        }
        case this.CONSTS.ITEM_TYPE_SHIELD: {
            sDataField = 'shieldType';
            break;
        }
        case this.CONSTS.ITEM_TYPE_AMMO: {
            sDataField = 'ammoType';
            break;
        }
        default: {
            sDataField = 'itemType';
            break;
        }
        }
        const subItemType = item[sDataField];
        return this.getSystemText('addv.' + sDataField + '.' + subItemType, {});
    }

    getSystemText (sString, oVariables) {
        return this._system.textRenderer.renderSync(sString, oVariables);
    }

    getDamageStr (sType, amount, resisted) {
        const ctx = {
            type: sType,
            amount,
            resisted
        };
        return resisted
            ? this.getSystemText('damageDisplay.damageAmountResisted', ctx)
            : amount
                ? this.getSystemText('damageDisplay.damageAmount', ctx)
                : this.getSystemText('damageDisplay.damage', ctx);
    }

    explainDamages (damageTypes, resistedTypes) {
        const damages = [];
        const aEntries = Object.entries(damageTypes);
        if (aEntries.length === 1) {
            const [sType, nAmount] = aEntries[0];
            damages.push(this.getDamageStr(sType, 0, resistedTypes[sType]));
        } else {
            for (const [sType, nAmount] of aEntries) {
                if (nAmount > 0 || resistedTypes[sType] > 0) {
                    damages.push(this.getDamageStr(sType, nAmount, resistedTypes[sType]));
                }
            }
        }
        return damages;
    }

    /**
     * Déclenché par un évènement ADDV
     * Gère le changement de distance entre une créature et sa cible
     *
     * @param value {number}
     * @param creature {Creature}
     * @param target {Creature}
     */
    addvCreatureTargetDistance ({ value, creature, target }) {
        const oAttacker = this._getMudEntity(creature);
        const oTarget = this._getMudEntity(target);
        if (value < 0) {
            const bInRange = creature.store.getters.isTargetInWeaponRange;
            const bInMeleeRange = creature.store.getters.isTargetInMeleeWeaponRange;
            const nTurns = creature.store.getters.getApproachToTargetTurns;
            const oContext = {
                attacker: oAttacker.name,
                target: oTarget.name,
                turns: nTurns,
                test: Math.random()
            };
            if (bInRange) {
                this.broadcastCombatMessage(
                    oAttacker,
                    oTarget,
                    'combat.move.you.rushInReach',
                    'combat.move.adv.rushInReach',
                    'combat.move.room.rushing',
                    oContext
                );
            } else if (nTurns === 1) {
                this.broadcastCombatMessage(
                    oAttacker,
                    oTarget,
                    'combat.move.you.rushNextTurn',
                    'combat.move.adv.rushNextTurn',
                    'combat.move.room.rushing',
                    oContext
                );
            } else {
                this.broadcastCombatMessage(
                    oAttacker,
                    oTarget,
                    'combat.move.you.rushFar',
                    'combat.move.adv.rushFar',
                    'combat.move.room.rushing',
                    oContext
                );
            }
        }
    }

    /**
     * Déclenché par un évènement ADDV
     * Gère la mort d'une créature.
     *
     * @param creature {Creature}
     * @param killer {Creature}
     */
    addvCreatureDeath ({ creature, killer }) {
        // Signifier au mud que la creature est morte
        const oMudDead = this._getMudEntity(creature);
        oMudDead.dead = true;
        // broadcaster l'évènement
        const oAttacker = this._getMudEntity(killer);
        const oTarget = this._getMudEntity(creature);
        const oContext = {
            attacker: oAttacker.name,
            target: oTarget.name
        };
        this.broadcastCombatMessage(
            oAttacker,
            oTarget,
            'combat.hit.you.victory',
            'combat.hit.adv.victory',
            'combat.hit.room.victory',
            oContext,
            true
        );
    }

    /**
     * Déclenché par un évènement ADDV
     * Gère l'enregistrement des dégâts subits par une créature
     *
     * @param creature {Creature}
     * @param damageType {string}
     * @param damageAmount {number}
     * @param resisted {number}
     * @param source {Creature}
     */
    addvCreatureDamaged ({ creature, type: damageType, amount: damageAmount, resisted, source }) {
        const oAttacker = this._getMudEntity(source);
        const oTarget = this._getMudEntity(creature);
        const oContext = {
            attacker: oAttacker.name,
            target: oTarget.name,
            damageAmount,
            damages: this.getDamageStr(damageType, 0, resisted)
        };
        this.broadcastCombatMessage(
            oAttacker,
            oTarget,
            'combat.hit.you.damages',
            'combat.hit.adv.damagesBy',
            'combat.hit.room.damagesBy',
            oContext
        );
    }

    addvCreatureOffensiveSlot(ev) {
        const { creature, current: { slot, weapon }} = ev;
        const oAttacker = this._getMudEntity(creature);
        const oTarget = this._getMudEntity(creature.getTarget());
        const oContext = {
            weapon: this.getEntityName(weapon),
            attacker: this.getEntityName(creature)
        };
        if (oContext.weapon) {
            this.broadcastCombatMessage(
                oAttacker,
                oTarget,
                'combat.attack.you.offensiveSlot',
                'combat.attack.adv.offensiveSlot',
                'combat.attack.room.offensiveSlot',
                oContext
            );
        }
    }

    /**
     * Gestion des attaque de créature
     * @param creature
     * @param outcome
     */
    addvCreatureAttack ({ creature, outcome }) {
        const {
            ac,
            distance,
            range,
            bonus,
            roll,
            critical,
            hit,
            dice,
            deflector,
            target,
            weapon,
            failed,
            failure,
            advantages,
            disadvantages,
            damages: {
                amount: damageAmount = 0,
                types: damageTypes = {},
                resisted: resistedTypes = {}
            }
        } = outcome;
        const oAttacker = this._getMudEntity(creature);
        const oTarget = this._getMudEntity(target);
        const sWeaponName = this.getEntityName(weapon);
        const sDeflector = deflector === this.CONSTS.ARMOR_DEFLECTOR_MISS
            ? 'miss.' + Math.floor(Math.random() * 10)
            : deflector.split('_').pop().toLowerCase();
        const oContext = {
            attacker: oAttacker.name,
            target: oTarget.name,
            weapon: sWeaponName,
            deflector: sDeflector,
            damageAmount,
            damages: this.explainDamages(damageTypes, resistedTypes).join(' + ')
        };
        if (sWeaponName) {
            oContext.context = 'weapon';
        }
        if ((range < distance) || (failed && failure === this.CONSTS.ATTACK_OUTCOME_UNREACHABLE)) {
            this.broadcastCombatMessage(
                oAttacker,
                oTarget,
                'combat.miss.you.outOfRange',
                'combat.miss.adv.outOfRange',
                'combat.miss.room.outOfRange',
                oContext
            );
            const nPrevDistance = creature.store.getters.getTargetDistance;
            creature.rushToTarget();
            const nNewDistance = creature.store.getters.getTargetDistance;
            this.addvCreatureTargetDistance({
                value: nNewDistance - nPrevDistance,
                creature,
                target
            });
            return;
        }
        if (critical) {
            this.broadcastCombatMessage(
                oAttacker,
                oTarget,
                'combat.hit.you.critical',
                'combat.hit.adv.critical',
                'combat.hit.room.critical',
                oContext
            );
            this.broadcastCombatMessage(
                oAttacker,
                oTarget,
                'combat.hit.you.damages',
                'combat.hit.adv.damagesBy',
                'combat.hit.room.damagesBy',
                oContext
            );
            return;
        }
        if (hit) {
            this.broadcastCombatMessage(
                oAttacker,
                oTarget,
                'combat.hit.you.regular',
                'combat.hit.adv.regular',
                'combat.hit.room.regular',
                oContext
            );
            this.broadcastCombatMessage(
                oAttacker,
                oTarget,
                'combat.hit.you.damages',
                'combat.hit.adv.damagesBy',
                'combat.hit.room.damagesBy',
                oContext
            );
            if (advantages.value) {
                const advrl = advantages.rules.length;
                const iAdv = Math.floor(Math.random() * advrl);
                const rule = iAdv < advrl ? advantages.rules[iAdv] : 'NONE';
                this.broadcastCombatMessage(
                    oAttacker,
                    oTarget,
                    'advantages.you.intro',
                    'advantages.adv.intro',
                    '',
                    {
                        rule
                    }
                );
            }
            return;
        } else if (disadvantages.rules) {
            if (disadvantages.value) {
                const disadvrl = disadvantages.rules.length;
                const iAdv = Math.floor(Math.random() * disadvrl);
                const rule = iAdv < disadvrl ? disadvantages.rules[iAdv] : 'NONE';
                this.broadcastCombatMessage(
                    oAttacker,
                    oTarget,
                    'disadvantages.you.intro',
                    'disadvantages.adv.intro',
                    '',
                    {
                        rule
                    }
                );
            }
        }

        if (dice === 1) {
            this.broadcastCombatMessage(
                oAttacker,
                oTarget,
                'combat.miss.you.critical',
                'combat.miss.adv.critical',
                'combat.miss.room.critical',
                oContext
            );
            return;
        }
        this.broadcastCombatMessage(
            oAttacker,
            oTarget,
            'combat.miss.you.regular',
            'combat.miss.adv.regular',
            'combat.miss.room.regular',
            oContext
        );
    }

    addvCreatureSavingThrow ({ creature, output }) {
        const oSource = this._getMudEntity(output.source);
        const oTarget = this._getMudEntity(creature);
        const oContext = {
            attacker: oSource.name,
            target: oTarget.name,
            ability: output.ability,
            roll: output.roll,
            bonus: output.bonus,
            value: output.value,
            dc: output.dc,
            context: output.circumstance > 0
                ? 'advantaged'
                : output.circumstance < 0
                    ? 'disadvantaged'
                    : '',
        };
        if (output.success) {
            this.broadcastCombatMessage(
                oTarget,
                oSource,
                'combat.savingThrow.you.action',
                'combat.savingThrow.adv.warning',
                '',
                oContext
            );
        } else {
            this.broadcastCombatMessage(
                oTarget,
                oSource,
                'combat.savingThrow.you.failure',
                'combat.savingThrow.adv.action',
                '',
                oContext
            );
        }
    }

    /**
     * Il faut transmettre cet évènement à l'entité
     * @param caster {Creature}
     * @param spell {string}
     * @param level {number}
     * @param parameters {*}
     */
    addvEntitySpellcastAt ({ caster, spell, level, parameters = undefined }) {
        let oAddvTarget = null;
        const oMUDCaster = this._getMudEntity(caster);
        if (parameters && ('item' in parameters)) {
            oAddvTarget = parameters.item;
        } else {
            oAddvTarget = caster.getTarget() || caster;
        }
        const oMUDTarget = this._getMudEntity(oAddvTarget);
        logDebug('sending spellcast-at event : spell %s from %s to %s', spell, oMUDCaster.name, oMUDTarget.name);
        this
            ._engine
            .triggerEntityEvent(oMUDTarget, 'spellcast-at', {
                spell,
                caster: oMUDCaster,
                target: oMUDTarget,
                parameters
            });
    }

    /**
     * Enregistre une creature qui a des effets mutables, afin de pouvoir mutationer ces effets de manière optimisée
     * @param effect {D20Effect}
     */
    addvCreatureEffectApplied ({ effect }) {
        const { type: sType, target, mutable, duration } = effect;
        if (mutable && duration > 0) {
            const oEntity = this._entities[target];
            if (!this._ewme.has(oEntity)) {
                this._ewme.add(oEntity);
                log('%s is now affected by %s for duration %d - %d creature(s) currently affected by mutable effect(s).', oEntity.name, sType, duration, this._ewme.size);
            }
        }
    }

    /**
     * toutes les créatures qui ont des effets mutables actifs sont priée de mutationer leurs effets mutables
     */
    processMutableEffects () {
        const ewme = this._ewme;
        const itewme = new Set(ewme);
        itewme.forEach(oCreature => {
            oCreature.processEffects();
            if (oCreature.store.getters.getEffects.some(eff => !eff.mutable)) {
                ewme.delete(oCreature);
                log('%s is no longer affected by mutable effects - %d creature(s) currently affected by mutable effect(s).', oCreature.name, this._ewme.size);
            }
        });
    }

    /**
     * Achemin différents messages à différent destinataire
     * @param oAttacker {MUDEntity}
     * @param oTarget {MUDEntity}
     * @param msgAttacker {string}
     * @param msgTarget {string}
     * @param msgRoom {string}
     * @param oContext {BroadcastI18nContext}
     * @param bPostpone {boolean} si true alors le message ne sera envoyé qu'après l'exécution de la boucle
     */
    broadcastCombatMessage (oAttacker, oTarget, msgAttacker, msgTarget, msgRoom = '', oContext, bPostpone = false) {
        if (bPostpone) {
            this._postponedBroadcasts.push({
                attacker: oAttacker,
                target: oTarget,
                msgAttacker,
                msgTarget,
                msgRoom,
                context: oContext
            });
            return;
        }
        // attacker
        this.sendText(oAttacker, msgAttacker, oContext);
        // target
        this.sendText(oTarget, msgTarget, oContext);
        // room
        if (msgRoom) {
            this._engine.getRoomPlayers(oTarget.location).forEach(p => {
                if (p !== oAttacker && p !== oTarget) {
                    this.sendText(p, msgRoom, oContext);
                }
            });
        }
    }

    /**
     *
     * @returns {CombatManager|null}
     */
    get combatManager () {
        return this._combatManager;
    }

    get events () {
        return this._events;
    }

    cmQuit (combat) {
        // log('combat ended : %s stop fighting', combat.fighter.name)
        this.log('%s is leaving combat - fighting entities: %d', combat.fighter.name, this._combatManager.combats.length);
    }

    cmCreated (combat) {
        const oAddvFighter = this.getAddvEntity(combat.fighter);
        const nDex = oAddvFighter.store.getters.getAbilityModifiers[this.CONSTS.ABILITY_DEXTERITY];
        const nInit = nDex + oAddvFighter.roll('1d20');
        combat.initiative.push(nInit, nDex, Math.random());
        this.log('%s is starting a combat - fighting entities: %d', combat.fighter.name, this._combatManager.combats.length);
    }

    describeActionCast (oMudFighter, oMudTarget, sAction) {
        const d = this.getMudEntityAddvData(oMudFighter);
        const {
            name: sActionName = sAction,
            desc: sActionDesc
        } = this.getMudEntityAddvString(oMudFighter, sAction);
        this.broadcastCombatMessage(
            oMudFighter,
            oMudTarget,
            'combat.sla.you.action.cast',
            'combat.sla.adv.warning.cast',
            'combat.sla.room.cast',
            {
                attacker: oMudFighter.name,
                target: oMudTarget.name,
                action: sActionName
            }
        );
        if (sActionDesc) {
            let bNeedToDescribe = true;
            if (this._combatManager.isCreatureFighting(oMudTarget)) {
                const combat = this._combatManager.getFighterCombat(oMudTarget);
                // explication de l'action
                if (!combat.data[COMBAT_DATA_VAR_USED_ACTIONS]) {
                    combat.data[COMBAT_DATA_VAR_USED_ACTIONS] = new Set();
                }
                if (combat.data[COMBAT_DATA_VAR_USED_ACTIONS].has(sAction)) {
                    bNeedToDescribe = false;
                } else {
                    combat.data[COMBAT_DATA_VAR_USED_ACTIONS].add(sAction);
                }
            }
            if (bNeedToDescribe) {
                this.sendText(oMudTarget, 'combat.sla.adv.info.cast', { desc: sActionDesc });
            }
        }
    }

    /**
     *
     * @param oFighter {MUDEntity}
     * @param oAtk {AttackOutcome}
     */
    manageAmmo (oFighter, oAtk) {
        const addvWeapon = oAtk.weapon;
        const addvAmmo = oAtk.ammo;
        if (!addvAmmo) {
            // pas de mun
            return;
        }
        if (!addvWeapon.attributes.includes(this.CONSTS.WEAPON_ATTRIBUTE_AMMUNITION)) {
            // pas une arme à mun
            return;
        }
        const sRequiredAmmo = addvWeapon.ammo?.type;
        const sProvidedAmmo = addvAmmo.ammoType;
        if (sRequiredAmmo !== sProvidedAmmo) {
            // Pas les mun qui correspondent
            console.error('an attack occurred with ammunition weapon improperly loaded ?');
            console.error(sRequiredAmmo, sProvidedAmmo);
            return;
        }
        const oMUDAmmo = this._getMudEntity(addvAmmo);
        const oMUDWeapon = this._getMudEntity(addvWeapon);
        if (!oMUDAmmo) {
            // sans doute des munitions de NPC inépuisables
            return;
        }
        if (this._engine.isItemStackable(oMUDAmmo)) {
            --oMUDAmmo.stack;
            if (oMUDAmmo.stack <= 0) {
                this.sendText(oFighter, 'combat.failure.outOfAmmo', {
                    weapon: this.getEntityName(addvWeapon),
                    ammo: this.getEntityName(addvAmmo)
                });
                this._engine.destroyEntity(oMUDAmmo);
            } else if (oMUDAmmo.stack < 9) {
                this.sendText(oFighter, 'combat.warning.ammoLeft', {
                    weapon: this.getEntityName(addvWeapon),
                    ammo: {
                        name: this.getEntityName(addvAmmo),
                        stack: oMUDAmmo.stack
                    }
                });
            }
        }
    }

    cmAttack (combat) {
        const { fighter, target } = combat;
        if (!this._combatManager.isCreatureFighting(target)) {
            this._combatManager.addFighter(target, fighter);
        }
        const addvFighter = this.getAddvEntity(fighter);
        const addvTarget = this.getAddvEntity(target);
        const oAtk = addvFighter.attack(addvTarget);
        this.manageAmmo(fighter, oAtk);
        return oAtk;
    }

    /**
     * Gestionnaire de lancement de spell like ability ou autres actions
     * @param combat
     * @param sAction
     * @returns {boolean}
     */
    cmAction (combat, sAction) {
        if (sAction) {
            const oMudFighter = combat.fighter;
            const oMudTarget = combat.target;
            const addvFighter = this.getAddvEntity(oMudFighter);
            const aActions = this.getAvailableActions(addvFighter);
            if (aActions.length > 0 && aActions.includes(sAction)) {
                // le message décoratif de l'action
                this.describeActionCast(oMudFighter, oMudTarget, sAction);
                addvFighter.action(sAction);
            } else {
                // Cette action n'existe pas.
                console.error(oMudFighter.name, 'action not available :', sAction);
                return false;
            }
            combat.action = '';
            return true;
        } else {
            return false;
        }
    }

    isCombatDone (fighter, target) {
        const addvFighter = this.getAddvEntity(fighter);
        const addvTarget = this.getAddvEntity(target);
        // addvFighter.processEffects() // sera géré dans le tick
        return fighter.location !== target.location ||
            addvFighter.store.getters.getHitPoints <= 0 ||
            addvTarget.store.getters.getHitPoints <= 0;
    }

    /**
     * Déclenche la procédure de mutation des effets des créatures
     */
    mutateAllEffects () {

    }

    getCombatRequestAttackCount (fighter) {
        return this.getAddvEntity(fighter).store.getters.getAttackCount;
    }

    /**
     * Certaine créature on droit à une action bonus
     * @param fighter
     */
    getCombatRequestBonusAction (fighter) {
        return false;
    }

    displayCombatState (combat) {
        const oMUDAttacker = combat.fighter;
        const oAddvAttacker = this.getAddvEntity(oMUDAttacker);
        const oMUDTarget = combat.target;
        const oAddvTarget = this.getAddvEntity(oMUDTarget);
        if (this._engine.isActor(oMUDAttacker)) {
            const fsg = oAddvAttacker.store.getters;
            const tsg = oAddvTarget.store.getters;
            const hpa = fsg.getHitPoints;
            const hpt = tsg.getHitPoints;
            // conditions
            const conda = [...fsg.getConditions];
            const condt = [...tsg.getConditions];
            const oContext = {
                turn: combat.turn,
                you: {
                    name: oMUDAttacker.name,
                    hp: hpa,
                    hpMax: fsg.getMaxHitPoints,
                    conditions: conda
                },
                adv: {
                    name: oMUDTarget.name,
                    hp: hpt,
                    hpMax: tsg.getMaxHitPoints,
                    conditions: condt
                }
            };
            this.sendText(oMUDAttacker, 'combat-stats', oContext);
        }
    }

    /**
     * Renvoie la liste des actions de la creature spécifiée
     * @param oAddvCreature
     * @returns {{innate: boolean, script: string, canUse: boolean, uses: {max: number, value: number}, action: string, clearUses: function, incUses: function, run: function }[]}
     */
    getAvailableActions (oAddvCreature) {
        return oAddvCreature
            .store
            .getters
            .getActions
            .map(a => {
                const uses = {
                    value: a.uses.value,
                    max: a.uses.max,
                };
                const script = a.script || '';
                return {
                    action: a.action,
                    innate: a.innate,
                    script,
                    uses,
                    canUse: a.uses.value < a.uses.max,
                    clearUses: () => {
                        oAddvCreature.store.mutations.setCounterValue({ counter: a.action, value: 0 });
                    },
                    incUses: () => {
                        const oCounter = oAddvCreature.store.getters.getCounters[a.action];
                        if (oCounter) {
                            oAddvCreature.store.mutations.setCounterValue({
                                counter: a.action,
                                value: oCounter.value + 1
                            });
                        }
                    },
                    run: target => {
                        const pScript = this._addv.assetManager.scripts[script];
                        if (pScript) {
                            pScript(oAddvCreature, target);
                        } else {
                            throw new Error('action cannot run this script : "' + script + '"');
                        }
                    }
                };
            });
    }

    /**
     * Effectue une action de combat
     * - l'action est intégré dans le flux du combat
     * - si l'action nécessite une cible, c'est la cible du combat qui est prise
     *
     * @param oMUDActor
     * @param sAction
     * @param bAsBonusAction {boolean} si true alors l'action sera une bonus action
     */
    doAction (oMUDActor, sAction, bAsBonusAction = false) {
        const cm = this.combatManager;
        const addvFighter = this.getAddvEntity(oMUDActor);
        const aActions = this.getAvailableActions(addvFighter);
        if (cm.isCreatureFighting(addvFighter) && aActions.includes(sAction)) {
            const combat = this.combatManager.getFighterCombat(oMUDActor);
            if (bAsBonusAction) {
                cm.requestBonusAction(combat, sAction);
            } else {
                combat.action = sAction;
            }
        }
        // pas de combat en cours : seule les actions de combats sont gérée ici pour le moment
    }

    doAttack (oMUDActor, oMUDTarget) {
        if (!this.getAddvEntity(oMUDActor)) {
            return null;
        }
        if (!this.getAddvEntity(oMUDTarget)) {
            return null;
        }
        this.combatManager.addFighter(oMUDActor, oMUDTarget);
        return true;
    }

    /**
     *
     * @param cm {CombatManager}
     */
    set combatManager (cm) {
        if (this._combatManager !== null) {
            throw new Error('One time setter !!');
        }
        this._combatManager = cm;
        cm.events.on('combat.quit', ({ combat }) => this.cmQuit(combat));
        cm.events.on('combat.created', ({ combat }) => this.cmCreated(combat));
        cm.events.on('combat.attack', ({ combat, turn }) => this.cmAttack(combat, turn));
        cm.events.on('combat.action', ({ combat, done, action }) => {
            this.cmAction(combat, action);
        });
        cm.events.on('combat.loop', ({ cancel, combat: { fighter, target, turn, tick } }) => {
            if (this.isCombatDone(fighter, target)) {
                cancel();
            }
        });
        cm.events.on('combat.requestAttackCount', ({ fighter, result }) => {
            result(this.getCombatRequestAttackCount(fighter));
        });
        cm.events.on('combat.requestBonusAction', ({ fighter, result}) => {
            result(this.getCombatRequestBonusAction(fighter));
        });
        cm.events.on('combat.tick.end', () => {
            while (this._postponedBroadcasts.length > 0) {
                const { attacker, target, msgAttacker, msgTarget, msgRoom, context } = this._postponedBroadcasts.shift();
                this.broadcastCombatMessage(attacker, target, msgAttacker, msgTarget, msgRoom, context);
            }
        });
        cm.events.on('combat.turn.end', () => {
            cm.combats.forEach(c => {
                this.displayCombatState(c);
                const oMUDEntity = c.fighter;
                const oMUDOpponent = c.target;
                if (!this._engine.isPlayer(oMUDEntity)) {
                    this._engine.triggerEntityEvent(oMUDEntity, 'think', {
                        interactor: oMUDOpponent
                    });
                }
            });
        });
    }

    /**
     * @param addvEntity {Creature|D20Item}
     * @return {MUDEntity}
     */
    _getMudEntity (addvEntity) {
        if (!(addvEntity instanceof Creature)) {
            if (addvEntity.entityType !== ADDV_CONSTS.ENTITY_TYPE_ITEM) {
                throw new TypeError('Not a ADDV Creature / Not a ADDV Item');
            }
        }
        return (('id' in addvEntity) && this._engine.isEntityExist(addvEntity.id))
            ? this._engine.getEntity(addvEntity.id)
            : null;
    }

    /**
     * @param addvEntity {Creature|D20Item}
     * @return {MUDEntity}
     */
    getEntityName (addvEntity) {
        const e = this._getMudEntity(addvEntity);
        if (e) {
            return e.name;
        } else if (addvEntity.entityType === this.CONSTS.ENTITY_TYPE_ITEM) {
            return this.getGenericItemName(addvEntity);
        } else {
            return '';
        }
    }

    output (oDestinataire, sMessage, oVariable) {

    }

    get CONSTS () {
        return ADDV_CONSTS;
    }

    get namespace() {
        return ADDV_NS;
    }

    log (...args) {
        if (this._verbose > 0) {
            log(...args);
        }
    }

    logDebug(...args) {
        if (this._verbose > 1) {
            if (this._verbose > 2) {
                console.debug(...args);
            }
            logDebug(...args);
        }
    }

    set verbose (value) {
        if (value >= 0 && value <= 3) {
            this._verbose = value;
        } else {
            throw new Error('addv connector - verbose level value may be : 0 (no verbose) 1 (log) 2 (debug)');
        }
    }

    get verbose () {
        return this._verbose;
    }

    init ({ system, engine }) {
        this.log('ADDV-SRD - Dungeons & Dragons 5e (SRD/OGL)');
        this.log('Implementation of the D&D5 combat rules described in the System Reference Document 5.1 (SRD) under Open Gaming License');
        this.log('Open Game License v1.0a © 2000, Wizards of the Coast, LLC.');
        this.logDebug('init - debug is on');
        this._addv.init();
        this._evolution.data = this.data;
        this.log('init - addv-srd is initialized');
        this._system = system;
        this._engine = engine;
    }

    getMudEntityDisplayName (oMudEntity) {
        const name = oMudEntity.name;
        const id = oMudEntity.id;
        return `[${name} (${id})]`;
    }

    /**
     * Récupération des données du namespace d'addv
     * @param oMudEntity {MUDEntity}
     * @returns {object}
     */
    getMudEntityAddvData (oMudEntity) {
        return oMudEntity.data[this.namespace];
    }

    getMudEntityAddvString (oMudEntity, strref) {
        const d = this.getMudEntityAddvData(oMudEntity);
        if (d && d.strings && d.strings[strref]) {
            return d.strings[strref];
        } else {
            return {};
        }
    }

    /**
     * Permet de récupérer l'instance de l'entité associée au MudEntity spécifié
     * @param oMudEntity {MUDEntity}
     * @return {Creature|object|null}
     */
    getAddvEntity (oMudEntity) {
        if (this._engine.isActor(oMudEntity) || this._engine.isItem(oMudEntity)) {
            const id = oMudEntity.id;
            if (id) {
                return this._entities[id] || null;
            } else {
                this.logDebug('getAddvEntity - mud-entity has no id -> returning null');
                return null;
            }
        } else {
            // ni un actor ni un item
            this.logDebug('getAddvEntity - [%s (%s)] mud-entity is neither actor nor item -> returning null', oMudEntity.name, oMudEntity.id);
            return null;
        }
    }

    /**
     * Désenregistre une entité
     * @param oMudEntity {MUDEntity}
     */
    unregisterEntity (oMudEntity) {
        const oAddvEntityLeaving = this.getAddvEntity(oMudEntity);
        if (oAddvEntityLeaving) {
            this.logDebug('unregisterEntity - mud-entity %s unregistered.', this.getMudEntityDisplayName(oMudEntity));
            if (this._engine.isActor(oMudEntity)) {
                this._combatManager.declareDeadFighter(oMudEntity);
                Object
                    .values(this._entities)
                    .filter(ent => ent.entityType === ADDV_CONSTS.ENTITY_TYPE_ACTOR && ent !== oAddvEntityLeaving)
                    .forEach(oEntity => {
                        /**
                         * @type {EffectProcessor}
                         */
                        const ep = oEntity.effectProcessor;
                        ep.removeCreatureFromRegistry(oEntity, oAddvEntityLeaving);
                    });
            } else if (this._engine.isItem(oMudEntity)) {
                // cet item est-il équipé par un actor ?
                const oOwner = this._engine.getItemOwner(oMudEntity);
                if (oOwner && this._engine.isActor(oOwner)) {
                    const oAddvOwner = this.getAddvEntity(oOwner);
                    const slot = this.getItemEquipmentSlot(oOwner, oMudEntity);
                    if (slot) {
                        // On bypass le curse, car l'item est détruit
                        oAddvOwner.unequipItem(slot, true);
                    }
                    oAddvOwner.store.mutations.setEncumbrance({ value: oOwner.weight });
                }
            }
            delete this._entities[oMudEntity.id];
        } else {
            this.logDebug('unregisterEntity - attempt to unregister non-registered entity %s.', this.getMudEntityDisplayName(oMudEntity));
        }
    }

    /**
     * Associe une entité plasmud à une entité addv. Pour dissocier, utiliser unregisterEntity
     * @param oMudEntity {MUDEntity}
     * @param oAddvEntity {Creature | D20Item}
     * @returns {Creature | D20Item}
     */
    bindAddvEntity (oMudEntity, oAddvEntity) {
        return this._entities[oMudEntity.id] = oAddvEntity;
    }

    /**
     * Créé une entité addv à partir d'un Blueprint et l'enregistre
     * @param oBlueprint
     * @param oMudEntity
     * @returns {Creature|D20Item}
     */
    createEntityAndRegister (oBlueprint, oMudEntity) {
        const oAddvEntity = this._addv.createEntity(oBlueprint);
        oAddvEntity.id = oMudEntity.id;
        oAddvEntity.name = oMudEntity.name;
        if (oAddvEntity.weight) {
            oMudEntity._baseWeight = oAddvEntity.weight;
        }
        return this.bindAddvEntity(oMudEntity, oAddvEntity);
    }

    /**
     * creation d'une entity addv à associer à la mud entity spécifiée
     * @param oMudEntity {MUDEntity}
     */
    generateAddvEntityFromMud (oMudEntity) {
        const entity = this.getAddvEntity(oMudEntity);
        if (entity) {
            this.logDebug('registerEntity - %s already registered . This is weird.', oMudEntity.id);
            // déja enregistrer avec cet id
            return null;
        }
        // déterminer si l'entité spécifiée possède les méta data nécessaire
        if (!oMudEntity.id) {
            // pas d'identifiant dans cet entité
            this.logDebug('registerEntity - mud-entity has no id. Should be an error.');
            throw new Error('Addv-connector.registerEntity - Cannot register mud-entity : has no id');
        }
        const entityDisplayName = this.getMudEntityDisplayName(oMudEntity);
        const oAddvData = this.getMudEntityAddvData(oMudEntity);
        if (oAddvData) {
            // déterminer si c'est un ref ou un blueprints
            if ('blueprint' in oAddvData) {
                this.logDebug('registerEntity - blueprints available in mud-entity %s data. Creating addv-entity.', entityDisplayName);
                this.log('creating addv entity %s', entityDisplayName);
                return this.createEntityAndRegister(oAddvData.blueprint, oMudEntity);
            } else {
                // on ne peut pas créer sans données
                this.logDebug('registerEntity - addv data present in mud-entity %s, but no blueprints available. Cannot create addv-entity.', entityDisplayName);
                throw new Error('Addv-connector.registerEntity - Cannot register mud-entity : no blueprints definition in addv.data');
            }
        } else {
            // pas de namespace
            // on ne traite que les entités joueurs
            if (this._engine.isPlayer(oMudEntity)) {
                this.logDebug('registerEntity - %s fresh new player entity.', entityDisplayName);
                this.log('registering new player %s', entityDisplayName);
                const oPlayerBaseBlueprint = this._system.getAsset('data', 'addv-player-base-blueprint');
                return this.createEntityAndRegister(oPlayerBaseBlueprint, oMudEntity);
            } else {
                // ce n'est pas une entité que l'on peut gérer
                this.logDebug('registerEntity - %s cannot be registered : has no addv-data and is not a player.', entityDisplayName);
            }
        }
        return null;
    }

    _moulinette (object, callback) {
        return shallowMap(object, (value, key) => callback(value, key));
    }

    /**
     * Sauvegarde un player
     * @param oMudEntity {MUDEntity}
     */
    exportEntity (oMudEntity) {
        const oAddvEntity = this.getAddvEntity(oMudEntity);
        if (oAddvEntity) {
            this.logDebug('saving %s', this.getMudEntityDisplayName(oMudEntity));
            if (oAddvEntity instanceof Creature) {
                const oState = this._addv.exportCreature(oAddvEntity);
                oState.equipment = this._moulinette(
                    oState.equipment,
                    it => it === null
                        ? null
                        : 'id' in it
                            ? it.id
                            : it
                );
                return oState;
            } else {
                return oAddvEntity;
            }
        } else {
            this.logDebug('mud entity %s is not registered as addv entity, and will not be saved', this.getMudEntityDisplayName(oMudEntity));
        }
    }

    /**
     * Transfère les données enregistrées dans la mud entité vers le registre de l'addv
     * Ne gère que les Items
     * @param oMudEntity {MUDEntity}
     */
    importItem (oMudEntity) {
        this._entities[oMudEntity.id] = {
            id: oMudEntity.id,
            ...this.getMudEntityAddvData(oMudEntity).entity
        };
    }

    /**
     *
     * @param oMudEntity {MUDEntity}
     */
    importCreature (oMudEntity) {
        const data = this.getMudEntityAddvData(oMudEntity)?.entity;
        if (data) {
            data.effects = data.effects.filter(eff => eff.source in this._entities);
            data.equipment = this._moulinette(
                data.equipment,
                it => {
                    return ((typeof it === 'string') && (it in this._entities))
                        ? this._entities[it]
                        : (typeof it === 'object')
                            ? it
                            : null;
                }
            );
            try {
                this._entities[oMudEntity.id] = this._addv.importCreature(this.getMudEntityAddvData(oMudEntity).entity);
            } catch (e) {
                console.error(e);
                this.log('The persisted data of ' + this.getMudEntityDisplayName(oMudEntity) + ' is not valid. Maybe data is of an old version.');
            }
        } else {
            throw new Error('No import data in this entity');
        }
    }

    /**
     * Transfère les données de la source vers la destination
     * source : MudEntity.data.addv.entity
     * destination : this._entity[MudEntity.id]
     * Pour que cela fonctionne correctement ilfaut que la mud entity ait un id
     * et une section addv dans ses data.
     * @param oMudEntity {MUDEntity}
     */
    importEntity (oMudEntity) {
        const addvData = this.getMudEntityAddvData(oMudEntity);
        const sDispName = this.getMudEntityDisplayName(oMudEntity);
        const oEntity = addvData?.entity;
        if (this._engine.isActor(oMudEntity)) {
            this.logDebug('restoring player addv entity %s.', sDispName);
            if (oEntity) {
                this.importCreature(oMudEntity);
            } else {
                this.logDebug('no addv data in the player entity %s.', sDispName);
            }
        } else if (this._engine.isItem(oMudEntity)) {
            if (oEntity) {
                this.logDebug('restoring item addv entity %s.', sDispName);
                this.importItem(oMudEntity);
            } else {
                this.logDebug('item addv entity %s has no state to restore.', sDispName);
            }
        } else {
            throw new Error('importEntity : invalid import data (not designing player or item)');
        }
    }

    /**
     * Renvoie le slot d'equipement de l'item sur la créature
     * @param creature {Creature}
     * @param item {D20Item}
     * @return {string}
     */
    getItemEquipmentSlot (creature, item) {
        const oAddvCreature = this.getAddvEntity(creature);
        const oAddvItem = this.getAddvEntity(item);
        if (oAddvCreature && oAddvItem) {
            const eq = oAddvCreature.store.getters.getEquippedItems;
            for (const [slot, oEqItem] of Object.entries(eq)) {
                if (oEqItem && oEqItem.id === oAddvItem.id) {
                    return slot;
                }
            }
        }
        return '';
    }
}

module.exports = AddvConnector;
