{
  "debug": {
    "levelup": "⚙ {{ name }} est maintenant niveau {{ level }}.",
    "heal": "⚙ {{ name }} retrouve toute sa santé."
  },
  "cmdAction": {
    "failure": {
      "unknown": "Action inconnue {{ action }}.",
      "depleted": "Nombre de tentatives épuisé pour cette action {{ action }}."
    },
    "action": {
      "runOnTarget": "Vous effectuez une action : {{ action }} sur {{ target }}.",
      "runOnSelf": "Vous effectuez une action : {{ action }}."
    }
  },
  "equip": {
    "action": {
      "equipped": "Vous vous équipez de {{ item }}."
    },
    "room": {
      "equipped": "{{ name }} s'équipe de {{ item }}."
    },
    "failure": {
      "equipped": "{{ item }} n'est pas un objet dont on peut s'équiper.",
      "cursedItem": "Vous ne pouvez pas vous équiper de {{ item }} car l'objet actuellement équipé : {{ cursedItem }}, est maudit !"
    }
  },
  "drop": {
    "failure": {
      "cursedItem": "{{ item }} ne peut pas être abandonné : c'est un objet maudit !"
    }
  },
  "give": {
    "failure": {
      "cursedItem": "{{ item }} ne peut pas être donné : c'est un objet maudit !"
    }
  },
  "unequip": {
    "action": {
      "unequipped": "Vous retirez {{ item }} de votre équipement."
    },
    "room": {
      "unequipped": "{{ name }} retire {{ item }} de son équipement."
    },
    "failure": {
      "unequipped": "{{ item }} ne fait pas partie de votre équipement.",
      "cursedItem": "Vous ne pouvez pas retirer {{ item }} car l'objet est maudit !"
    }
  },
  "kill": {
    "failure": {
      "invalidTarget": "{{ name }} n'est pas considéré(e) comme une cible combattante valide."
    }
  },
  "inventory": {
    "itemUnidentified": "L'objet n'est pas identifié.",
    "equipmentSlot": "$t(addv.equipmentSlot.{{ slot }})",
    "weaponType": "$t(addv.weaponType.{{ weaponType }})",
    "armorType": "$t(addv.armorType.{{ armorType }})",
    "shieldType": "$t(addv.shieldType.{{ shieldType }})",
    "ammoType": "$t(addv.ammoType.{{ ammoType }})",
    "weaponMaterial": "$t(addv.material.{{ material }})",
    "weaponDamageType": "$t(addv.damageType.{{ damageType }})",
    "itemProperty": "$t(inventory.itemProperties.{{ property }})",
    "itemPropertyList": "- $t(addv.itemProperty.{{ property }}) $t(inventory.itemProperties.{{ property }})",
    "itemProperties": {
      "ITEM_PROPERTY_ABILITY_BONUS": "$t(addv.ability.{{ data.ability }}): {{ amp }}",
      "ITEM_PROPERTY_AC_BONUS": ": {{ amp }}",
      "ITEM_PROPERTY_ADVANTAGE": "",
      "ITEM_PROPERTY_ATTACK_BONUS": ": {{ amp }}",
      "ITEM_PROPERTY_CONDITION_IMMUNITY": ": $t(addv.condition.{{ data.condition }})",
      "ITEM_PROPERTY_CRITICAL_THREAT": ": {{ 20 - amp }}-20",
      "ITEM_PROPERTY_DAMAGE_BONUS": ": {{ amp }} $t(addv.damageType.{{ data.type }})",
      "ITEM_PROPERTY_DAMAGE_IMMUNITY": ": $t(addv.damageType.{{ data.type }})",
      "ITEM_PROPERTY_DAMAGE_REDUCTION": ": {{ amp }} $t(addv.damageType.{{ data.type }})",
      "ITEM_PROPERTY_DAMAGE_RESISTANCE": ": $t(addv.damageType.{{ data.type }})",
      "ITEM_PROPERTY_DAMAGE_VULNERABILITY": ": $t(addv.damageType.{{ data.type }})",
      "ITEM_PROPERTY_DISADVANTAGE": "",
      "ITEM_PROPERTY_ENHANCEMENT": ": {{ amp }}",
      "ITEM_PROPERTY_MASSIVE_CRITICAL": ": {{ amp }}",
      "ITEM_PROPERTY_MATERIAL_VULNERABILITY": ": $t(addv.material.{{ data.type }})",
      "ITEM_PROPERTY_MELLE_ATTACK_BONUS": ": {{ amp }}",
      "ITEM_PROPERTY_PHARMA": "",
      "ITEM_PROPERTY_RANGED_ATTACK_BONUS": ": {{ amp }}",
      "ITEM_PROPERTY_REGEN": "{{ amp }} PV/tour",
      "ITEM_PROPERTY_REROLL": "",
      "ITEM_PROPERTY_SAVING_THROW_BONUS": "{{ amp }}",
      "ITEM_PROPERTY_SKILL_BONUS": "$t(addv.skill.{{ data.skill }}): {{ amp }}"
    }
  },
  "consider": {
    "noone": "",
    "look": "Vous considérez un éventuel affrontement avec {{ name }} (cr {{ cr }} vs. {{ crme }}).",
    "result": "$t(consider.challenges.{{ index }})",
    "challenges": [
      "Il n'y a aucune difficulté à affronter un adversaire de la sorte, ni aucune gloire d'ailleurs.",
      "Il n'y a aucune difficulté à affronter un adversaire de la sorte, ni aucune gloire d'ailleurs.",
      "Ce genre d'adversaire est inoffensif, même à plusieurs ils ne pourraient pas vous vaincre.",
      "C'est un combat facile. A condition de ne pas faire fuir votre adversaire.",
      "Vous allez remporter ce combat si vous affrontez cet adversaire. Cela ne fait aucun doute.",
      "Ce sera un combat facile, votre adversaire semble faible.",
      "Vous avez de grandes chances de gagner face à cet adversaire.",
      "Vous pensez que vous avez vos chances face à cet adversaire. Il n'a pas l'air aussi fort que çà.",
      "Cet adversaire semble être moins solide que vous. Mais il faut parfois faire preuve de méfiance.",
      "Vous pensez que vous pourriez avoir le dessus lors d'un combat. Néanmoins cela reste risqué de l'attaquer.",
      "Cet adversaire semble être de niveau comparable au vôtre. L'issue d'un éventuel combat est incertaine.",
      "Cet adversaire présente un certain défi. Il vous faudra faire preuve de prudence.",
      "Il n'est pas conseillé d'affronter cet adversaire sans aide.",
      "Il serait dangereux d'affronter cet adversaire sans aide.",
      "Cet adversaire est très probablement dangereux. Ca risque de piquer quand il va vous taper.",
      "Cet adversaire est dangereux, vos chances de survie son minces.",
      "Vous pourriez vaincre cet adversaire avec une bonne équipe, mais seul(e)... oubliez.",
      "Cet adversaire ne fera qu'une bouchée de vous. Fuyez avant de vous faire défoncer.",
      "Autant creuser votre tombe tout de suite, cet adversaire va vous écraser.",
      "Gagner ce combat serait tout bonnement miraculeux.",
      "Gagner ce combat serait tout bonnement miraculeux"
    ]
  },
  "abilities": {
    "ABILITY_STRENGTH": "Force",
    "ABILITY_DEXTERITY": "Dexterité",
    "ABILITY_CONSTITUTION": "Constitution",
    "ABILITY_INTELLIGENCE": "Intelligence",
    "ABILITY_WISDOM": "Sagesse",
    "ABILITY_CHARISMA": "Charisme"
  },
  "score": {
    "exhaustionColor": [
      "{{ style.pos }}",
      "{{ style.warn }}",
      "{{ style.warn }}",
      "{{ style.warn }}",
      "{{ style.warn }}",
      "{{ style.neg }}"
    ],
    "moreinfo": {
      "notProficientWeapon": "Ne maîtrise pas l'arme actuelle",
      "notProficientArmor": "Equipment défensif contraignant",
      "exhaustionLevel": "Niveau de fatigue : $t(score.exhaustionColor.{{ value }}){{ value }}{{ style.end }}",
      "rangedWeaponNotLoaded": "L'arme à distance équipée n'a pas de munition adaptée",
      "condition": "Affliction : $t(addv.condition.{{ condition }})",
      "noWeapon": "Pas d'arme équipée"
    }
  },
  "combat": {
    "styles": {
      "move": "{{ style.info }}⮕",
      "moveRoom": "⮕",
      "youHit": "\uD83C\uDFAF {{ style.pos }}",
      "youAreHit": "\uD83D\uDC80 $t(combat.styles.danger)",
      "youAreDead": "\uD83D\uDC80 {{ style.neg }}{{ style.strong }}",
      "numeric": "[#fff]{{ style.strong }}",
      "damages": "$t(combat.styles.numeric){{ damageAmount }}[#:] - {{ damages }}",
      "danger": "[#f20]",
      "combat": "⚔",
      "miss": "{{ style.warn }}$t(combat.styles.combat)"
    },
    "savingThrow": {
      "you": {
        "premfail": "Vous ratez votre jet de sauvegarde de $t(addv.ability.{{ ability }}) {{ roll }} + {{ bonus }} = {{ value }} vs. {{ dc }}",
        "premsucc": "Vous réussissez votre jet de sauvegarde de $t(addv.ability.{{ ability }}) {{ roll }} + {{ bonus }} = {{ value }} vs. {{ dc }}",
        "failure": "$t(combat.savingThrow.you.premfail).",
        "failure_advantaged": "$t(combat.savingThrow.you.premfail) (avantagé).",
        "failure_disadvantaged": "$t(combat.savingThrow.you.premfail) (désavantagé).",
        "action": "$t(combat.savingThrow.you.premsucc).",
        "action_advantaged": "$t(combat.savingThrow.you.premsucc) (avantagé).",
        "action_disadvantaged": "$t(combat.savingThrow.you.premsucc) (désavantagé)."
      },
      "adv": {
        "premfail": "{{ target }} rate son jet de sauvegarde de $t(addv.ability.{{ ability }}) {{ roll }} + {{ bonus }} = {{ value }} vs. {{ dc }}",
        "premsucc": "{{ target }} réussi son jet de sauvegarde de $t(addv.ability.{{ ability }}) {{ roll }} + {{ bonus }} = {{ value }} vs. {{ dc }}",
        "action": "$t(combat.savingThrow.adv.premfail).",
        "action_advantaged": "$t(combat.savingThrow.adv.premfail) (avantagé).",
        "action_disadvantaged": "$t(combat.savingThrow.adv.premfail) (désavantagé).",
        "warning": "$t(combat.savingThrow.adv.premsucc).",
        "warning_advantaged": "$t(combat.savingThrow.adv.premsucc) (avantagé).",
        "warning_disadvantaged": "$t(combat.savingThrow.adv.premsucc) (désavantagé)."
      }
    },
    "sla": {
      "you": {
        "action": {
          "cast": "Vous effectuez une action : {{ action }}"
        }
      },
      "adv": {
        "warning": {
          "cast": "$t(combat.styles.combat) {{ attacker }} effectue une action : {{ action }}"
        },
        "info": {
          "cast": "{{ desc }}"
        }
      },
      "room": {
        "cast": "$t(combat.styles.combat) {{ attacker }} effectue une action : {{ action }}, sur {{ target }}"
      }
    },
    "damageType": "$t(addv.damageType.{{ type }})",
    "action": {
      "start": "$t(combat.styles.combat) Vous engagez le combat avec {{ target }}."
    },
    "warning": {
      "ammoLeft": "$t(combat.styles.combat) Munitions de {{ weapon }}: Il reste {{ ammo.stack }} x {{ ammo.name }}."
    },
    "failure": {
      "outOfAmmo": "{{ weapon }} n'a plus de {{ ammo }}."
    },
    "attack": {
      "you": {
        "offensiveSlot": "$t(combat.styles.combat) Vous dégainez votre {{ weapon }}.",
        "attacking_weapon": "Vous attaquez {{ target }} avec votre {{ weapon }}",
        "attacking": "Vous attaquez {{ target }}"
      },
      "adv": {
        "offensiveSlot": "$t(combat.styles.combat) {{ attacker }} dégaine {{ weapon }}.",
        "attacking_weapon": "{{ attacker }} vous attaque avec {{ weapon }}",
        "attacking": "{{ attacker }} vous attaque"
      },
      "room": {
        "offensiveSlot": "$t(combat.styles.combat) {{ attacker }} dégaine {{ weapon }}.",
        "attacking_weapon": "{{ attacker }} attaque {{ target }} avec {{ weapon }}",
        "attacking": "{{ attacker }} attaque {{ target }}"
      }
    },
    "move": {
      "you": {
        "left": "$t(combat.styles.move) Vous avez fuit le combat contre {{ target }}.",
        "rushInReach": "$t(combat.styles.move) Vous vous positionnez pour attaquer {{ target }}.",
        "rushNextTurn": "$t(combat.styles.move) Vous foncez vers {{ target }}, qui sera à portée de votre arme au tour suivant.",
        "rushFar": "$t(combat.styles.move) Vous vous rapprochez de {{ target }}, qui sera à portée de votre arme dans {{ turns }} tours."
      },
      "adv": {
        "left": "$t(combat.styles.move) {{ attacker }} à pris la fuite en quittant la zone.",
        "rushInReach": "$t(combat.styles.move) {{ attacker }} se positionne pour vous attaquer.",
        "rushNextTurn": "$t(combat.styles.move) {{ attacker }} se rue vers vous. Vous serez à portée de son arme au tour suivant.",
        "rushFar": "$t(combat.styles.move) {{ attacker }} se rue vers vous. Vous serez à portée de son arme dans {{ turns }} tours."
      },
      "room": {
        "rushing": "$t(combat.styles.moveRoom) {{ attacker }} se rue vers {{ target }}."
      }
    },
    "hit": {
      "you": {
        "victory": "$t(combat.styles.youHit)[:b]Vous avez vaincu : {{ target }}.",
        "fatal": "$t(combat.styles.youHit)Vous portez un coup fatal à {{ target }}.",
        "critical": "$t(combat.styles.youHit)$t(combat.attack.you.attacking), et vous lui portez un coup dévastateur !",
        "regular": "$t(combat.styles.youHit)$t(combat.attack.you.attacking), et vous lui portez un coup.",
        "damages": "$t(combat.styles.youHit)Vous infligez des dégâts à {{ target }} : $t(combat.styles.damages)."
      },
      "adv": {
        "victory": "$t(combat.styles.youAreDead){{ attacker }} vous a vaincu !",
        "fatal": "$t(combat.styles.youAreHit){{ attacker }} vous a porté un coup fatal.",
        "critical": "$t(combat.styles.youAreHit)$t(combat.attack.adv.attacking) et vous inflige un coup dévastateur !",
        "regular": "$t(combat.styles.youAreHit)$t(combat.attack.adv.attacking) et vous inflige un coup.",
        "damages": "$t(combat.styles.youAreHit)Vous subissez des dégâts : $t(combat.styles.damages).",
        "damagesBy": "$t(combat.styles.youAreHit)Vous subissez des dégâts infligés par {{ attacker }} : $t(combat.styles.damages)."
      },
      "room": {
        "critical": "$t(combat.styles.combat) $t(combat.attack.room.attacking) et lui porte un coup critique !",
        "regular": "$t(combat.styles.combat) $t(combat.attack.room.attacking) et lui porte un coup.",
        "damages": "$t(combat.styles.combat) {{ target }} subit des dégâts : $t(combat.styles.damages).",
        "damagesBy": "$t(combat.styles.combat) {{ target }} subit des dégâts infligés par {{ attacker }} : $t(combat.styles.damages).",
        "victory": "$t(combat.styles.combat) {{ attacker }} assène un coup fatal à {{ target }}."
      }
    },
    "miss": {
      "you": {
        "critical": "$t(combat.styles.miss) $t(combat.attack.you.attacking), mais vous ratez complètement votre coup !",
        "regular": "$t(combat.styles.miss) $t(combat.attack.you.attacking), mais $t(combat.deflector.you.{{ deflector }}).",
        "outOfRange": "$t(combat.styles.miss) {{ target }} n'est pas à portée d'arme."
      },
      "adv": {
        "critical": "$t(combat.styles.miss) $t(combat.attack.adv.attacking), mais rate complètement son coup !",
        "regular": "$t(combat.styles.miss) $t(combat.attack.adv.attacking), mais $t(combat.deflector.adv.{{ deflector }}).",
        "outOfRange": "$t(combat.styles.miss) $t(combat.attack.adv.attacking) de loin mais ne peut pas vous atteindre."
      },
      "room": {
        "outOfRange": "$t(combat.styles.combat) {{ attacker }} se déplace pour se mettre à portée de {{ target }} et l'attaquer.",
        "critical": "$t(combat.styles.combat) $t(combat.attack.room.attacking) mais rate complètement son coup.",
        "regular": "$t(combat.styles.combat) $t(combat.attack.room.attacking), mais $t(combat.deflector.room.{{ deflector }})."
      }
    },
    "deflector": {
      "you": {
        "miss": [
          "le coup manque de précision",
          "le coup est mal orienté",
          "le coup est mal ajusté",
          "votre tentative échoue lamentablement",
          "vous visez à côté",
          "vous ne faîtes que brasser de l'air",
          "vous n'arrivez pas à toucher votre cible",
          "vous ratez votre cible",
          "vous manquez votre cible",
          "sans succès"
        ],
        "armor": "le coup est trop faible pour transpercer son armure",
        "shield": "le coup est dévié par son bouclier",
        "dexterity": "le coup est esquivé avec agilité",
        "properties": "le coup est dévié par son équipement défensif",
        "effects": "le coup semble amorti comme par magie, et {{ target }} s'en sort indemne"
      },
      "adv": {
        "miss": [
          "le coup manque de précision",
          "le coup est mal orienté",
          "le coup est mal ajusté",
          "sa tentative échoue lamentablement",
          "vise à côté",
          "ne fait que brasser de l'air",
          "sans vous toucher",
          "vous rate",
          "vous manque",
          "sans succès"
        ],
        "armor": "le coup est trop faible pour transpercer votre armure",
        "shield": "le coup est dévié par votre bouclier",
        "dexterity": "vous esquivez brillamment le coup",
        "properties": "le coup est dévié par votre équipement défensif",
        "effects": "le coup semble amorti comme par magie, vous vous en sortez indemne"
      },
      "room": {
        "miss": [
          "le coup manque de précision",
          "le coup est mal orienté",
          "le coup est mal ajusté",
          "sa tentative échoue lamentablement",
          "vise à côté",
          "ne fait que brasser de l'air",
          "sans toucher sa cible",
          "rate",
          "manque sa cible",
          "sans succès"
        ],
        "armor": "le coup est trop faible pour transpercer l'armure de {{ target }}",
        "shield": "le coup est dévié par le bouclier de {{ target }}",
        "dexterity": "{{ target }} esquive brillamment le coup",
        "properties": "le coup est dévié par l'équipement défensif de {{ target }}",
        "effects": "le coup semble amorti comme par magie, {{ target }} s'en sort indemne"
      }
    }
  },
  "advantages": {
    "you": {
      "intro": "{{ styles.pos }}$t(combat.styles.combat) Vous bénéficiez d'un avantage : $t(advantages.you.{{ rule }}).",
      "TARGET_CHARMED": "L'adversaire est charmé",
      "TARGET_INCAPACITATED": "L'adversaire est sans défense",
      "UNDETECTED": "L'adversaire ne vous détecte pas",
      "TARGET_PRONE_AND_CLOSE": "L'adversaire est à terre",
      "EQUIPMENT": "Votre équipement vous avantage"
    },
    "adv": {
      "intro": "$t(combat.styles.danger)$t(combat.styles.combat) Votre adversaire est avantagé : $t(advantages.adv.{{ rule }}).",
      "TARGET_CHARMED": "Vous subissez un effet de charme",
      "TARGET_INCAPACITATED": "Vous êtes sans défense",
      "UNDETECTED": "Vous ne détectez pas votre adversaire",
      "TARGET_PRONE_AND_CLOSE": "Vous êtes à terre",
      "EQUIPMENT": "Son équipement lui procure un avantage"
    }
  },
  "disadvantages": {
    "you": {
      "intro": "$t(combat.styles.danger)$t(combat.styles.combat) Vous écopez d'un désavantage : $t(disadvantages.you.{{ rule }}).",
      "NONE": "Origine inconnue",
      "EXHAUSTION_LEVEL_3": "Épuisement",
      "NON_PROFICIENT_ARMOR_SHIELD": "Equipment défensif inadapté",
      "TARGET_UNSEEN": "Adversaire indétectable",
      "POISONED": "Empoisonnement",
      "PRONE": "A terre",
      "TARGET_PRONE_AND_FAR": "A terre, à distance",
      "FRIGHTENED": "Terreur",
      "RESTRAINED": "Enchevêtrement",
      "HEAVY_WEAPON": "Arme trop lourde",
      "HEAVILY_ENCUMBERED": "Inventaire trop lourd",
      "AREA_UNDERWATER": "Combat sous-marin",
      "TARGET_TOO_CLOSE": "Arme inadaptée au combat rapproché"
    },
    "adv": {
      "intro": "$t(combat.styles.combat) Votre adversaire écope d'un désavantage : $t(disadvantages.adv.{{ rule }})",
      "EXHAUSTION_LEVEL_3": "Épuisement",
      "NON_PROFICIENT_ARMOR_SHIELD": "Equipment défensif inadapté",
      "TARGET_UNSEEN": "Vous êtes indétectable",
      "POISONED": "Empoisonnement",
      "PRONE": "A terre",
      "TARGET_PRONE_AND_FAR": "A terre, à distance",
      "FRIGHTENED": "Terreur",
      "RESTRAINED": "Enchevêtrement",
      "HEAVY_WEAPON": "Arme trop lourde",
      "HEAVILY_ENCUMBERED": "Inventaire trop lourd",
      "AREA_UNDERWATER": "Combat sous-marin",
      "TARGET_TOO_CLOSE": "Arme inadaptée au combat rapproché"
    }
  },
  "damageDisplay": {
    "damageType": "[#$t(damageDisplay.colors.{{ type }})]$t(addv.damageType.{{ type }})[#]",
    "damageAmountResisted": "$t(damageDisplay.damageType): {{ amount }} \ud83d\udee1 {{ resisted }}",
    "damageAmount": "$t(damageDisplay.damageType): {{ amount }}",
    "damage": "$t(damageDisplay.damageType)",
    "colors": {
      "DAMAGE_TYPE_WITHERING": "b00",
      "DAMAGE_TYPE_FIRE": "f70",
      "DAMAGE_TYPE_RADIANT": "ff6",
      "DAMAGE_TYPE_ACID": "bd0",
      "DAMAGE_TYPE_POISON": "4f0",
      "DAMAGE_TYPE_COLD": "2cf",
      "DAMAGE_TYPE_FORCE": "70f",
      "DAMAGE_TYPE_ELECTRICITY": "b5f",
      "DAMAGE_TYPE_PSYCHIC": "f8a",
      "DAMAGE_TYPE_SLASHING": "999",
      "DAMAGE_TYPE_CRUSHING": "999",
      "DAMAGE_TYPE_PIERCING": "999",
      "DAMAGE_TYPE_THUNDER": "bbb",
      "DAMAGE_TYPE_DEFAULT": "999"
    }
  }
}