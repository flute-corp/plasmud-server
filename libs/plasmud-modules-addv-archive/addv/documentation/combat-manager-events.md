# Events du combat manager

## Structure du combat

```ts
interface Fighter {
    id: string
    name: string
} 

interface Combat {
    fighter: Fighter  // instance du combattant
    target: Fighter // instance de la cible
    turn: number  // nombre de tours écoulés depuit le début du combat
    attackCount: number // nombre d'attaques effectuées ce tour (mis à 0 chaque début de tour) 
    attackPerTurn: number // nombre d'attaques par tour disponibles pour le combattant (mis à jour chaque tour) 
    isNew: boolean // isNew reste true tant que le debut du tour n'a pas commencé
    plan: number[] // planification des attaques (privé)
}
```

## Evènements

### combat.created

Un fighter vient d'initier un combat contre quelqu'un

```ts
interface CombatCreatedEvent {
    combat: Combat
}
```

### combat.quit

Un fighter vient de quitter son combat.

```ts
interface CombatquitEvent {
    combat: Combat
}
```

### combat.requestAttackCount

Le système de combat requiert le nombre d'attaques par tour qu'un fighter peut effectuer.
Le gestionnaire de cet évènement doit appeler result() pour poster la valeur du nombre d'attaques par tour.

```ts
interface CombatRequestAttackCount {
    fighter: Fighter
    result (n: number): void
}
```

### combat.requestBonusAction

Le système de combat requiert l'information selon laquelle la creature 
spécifié aurait droit à une action bonus pendant le tour qui vient.

```ts
interface CombatRequestBonusAction {
    fighter: Fighter
    result (n: number): void
}
```

### combat.loop

Le système de combat va traiter un combat particulier.
Le gestionnaire de cet évènement peut appeler "cancel" pour annuler le combat.

```ts
interface CombatLoopEvent {
    turn: number
    fighter: Fighter
    target: Fighter
    cancel (b: boolean = true)
}
```

### combat.attack

Le system de combat indique qu'une tentative attaque a lieu.
Le gestionnaire de cet évènement peut appeler cancel pour annuler l'attaque (juste l'attaque, pas le combat).

```ts
interface CombatAttackEvent {
    fighter: Fighter,
    target: Fighter,
    turn: number,
    cancel (b: boolean = true)
}
```

### combat.tick.end

Déclenché à la fin d'un tick. Comme il peut y avoir des créatures qui attaquent plusieurs fois par tour,
les ticks sont déclenchés à intervalles réguliers pour sub-diviser les tours, et répartir les multi-attaques
de manière uniforme, pour fluidifier l'affichage du combat.

### combat.turn.end

Déclenché à la fin d'un tour.
