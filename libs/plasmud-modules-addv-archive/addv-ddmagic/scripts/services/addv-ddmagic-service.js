const debug = require('debug');
const Creature = require('@laboralphy/addv-srd/src/Creature');
const { suggest, stripAccents } = require('@laboralphy/did-you-mean');
const log = debug('mod:addv');


const RX_SPELL_PARSE = /^([^+]+)\s*(\+\s*(\d+))?$/;
const RX_WIDE_SPACE = / +/g;
const ADDV_MAGIC_SERVICE = 'addvddmagic';
const RELEVANCE_1_WORD = 234;
const RELEVANCE_2_WORDS = 234;
const RELEVANCE_MORE_WORDS = 234;


class AddvDdmagicService {
    constructor () {
        this._system = null;
        this._addv = null;
    }

    init ({ system }) {
        this.log('ADDV-SRD - Spellcasting Module');
        this.log('Adding classic spellcasting capabilities');
        this._system = system;
        this._addv = system.services.addv;
        if (!this._addv) {
            throw new Error('ADDV required');
        }
        this._addv.manager.events.on('spell-ranged-attack', ev => {
            this.addvEventSpellRangedAttack(ev);
        });

        this._addv.manager.events.on('spellcast', ev => {
            this.addvEventSpellCast(ev);
        });
    }

    log (...args) {
        log(...args);
    }

    getSpellNameList () {
        return this._addv.publicAssets.strings.spells;
    }

    sendMessageToActor (
        oActor,
        oTarget,
        sMessageActor,
        sMessageTarget,
        sMessageRoom,
        oMessageContext = {}
    ) {
        if (oActor.uid in this._system.players) {
            const oActorContext = this._system.players[oActor.uid].context;
            oActorContext.print(sMessageActor, oMessageContext);
            oActorContext.print.room.except([oActor, oTarget], oActor.location, sMessageRoom, oMessageContext);
        }
        if (oTarget.uid in this._system.players) {
            const oTargetContext = this._system.players[oTarget.uid].context;
            oTargetContext.print(sMessageTarget, oMessageContext);
        }
    }

    addvEventSpellRangedAttack ({ caster, target, hit }) {
        const oMudCaster = this._addv._getMudEntity(caster);
        const oMudTarget = this._addv._getMudEntity(target);
        this.sendMessageToActor(
            oMudCaster,
            oMudTarget,
            hit ? 'ddmagic.rangedAttack.you.action' : 'ddmagic.rangedAttack.you.failure',
            hit ? 'ddmagic.rangedAttack.adv.warn' : 'ddmagic.rangedAttack.adv.info',
            hit ? 'ddmagic.rangedAttack.room.hit' : 'ddmagic.rangedAttack.room.miss',
            {
                caster: oMudCaster.name,
                target: oMudTarget.name
            }
        );
    }

    addvEventSpellCast ({ caster, spell, target = null, level }) {
        const oMudCaster = this._addv._getMudEntity(caster);
        const oMudTarget = this._addv._getMudEntity(target);
        const bNeedTarget = !!oMudTarget && oMudTarget !== oMudCaster;
        this.sendMessageToActor(
            oMudCaster,
            oMudTarget,
            bNeedTarget ? 'ddmagic.castSpell.you.onActor' : 'ddmagic.castSpell.you.onSelf',
            bNeedTarget ? 'ddmagic.castSpell.adv.onMe' : 'ddmagic.castSpell.adv.onSelf',
            bNeedTarget ? 'ddmagic.castSpell.room.onActor' : 'ddmagic.castSpell.room.onSelf',
            {
                caster: oMudCaster.name,
                target: oMudTarget.name,
                spell: this.getSpellNameList()[spell],
                level
            }
        );
    }


    /**
     * Lancement d'un sort
     * @param spell {string} référence du sort à lancer
     * @param power {number} puissance additionnel transmise au sort (over slot)
     * @param caster {Creature}
     * @param target {Creature|D20Item|String}
     * @param friends {Creature[]}
     * @param hostiles {Creature[]}
     * @param parameters {*}
     * @return {boolean}
     */
    castSpell (spell, power, caster, target, friends, hostiles, parameters) {
        const pCast = this._addv.manager.assetManager.scripts['ddmagic-cast-spell'];
        if (typeof pCast !== 'function') {
            throw new Error('could not find spellcast function');
        }
        if (!target) {
            // Pas de cible = self
            target = caster;
        } else if (target instanceof Creature) {
            // ciblage d'une créature
        } else if (typeof target === 'string') {
            // Ciblage d'une direction
        } else {
            // Ciblage d'un truc qui n'est pas une créature ni une direction
            // Un item ?
        }
        return pCast({
            spell,
            caster,
            target,
            power,
            hostiles,
            friends,
            parameters
        });
    }

    /**
     * Simplification d'une chaine : enlevage des accents et des espaces
     * @param s {string}
     * @returns {string}
     */
    simplifyString (s) {
        return stripAccents(s).toLowerCase().replace(RX_WIDE_SPACE, '');
    }

    /**
     * Trouvage de la référence à partir d'une chaine de caractère quelconque
     * @param sSearch {string}
     * @returns {string}
     */
    findRef (sSearch) {
        const aSpells = Object.entries(this.getSpellNameList());
        let x = aSpells.find(([, name]) => name === sSearch);
        if (!x) {
            const sSimpSearch = this.simplifyString(sSearch);
            x = aSpells.find(([, name]) => {
                const s1 = this.simplifyString(name);
                return s1.startsWith(sSimpSearch) || s1.includes(sSimpSearch);
            } );
        }
        return x ? x[0] : '';
    }

    /**
     * Renvoie la relevance à utiliser avec suggest en fonction du nombre de mots dans l'expreession
     * @param s {string}
     * @returns {number}
     */
    getSuitableRelevance (s) {
        const nWords = s.split(' ').length;
        return nWords === 1
            ? RELEVANCE_1_WORD
            : nWords === 2
                ? RELEVANCE_2_WORDS
                : RELEVANCE_MORE_WORDS;
    }

    /**
     * Analyse d'une chaine de caractère pour en extraire la référence du sorpt supposée
     * ainsi que le niveau de puissance
     * @param sString {string}
     * @returns {{ref: string, power: number, name: string}|null}
     */
    getSpellRefAndPower (sString) {
        const r = sString.trim().match(RX_SPELL_PARSE);
        if (r) {
            const [, sSpell, , sPower] = r;
            const nPower = sPower !== undefined ? parseInt(sPower) : 0;
            if (sSpell.length < 4) {
                return null;
            }
            const relevance = this.getSuitableRelevance(sSpell);
            const s = suggest(sSpell, Object.values(this.getSpellNameList()), { relevance });
            const sName = (Array.isArray(s) && s.length > 0) ? s.shift() : sSpell;
            const ref = this.findRef(sName);
            const data = this.getSpellData(ref);
            return {
                power: nPower,
                ref,
                name: sName,
                ...data
            };
        }
    }

    /**
     *
     * @param ref {string}
     * @returns {{ level: number, ritual: boolean, concentration: boolean, target: string, school: string }|undefined}
     */
    getSpellData (ref) {
        const db = this._addv.manager.assetManager.data['data-ddmagic-spell-database'];
        return db[ref];
    }

    /**
     *
     * @param oMudWizard
     * @param sSpell
     * @returns {{maxLevel: number, ref: string, level: *, success: boolean, alreadyLearned: boolean}}
     */
    learnSpell (oMudWizard, sSpell) {
        const addv = this._addv;
        const { ref, level: nSpellLevel } = this.getSpellRefAndPower(sSpell);
        const oAddvPC = addv.getAddvEntity(oMudWizard);
        const ss = oAddvPC.store.getters.getSpellSlotStatus;
        let nMaxSpellLevel = 0;
        for (let i = 1; i <= 9; ++i) {
            if (ss[i - 1].count > 0) {
                nMaxSpellLevel = i;
            } else {
                break;
            }
        }
        const outcome = {
            success: false,
            ref,
            level: nSpellLevel,
            maxLevel: nMaxSpellLevel,
            alreadyLearned: false
        };
        outcome.alreadyLearned = oAddvPC.store.state.data.spellbook.knownSpells.includes(ref);
        if (!outcome.alreadyLearned && nSpellLevel <= nMaxSpellLevel) {
            oAddvPC.store.mutations.learnSpell({ spell: ref });
            outcome.success = true;
        }
        return outcome;
    }

}

module.exports = AddvDdmagicService;
