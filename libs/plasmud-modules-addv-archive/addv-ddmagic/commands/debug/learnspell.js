function learnSpell (context, sSpell) {
    const magic = context.system.services.addvddmagic;
    const outcome = magic.learnSpell(context.pc, sSpell);
    if (outcome.success) {
        context.print('you learn ' + outcome.ref);
    } else {
        if (outcome.alreadyLearned) {
            context.print(outcome.ref + ' already known');
        } else {
            context.print(outcome.ref + ' level too high : ' + outcome.level + ' vs. ' + outcome.maxLevel);
        }
    }
}

module.exports = function main (context, aArguments) {
    learnSpell(context, aArguments.join(' '));
};
