function prepareSpell (context, sSpell) {
    const addv = context.system.services.addv;
    const magic = context.system.services.addvddmagic;
    const { ref } = magic.getSpellRefAndPower(sSpell);
    const oAddvPC = addv.getAddvEntity(context.pc);
    oAddvPC.store.mutations.prepareSpell({ spell: ref });
    context.print('you prepare ' + ref);
}

module.exports = function main (context, aArguments) {
    prepareSpell(context, aArguments.join(' '));
};
