const ADDV_MAGIC_SERVICE = 'addvddmagic';
const ADDV_SERVICE = 'addv';
const CONSTS = require('../../../consts');

/**
 * lancement d'un sort sur un personnage
 * @param context {MUDContext}
 * @param sSpell {string} sort (chaine à analyser)
 * @param oTarget {MUDEntity}
 * @param oParams {object}
 */
function castSpellOnTarget (context, sSpell, oTarget, oParams = {}) {
    const addv = context.system.services[ADDV_SERVICE];
    const addvddmagic = context.system.services[ADDV_MAGIC_SERVICE];
    const oSelf = addv.getAddvEntity(context.pc);
    const oAddvTarget = addv.getAddvEntity(oTarget);
    const engine = context.engine;
    const aEntities = engine.getLocalEntities(context.pc.location); // getLocalEntities: ok
    const oSpellDB = addv.data['data-ddmagic-spell-database'];
    // Tant qu'il n'y a pas de système de factions, on considère que les hostiles sont tous ceux qui ne sont pas amis
    const aHostiles = aEntities
        .filter(e => engine.isActor(e) && !engine.isPlayer(e))
        .map(e => addv.getAddvEntity(e));
    // Tant qu'il n'y a pas de factions, on considère que tous les joueurs sont alliés, c'est un jeu plutôt coopératif
    // à la base, après tout
    const aFriends = aEntities
        .filter(e => engine.isPlayer(e))
        .map(e => addv.getAddvEntity(e));
    const fswap = (x, aFrom, aTo) => {
        const iFrom = aFrom.indexOf(x);
        const iTo = aTo.indexOf(x);
        if (iFrom >= 0) {
            aFrom.splice(iFrom, 1);
            if (iTo < 0) {
                aTo.push(x);
            }
        }
    };
    const { power, ref, name: sFullSpellName } = addvddmagic.getSpellRefAndPower(sSpell);
    if (!context.engine.isActor(oTarget)) {
        throw new Error('Bad target type for spell ' + ref + ' : requires actor');
    }
    if (ref) {
        if (ref in oSpellDB) {
            const oSpellData = oSpellDB[ref];
            switch (oSpellData.target) {
            case 'TARGET_TYPE_HOSTILE': {
                if (oTarget === oSelf) {
                    throw new Error('Bad target type for spell ' + ref + ' : targeting self, but spell requires hostile');
                } else {
                    // le cas échant la cible doit être retirée de la liste des amis pour être dans celle des ennemis
                    fswap(oAddvTarget, aFriends, aHostiles);
                }
                break;
            }
            case 'TARGET_TYPE_FRIEND': {
                // Targeting self allowed
                // le cas échant la cible doit être retirée de la liste des ennemis pour être dans celle des amis
                // On a le droit en effet de soigner un adversaire, même si c'est assez contre productif comme comportement
                fswap(oAddvTarget, aHostiles, aFriends);
                break;
            }
            case 'TARGET_TYPE_SELF': {
                if (oTarget !== oSelf) {
                    context.print('ddmagic.failure.badTarget', { spell: sFullSpellName, target: oTarget.name });
                    throw new Error('Bad target type for spell ' + ref + ' : must target self');
                }
                break;
            }
            case 'TARGET_TYPE_SPECIAL': {
                context.print('ddmagic.failure.badTarget', { spell: sFullSpellName, target: oTarget.name });
                throw new Error('Bad target type for spell ' + ref + ' : must target actor');
            }

            default: {
                throw new Error('Unsupported target type for spell ' + ref + ' : ' + oSpellData.target);
            }
            }
        } else {
            throw new Error('spell ' + ref + ' is not in spell db');
        }
    } else {
        context.print('ddmagic.failure.badSpellName', { spell: sSpell });
        throw new Error('could not recognize spell "' + sSpell + '"');
    }
    if (!addvddmagic.castSpell(ref, power, oSelf, oAddvTarget, aFriends, aHostiles, oParams)) {
        context.print('ddmagic.failure.unavailable', { spell: sFullSpellName });
    }
}

/**
 * lancement d'un sort sur soi-même
 * @param context {MUDContext}
 * @param sSpell {string} sort (chaine à analyser)
 */
function castSpellOnSelf (context, sSpell) {
    return castSpellOnTarget(context, sSpell, context.pc);
}

/**
 * lancement d'un sort sur un objet de l'inventaire
 * @param context {MUDContext}
 * @param sSpell {string} sort (chaine à analyser)
 * @param oItem {MUDEntity}
 */
function castSpellOnItem (context, sSpell, oItem) {
    const addv = context.system.services[ADDV_SERVICE];
    const addvddmagic = context.system.services[ADDV_MAGIC_SERVICE];
    const oSpellDB = addv.data['data-ddmagic-spell-database'];
    const { power, ref, name: sFullNameSpell } = addvddmagic.getSpellRefAndPower(sSpell);
    if (ref) {
        if (ref in oSpellDB) {
            const oSpellData = oSpellDB[ref];
            if (oSpellData.target === 'TARGET_TYPE_SPECIAL') {
                const oSelf = addv.getAddvEntity(context.pc);
                addvddmagic.castSpell(ref, power, oSelf, oSelf, [], [], {
                    item: addv.getAddvEntity(oItem)
                });
            } else {
                context.print('ddmagic.failure.badTarget', { spell: sFullNameSpell, target: oItem.name });
                throw new Error('Bad target type for spell ' + ref + ' : requires TARGET_TYPE_SPECIAL ; given : ' + oSpellData.target);
            }
        } else {
            throw new Error('spell ' + ref + ' is not in spell db');
        }
    } else {
        context.print('ddmagic.failure.badSpellName', { spell: sSpell });
        throw new Error('could not recognize spell "' + sSpell + '"');
    }
}

/**
 * lancement d'un sort sur une issue. Il n'y a guère que "knock" ou "arcane lock" qui soient concernés.
 * @param context {MUDContext}
 * @param sSpell {string}
 * @param sExit {string} direction pour laquelle s'applique le sort
 */
function castSpellOnExit (context, sSpell, sExit) {

}

/**
 * Fonction principal du lancement des sorts
 * @param context {MUDContext}
 * @param aArguments {string[]}
 */
module.exports = function main (context, aArguments) {
    try {
        context.parse(aArguments, {
            [context.PARSER_PATTERNS.STRING_ON_ACTOR]: p => castSpellOnTarget(context, p.string, p.actor),
            [context.PARSER_PATTERNS.STRING_ON_ITEM]: p => castSpellOnItem(context, p.string, p.item),
            [context.PARSER_PATTERNS.NONE]: () => context.print('generic.error.needArgument'),
            [context.PARSER_PATTERNS.DEFAULT]: () => castSpellOnSelf(context, aArguments.join(' '))
        });
    } catch (e) {
        if (e.message.startsWith('ERR_SPELL_SCRIPT_NOT_FOUND')) {
            console.error(e.message);
            context.print('ddmagic.error.missingSpellScript', { spell: e.message.split(': ').pop() });
        } else {
            console.error(e);
        }
    }
};
