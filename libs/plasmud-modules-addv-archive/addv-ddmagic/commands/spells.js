const ADDV_MAGIC_SERVICE = 'addvddmagic';
const ADDV_SERVICE = 'addv';

function displaySpellBook (context) {
    const addv = context.system.services[ADDV_SERVICE];
    const magic = context.system.services[ADDV_MAGIC_SERVICE];
    const oAddvPC = addv.getAddvEntity(context.pc);
    const ps = oAddvPC.store.getters.getPreparedSpells;
    const aPreparedSpells = new Set(ps.spells);
    const SPELLS = magic.getSpellNameList();
    const spells = oAddvPC
        .store
        .getters
        .getSpellSlotStatus
        .map(({ count, used }, i) => ({
            level: i + 1,
            slots: {
                total: count,
                count: count - used
            },
            prepared: [],
            unprepared: []
        }));
    spells.unshift({
        level: 0,
        slots: {
            total: Infinity,
            count: Infinity
        },
        prepared: ps.cantrips.map(s => ({
            ref: s,
            name: SPELLS[s],
            ritual: false,
            concentration: s.concentration
        })),
        unprepared: []
    });
    ps.repository.forEach(s => {
        const oSpellRepLevel = spells[s.level];
        const oSpellData = {
            ref: s.ref,
            name: SPELLS[s.ref],
            ritual: s.ritual,
            concentration: s.concentration
        };
        if (aPreparedSpells.has(s.ref)) {
            oSpellRepLevel.prepared.push(oSpellData);
        } else {
            oSpellRepLevel.unprepared.push(oSpellData);
        }
    });
    const memory = {
        spells: {
            total: oAddvPC.store.getters.getMaxPreparableSpells,
            count: ps.spells.length
        },
        cantrips: {
            total: oAddvPC.store.getters.getMaxPreparableCantrips,
            count: ps.cantrips.length
        }
    };
    console.log(spells[1]);
    context.print('spell-report', {
        spells,
        memory
    });
}

module.exports = function main (context, aArguments) {
    displaySpellBook(context);
};
