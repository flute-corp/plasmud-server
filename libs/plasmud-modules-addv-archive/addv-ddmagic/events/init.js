const { CONFIG } = require('@laboralphy/addv-srd');
const debug = require('debug');
const AddvDDMagic = require('../scripts/services/addv-ddmagic-service');

const ADDV_MODULE = 'ddmagic';

const log = debug('mod:addv');


module.exports = function main (ctx) {
    const { system: { services: { addv }} } = ctx;
    const { system, engine } = ctx;
    log('loading addv sub-module "%s"', ADDV_MODULE);
    const ddMagicModule = CONFIG.modules.find(({ id }) => id === ADDV_MODULE);
    CONFIG.setModuleActive(ADDV_MODULE, true);

    const addvddmagic = new AddvDDMagic();
    addvddmagic.init({ system, engine });

    // si déja initialisé alors il faut l'ajouter manuellement.
    if (addv && addv.manager.assetManager.initialized) {
        addv.manager.assetManager.loadModule(ddMagicModule.path);
        addv.manager.assetManager.scripts['ddmagic.init']({ manager: addv.manager });
    }

    system.addContextService('addvddmagic', addvddmagic);

};
