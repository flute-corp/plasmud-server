const Trainer = require('./helpers/addv-evolution-trainer');

module.exports = function main (evt) {
    const { variables, context } = evt;
    const sClass = variables.selectedClass.id;
    // Augmenter cette classe : sClass
    const trainer = new Trainer(context);
    trainer.resetCharacter();
    trainer.levelUp(sClass);
};

