// script de test dialogue : if-addv-evolution-is-tourist
//
// Renvoie true si le personnage du joueur est un touriste
// C'est à dire qu'il ne possède de niveau dans aucune autre classe à part la classe touriste
//
// @date 2023-10-11
// @author ralphy
const Trainer = require('./helpers/addv-evolution-trainer');

module.exports = function main ({ context, result }) {
    const t = new Trainer(context);
    result(t.isTourist());
};

