module.exports = function main (evt) {
    const { variables, data } = evt;
    variables.selectedClass = variables[data.multiClass ? 'extraClasses' : 'classes'][data.indexClass];
};

