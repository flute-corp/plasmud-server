// script de test dialogue : if-addv-evolution-level-0
//
// Renvoie true si le personnage du joueur est de niveau 0
//
// @date 2023-10-11
// @author ralphy

module.exports = function main ({ result, player, system }) {
    result(system
        .services
        .addv
        .getAddvEntity(player)
        .store
        .getters
        .getLevel < 1
    );
};

