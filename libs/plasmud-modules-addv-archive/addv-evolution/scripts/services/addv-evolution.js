const Trainer = require('../helpers/addv-evolution-trainer');
class AddvEvolution {
    constructor () {
        this._system = null;
        this._engine = null;
        this._addv = null;
    }

    /**
     *
     * @param system {System}
     * @param engine {Engine}
     */
    init ({ system }) {
        this._system = system;
        this._addv = system.services.addv;
    }

    buildTrainer (oMUDEntity) {
        const { context } = this._system.players[oMUDEntity.uid];
        return new Trainer(context);
    }

    /**
     * Octroit ou enlève un certain nombre de point d'expérience à une créature
     * @param oMUDEntity {MUDEntity}
     * @param nAmount {number}
     */
    gainXP (oMUDEntity, nAmount) {
        const t = this.buildTrainer(oMUDEntity);
        t.gainXP(nAmount);
        const e = t.hasEnoughXP();
        // TODO Afficher un texte indiquant au joueur qu'il a gagné de l'XP
        if (nAmount >= 0) {
            t.context.print('evolUpdateXP.action.gained', { count: nAmount });
        } else {
            t.context.print('evolUpdateXP.failure.lost', { count: -nAmount });
        }
        // Ainsi qu'un petit pourcentage de progression
        if (e.enough) {
            // TODO afficher une ligne d'information indiquant qu'il peut changer de niveau s'il a assez de point
            t.context.print('evolUpdateXP.info.levelUp', { level: e.level + 1 });
        }
    }

    /**
     * Renvoie le nombre de points d'expérience gagné par le joueur,
     * ainsi que le nombre requis pour passer au niveau suivant.
     * @param oMUDEntity {MUDEntity}
     */
    getXPStatus (oMUDEntity) {
        return this.buildTrainer(oMUDEntity).hasEnoughXP();
    }

    dialogLevelUp (oMUDEntity) {
        this.buildTrainer(oMUDEntity).runDialogSelectClass();
    }

    getAvailableClasses (oMUDEntity) {
        this.buildTrainer(oMUDEntity).getAvailableClasses();
    }
}

module.exports = AddvEvolution;
