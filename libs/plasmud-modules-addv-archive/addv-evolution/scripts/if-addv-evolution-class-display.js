module.exports = function main (evt) {
    const { result, variables, data } = evt;
    const sRubrik = data.multiClass ? 'extraClasses' : 'classes';
    const nLength = variables[sRubrik].length;
    result(data.indexClass < nLength);
};

