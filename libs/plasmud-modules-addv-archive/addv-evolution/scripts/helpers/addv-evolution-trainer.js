/**
 * Gestion de l'évolution d'un personnage
 * La classe est initialisée avec un contexte de commande qui en extrait le joueur concerné
 * Ainsi que la référence au service ADDV
 *
 */
class AddvEvolutionTrainer {
    constructor (context) {
        this._context = context;
        this._addv = context.services.addv;
        this._TEMPLATES = null;
    }

    get context () {
        return this._context;
    }

    /**
     * Le personnage joueur (MUD)
     * @returns {MUDEntity}
     */
    get pc () {
        return this._context.pc;
    }

    /**
     * Le personnage joueur version ADDV
     * @returns {Creature}
     */
    get addvPC () {
        return this._addv.getAddvEntity(this.pc);
    }

    /**
     * Renvoie une ref au service dialog, afin de pouvoir instancier les dialogues de passage à niveau par exemple
     * @returns {*}
     */
    get dialogs () {
        return this._context.services.dialogs;
    }

    /**
     * Lance un dialogue
     * @param sDialog {string} référence dialogue
     * @param oVariables {object}
     * @returns {*}
     */
    runDialog (sDialog, oVariables) {
        return this.dialogs.createDialogContext(this._context, sDialog, oVariables);
    }

    /**
     *
     * @returns {*}
     */
    runDialogSelectClass () {
        const aClasses = Object.keys(this.TEMPLATES);
        const aTakenClasses = Object.keys(this.addvPC.store.getters.getLevelByClass);
        const aDlgClasses = aClasses
            .filter(c => {
                return this.isTourist() || this._addv.evolution.canCreatureLevelUpTo(this.addvPC, c);
            })
            .map(c => ({
                'id': c,
                'text': this._context.text('addvClassDescription.' + c + '.name'),
                'description': this._context.text('addvClassDescription.' + c + '.description')
            }));
        const exp = this.hasEnoughXP();
        return this.runDialog('dlg-addv-evolution-select-class', {
            'classes': aDlgClasses,
            'enough': exp.enough,
            'xp': exp.current,
            'nextxp': exp.required,
            'level': this.addvPC.store.getters.getLevel + 1,
            'multiclass': false,
            'extraClasses': aDlgClasses.filter(t => !aTakenClasses.includes(t.id)),
            'takenClasses': aTakenClasses.map(c => aDlgClasses.find(t => t.id === c))
        });
    }

    get TEMPLATES () {
        if (!(this._TEMPLATES)) {
            const d = this._addv.data;
            this._TEMPLATES = {
                fighter: d['template-fighter-generic'],
                rogue: d['template-rogue-generic'],
                wizard: d['template-wizard-generic']
            };
        }
        return this._TEMPLATES;
    }

    isTourist () {
        const aClasses = Object.keys(this
            .addvPC
            .store
            .getters
            .getLevelByClass
        );
        return aClasses.length === 1 && aClasses[0] === 'tourist';
    }

    /**
     * Renvoie la liste des classes disponible pour le PC
     * @returns {string[]}
     */
    getAvailableClasses () {
        const oAddvPC = this.addvPC;
        const evolution = this._addv.evolution;
        const aClasses = this.TEMPLATES.map(t => t.class);
        return aClasses.filter(c => evolution.canCreatureLevelUpTo(oAddvPC, c));
    }

    print (...aArgs) {
        return this._context.print(...aArgs);
    }

    /**
     * Effectue un level up
     */
    levelUp (sClass) {
        // Récupérer les données d'évolution
        if (!(sClass in this.TEMPLATES)) {
            throw new Error('Evol template not found');
        }
        const oLevelUpStruct = {
            selectedClass: sClass,
            selectedFeats: [],
            selectedAbility: '',
            selectedSkills: []
        };
        const oTemplate = this.TEMPLATES[sClass];
        // Déterminer le niveau actuel de la classe
        const oAddvPC = this.addvPC;
        const nCurrLevel = oAddvPC.store.getters.getLevelByClass[sClass] || 0;
        // Déterminer le nouveau niveau de la classe qu'on souhaite augmenter
        const nNewLevel = nCurrLevel + 1;
        const nGlobLevel = oAddvPC.store.getters.getLevel;
        // Si on est niveau 0 il faut appliquer les caractéristiques et skills initiaux
        if (nGlobLevel === 0) {
            // Définir caractéristiques
            for (const [ability, value] of Object.entries(oTemplate.abilities)) {
                oAddvPC.store.mutations.setAbility({ ability, value });
            }
            // Skills
            for (const skill of oTemplate.levels[0].skills) {
                oLevelUpStruct.selectedSkills.push(skill);
            }
        } else if (nCurrLevel === 0) {
            // On multiclasse, ajouter les skills de multiclassage
            const nMCSkillCount = oTemplate.multiclass.skillCount;
            const aMCSkills = oTemplate.skills.slice(0, nMCSkillCount);
            for (const sSkill of aMCSkills) {
                oLevelUpStruct.selectedSkills.push(sSkill);
            }
        }
        // Déterminer le template part
        const oTempPart = oTemplate.levels.find(t => t.level === nNewLevel);
        if (oTempPart) {
            if ('feats' in oTempPart) {
                for (const sFeat of oTempPart.feats) {
                    oLevelUpStruct.selectedFeats.push(sFeat);
                }
            }
            if ('ability' in oTempPart) {
                oLevelUpStruct.selectedAbility = oTempPart.ability;
            }
        }
        const oResult = this._addv.evolution.creatureLevelUp(oAddvPC, oLevelUpStruct);
        const nLevel = oAddvPC.store.getters.getLevelByClass[oResult.class];
        this.print('evolLevelUpSumUp.info.whatClass', { class: oResult.class, level: nLevel });

        this.print('evolLevelUpSumUp.info.whatScore', {
            hp: oAddvPC.store.getters.getMaxHitPoints,
            prof: oAddvPC.store.getters.getProficiencyBonus,
        });
        if (oResult.augmentedAbility) {
            this.print('evolLevelUpSumUp.info.whatAbility', { ability: oResult.augmentedAbility });
        }
        if (oResult.skills) {
            oResult.skills.forEach(s => {
                this.print('evolLevelUpSumUp.info.whatSkill', { skill: s });
            });
        }
        if (oResult.feats) {
            if (oResult.feats.newFeats) {
                oResult.feats.newFeats.forEach(f => {
                    this.print('evolLevelUpSumUp.info.whatFeat', { feat: f });
                });
            }
            if (oResult.feats.newFeatUses) {
                oResult.feats.newFeatUses.forEach(f => {
                    this.print('evolLevelUpSumUp.info.whatFeatUses', f);
                });
            }
        }
        if (oResult.extraAttacks) {
            this.print('evolLevelUpSumUp.info.extraAttacks', { attacks: oResult.extraAttacks });
        }
        return oResult;
    }

    /**
     * Remise à zero du personnage pour qu'il choisisse une nouvelle classe de départ
     */
    resetCharacter () {
        this.addvPC.store.mutations.resetCharacter();
        this.addvPC.store.mutations.setCounterValue({ create: true, counter: 'experiencePoints', value: 0 });
    }

    /**
     * Renvoie le nombre de point d'expérience accumulé
     * @returns {number}
     */
    getXP () {
        return this.addvPC.store.getters.getCounters.experiencePoints?.value || 0;
    }

    /**
     * Renvoie le nombre de XP nécessaire pour passer le niveau spécifié
     * @param nLevel {number}
     * @return {number}
     */
    getLevelRequiredXP (nLevel) {
        return 1000 * nLevel * (nLevel + 1) / 2;
    }

    /**
     * Renvoie true si le personnage à assez de point pour son prochain niveau
     * @return {{ level: number, current: number, required: number, enough: boolean }}
     */
    hasEnoughXP () {
        // 1 déterminer le niveau
        const level = this.addvPC.store.getters.getLevel;
        const xp = this.getXP();
        const rxp = this.getLevelRequiredXP(level);
        return {
            current: xp,
            required: rxp,
            level,
            enough: this.isTourist() || xp >= rxp
        };
    }

    /**
     * Augmente (ou diminue) le nombre de points d'expérience accumulés
     * @param n {number}
     */
    gainXP (n) {
        this.addvPC.store.mutations.setCounterValue({ create: true, counter: 'experiencePoints', value: this.getXP() + n });
    }
}

module.exports = AddvEvolutionTrainer;
