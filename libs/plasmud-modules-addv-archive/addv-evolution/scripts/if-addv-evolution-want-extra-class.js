// script de test dialogue : if-addv-evolution-is-tourist
//
// Renvoie true si le personnage du joueur est un touriste
// C'est à dire qu'il ne possède de niveau dans aucune autre classe à part la classe touriste
//
// @date 2023-10-11
// @author ralphy
module.exports = function main ({ variables, result }) {
    result(variables.multiclass);
};

