/**
 * ADDV-EVOLUTION init event
 *
 * Ce script initialise le système ADDV-EVOLUTION et l'installe en tant que service du système mud.
 * Les autres scripts d'évènement peuvent y accéder via $event.system.services.addvevol
 * Les scripts de commandes peuvent y accéder via context.services.addvevol
 *
 * @author ralphy
 * last update : 2023-10-17
 */

const AddvEvolution = require('../scripts/services/addv-evolution');


module.exports = function main ({ system, engine }) {
    const addv = new AddvEvolution();
    addv.init({ system });
    system.addContextService('addvevol', addv);
};
