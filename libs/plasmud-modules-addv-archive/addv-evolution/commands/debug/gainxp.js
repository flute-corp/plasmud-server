module.exports = function main ({ pc, system: { services: { addvevol }} }, aArgs) {
    const xp = parseInt(aArgs[0]);
    if (!isNaN(xp)) {
        addvevol.gainXP(pc, xp);
    }
};
