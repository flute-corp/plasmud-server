// Event mephit-lag-combat-turn
//
// Ce script est joué lorsqu'un méphite doit jouer son prochain tour de combat
// Il y a 20% de chance que le méphite utilise son attaque spécial souffle de feu
//
// Author : Ralphy
// Last update 2023-08-01

module.exports = function main ({ engine, services, self, interactor }) {
    const addv = services.addv;
    const oAddvSelf = addv.getAddvEntity(self);
    const bHasTarget = !!oAddvSelf.getTarget();
    if (bHasTarget && Math.random() > 0.7) {
        addv.doAction(self, 'sla-fire-breath');
    }
    // Si le mephite ne choisi pas de faire l'action, le systeme fera une attaque à la place.
};
