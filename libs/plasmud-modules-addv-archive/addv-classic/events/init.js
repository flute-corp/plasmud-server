const { CONFIG } = require('@laboralphy/addv-srd');
const debug = require('debug');

const ADDV_MODULE = 'classic';

const log = debug('mod:addv');


module.exports = function main ({ system: { services: { addv }} }) {
    log('loading addv sub-module "%s"', ADDV_MODULE);
    const classicModule = CONFIG.modules.find(({ id }) => id === ADDV_MODULE);
    CONFIG.setModuleActive(ADDV_MODULE, true);
    // si déja initialisé alors il faut l'ajouter manuellement.
    if (addv && addv.manager.assetManager.initialized) {
        addv.manager.assetManager.loadModule(classicModule.path);
    }
};
