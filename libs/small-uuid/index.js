const ShortUniqueId = require('short-unique-id');
const shortuuid = new ShortUniqueId({ dictionary: 'alphanum', length: 10 });

function generate () {
    return shortuuid.rnd();
}

module.exports = {
    generate
};
