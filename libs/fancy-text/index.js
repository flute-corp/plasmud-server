const ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWYYZ';
const FANCY_ORDER = ALPHABET + ALPHABET.toLowerCase();

function fancyLetter (s, nStyle = 0) {
    const i = FANCY_ORDER.indexOf(s);
    return i >= 0
        ? '\uD835' + String.fromCharCode(0xDC00 + i + 52 * nStyle)
        : s;
}

function fancyString (s, nStyle = 0) {
    if (typeof s !== 'string') {
        throw new TypeError('Expecting a string as first argument');
    }
    if (typeof nStyle !== 'number') {
        throw new TypeError('Expecting a number as optional second argument');
    }
    const sNormalized = s.normalize('NFD');
    let sOutput = '';
    for (let i = 0, l = sNormalized.length; i < l; ++i) {
        const sLetter = sNormalized.substring(i, i + 1);
        const sFancyLetter = fancyLetter(sLetter, nStyle);
        sOutput += sFancyLetter;
    }
    return sOutput;
}

module.exports = {
    fancy: fancyString,
    STYLE_NORMAL: 0,
    STYLE_ITALIC: 1,
    STYLE_CURSIVE: 2,
    STYLE_MANUSCRIPT: 4,
    STYLE_GOTHIC: 5,
    STYLE_DECO: 6,
    STYLE_GOTHIC_BOLD: 7,
    STYLE_SANS: 8,
    STYLE_SANS_BOLD: 9,
    STYLE_SANS_ITALIC: 10,
    STYLE_SANS_ITALIC_BOLD: 11,
    STYLE_FLAT: 11
};
