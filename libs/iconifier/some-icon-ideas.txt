Ce fichier permet de noter les idées d'icones en unicode au cas où on aurait envie de changer le référentiel d'icones.

warn : \u26a0  ⚠  /!\
info : ❨ℹ❩ \u2768\u2139\u2769 (i) ⓘ
error: ❨!❩ (!) ❨‼❩
speak: 🗣 💬
shout: 📢 📣
blessed: ☥ \u2625
cursed: 💢 呪 +
locked:  🔐 🔑
unlock: 🔓
death: 💀 ☠
