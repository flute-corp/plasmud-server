const http = require('http');
const https = require('https');

function getContentType (headers) {
    for (const [key, value] of Object.entries(headers)) {
        if (key.toLowerCase() === 'content-type') {
            return value;
        }
    }
    return undefined;
}

function tryParse (data, sType) {
    try {
        return JSON.parse(data);
    } catch (e) {
        if (e instanceof SyntaxError) {
            return data;
        } else {
            console.error(data);
            throw e;
        }
    }
}

function get (sURL) {
    return request('GET', sURL);
}

function request (sMethod, sURL, body = undefined) {
    return new Promise((resolve, reject) => {
        const url = new URL(sURL);
        const reqOptions = {
            host: url.hostname,
            port: parseInt(url.port),
            path: url.pathname,
            method: sMethod.toUpperCase(),
            headers: {
                'content-type': 'application/json; charset=utf-8'
            }
        };

        const h = url.protocol === 'https:'
            ? https
            : http;

        const req = h.request(reqOptions, res => {
            const chunks = [];
            res.setEncoding('utf8');
            res.on('data', chunk => {
                chunks.push(chunk);
            });

            res.on('end', () => {
                const data = chunks.join('');
                if (res.statusCode >= 400) {
                    reject('Error ' + res.statusCode + ' - ' + res.statusMessage + ' : ' + data);
                } else {
                    resolve(tryParse(data, getContentType(res.headers)));
                }
            });

            res.on('error', err => {
                reject(err);
            });
        });

        req.on('error', err => {
            reject(err);
        });

        if (body !== undefined && body !== null) {
            req.write(JSON.stringify(body));
        }
        req.end();
    });
}

function put (sURL, body = {}) {
    return request('PUT', sURL, body);
}

function post (sURL, body = {}) {
    return request('POST', sURL, body);
}

function patch (sURL, body = {}) {
    return request('PATCH', sURL, body);
}

function del (sURL) {
    return request('DELETE', sURL);
}

class WebFetcher {
    constructor ({ host = '', port = 0 }) {
        this.setHostPost(host, port);
    }

    setHostPost (host, port = 0) {
        let h = host + (port > 0 ? (':' + port.toString()) : '');
        if (!h.endsWith('/')) {
            h += '/';
        }
        this._host = h;
    }

    buildURL (sPath) {
        return this._host + sPath.split('/').filter(s => s !== '').join('/');
    }

    get (url) {
        return get(this.buildURL(url));
    }

    post (url, body = {}) {
        return post(this.buildURL(url), body);
    }

    patch (url, body = {}) {
        return patch(this.buildURL(url), body);
    }

    put (url, body = {}) {
        return put(this.buildURL(url), body);
    }

    delete (url) {
        return del(this.buildURL(url));
    }
}

module.exports = WebFetcher;
