const {checkType} = require('../get-type');

class MUDExit {
    constructor ({
        direction,
        destination = null,
        room = null,
        nav
    }) {
        const oExitLock = nav?.lock;
        this._room = room;
        this._direction = direction;
        this._name = nav?.name || '';
        this._desc = nav?.desc || [];
        this._lockable = !!oExitLock;
        this._dcLockpick = 0;
        this._secret = !!nav?.secret;
        this._dcSearch = this._secret
            ? 'difficulty' in nav.secret
                ? nav.secret.difficulty
                : Infinity
            : 0;
        this._visible = !this._secret;
        this._key = '';
        this._destination = destination;
        this._discardKey = false;
        this._nav = nav;
        this._locked = this._lockable ? oExitLock.locked : false;
        this._spottedBy = new Set();
        if (this.lockable) {
            this.locked = this.lockable && oExitLock.locked;
            this._dcLockpick = 'difficulty' in oExitLock ? oExitLock.difficulty : Infinity;
            this._key = oExitLock.key || '';
            this._discardKey = !!oExitLock.discardKey;
        }
    }

    get valid () {
        return !!this._nav && !!this._destination;
    }

    get room () {
        return this._room;
    }

    get locked () {
        return this.lockable && this._locked;
    }

    set locked (value) {
        if (this.lockable) {
            checkType(value, 'boolean');
            this._locked = value;
        }
    }

    get direction () {
        return this._direction;
    }
    get lockable () {
        return this._lockable;
    }
    get name () {
        return this._name;
    }
    get dcLockpick () {
        return this._dcLockpick;
    }
    get dcSearch () {
        return this._dcSearch;
    }
    get key () {
        return this._key;
    }
    get secret () {
        return this._secret;
    }
    get visible () {
        return this._visible;
    }
    get destination () {
        return this._destination;
    }
    get discardKey () {
        return this._discardKey;
    }
    get desc () {
        return this._desc;
    }
    get nav () {
        return this._nav;
    }

    setDetectedBy (oActor, value) {
        checkType(value, 'boolean');
        if (value) {
            this._spottedBy.add(oActor.id);
        } else {
            this._spottedBy.delete(oActor.id);
        }
    }

    isDetectedBy (oActor) {
        return !this.secret || this._spottedBy.has(oActor.id);
    }
}

module.exports = MUDExit;
