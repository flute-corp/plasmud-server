const MUDEntity = require('./MUDEntity');
const { checkType } = require('../get-type');

class MUDItem extends MUDEntity {
    constructor (options) {
        super(options);
        const count = options.count || 1;
        this._baseWeight = this.blueprint.weight || 0;
        this._stack = Math.max(0, count);
        this._locked = this.blueprint.lock ? this.blueprint.lock.locked : false;
    }

    get inventory () {
        return this._inventory;
    }

    get stack () {
        return this._stack;
    }

    set stack (value) {
        checkType(value, 'number');
        this._stack = value;
    }

    get weight () {
        const nInvWeight = this.inventory
            ? this.engine
                .getLocalEntities(this) // getLocalEntities: ok
                .reduce((prev, curr) => curr.weight + prev, 0)
            : 0;
        const nMyWeight = this._baseWeight;
        const nStack = this.stackable && this._stack || 1;
        return nMyWeight * nStack + nInvWeight;
    }

    set weight (value) {
        checkType(value, 'number');
        this._baseWeight = value;
    }

    get locked () {
        return this._locked;
    }

    set locked (value) {
        checkType(value, 'boolean');
        this._locked = value;
    }

    get stackable () {
        !!this.blueprint.stackable;
    }
}

module.exports = MUDItem;
