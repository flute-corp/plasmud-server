const MUDEntity = require('./MUDEntity');
const {checkType} = require('../get-type');

class MUDPlaceable extends MUDEntity {
    constructor (options) {
        super(options);
        this._locked = this.blueprint.lock ? this.blueprint.lock.locked : false;
    }

    get locked () {
        return this._locked;
    }

    set locked (value) {
        checkType(value, 'boolean');
        this._locked = value;
    }
}

module.exports = MUDPlaceable;
