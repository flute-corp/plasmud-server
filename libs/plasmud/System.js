const path = require('path');
const Events = require('events');
const Engine = require('./Engine');
const mc = require('../multiple-chrono');
const { deepMerge } = require('@laboralphy/object-fusion');
const { AbortError } = require('./errors');
const debug = require('debug');
const ResourceLoader = require('../resource-loader');
const TextRenderer = require('./helpers/TextRenderer');
const { getType, checkType, switchType } = require('../get-type');
const Parser = require('./helpers/parser');
const ModuleConfigParser = require('./ModuleConfigParser');
const ChronoTrigger = require('../chrono-trigger');
const logMud = debug('serv:plasmud');
const SERVICE_CONTEXT_NS = 'services';
const COMMAND_ROOT_PREFIX = '@';
const MESSAGE_TYPES = require('./data/message-types.json');

/**
 * @typedef MUDContext
 * @property system {System}
 * @property engine {Engine}
 * @property services {object}
 * @property command {function}
 * @property parser {Parser}
 * @property pc {MUDEntity}
 * @property print {object}
 * @property text {function}
 * @property abort {function}
 * @property parse {function(string[], Object.<string, function)}
 * @property PARSER_PATTERNS {Object.<string, string>}
 */

/**
 * Gestion du MUD au niveau système.
 * @desc Cette classe permet de construire tout l'environnement technique nécessaire au bon fonctionnement de l'Engine.
 * Propose la fonctionnalité de module, propose la possibilité de charger les commands, les templates, les chaînes
 * d'internationalisation.
 * @class System
 */
class System {
    constructor () {
        this._acceptNumericCommand = true;
        this._engine = new Engine();
        this._textRenderer = new TextRenderer();
        this._players = {};
        this._events = new Events();
        this._contextAdditionalProperties = {};
        this._interval = 0;
        this._chronoTrigger = new ChronoTrigger();
        this._module = {
            blueprints: {},
            sectors: {},
            rooms: {},
            data: {}
        };
        this._dupChecker = new Set();
        this._scripts = {
            commands: {},
            scripts: {},
            events: {},
            modules: {}
        };
        this._loadedModules = new Set();
        this.declareEventHandlers();
    }

    /**
   * Liste des players enregistrés
   * @returns {*|{}}
   */
    get players () {
        return this._players;
    }

    /**
   * Instance de l'Engine
   * @returns {Engine}
   */
    get engine () {
        return this._engine;
    }

    /**
   * Instance du gestionnaire d'évènements
   * @returns {module:events.EventEmitter}
   */
    get events () {
        return this._events;
    }

    /**
   * Objet de configuration
   * @returns {*|{modules, assets}}
   */
    get config () {
        return this._config;
    }

    /**
   * Définition de la configuration
   * Permet de définir les modules et les assets.
   * @param modules {object}
   * @param language {string}
   * @param style {object}
   */
    set config ({ modules, language = '', styles }) {
        this._config = { modules, language, styles };
    }

    /**
   *
   * @returns {TextRenderer}
   */
    get textRenderer () {
        return this._textRenderer;
    }

    /**
   * Dans le cas ou le text renderer par defaut ne conviendrait pas
   * Respecter ../text-renderer/TextRenderer
   *
   */
    set textRenderer (tr) {
        this._textRenderer = tr;
    }


    /**
   * Initialisation du système. La configuration doit avoir été initialisée avant d'appeler cette méthode, avec la
   * propriété config.
   * @returns {Promise<System>}
   */
    async init () {
        try {
            logMud('initialization');
            const {
                modules,
                language,
                styles
            } = this.config;
            const c = mc.start();
            await this._textRenderer.init({
                strings: {
                    lang: language
                },
                templates: {
                    resetEachLine: true
                },
                styles
            });
            for (let imod = 0, l = modules.length; imod < l; ++imod) {
                await this.addModule(modules[imod]);
            }
            this.setEngineModule(this._module);
            this._engine.events.on('entity.create', ({ entity: oEntity }) => {
                if (oEntity.blueprint.type === this._engine.CONSTS.ENTITY_TYPES.PLAYER) {
                    const idPlayer = oEntity.uid;
                    logMud('player entity created : user::%s -> %s', idPlayer, oEntity.id);
                    if (idPlayer in this._players) {
                        this._players[idPlayer].context.pc = oEntity;
                    } else {
                        throw new Error('Could not find player : ' + idPlayer);
                    }
                }
            });
            this._engine.events.on('resolve.textref', oEvent => {
                oEvent.text = this._textRenderer.renderSync(oEvent.textref, oEvent.context);
            });
            logMud('initializing modules');
            this._events.emit('init');
            logMud('module initialization done.');
            const t = mc.stop(c);
            await this._textRenderer.initDone();
            const oResourceCount = this._textRenderer.count;
            logMud(
                '%d script(s) & %d template(s) & %d string(s) loaded in %s',
                Object.keys(this._scripts.events).length +
          Object.keys(this._scripts.commands)
              .filter(s => s.startsWith(COMMAND_ROOT_PREFIX)).length +
          Object.keys(this._scripts.scripts).length,
                oResourceCount.templates,
                oResourceCount.strings,
                this._renderTime(t)
            );
            return this;
        } catch (e) {
            logMud('[err]', 'something went wrong during events phase');
            throw e;
        }
    }

    runScript (sScript, ...aParams) {
        return switchType (sScript, {
            'string': s => this._scripts.scripts[s](...aParams),
            'function': f => f(...aParams)
        });
    }

    declareEventHandlers () {
    // L'engine commande l'exécution d'un script lié à une entité
        this.engine.events.on('entity.event.script', ({
            script,
            payload
        }) => {
            if (!('engine' in payload)) {
                throw new Error('WTF');
            }
            const services = this.services;
            this.runScript(script, {
                services,
                ...payload
            });
        });
    }

    /**
   * This starts the internal clock manager, but you should rather call this externally every second
   */
    startClockHandler () {
        this.stopClockHandler();
        this._interval = setInterval(() => this.handleClock(), 1000);
    }

    /**
   * stops internal clock manager
   */
    stopClockHandler () {
        if (this._interval) {
            clearInterval(this._interval);
            this._interval = 0;
        }
    }

    handleClock () {
        this._chronoTrigger.begin();
        this._events.emit('clock');
        this._engine.handleClock();
        this._chronoTrigger.commit();
        const t = this._chronoTrigger.time / 1000;
        if (t > 0.5) {
            if (Math.random() > t) {
                const t100 = Math.min(100, 100 * t);
                logMud('average clock time usage : %d %%', t100);
            }
        }
    }

    /**
   * Shutdown all timers
   */
    async shutdown () {
        this.stopClockHandler();
    }

    /**
   *
   * @param id {string} identifiant de l'utilisateur à qui envoyer ce message
   * @param content {string} contenu du message
   * @param code {number} code indiquand la nature du message (./data/message-types)
   */
    output (id, content, code) {
        this._events.emit('output', {
            id,
            content: content,
            code
        });
    }

    /**
   * Si la chaine de ressource spécifiée contient certains mots clé (don la liste est dans STYLE_PREFIXES)
   * alors on revoie un code permettant de détailler le type de message correspondant
   * exemple : si la res ref est open.failure alors le message correspont à un échec est le code MESSAGE_TYPE_FAILURE est renvoyé
   * si rin n'est identifié : MESSAGE_TYPE_NORMAL
   * @param sResRef {string}
   * @returns {{ code: number, style: string }}
   */
    static getResRefPrefixCode (sResRef) {
        for (const { pattern, code, style } of MESSAGE_TYPES) {
            if (sResRef.includes('.' + pattern + '.') || sResRef.endsWith('.' + pattern)) {
                return { code, style };
            }
        }
        return { code: 0, style: '' };
    }

    /**
   * Envoie une chaine de texte (qui peut être complétée par i18n) à l'utilisateur spécifié
   * La chaine est décorée par le préfixe auquel le resref est associé
   * (ex: .action est asssocié au préfixe [succ])
   * @param uid {string} identifiant de l'utilisateur destinataire
   * @param sString {string} resref
   * @param oVariables {{}} objet contenant les variables de substitution
   */
    sendTextToUser (uid, sString, oVariables = {}) {
        const sRenderedText = this._textRenderer.renderSync(sString, oVariables);
        const { style, code } = System.getResRefPrefixCode(sString);
        const aRenderedText = sRenderedText.split('\n');
        aRenderedText.forEach(l => {
            this.output(uid, style + l, code);
        });
    }

    /**
   * Enregistre un player dans le système et lui attribue une Entité et un contexte.
   * @param uid {string} identifiant du player
   * @return {{ context: MUDContext }|null}
   */
    registerPlayer (uid) {
        if (this.isPlayerRegistered(uid)) {
            logMud('failed to register user::%s : already registered !', uid);
            return this._players[uid];
        }
        const engine = this._engine;

        /**
     * Call the text rendering system to get the content of a string
     * @param sString {string} string ref
     * @param oVariables {{}} registry of substitution variables
     * @returns {string|null}
     */
        const text = (sString, oVariables) => this._textRenderer.renderSync(sString, oVariables);

        /**
     * Sends a text to the context user (the user whose context is being built here in this method)
     * @param sString {string} string ref
     * @param oVariables {{}} registry of substitution variables
     * @returns {Promise<void>}
     */
        const print = (sString, oVariables) => {
            this.sendTextToUser(uid, sString, oVariables);
        };

        /**
     * This method abort the execution of the current method. This is like throwing an exception that will be
     * softly catch.
     * @param sMessage {string} message to be passed to parent execution thread
     * @returns {never}
     */
        const abort = sMessage => this.abort(sMessage);

        const context = {
            system: this,
            engine,
            services: this.services,
            command: (sId, aArgs) => {
                return this.command(uid, sId, aArgs);
            },
            pc: null,
            print,
            text,
            abort,
            lastCommand: {
                command: '',
                parameters: [],
            },
            ...this._contextAdditionalProperties
        };


        /**
     * Returns a list of players present in the same room as context.pc
     * @returns {MUDEntity[]}
     */
        const getPlayerRoomPlayers = () => engine.getRoomPlayers(context.pc.location);

        /**
     * Sends arguments to specified players
     * @param aPlayers {MUDEntity[]}
     * @param aArguments {*}
     */
        const sendPrintToPlayers = (aPlayers, aArguments) => {
            aPlayers
                .map(entity => entity.uid) // ne garder que les uid de chaque entity
                .forEach(uid => this._players[uid].context.print(...aArguments)); // envoyer le message
        };

        /**
     * Envoie le message à tous les players de la room qui passent le filtre
     * Si la pièce est une pièce technique comme les limbes alors la fonction n'envoi rien
     * @param pFilter {function(MUDEntity): boolean}
     * @param aArguments {array} le premier élément peut etre une Pièce
     */
        const sendPrintToRoom = (pFilter, aArguments) => {
            const oRoom = aArguments.length > 0 && engine.isRoom(aArguments[0])
                ? aArguments.shift()
                : null;
            const p = oRoom ? engine.getRoomPlayers(oRoom) : getPlayerRoomPlayers();
            const aPlayers = pFilter ? p.filter(pFilter) : p;
            sendPrintToPlayers(aPlayers, aArguments);
        };

        /**
     * Sends text to aware players only
     * i.e. players that can perceive context.pc
     * @param pFilter {function(MUDEntity): boolean}
     * @param aArguments {*}
     */
        sendPrintToRoom.aware = (pFilter, aArguments) => {
            if (pFilter) {
                sendPrintToRoom(entity =>
                    pFilter(entity) &&
                engine.getEntityVisibility(context.pc, entity) === engine.CONSTS.VISIBILITY_VISIBLE,
                aArguments
                );
            } else {
                sendPrintToRoom(entity =>
                    engine.getEntityVisibility(context.pc, entity) === engine.CONSTS.VISIBILITY_VISIBLE,
                aArguments
                );
            }
        };

        /**
     * Sends text to unaware players only
     * i.e. players that can't perceive context.pc
     * @param pFilter {function(MUDEntity): boolean}
     * @param aArguments {*}
     */
        sendPrintToRoom.unaware = (pFilter, aArguments) => {
            if (pFilter) {
                sendPrintToRoom(entity =>
                    pFilter(entity) &&
                engine.getEntityVisibility(context.pc, entity) !== engine.CONSTS.VISIBILITY_VISIBLE,
                aArguments
                );
            } else {
                sendPrintToRoom(entity =>
                    engine.getEntityVisibility(context.pc, entity) !== engine.CONSTS.VISIBILITY_VISIBLE,
                aArguments
                );
            }
        };

        /**
     * Print text in log
     * @param aArguments
     */
        print.log = (...aArguments) => {
            logMud(...aArguments);
        };

        /**
     * Sends text to all perceiving players (players who can perceive context.pc)
     * @param aArguments {*}
     */
        print.room = (...aArguments) => {
            sendPrintToRoom.aware(entity => entity.uid !== uid, aArguments);
        };

        /**
     * Sends text to all unaware players (players who can't perceive context.pc)
     * @param aArguments {*}
     */
        print.room.unaware = (...aArguments) => {
            sendPrintToRoom.unaware(entity => entity.uid !== uid, aArguments);
        };

        /**
     * Sends text to all player in room, except to ones specified, regardless of visibility outcome
     * @param aEntities {MUDEntity[]}
     * @param aArguments {*}
     */
        print.room.except = (aEntities, ...aArguments) => {
            const aEntitiesId = aEntities.map(e => e.id);
            sendPrintToRoom.aware(entity => !aEntitiesId.includes(entity.id), aArguments);
        };

        /**
     * Sends text explicitly to player
     * @param oPlayer {MUDEntity}
     * @param aArguments {*}
     */
        print.to = (oPlayer, ...aArguments) => {
            const e = engine;
            if (e.isEntityExist(oPlayer.id)) {
                /**
         * @type {MUDEntity}
         */
                if (e.isPlayer(oPlayer)) {
                    const uid = oPlayer.uid;
                    this._players[uid].context.print(...aArguments);
                } else {
                    logMud('attempt to send message to an non-player entity : %s (type: %s)', oPlayer.id, oPlayer.blueprint.type);
                }
            } else {
                logMud('attempt to send message to an unknown player entity : %s', oPlayer.id);
            }
        };
        context.parse = (aArguments, oPatterns) => Parser.parse(context, aArguments, oPatterns);
        context.PARSER_PATTERNS = Parser.PARSER_PATTERNS;

        this._players[uid] = {
            context
        };
        logMud('player %s context created and registered', uid);
        return this._players[uid];
    }

    /**
   * Renvoie true si le joueur est déja enregistré dans le plasmud
   * @param uid {number|string}
   * @returns {boolean}
   */
    isPlayerRegistered (uid) {
        return uid.toString() in this._players;
    }

    /**
   * Renvoie le context d'un joueur
   * @param uid {string|number}
   * @returns {*|null}
   */
    getPlayerContext (uid) {
        return this.isPlayerRegistered(uid) ? this._players[uid.toString()].context : null;
    }

    /**
   * Supprime un player du système.
   * Ne supprime pas l'entité qui est associée au joueur.
   * @param uid {string} identifiant du player
   */
    unregisterPlayer (uid) {
        if (this.isPlayerRegistered(uid)) {
            logMud('user::%s is unregistered from plasmud.', uid);
            // Placer le personnage correspondant dans les limbes pour éviter toute interaction avec
            // le reste du monde.
            // Soit on "freeze" le joueur
            // Soit on place le joueur dans les limbes
            const { context } = this._players[uid];
            const oPlayer = context.pc;
            this.exportPlayerCharacter(oPlayer.id);
            this.engine.destroyEntity(oPlayer);
            delete this._players[uid];
        } else {
            logMud('failed to remove unregistered user::%s !', uid);
        }
    }

    exportPlayerCharacter (idCharacter) {
        if (this._engine.isEntityExist(idCharacter)) {
            const ps = this._engine.buildPlayerState(this._engine.getEntity(idCharacter));
            this._events.emit('player.character.export', { state: ps });
            return ps;
        } else {
            logMud('failed to export character from offline character %s', idCharacter);
            return {};
        }
    }

    importPlayerCharacter (state) {
        return this._engine.restorePlayerState(state);
    }

    /**
   * Prend en charge le fichier de configuration d'un module
   * @param oConfig {{}}
   */
    addModuleConfig (oConfig) {
        if (this._module.data) {
            deepMerge(this._module.data, oConfig);
        } else {
            this._module.data = oConfig;
        }
    }

    parseConfigLanguages (lang) {
        switch (getType(lang)) {
        case 'string': {
            return this.parseConfigLanguages(lang.split(',').map(s => s.trim()));
        }

        case 'array': {
            const o = {
                _default: ''
            };
            lang.forEach(s => {
                if (o._default === '') {
                    o._default = s;
                }
                o[s] = s;
            });
            return o;
        }

        case 'object': {
            const _default = '_default' in lang
                ? lang._default
                : Object.keys(lang).shift();
            return {
                ...lang,
                _default
            };
        }

        default: {
            return {};
        }
        }
    }

    /**
   * Chargement d'un module
   * @param xModule {string|object}
   * @param sOverrideName {string}
   * @returns {Promise<void>}
   */
    async addModule (xModule, sOverrideName = '') {
        const sLang = this._config.language;
        let sModuleName = sOverrideName;
        const fetchResources = () => {
            if (typeof xModule === 'string') {
                sModuleName = sModuleName === '' ? path.basename(xModule) : '';
                logMud('loading module "%s" using language "%s"', sModuleName, sLang === '' ? 'NONE' : sLang);
                const rl = new ResourceLoader();
                return rl.load(xModule);
            } else if (typeof xModule === 'object') {
                logMud('using preloaded module "%s" using language "%s"', sModuleName, sLang === '' ? 'NONE' : sLang);
                return Promise.resolve(xModule);
            }
        };

        const oResources = await fetchResources();

        const oConfig = oResources['config.json'] || {
            startingLocation: '',
            defaultLanguage: '',
            languages: [],
            dependencies: []
        };
        const aMissings = oConfig.dependencies
            ? oConfig.dependencies.filter(dep => !this._loadedModules.has(dep))
            : [];
        if (aMissings.length > 0) {
            const sMissings = aMissings.map(m => '"' + m + '"').join(', ');
            throw new Error('Dependency error : module "' + sModuleName + '" needs : ' + sMissings + '.');
        }

        const mcp = new ModuleConfigParser(oConfig);
        this.addModuleConfig(oConfig);
        const sLangPath = mcp.getLangPath(sLang);

        const PATH_SEPARATOR = '/';
        const sPathSepLang = sLangPath !== ''
            ? PATH_SEPARATOR + sLangPath + PATH_SEPARATOR
            : PATH_SEPARATOR;
        const ASSET_LOCATION = 'assets' + sPathSepLang;

        logMud('module %s assets location : %s', sModuleName, ASSET_LOCATION);

        const rFindData = new RegExp('^data' + PATH_SEPARATOR + '(.*)\\.json$');
        const rFindAsset = new RegExp('^' + ASSET_LOCATION + '(?!templates' + PATH_SEPARATOR + '|strings' + PATH_SEPARATOR + ')([-a-z0-9]+)' + PATH_SEPARATOR);
        const rFindScriptCommand = new RegExp('^(scripts|commands)' + PATH_SEPARATOR + '(.*)\\.js$');
        const rFindEvent = new RegExp('^events' + PATH_SEPARATOR + '(.*)\\.js$');
        const rFindTemplate = new RegExp('^' + ASSET_LOCATION + 'templates' + PATH_SEPARATOR + '(.*)\\.twig$');
        const rFindString = new RegExp('^' + ASSET_LOCATION + 'strings' + PATH_SEPARATOR);

        const tryRegisterEntryAsData = (sResourceName, data) => {
            const r = sResourceName.match(rFindData);
            if (Array.isArray(r)) {
                const sId = path.basename(sResourceName, '.json');
                this.defineAsset(sId, 'data', data);
            }
        };

        const tryRegisterEntryAsAsset = (sResourceName, data) => {
            const r = sResourceName.match(rFindAsset);
            if (Array.isArray(r)) {
                const sAssetType = r[1];
                const sId = path.basename(sResourceName, '.json');
                this.defineAsset(sId, sAssetType, data);
            }
        };

        const tryRegisterEntryAsScript = (sResourceName, oScript) => {
            const r = sResourceName.match(rFindScriptCommand);
            if (Array.isArray(r)) {
                const sType = r[1];
                const sId = r[2];
                this._scripts[sType][sId] = oScript;
                this._scripts[sType][COMMAND_ROOT_PREFIX + sModuleName + '/' + sId] = oScript;
            }
        };

        const tryRegisterEntryAsEvent = (sResourceName, oScript) => {
            const r = sResourceName.match(rFindEvent);
            if (Array.isArray(r)) {
                checkType(oScript, 'function', sResourceName);
                const sId = path.basename(r[1]);
                const se = this._scripts.events;
                if (!(sId in se)) {
                    // cet id de script n'a pas encore été déclaré
                    se[sId] = [];
                    switch (sId) {
                    case 'output':
                    case 'clock':
                    case 'command':
                    case 'numeric':
                    case 'init': {
                        this.events.on(sId, event => {
                            this.runEventScript(sId, event);
                        });
                        break;
                    }

                    case 'entity.event.script': {
                        this.engine.events.on(sId, ({ script, payload }) => {
                            const services = this.services;
                            this.runScript(script, {
                                services,
                                ...payload
                            });
                        });
                        break;
                    }

                    case 'entity.destroy':
                    case 'entity.create':
                    case 'entity.save':
                    case 'entity.restore':
                    case 'entity.room.enter':
                    case 'entity.room.exit':
                    case 'entity.speak':
                    case 'entity.task.attempt':
                    case 'item.acquire':
                    case 'item.drop':
                    case 'item.lose':
                    case 'container.unlock':
                    case 'container.unlock.failure':
                    case 'exit.unlock':
                    case 'exit.unlock.failure':
                    case 'resolve.entity.visibility':
                    case 'resolve.room.visibility': {
                        this.engine.events.on(sId, event => {
                            this.runEventScript(sId, event);
                        });
                        break;
                    }
                    }
                }
                se[sId].push(oScript);
            }
        };

        const tryRegisterEntryAsStrings = (sResourceName, data) => {
            const r = sResourceName.match(rFindString);
            if (Array.isArray(r)) {
                this
                    ._textRenderer
                    .producers
                    .get('strings')
                    .defineResource(this.config.language, data);
            }
        };

        const tryRegisterEntryAsTemplate = (sResourceName, data) => {
            const r = sResourceName.match(rFindTemplate);
            if (Array.isArray(r)) {
                const idTemplate = r[1];
                this
                    ._textRenderer
                    .producers
                    .get('templates')
                    .defineResource(idTemplate, data);
            }
        };

        for (const [sResourceName, data] of Object.entries(oResources)) {
            tryRegisterEntryAsData(sResourceName, data);
            tryRegisterEntryAsAsset(sResourceName, data);
            tryRegisterEntryAsScript(sResourceName, data);
            tryRegisterEntryAsEvent(sResourceName, data);
            tryRegisterEntryAsStrings(sResourceName, data);
            tryRegisterEntryAsTemplate(sResourceName, data);
        }

        this._loadedModules.add(sModuleName);
    }

    /**
   * Compose un chaine en seconde ou milisecondes en fonction de la grandeur de la valeur passée en paramètre
   * @param nTime {number} durée en ms
   * @return {string} chaine de rendu
   * @private
   */
    _renderTime (nTime) {
        return nTime >= 1000 ? (Math.floor(nTime / 100) / 10).toString() + 's' : nTime.toString() + 'ms';
    }

    /**
   * Déclenche une exception. Cette methode est utilisé par les commandes afin de s'auto-terminer.
   * @param sWhy {string} raison pour laquelle la commande se termine.
   */
    abort (sWhy) {
        throw new AbortError(sWhy);
    }

    /**
   * Détermine si une commande peut être lancée, fait appel à un évènement "command" que les module pourront écouter
   * afin de définir "allowed".
   * @param uid {string} identifiant du player qui lance la commande
   * @param sCommand {string} commande lancée
   * @return {{reason: string, allowed: boolean}}
   */
    checkCommandUsability (uid, sCommand) {
        if (!uid) {
            return {
                reason: 'admin command',
                allowed: false
            };
        }
        let bAllowed = true;
        let sReason = '';
        const oData = {
            system: this,
            command: sCommand,
            deny: (reason = '') => {
                bAllowed = false;
                sReason = reason;
            },
            uid
        };
        this._events.emit('command', oData);
        return {
            allowed: bAllowed,
            reason: sReason
        };
    }

    isNumericCommand (sCommand) {
        const nCommand = parseInt(sCommand);
        return !isNaN(nCommand);
    }

    /**
   * Renvoie true si la commande spécifiée existe.
   * @param sCommand {string} opcode d ela commande
   * @returns {boolean} true = la commande existe
   */
    isCommandExist (sCommand) {
        if (sCommand in this._scripts.commands) {
            return true;
        }
        return !!(this.isNumericCommand(sCommand) && this._acceptNumericCommand);
    }

    /**
   * Renvoie true si la pièce dans laquelle se trouve le joueur possède un hook de la commande spécifiée
   * @param uid {string} identifiant joueur
   * @param sCommand {string} commande envoyée
   * @return {string|undefined}
   */
    getRoomCommandHookScript (uid, sCommand) {
        const { context } = this._players[uid];
        const oPlayer = context.pc;
        const oRoom = oPlayer.location;
        const oCommandRegistry = oRoom.commands;
        return oCommandRegistry !== undefined && (sCommand in oCommandRegistry)
            ? oCommandRegistry[sCommand]
            : undefined;
    }

    /**
   * Lance un script en fonction de la commande.
   * @param uid {string} identifiant du joueur
   * @param sCommand {string} commande
   * @param args {string[]} liste des arguments
   * @throws
   */
    command (uid, sCommand, args) {
    /**
     * @var context {MUDContext}
     */
        const { context } = this._players[uid];
        // Est-ce que la pièce dans lequel se trouve le joueur
        // possède un event "command" ?
        const sRoomCommandScript = this.getRoomCommandHookScript(uid, sCommand);
        if (sRoomCommandScript) {
            const oCmdPayload = {
                ...context,
            };
            const symPreventDefault = Symbol('__preventDefault__');
            Object.defineProperty(oCmdPayload, symPreventDefault, {
                enumerable: false,
                configurable: false,
                writable: true,
                value: false
            });
            Object.defineProperty(oCmdPayload, '$preventDefault', {
                enumerable: true,
                configurable: false,
                writable: false,
                value: function (v = true) {
                    oCmdPayload[symPreventDefault] = v;
                }
            });
            const xResult = this.runScript(sRoomCommandScript, oCmdPayload, ...args);
            if (oCmdPayload[symPreventDefault]) {
                return xResult;
            }
        }
        if (this.isCommandExist(sCommand)) {
            // vérifier s'il est possible de lancer cette commande (droit, état)
            const {
                allowed,
                reason
            } = this.checkCommandUsability(uid, sCommand);
            if (allowed) {
                if (this.isNumericCommand(sCommand)) {
                    this._events.emit('numeric', {
                        $context: context,
                        $arguments: args,
                        command: parseInt(sCommand)
                    });
                } else {
                    return this.runScript(this._scripts.commands[sCommand], context, args);
                }
            } else {
                context.print(reason);
            }
        } else {
            throw new Error('ERR_UNKNOWN_COMMAND');
        }
    }

    defineAsset (id, sCategory, oData, bMerge = false) {
    // Checking ID duplicates
        const oDupChecker = this._dupChecker;
        if (oDupChecker.has(id) && !bMerge) {
            throw new Error('Asset id duplication : ' + id);
        }
        oDupChecker.add(id);
        const m = this._module;
        if (!(sCategory in m)) {
            m[sCategory] = {};
        }
        if (bMerge) {
            m[sCategory][id] = deepMerge(m[sCategory][id], oData);
        } else {
            m[sCategory][id] = oData;
        }
    }

    /**
   * Ajoute une propriété au contexte qui sera utilisé dans les commandes
   * @param sProperty {string} nom de la propriété
   * @param value {*} valeur de la propriété
   */
    addContextProperty (sProperty, value) {
        this._contextAdditionalProperties[sProperty] = value;
    }

    /**
   * Ajoute un nouveau service au contexte
   * @param sService {string} nom du service
   * @param instance {object} instance du service
   */
    addContextService (sService, instance) {
        if (!(SERVICE_CONTEXT_NS in this._contextAdditionalProperties)) {
            this._contextAdditionalProperties[SERVICE_CONTEXT_NS] = {};
        }
        logMud('Adding new service : %s', sService);
        this._contextAdditionalProperties[SERVICE_CONTEXT_NS][sService] = instance;
    }

    getContextService(sService) {
        return this.services[sService];
    }

    get services () {
        return this._contextAdditionalProperties[SERVICE_CONTEXT_NS] || {};
    }

    /**
   * Lance un script d'évènement
   * @param sEvent {string} nom de l'évènement
   * @param oEvent {object} payload du l'évènement
   * @returns {*&{system: System, engine: Engine}}
   */
    runEventScript (sEvent, oEvent) {
        const aEventScripts = this._scripts.events[sEvent];
        if (!oEvent) {
            oEvent = {};
        }
        oEvent.system = this;
        oEvent.engine = this._engine;
        if (aEventScripts) {
            for (const script of aEventScripts) {
                script(oEvent);
            }
        }
        return oEvent;
    }

    /**
   * Charge l'état initial du MUD dans l'engine.
   * @param oModule {object}
   */
    setEngineModule (oModule) {
        this._engine.setAssets(oModule);
        Object.keys(oModule).forEach(m => {
            const n = Object.values(oModule[m]).length;
            logMud('%s: %d item%s loaded', m, n, n > 1 ? 's' : '');
        });
    }

    getAsset (sCategory, sId = undefined) {
        if (sCategory in this._module) {
            if (sId !== undefined) {
                return this._module[sCategory][sId];
            } else {
                return this._module[sCategory];
            }
        } else {
            return {};
        }
    }

    getCommandList () {
        return Object.keys(this
            ._scripts
            .commands)
            .filter(s => !s.startsWith(COMMAND_ROOT_PREFIX) && !s.includes('/'))
            .map(s => {
                const name = s;
                const description = this._textRenderer.renderSync('commands.' + name, {});
                return {
                    name,
                    description
                };
            });
    }
}

module.exports = System;
