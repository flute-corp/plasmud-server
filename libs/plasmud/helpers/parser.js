const ArgumentSplitter = require('./argument-splitter');
const {
    findDirection,
    findContainer,
    findItem,
    findActorOrPlayer, findObject
} = require('./object-finder');

const PARSER_PATTERNS = {
    ITEM_FROM_CONTAINER: 'ITEM IN CONTAINER', // Identifie un item suivi d'un contenant. L'item est nécessairement dans le contenant
    ITEM_TO_CONTAINER: 'ITEM TO CONTAINER', // identifie un item de l'INVENTAIRE suivit d'un contenant
    ITEM_TO_ENTITY: 'ITEM TO ENTITY', // identifie un item de l'INVENTAIRE suivit d'une entity
    STRING_ON_ITEM: 'STRING ON ITEM', // une chaine (sort, compétence...) suivit d'un item de l'inventaire
    STRING_ON_ACTOR: 'STRING ON ACTOR', // une chaine (sort, compétence...) suivit d'un acteur
    STRING_ON_ACTOR_WITH_ITEM: 'STRING ON ACTOR WITH ITEM', // une chaine (sort, compétence) suivie d'un acteur, suivit d'un item (réactif)
    STRING_WITH_ITEM: 'STRING WITH ITEM', // une chaine (sort, compétence) suivit d'un item (réactif)
    STRING_ON_DIRECTION: 'STRING ON DIRECTION', // une chaine (sort, compétence...) suivit d'une direction
    DIRECTION_WITH_ITEM: 'DIRECTION WITH ITEM', // identifie une direction et un item de l'INVENTAIRE
    OBJECT_WITH_ITEM: 'OBJECT WITH ITEM', // identifie un objet de la pièce (uniquement)
    DIRECTION: 'DIRECTION', // identifie une direction
    ENTITY: 'ENTITY', // identifie une entité quelle que soit sa nature, de la pièce ou de l'inventaire
    ACTOR: 'ACTOR', // identifie un acteur
    ITEM: 'ITEM', // identifie un item de l'inventaire uniquement
    NONE: 'NONE', // identifie une ligne de commande vide
    DEFAULT: 'DEFAULT' // identifie la ligne quand toutes les autres échouent
};

const KEYWORDS = {
    TO: ['INTO', 'IN', 'TO'],
    IN: ['IN', 'FROM'],
    WITH: ['WITH', 'USING'],
    ON: ['ON']
};

function du (x) {
    return x === undefined
        ? 'undefined'
        : x === null
            ? 'null'
            : 'defined';
}

function db (x) {
    return !!x;
}

/**
 * A partir des arguments spécifiés, tente de reconnaitre un objet (item) à l'intérieur d'un contenant
 * ce parser est utilisé lorsqu'on souhaite regarder ou prendre un objet qui se trouve dans un contenant
 * @param context
 * @param aArguments
 * @returns {{container: MUDEntity, item: MUDEntity, count: number, keyword: string}|null}
 */
function parseItemInContainer (context, aArguments) {
    const ap = new ArgumentSplitter();
    ap.keywords = KEYWORDS.IN;
    const [oItemSpec, oContSpec] = ap.splitBetweenKeywords(aArguments);
    // il faut s'attendre à deux éléments
    if (oItemSpec && oContSpec) {
        const sKeyword = oContSpec.keyword;
        // Déterminer le contenant
        const { pc: oPlayer, engine } = context;
        // rechercher le container dans la pièce et dans l'inventaire
        /**
     * @type {{count: number, entity: MUDEntity}|null}
     */
        const oContainer = findContainer(engine, oContSpec.arguments, [oPlayer, oPlayer.location]); // ok entity
        if (oContainer.entity) {
            /**
       * @type {{count: number, entity: MUDEntity}}
       */
            const oItem = findItem(engine, oItemSpec.arguments, [oContainer.entity]); // ok entity
            if (oItem.entity) {
                // item trouvé
                return {
                    keyword: sKeyword,
                    container: oContainer.entity,
                    item: oItem.entity,
                    count: oItem.count
                };
            }
        }
    }
    return null;
}

/**
 * Recherche un item de l'inventaire du joueur ainsi qu'un contenant
 * @param context
 * @param aArguments
 * @returns {{container, item, count, keyword: string}}
 */
function parseInvItemToContainer (context, aArguments) {
    const ap = new ArgumentSplitter();
    ap.keywords = KEYWORDS.TO;
    const [oItemSpec, oContSpec] = ap.splitBetweenKeywords(aArguments);
    if (oItemSpec && oContSpec) {
        const sKeyword = oContSpec.keyword;
        // Déterminer le contenant
        const { pc: oPlayer, engine } = context;
        const oContainer = findContainer(engine, oContSpec.arguments, [oPlayer, oPlayer.location]); // ok entity
        const oItem = findItem(engine, oItemSpec.arguments, [oPlayer]); // ok entity
        if (oContainer.entity && oItem.entity && oContainer.entity.blueprint.inventory) {
            // item trouvé
            return {
                keyword: sKeyword,
                container: oContainer.entity,
                item: oItem.entity,
                count: oItem.count
            };
        }
    }
}

function parseInvItemToEntity (context, aArguments) {
    const ap = new ArgumentSplitter();
    ap.keywords = KEYWORDS.TO;
    const [oItemSpec, oEntitySpec] = ap.splitBetweenKeywords(aArguments);
    if (oItemSpec && oEntitySpec) {
        const sKeyword = oEntitySpec.keyword;
        // Déterminer le contenant
        const { pc: oPlayer, engine } = context;
        const oTarget = findObject(engine, oEntitySpec.arguments, [oPlayer.location]); // ok entity
        const oItem = findItem(engine, oItemSpec.arguments, [oPlayer]); // ok entity
        if (oTarget.entity && oItem.entity) {
            // item trouvé
            return {
                keyword: sKeyword,
                target: oTarget.entity,
                item: oItem.entity,
                count: oItem.count
            };
        }
    }
}

function parseRoomEntityWithItem (context, aArguments) {
    const { engine, pc: oPlayer } = context;
    const ap = new ArgumentSplitter();
    ap.keywords = KEYWORDS.WITH;
    const [oContSpec, oItemSpec] = ap.splitBetweenKeywords(aArguments);
    if (oContSpec && oItemSpec) {
        const oRoomEntity = findObject(engine, oContSpec.arguments, [oPlayer.location]); // ok entity
        const oItem = findItem(engine, oItemSpec.arguments, [oPlayer]); // ok entity
        return {
            entity: oRoomEntity.entity,
            item: oItem.entity
        };
    }
    return null;
}

/**
 * A partir des arguments spécifiés, tente de reconnaitre une entité de la pièce (item, creature, placeable)
 * Ce parser est utilisé lorsqu'on veut agir sur une entité de la pièce
 * @param context
 * @param aArguments
 * @returns {{count: number, entity: MUDEntity}|null}
 */
function parseRoomEntity (context, aArguments) {
    const { pc: oPlayer, engine } = context;
    return findObject(engine, aArguments, [oPlayer.location]); // ok return
}

/**
 * A partir des arguments spécifiés, tente de reconnaitre un item présent exclusivement dans l'inventaire du PJ
 * Ce parser est utilisé lorsqu'on veut agir sur un item de l'inventaire
 * @param context
 * @param aArguments
 * @returns {{count: number, item: MUDEntity}|null}
 */
function parseInvItem (context, aArguments) {
    const { pc, engine } = context;
    const r = findItem(engine, aArguments, [pc]);
    if (r) {
        r.item = r.entity;
        delete r.entity;
        return r;
    } else {
        return null;
    }
}

/**
 * A partir des arguments spécifiés, tente de reconnaitre un item présent dans la room ou dans l'inventaire du PC
 * @param context
 * @param aArguments
 * @returns {{count: number, item: MUDEntity}|null}
 */
function parseInvOrRoomItem (context, aArguments) {
    const { pc, engine } = context;
    const r = findItem(engine, aArguments, [pc, pc.location]);
    if (r) {
        if (r.entity) {
            r.item = r.entity;
            delete r.entity;
            return r;
        } else {
            throw new Error('findItem could not find : ' + aArguments.toString());
        }
    } else {
        return null;
    }
}


/**
 * Ce parser est utilisé lorsqu'on veut agir sur une direction avec un item de l'inventaire.
 * Typiquement ouvrir une porte avec une clé, ou crocheter la serrure d'une porte avec un outil
 * @param context
 * @param aArguments
 */
function parseDirectionWithInvItem (context, aArguments) {
    const ap = new ArgumentSplitter();
    ap.keywords = KEYWORDS.WITH;
    const [oDirSpec, oItemSpec] = ap.splitBetweenKeywords(aArguments);
    if (oDirSpec && oItemSpec) {
        const oDirection = findDirection(context, oDirSpec.arguments.join(' '));
        if (!oDirection) {
            return null;
        }
        const oItem = parseInvItem(context, oItemSpec.arguments);
        return {
            keyword: oItemSpec.keyword,
            direction: oDirection.direction,
            item: oItem.item
        };
    }
    return null;
}

/**
 * Analyse une chaine : elle doit etre du type "chaine quelconque ON désignation d'item"
 * @param context {*}
 * @param aArguments {string[]}
 * @returns {{item: MUDEntity, string: string, keyword: string}|null}
 */
function parseStringOnItem (context, aArguments) {
    // Instancier un argument parser
    const ap = new ArgumentSplitter();
    // déclaration des keywords possibles
    ap.keywords = KEYWORDS.ON;
    // Couper entre les keywords
    const [oStringSpec, oItemSpec] = ap.splitBetweenKeywords(aArguments);
    // On a bien le bon nombre de parts ?
    if (oStringSpec && oItemSpec) {
    // Rechercher l'item correspondant
        const oItem = parseInvItem(context, oItemSpec.arguments);
        if (!oItem) {
            // On n'a pas trouver d'item, on quitte
            return null;
        }
        // on a effectivement trouvé un item
        return {
            keyword: oItemSpec.keyword,
            item: oItem.item,
            string: oStringSpec.arguments.join(' ')
        };
    }
    // La commande ne correspond pas a une STRING ON ITEM
    return null;
}

function parseStringOnActorWithItem (context, aArguments) {
    const { engine, pc: oPlayer } = context;
    const ap = new ArgumentSplitter();
    ap.keywords = ['ON', 'USING'];
    const [oStringSpec, oTargetSpec, oItemSpec] = ap.splitBetweenKeywords(aArguments);
    if (oStringSpec && oTargetSpec && oItemSpec) {
        const oTarget = findActorOrPlayer(engine, oTargetSpec.arguments, [oPlayer.location]);
        const oItem = findItem(engine, oItemSpec.arguments, [oPlayer]);
        const sString = oStringSpec.arguments.join(' ');
        return {
            target: oTarget,
            item: oItem,
            string: sString
        };
    } else {
        return null;
    }
}

function parseStringWithItem (context, aArguments) {
    const { engine, pc: oPlayer } = context;
    const ap = new ArgumentSplitter();
    ap.keywords = ['USING'];
    const [oStringSpec, oItemSpec] = ap.splitBetweenKeywords(aArguments);
    if (oStringSpec && oItemSpec) {
        const oReagent = findItem(engine, oItemSpec.arguments, [oPlayer]);
        const sSpellName = oStringSpec.arguments.join(' ');
        return {
            item: oReagent,
            string: sSpellName
        };
    } else {
        return null;
    }
}

function parseStringOnActor (context, aArguments) {
    const { engine, pc: oPlayer } = context;
    const ap = new ArgumentSplitter();
    ap.keywords = KEYWORDS.ON;
    const [oStringSpec, oActorSpec] = ap.splitBetweenKeywords(aArguments);
    if (oStringSpec && oActorSpec) {
        const oTarget = findActorOrPlayer(engine, oActorSpec.arguments, [oPlayer.location]);
        if (!oTarget) {
            return null;
        }
        return {
            keyword: oActorSpec.keyword,
            actor: oTarget.entity,
            string: oStringSpec.arguments.join(' ')
        };
    }
    return null;
}

/**
 * Fonction de parsing d'une ligne de commande.
 * On défini les types de token suivant :
 * - OBJECT : Une entité (item, creature, placeable) présente dans la pièce
 * - ITEM : Une entité de type "item" uniquement, présent dans un container (le dernier ouvert, ou l'inventaire etc...)
 * - DIRECTION : une direction de la pièce courante.
 * - USING : sert de token de séparation spécifiant que la commande se sert d'un élément particulier pour resoudre
 * son action
 * @param context
 * @param aArguments
 * @param oPatterns
 * @returns {null|*}
 */
function parse (context, aArguments, oPatterns) {
    if (PARSER_PATTERNS.DIRECTION_WITH_ITEM in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.DIRECTION_WITH_ITEM];
        const r = parseDirectionWithInvItem(context, aArguments);
        if (r) {
            return fn(r);
        }
    }
    if (PARSER_PATTERNS.OBJECT_WITH_ITEM in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.OBJECT_WITH_ITEM];
        const r = parseRoomEntityWithItem(context, aArguments);
        if (r) {
            return fn(r);
        }
    }
    if (PARSER_PATTERNS.ITEM_FROM_CONTAINER in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.ITEM_FROM_CONTAINER];
        const r = parseItemInContainer(context, aArguments);
        if (r) {
            return fn(r);
        }
    }
    if (PARSER_PATTERNS.ITEM_TO_CONTAINER in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.ITEM_TO_CONTAINER];
        const r = parseInvItemToContainer(context, aArguments);
        if (r) {
            return fn(r);
        }
    }
    if (PARSER_PATTERNS.ITEM_TO_ENTITY in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.ITEM_TO_ENTITY];
        const r = parseInvItemToEntity(context, aArguments);
        if (r) {
            return fn(r);
        }
    }
    if (PARSER_PATTERNS.STRING_ON_ACTOR_WITH_ITEM in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.STRING_ON_ACTOR_WITH_ITEM];
        const r = parseStringOnActorWithItem(context, aArguments);
        if (r) {
            return fn(r);
        }
    }
    if (PARSER_PATTERNS.STRING_ON_ACTOR in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.STRING_ON_ACTOR];
        const r = parseStringOnActor(context, aArguments);
        if (r) {
            return fn(r);
        }
    }
    if (PARSER_PATTERNS.STRING_WITH_ITEM in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.STRING_WITH_ITEM];
        const r = parseStringWithItem(context, aArguments);
        if (r) {
            return fn(r);
        }
    }
    if (PARSER_PATTERNS.STRING_ON_ITEM in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.STRING_ON_ITEM];
        const r = parseStringOnItem(context, aArguments);
        if (r) {
            return fn(r);
        }
    }
    if (PARSER_PATTERNS.DIRECTION in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.DIRECTION];
        const r = findDirection(context, aArguments.join(' '));
        if (r) {
            return fn(r);
        }
    }
    if (PARSER_PATTERNS.ACTOR in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.ACTOR];
        const r = parseRoomEntity(context, aArguments);
        if (r) {
            if (context.engine.isActor(r.entity)) {
                return fn({ actor: r.entity });
            }
        }
    }
    if (PARSER_PATTERNS.ENTITY in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.ENTITY];
        const r = parseRoomEntity(context, aArguments);
        if (r) {
            return fn(r);
        }
    }
    if (PARSER_PATTERNS.ITEM in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.ITEM];
        const r = parseInvItem(context, aArguments);
        if (r) {
            return fn(r);
        }
    }
    if (PARSER_PATTERNS.NONE in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.NONE];
        if (aArguments.length === 0) {
            return fn();
        }
    }
    if (PARSER_PATTERNS.DEFAULT in oPatterns) {
        const fn = oPatterns[PARSER_PATTERNS.DEFAULT];
        return fn();
    }
}

module.exports = {
    parse,
    parseItemInContainer,
    parseRoomEntity,
    parseInvItem,
    parseDirectionWithInvItem,
    PARSER_PATTERNS
};
