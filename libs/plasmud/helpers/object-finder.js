const { suggest } = require('@laboralphy/did-you-mean'); // 2.2.1
const CONSTS = require('../consts');

let DIRECTION_REGISTRY = null;

const REGEXP_OBJECT = /^(.+)$/;
const REGEXP_N_OBJECT = /^(\d+)\s+(.+)$/;
const REGEXP_OBJECT_N = /^(.+)\s+(\d+)$/;
const REGEXP_OBJECT_X = /^(.+)\s*#(\d+)$/;
const REGEXP_N_OBJECT_X = /^(\d+)\s+(.+)\s*#(\d+)$/;
const REGEXP_OBJECT_X_N = /^(.+)\s*#(\d+)\s+(\d+)$/;

// Permet de filtrer le type de container auquel on souhaite restraindre la commande
const CONTAINER_TYPES = {
    ROOM: 1,
    INVENTORY: 2,
    CONTAINER: 4,
    ANY: 7
};

/**
 * Fonction de recherche avec preset
 * @param sWord {string} mot qu'on a tapé
 * @param aList {string[]} liste de mots valides
 * @returns {string} mot de la liste le plus proche de ce qu'on a tapé
 */
function presetSuggest (sWord, aList) {
    return suggest(sWord.trim(), aList, { multiterm: true, limit: 0 });
}

/**
 * Construit un registre de reconnaissance des directions à partir des noms de directions locales
 * @param engine
 * @returns {*[]}
 */
function buildDirectionRegistry (engine) {
    const aDirRegistry = [];
    const aAntiDup = new Set();

    /**
   * Pushes a new reg exp in registry if not already pushed
   * We need locale short/long but avoid name overlap
   * We don't want "n" (north) and "n" (nord)
   * @param sDirectionCode {string} direction code (n, e, w, s, ne, ...)
   * @param sDirectionDisplayName {string} direction display name
   */
    const pushDirReg = (sDirectionCode, sDirectionDisplayName) => {
        if (!aAntiDup.has(sDirectionDisplayName)) {
            aDirRegistry.push({
                expr: new RegExp('^(' + sDirectionDisplayName + ')$', 'i'),
                direction: sDirectionCode,
                code: false
            });
            aDirRegistry.push({
                expr: new RegExp('^(' + sDirectionDisplayName + ') +([0-9]+)$', 'i'),
                direction: sDirectionCode,
                code: true
            });
            aAntiDup.add(sDirectionDisplayName);
        }
    };

    const aDirKeys = ['n', 'e', 'w', 's', 'ne', 'nw', 'se', 'sw', 'u', 'd'];
    aDirKeys.forEach(d => {
        const aWords = d.split('').map(w => engine.text('dir.' + w));
        const aMiniWords = aWords.map(w => w.substring(0, 1));
        const sExp = aWords.join('.');
        const sMiniExp = aMiniWords.join('');
        pushDirReg(d, sExp);
        pushDirReg(d, sMiniExp);
        pushDirReg(d, d);
    });
    return aDirRegistry;
}

/**
 * Construit une fonction qui va mapper une liste d'objet (issue de la fonction getLocalEntities) en une liste
 * d'objet un peu plus riche
 * @returns {function(*): {lid: *, name: *, containerType: *, entity: *}}
 */
function fObjectMapper () {
    return e => ({
        name: e.name, // nom de l'objet
        entity: e // objet
    });
}

/**
 * Fabrique la liste d'objet contenue dans le container spécifié
 * @param engine {*}
 * @param roomOrEntity {MUDEntity|string} identifiant du conteneur
 * @returns {MUDEntity[]}
 */
function buildContainerObjectList (engine, roomOrEntity) {
    return engine
        .getLocalEntities(roomOrEntity); // getLocalEntities: ok
}

/**
 * Construit la liste des objet qu'il sera possible de consulter pour tenter de trouver celui spécifié par la dernière
 * commande tapée par le joueur.
 * en fonction du type de container type, la fonction se limitera à un certain type de container.
 * ROOM : on se limite à ce que est possé au sol
 * INVENTORY : on se limite à ce qu'il y a dans l'inventaire du joueur
 * CONTAINER : on se limite à ce qu'il y a dans le "dernier container ouvert"
 * @param engine
 * @param aContainers
 * @param fEntityFilter {function}
 * @returns {MUDEntity[]}
 */
function buildObjectList (engine, aContainers, fEntityFilter = undefined) {
    const c1 = aContainers.map(oContainer => buildContainerObjectList(engine, oContainer)).flat();
    return (typeof fEntityFilter === 'function')
        ? c1.filter((entity, i, a) => fEntityFilter(entity, i, a))
        : c1;
}

/**
 * Parse une direction et renvoie le code-directionnel évalué
 * @param engine
 * @param sDirection {string}
 * @returns {{direction: string, code: number}|null}
 */
function findDirection (engine, sDirection) {
    if (!DIRECTION_REGISTRY) {
        DIRECTION_REGISTRY = buildDirectionRegistry(engine);
    }
    for (const { expr, exprAlias, direction, code } of DIRECTION_REGISTRY) {
    // pushing locale
        const aExpr = [expr];
        if (exprAlias) {
            // Additional expression alias (standard form n, e, w, s, ne, nw....)
            aExpr.push(exprAlias);
        }
        for (const r of aExpr) {
            const result = sDirection.match(r);
            if (result) {
                return {
                    direction,
                    code: code ? parseInt(result[2]) : 0
                };
            }
        }
    }
    return null;
}

/**
 * Trouve un objet par son nom, en fonction du filtre, associe un nombre d'exemplaires
 * Si il existe plusieurs objets du même nom, on peut distinguer l'objet désiré par son rang (un nombre entier commençant par zéro)
 *
 * @param engine {*}
 * @param sName {string}
 * @param nObjectCount {number}
 * @param aContainers {string[]}
 * @param iRank {number}
 * @param fEntityFilter {function} filtre sur les types d'objets
 * @returns {{ entity: MUDEntity, count: number }|null}
 */
function findObjectByName (engine, sName, nObjectCount, aContainers, iRank = 0, fEntityFilter = undefined) {
    if (iRank < 0) {
        throw new Error('ERR_INVALID_LIST_OBJECT_RANK');
    }
    const aObjectList = buildObjectList(engine, aContainers, fEntityFilter);
    const aNames = aObjectList.map(entity => entity.name);
    const sSelectedName = presetSuggest(sName, aNames);
    const aEntities = aObjectList.filter(entity => entity.name === sSelectedName);
    const oEntity = aEntities[iRank];
    if (oEntity) {
        return {
            entity: oEntity,
            count: engine.isItemStackable(oEntity) ? nObjectCount : 1
        };
    } else {
        return null;
    }
}

/**
 * Parmis toutes les entités contenus dans l'ensemble des contenants spécifiés, on recherche l'entité dont le nom
 * correspond le mieux à la chaine de caractètre spécifiée
 * @param engine {*}
 * @param sString {string|string[]} nom de l'objet recherché (critère de recherche)
 * @param aContainers {string[]} ensemble des identifiant de contenant dans lesquels chercher l'objet
 * @param fEntityFilter {function} filtre sur les types d'entité recherchées
 * @returns {{ entity: MUDEntity, count: number }|null}
 */
function findObject (engine, sString, aContainers, fEntityFilter = undefined) {
    if (Array.isArray(sString)) {
        sString = sString.join(' ');
    }
    let r;

    // command <N> <OBJECT> <X>
    r = sString.match(REGEXP_N_OBJECT_X);
    if (r) {
        const [dummy, n, sObject, nRank] = r;
        return findObjectByName(engine, sObject, parseInt(n), aContainers, parseInt(nRank) - 1, fEntityFilter);
    }

    // command <OBJECT> <X> <N>
    r = sString.match(REGEXP_OBJECT_X_N);
    if (r) {
        const [dummy, sObject, nRank, n] = r;
        return findObjectByName(engine, sObject, parseInt(n), aContainers, parseInt(nRank) - 1, fEntityFilter);
    }

    // command <OBJECT> <X>
    r = sString.match(REGEXP_OBJECT_X);
    if (r) {
        const [dummy, sObject, nRank] = r;
        return findObjectByName(engine, sObject, 1, aContainers, parseInt(nRank) - 1, fEntityFilter);
    }

    // command <N> <OBJECT>
    r = sString.match(REGEXP_N_OBJECT);
    if (r) {
        const [dummy, n, sObject] = r;
        return findObjectByName(engine, sObject, parseInt(n), aContainers, 0, fEntityFilter);
    }

    // command <OBJECT> <N>
    r = sString.match(REGEXP_OBJECT_N);
    if (r) {
        const [dummy, sObject, n] = r;
        return findObjectByName(engine, sObject, parseInt(n), aContainers, 0, fEntityFilter);
    }

    // command <OBJECT>
    r = sString.match(REGEXP_OBJECT);
    if (r) {
        const [dummy, sObject] = r;
        return findObjectByName(engine, sObject, Infinity, aContainers, 0, fEntityFilter);
    }

    return null;
}

/**
 * Trouve un container dans la liste des location spécifiée
 * @param engine
 * @param aArguments
 * @param aLocations
 * @returns {{count: number, entity: *}|null|{count, entity}}
 */
function findContainer (engine, aArguments, aLocations) {
    return findObject(engine, aArguments, aLocations, ent => engine.isContainer(ent)); // ok return
}

/**
 * Trouve un item transportable dans la liste des locations spécifiée
 * @param engine
 * @param aArguments
 * @param aLocations
 * @returns {{count: number, entity: MUDEntity}|null}
 */
function findItem (engine, aArguments, aLocations) {
    const ENTITY_TYPE_ITEM = CONSTS.ENTITY_TYPES.ITEM;
    return findObject(engine, aArguments, aLocations, ent => ent.blueprint.type === ENTITY_TYPE_ITEM); // ok return
}

function findActorOrPlayer (engine, aArguments, aLocations) {
    const TYPE_ACTOR = CONSTS.ENTITY_TYPES.ACTOR;
    const TYPE_PLAYER = CONSTS.ENTITY_TYPES.PLAYER;
    return findObject(engine, aArguments, aLocations, ent => { // ok return
        const sType = ent.blueprint.type;
        return sType === TYPE_ACTOR || sType === TYPE_PLAYER;
    });
}

module.exports = {
    findObject,
    findContainer,
    findItem,
    findActorOrPlayer,
    findDirection,
    buildDirectionRegistry,
    CONTAINER_TYPES
};
