const ENTITY_COOLDOWN_REGISTRY = 'ENTITY_COOLDOWN_REGISTRY';

class Cooldown {
    constructor () {
        this._registryName = ENTITY_COOLDOWN_REGISTRY;
        this._dateNowMethod = function () {
            return Date.now();
        };
    }
    set registryName (value) {
        this._registryName = value;
    }

    get registryName () {
        return this._registryName;
    }

    /**
     *
     * @param value {function}
     */
    set dateNowMethod (value) {
        this._dateNowMethod = value;
    }

    /**
     *
     * @returns {function}
     */
    get dateNowMethod () {
        return this._dateNowMethod;
    }

    /**
     *
     * @returns {number}
     */
    get now () {
        return this.dateNowMethod();
    }

    /**
     *
     * @param data
     * @param sKey
     * @returns {{init: boolean, ts: number, delay: number}}
     */
    getCooldownRegistry (data, sKey) {
        if (!(this.registryName in data)) {
            data[this.registryName] = {};
        }
        if (!(sKey in data[this.registryName])) {
            data[this.registryName][sKey] = {
                init: false,
                ts: 0,
                delay: 0
            };
        }
        return data[this.registryName][sKey];
    }

    isCooldown (data, sKey, nDelay) {
        const n = this.now;
        const nDelayms = nDelay * 1000;
        const d = this.getCooldownRegistry(data, sKey);
        const { init, ts, delay } = d;
        if (!init || (n < ts) || (n >= (ts + delay))) {
            d.init = true;
            d.delay = nDelayms;
            d.ts = n;
            return true;
        } else {
            return false;
        }
    }
}

module.exports = Cooldown;
