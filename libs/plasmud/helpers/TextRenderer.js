const TR = require('../../text-renderer');
const TPI18n = require('../../text-renderer/TextProducerI18next');
const TPTwig = require('../../text-renderer/TextProducerTwig');

class TextRenderer extends TR {
    constructor () {
        super();
        this.addProducer('strings', new TPI18n());
        this.addProducer('templates', new TPTwig());
    }
}

module.exports = TextRenderer;
