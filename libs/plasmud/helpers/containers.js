const CONSTS = require('../consts');
/**
 * Renvoie true si le contenant est verrouillable
 * @param oEntity {MUDEntity}
 * @returns {boolean}
 */
function isEntityLockable(oEntity) {
    return !!oEntity.blueprint.lock;
}

/**
 * Renvoie la liste des item contenu dans un contenant
 * @param context
 * @param roomOrEntity
 * @returns {any}
 */
function getEntityList (context, roomOrEntity) {
    const { engine, pc } = context;
    return Object
        .values(context.engine.getLocalEntities(roomOrEntity)) // getLocalEntities: ok
        .filter(entity => entity !== pc)
        .map(entity => ({
            id: entity.id,
            name: entity.name,
            desc: entity.desc,
            dead: entity.dead,
            visible: engine.getEntityVisibility(entity, pc) === CONSTS.VISIBILITY_VISIBLE,
            is: {
                placeable: engine.isPlaceable(entity),
                item: engine.isItem(entity),
                actor: engine.isActor(entity)
            },
            remark: entity.remark,
            stack: entity.stack,
            locked: entity.locked,
            lockable: isEntityLockable(entity),
            inventory: entity.inventory && !entity.locked
                ? getEntityList(context, entity)
                : []
        }));
}

function moreFilter (context, array, max) {
    return (array.length > max || context.services.more)
        ? context.services.more.filter(context.pc, array, max)
        : {
            page: 0,
            items: array,
            start: 0,
            left: 0,
            total: array.length
        };
}


module.exports = {
    getEntityList,
    isEntityLockable,
    moreFilter
};
