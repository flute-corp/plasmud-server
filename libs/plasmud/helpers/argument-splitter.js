class ArgumentSplitter {
    constructor () {
        this._keywords = new Set();
        this._types = {};
    }

    /**
   * Défini la liste des mots clé
   * @param value {string[]} tableau de mots clés. Les doublons sont éliminés, les mots-clé sont minusculisé
   */
    set keywords (value) {
        this._keywords = new Set(value.map(s => s.toLowerCase()));
    }

    /**
   * Renvoie la liste des mots clés précédemment défini
   * @returns {string[]}
   */
    get keywords () {
        return [...this._keywords];
    }

    /**
   * Repond TRUE si la chaine spécifié est un des mots clé précédemment définis
   * @param s {string}
   * @returns {boolean}
   */
    isKeyword (s) {
        if (typeof s !== 'string') {
            return false;
        }
        return this._keywords.has(s.toLowerCase());
    }

    /**
   * Découpe un tableau selon de chaine les mots clés précédemment définis
   * Les mots clés sont défini grâce à set keywords
   * @param aArguments {string[]}
   * @returns {{keyword: string, arguments: string[]}[]}
   */
    splitBetweenKeywords (aArguments) {
        let oCurrent = {
            keyword: '',
            arguments: []
        };
        const oStructure = [oCurrent];
        for (const sArg of aArguments) {
            if (this.isKeyword(sArg)) {
                // changement de mot clé
                oCurrent = {
                    keyword: sArg.toLowerCase(),
                    arguments: []
                };
                oStructure.push(oCurrent);
            } else {
                oCurrent.arguments.push(sArg);
            }
        }
        return oStructure;
    }

    /**
   * Définition d'un type. Le callback spécifié a besoin d'un parametre qui sera un string[]
   * et doit retourner la valeur a retransmettre dans la chaine de sequence
   * @param sType {string}
   * @param fnValidator {function}
   */
    defineType (sType, fnValidator) {
        this._types[sType.toLowerCase()] = fnValidator;
    }

    isType (sType) {
        return sType.toLowerCase() in this._types;
    }

    callTypeValidator (sType, args) {
        return this._types[sType.toLowerCase()](args);
    }

    /**
   * @param aSlices {{keyword: string, arguments: string[]}[]} ce qui à été analysé par sliceBetweenKeywords
   * @param aSequence {string[]} sequence de pattern attendu
   */
    checkSlices (aSlices, aSequence) {
        let iSlice = 0, iSeq = 0;
        const nLenSeq = aSequence.length;
        let sNextKeyword = '';
        const aOutput = [];
        while (iSeq < nLenSeq) {
            const seq = aSequence[iSeq].toLowerCase();
            const slice = aSlices[iSlice];
            if (slice.arguments === undefined) {
                throw new Error('arguments not defined in this slice');
            }
            if (this.isType(seq)) {
                aOutput.push(this.callTypeValidator(seq, slice.arguments));
                ++iSlice;
            } else if (this.isKeyword(seq)) {
                sNextKeyword = seq.toLowerCase();
            }
            ++iSeq;
        }
        return aOutput.some(s => s === null) ? null : aOutput;
    }
}

module.exports = ArgumentSplitter;
