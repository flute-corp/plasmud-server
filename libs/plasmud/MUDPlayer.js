const MUDActor = require('./MUDActor');
const { checkType } = require('../get-type');
const CONSTS = require('./consts');

class MUDPlayer extends MUDActor {
    /**
     *
     * @param options {{ name: string, uid: string, blueprint: MUDBlueprint }}
     */
    constructor (options) {
        super({
            ...options,
            ref: '',
            blueprint: {
                type: CONSTS.ENTITY_TYPES.PLAYER,
                name: options.name,
                desc: [],
                inventory: true
            }
        });
        this._uid = options.uid;
        this._creation = Date.now();
    }

    get uid () {
        return this._uid;
    }

    get creation () {
        return this._creation;
    }

    get dead () {
        return this._dead;
    }

    set dead (value) {
        checkType(value, 'boolean');
        this._dead= value;
    }
}

module.exports = MUDPlayer;
