const MUDEntity = require('./MUDEntity');
const {checkType} = require('../get-type');

class MUDActor extends MUDEntity {
    constructor (options) {
        super(options);
        this._dead = false;
    }

    get weight () {
        return this.inventory
            ? this.engine
                .getLocalEntities(this) // getLocalEntities: ok
                .reduce((prev, curr) => curr.weight + prev, 0)
            : 0;
    }

    get dead () {
        return this._dead;
    }

    set dead (value) {
        checkType(value, 'boolean');
        this._dead = value;
    }
}

module.exports = MUDActor;
