const { checkType} = require('../get-type');
const { deepClone} = require('@laboralphy/object-fusion');

class MUDRoom {
    constructor ({
        id,
        sector,
        nav = {},
        position = null,
        content = [],
        desc,
        name,
        events = {},
        commands = {},
        data = {}
    }) {
        checkType(name, 'string');
        checkType(id, 'string');
        checkType(sector, 'string');
        desc.every(s => checkType(s, 'string'));
        this._factoryData = {
            data
        };
        this._id = id;
        this._sector = sector;
        this._nav = nav;
        this._content = content;
        this._desc = desc;
        this._name = name;
        this._events = events;
        this._commands = commands;
        this._position = position;
        this._data = deepClone(data);
        this._exits = Object.keys(nav);
    }

    get id () {
        return this._id;
    }

    get sector () {
        return this._sector;
    }

    get nav () {
        return this._nav;
    }

    get content () {
        return this._content;
    }

    get data () {
        return this._data;
    }

    get desc () {
        return this._desc;
    }

    get name () {
        return this._name;
    }

    get events () {
        return this._events;
    }

    get commands () {
        return this._commands;
    }

    get position () {
        return this._position;
    }

    get exits () {
        return this._exits;
    }

    reset () {
        this._data = deepClone(this._factoryData.data);
    }
}

module.exports = MUDRoom;
