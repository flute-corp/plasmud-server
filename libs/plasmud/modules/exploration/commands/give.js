/**
 *
 * @param context {MUDContext}
 * @param item
 * @param count
 * @param other
 */
function giveItemToOther (context, item, count, other) {
    const { engine, print, pc } = context;
    if (other !== pc && (engine.isActor(other) || engine.isPlayer(pc))) {
        engine.moveEntity(item, other, count);
        const giver = pc.name;
        const receiver = other.name;
        print('give.action.itemGiven', { item: item.name, count, receiver });
        print.to(other, 'give.action.itemReceived', { item: item.name, count, giver });
        print.room.except([pc, other], 'give.room.itemGiven', { giver, item: item.name, count, receiver });
    } else {
        print('give.error.badPlayer');
    }
}

module.exports = function main (context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.ITEM_TO_ENTITY]: p => giveItemToOther(context, p.item, p.count, p.target),
        [context.PARSER_PATTERNS.NONE]: () => context.print('generic.error.needArgument'),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};

