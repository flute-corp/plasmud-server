const CONSTS = require('../../../consts');
/**
 * SEARCH
 * @author ralphy
 * @date 25-12-2021
 * Le joueur utilise cette commande pour tenter de détecter les issues cachées dans une pièce.
 * Les passages secrets etc...
 * Il faut pouvoir limiter cette compétence. Un PJ ne peut pas passer son temps à chercher des passages secrets sans
 * qu'il y ait une répercussion sur le temps et l'énergie que ça lui coute.
 * 1) soit la commande "prend du temps"...
 * 2) soit la commande prend de l'énergie (celle utilisée pour effectuer les autres actions). L'energie se recharge au cour du temps
 * 3) soit la commande nécessite un outil consommable (une "sonde"...)
 * 4) soit un item du type carte au trésor ou indice
 */

/**
 * Chercher dans une direction s'il existe une issue secrete, ou si l'issue visible est piégée
 * @param context {MUDContext}
 * @param sDirection {string}
 * @param oTool {MUDEntity|null}
 */
function searchDirection (context, sDirection, oTool = null) {
    const { print, engine, pc } = context;
    const { success, outcome } = engine.actionSearchSecret(pc, sDirection, oTool ? oTool.id : null);
    // obtenir valeur skill
    // déterminer succès
    // a-t-on besoin de détecter la porte ?
    if (outcome === CONSTS.SEARCH_OUTCOMES.NO_NEED_DETECTION) {
        print('search.failure.exitVisible', { dir: sDirection });
        return;
    }
    const oRoom = pc.location;
    // La découverte d'un secret sera partagée avec les autres joueurs de la pièce
    const aOtherPlayers = engine.getLocalEntities(oRoom) // getLocalEntities: ok
        .filter(entity => entity.blueprint.type === engine.CONSTS.ENTITY_TYPES.PLAYER);
    if (success) {
    // partager la découverte avec les autres joueurs de la pièce
        aOtherPlayers
            .forEach(p => {
                engine.setEntityExitSpotted(p, sDirection, true);
            });
        // on a découvert une nouvelle porte cachée : le dire à tous
        print('search.action.exitFound', { dir: sDirection });
        print.room('search.room.exitFound', { name: pc.name, dir: sDirection });
    } else {
    // on n'arrive pas à déceler la porte
        if (outcome === CONSTS.SEARCH_OUTCOMES.COOLDOWN) {
            print('search.failure.cooldown');
        } else {
            print('search.failure.exitNotFound', { dir: sDirection });
        }
    }
}

module.exports = function main (context, params) {
    context.parse(params, {
        [context.PARSER_PATTERNS.DIRECTION_WITH_ITEM]: p => searchDirection(context, p.direction, p.item),
        [context.PARSER_PATTERNS.DIRECTION]: p => searchDirection(context, p.direction),
        [context.PARSER_PATTERNS.NONE]: () => context.print('generic.error.needArgument'),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};

