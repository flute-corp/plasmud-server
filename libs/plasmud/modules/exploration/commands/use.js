/**
 * use
 * @author ralphy
 * @date 28-08-2024
 * Cas d'utilisation : Le joueur utilise cette commande lorsqu'il souhaite activer un objet placeable, ou
 * un objet de son inventaire
 */

/**
 * Use a placeable with an inventory item
 * @param context {MUDContext}
 * @param oPlaceable {MUDEntity}
 * @param oItem {MUDItem}
 */
function usePlaceableWithItem (context, oPlaceable, oItem) {
    context.engine.actionUse(context.pc, oPlaceable, oItem);
}

/**
 * Use a placeable without any item
 * @param context {MUDContext}
 * @param oPlaceable {MUDEntity}
 */
function usePlaceable (context, oPlaceable) {
    context.engine.actionUse(context.pc, oPlaceable);
}

/**
 * Use an inventory item
 * @param context {MUDContext}
 * @param oItem
 */
function useItem (context, oItem) {
    context.engine.actionUse(context.pc, oItem);
}

module.exports = function main (context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.OBJECT_WITH_ITEM]: p => usePlaceableWithItem(context, p.entity, p.item),
        [context.PARSER_PATTERNS.ITEM]: p => useItem(context, p.item),
        [context.PARSER_PATTERNS.ENTITY]: p => usePlaceable(context, p.entity),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};
