function goDirection (context, sDirection) {
    const { print, engine, pc: oPlayer } = context;
    const { locked, destination, from, success } = engine.actionPassThruExit(oPlayer, sDirection);
    if (success) {
        print.room(from, 'go.room.playerExitRoom', { dir: sDirection, name: oPlayer.name });
        print('go.action.youMoveRoom', { dir: sDirection, room: destination.name });
        print.room('go.room.playerEnterRoom', { dir: sDirection, name: oPlayer.name });
    } else {
        if (locked) {
            print('go.failure.exitIsLocked', { dir: sDirection });
        } else {
            print('go.failure.noExitHere', { dir: sDirection });
        }
    }
}

module.exports = function main (context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.DIRECTION]: p => goDirection(context, p.direction),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};

