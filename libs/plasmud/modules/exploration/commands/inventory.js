const { getEntityList } = require('../../../helpers/containers');

/**
 * GET
 * @author ralphy
 * @date 2022-03-03
 * Cette commande visualise le contenu de l'inventaire du PJ
 * Syntaxe : INV
 */

function displayInventory (context) {
    const { engine, print, pc } = context;
    print('inventory-list', { entities: engine.getLocalEntities(pc).map(entity => ({ // getLocalEntities: ok
        name: entity.name,
        visible: true,
        remark: entity.remark,
        dead: false,
        stack: entity.stack
    }))
    });
}

/**
 * Affiche la description d'un objet
 * @param context
 * @param entity {MUDEntity}
 */
function displayItem (context, entity) {
    const { print } = context;
    const oDescriptor = getItemDescriptor(context, entity);
    print('look/item', oDescriptor);
}

function getItemDescriptor (oContext, oEntity) {
    const { pc } = oContext;
    // si c'est un contenant, récupérer les liste des objets contenu
    const bLocked = oEntity.locked;
    const bContainer = oEntity.blueprint.inventory;
    const aInventory = (bContainer && !bLocked)
        ? getEntityList(oContext, oEntity)
        : [];
    const bInMyPocket = oEntity.location === pc;
    return {
        name: oEntity.name,
        desc: oEntity.desc,
        type: oEntity.blueprint.subtype,
        weight: oEntity.weight,
        stack: oEntity.stack,
        locked: bLocked,
        lockable: oEntity.lockable,
        container: bContainer,
        inventory: aInventory,
        inmypocket: bInMyPocket
    };
}


module.exports = function main (context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.ITEM]: p => displayItem(context, p.item),
        [context.PARSER_PATTERNS.NONE]: () => displayInventory(context),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};
