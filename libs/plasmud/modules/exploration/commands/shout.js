module.exports = function main (context, text) {
    const { engine, pc: oPlayer } = context;
    const sText = text.join(' ');
    engine.speakString(oPlayer, oPlayer.location, sText, engine.CONSTS.VOLUME_SHOUT);
};

