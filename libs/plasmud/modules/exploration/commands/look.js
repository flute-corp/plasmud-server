const { getEntityList, isEntityLockable } = require('../../../helpers/containers');

/**
 * LOOK
 * @author ralphy
 * @date 25-12-2021
 * Le joueur utilise cette commande pour regarder la description de :
 * 1) la pièce dans laquelle il se trouve.
 * 2) n'importe quelle entité du jeu (créature, meuble, objet, issue).
 * 3) un objet de l'inventaire du dernier contenant ouvert
 */

function getRoomDescriptor (oContext) {
    const { pc, engine, text } = oContext;
    const oRoom = pc.location;
    const name = oRoom.name;
    const aEnvironments = engine.getRoomEnvironment(oRoom);
    const aExitStatus = oRoom.exits.map(dir => engine.getExit(pc, dir)); // ok getExit
    const exits = aExitStatus
        .filter(e => e.isDetectedBy(pc)).map(e => ({
            dir: e.direction,
            label: text('dir.' + e.direction),
            locked: e.locked,
            lockable: e.lockable
        }));
    const sRoomPerception = engine.getRoomVisibility(oRoom, pc);
    const bCanSeeAround = sRoomPerception === engine.CONSTS.VISIBILITY_VISIBLE;
    const desc = bCanSeeAround ? oRoom.desc : [];
    const entities = bCanSeeAround ? getEntityList(oContext, oRoom) : [];
    return {
        name,
        desc,
        perception: {
            blindness: sRoomPerception === engine.CONSTS.VISIBILITY_BLIND,
            darkness: sRoomPerception === engine.CONSTS.VISIBILITY_DARKNESS
        },
        environment: aEnvironments
            .map(e => {
                const sLabel = text('environment.' + e.environment);
                const sDesc = e.desc;
                return sLabel + ' : ' + sDesc;
            }),
        exits,
        entities: {
            placeables: entities.filter(e => e.is.placeable),
            items: entities.filter(e => e.is.item),
            actors: entities.filter(e => e.is.actor)
        }
    };
}

function getItemDescriptor (oContext, oEntity) {
    const { pc } = oContext;
    // si c'est un contenant, récupérer les liste des objets contenu
    const bLocked = oEntity.locked;
    const bContainer = oEntity.blueprint.inventory;
    const aInventory = (bContainer && !bLocked)
        ? getEntityList(oContext, oEntity)
        : [];
    const bInMyPocket = oEntity.location === pc;
    return {
        name: oEntity.name,
        desc: oEntity.desc,
        type: oEntity.blueprint.subtype,
        weight: oEntity.weight,
        stack: oEntity.stack,
        locked: bLocked,
        lockable: isEntityLockable(oEntity),
        container: bContainer,
        inventory: aInventory,
        inmypocket: bInMyPocket
    };
}

function getPlaceableDescriptor (oContext, oEntity) {
    // si c'est un contenant, récupérer les liste des objets contenu
    const bLocked = oEntity.locked;
    const bContainer = oEntity.blueprint.inventory;
    const aInventory = (bContainer && !bLocked)
        ? getEntityList(oContext, oEntity)
        : [];
    return {
        name: oEntity.name,
        desc: oEntity.desc,
        type: oEntity.blueprint.subtype,
        locked: bLocked,
        lockable: isEntityLockable(oEntity),
        container: bContainer,
        inventory: aInventory
    };
}

function getActorDescriptor ({ engine, text }, entity) {
    return {
        name: entity.name,
        desc: entity.desc,
        dead: entity.dead
    };
}

function getPlayerDescriptor ({ engine, text }, entity) {
    return {
        name: entity.name,
        desc: entity.desc,
        dead: entity.dead
    };
}

function getDirectionDescriptor ({ pc, engine, text }, direction) {
    const oExit = engine.getExit(pc, direction); // ok getExit
    if (oExit.valid && oExit.isDetectedBy(pc)) {
        return {
            direction: text('toDirection', { dir: direction }),
            name: oExit.name,
            desc: oExit.desc.length > 0 ? oExit.desc : null,
            locked: oExit.locked,
            lockable: oExit.lockable,
            discovered: true,
            roomdest: oExit.destination.name
        };
    } else {
        return null;
    }
}

/**
 * Affiche la description d'un objet
 * @param context
 * @param entity {MUDEntity}
 */
function lookEntity (context, entity) {
    const { print } = context;
    const CONST = context.engine.CONSTS;
    switch (entity.blueprint.type) {
    case CONST.ENTITY_TYPES.ITEM: {
        const oDescriptor = getItemDescriptor(context, entity);
        print('look/item', oDescriptor);
        break;
    }
    case CONST.ENTITY_TYPES.PLACEABLE: {
        const oDescriptor = getPlaceableDescriptor(context, entity);
        print('look/placeable', oDescriptor);
        break;
    }
    case CONST.ENTITY_TYPES.ACTOR: {
        const oDescriptor = getActorDescriptor(context, entity);
        print('look/actor', oDescriptor);
        break;
    }
    case CONST.ENTITY_TYPES.PLAYER: {
        const oDescriptor = getPlayerDescriptor(context, entity);
        print('look/player', oDescriptor);
        break;
    }
    }
}

/**
 * affiche la description d'une direction
 * si la direction est invalide : affiche un message d'erreur
 * @param context
 * @param direction {string}
 */
function lookDirection (context, direction) {
    const { print } = context;
    const oDescriptor = getDirectionDescriptor(context, direction);
    if (oDescriptor) {
        print('look/direction', oDescriptor);
    } else {
        print('generic.error.directionInvalid');
    }
}

/**
 * Affichage d'un descriptif de la pièce dans laquel se trouve le joueur
 * @param context
 */
function lookRoom (context) {
    const { print } = context;
    const oDescriptor = getRoomDescriptor(context);
    print('look/room', oDescriptor);
}

module.exports = function main (context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.ITEM_FROM_CONTAINER]: p => lookEntity(context, p.item),
        [context.PARSER_PATTERNS.DIRECTION]: p => lookDirection(context, p.direction),
        [context.PARSER_PATTERNS.ENTITY]: p => lookEntity(context, p.entity),
        [context.PARSER_PATTERNS.ITEM]: p => lookEntity(context, p.item),
        [context.PARSER_PATTERNS.NONE]: () => lookRoom(context),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};

