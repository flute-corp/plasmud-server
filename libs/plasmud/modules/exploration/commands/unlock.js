/**
 * unlock
 * @author ralphy
 * @date 25-12-2021
 * Cas d'utilisation : Le joueur utilise cette commande quand il veut déverrouiller un contenant (coffre, sac)
 * fermé à clé et qui se situe dans la pièce ou dans son inventaire.
 * La commande sert également à déverrouiller des portes verrouillées.
 * Syntaxe 1 : unlock <container> using <key-or-tool>
 * - container : objet visible possédant un inventaire.
 * - key-or-tool : référence à une clé de l'inventaire, ou un outil de crochetage de l'inventaire
 * Syntaxe 2 : unlock <direction> using <key-or-tool>
 * permet d'ouvrir une porte dans une direction donnée, n'est utile que pour les portes
 * verrouillées, permet d'utiliser une clé spécifique, ou un outil de crochetage
 */

function unlockContainerUsingKeyOrTool (context, oContainer, oItem) {
    const { engine, pc, print } = context;
    const { success, outcome } = engine.actionUnlock(pc, oContainer, oItem.id);
    const OUTCOMES = engine.CONSTS.UNLOCK_OUTCOMES;
    const name = pc.name;
    if (success) {
        print('unlock.action.containerUnlocked', {
            container: oContainer.name,
            item: oItem.name
        });
        print.room('unlock.room.containerUnlocked', {
            name,
            container: oContainer.name,
            item: oItem.name
        });
    } else {
        switch (outcome) {
        case OUTCOMES.NOT_LOCKED: {
            print('unlock.error.containerNotLocked');
            break;
        }

        case OUTCOMES.WRONG_KEY: {
            print('unlock.failure.wrongKey');
            break;
        }

        case OUTCOMES.KEY_INVALID: {
            print('unlock.failure.wrongTool');
            break;
        }

        case OUTCOMES.NOT_A_CONTAINER: {
            print('unlock.error.notAContainer');
            if (engine.isActor(oContainer.id)) {
                print('unlock.info.notAContainerButACreature', { creature: oContainer.name, key: oItem.name });
            }
        }
        }
    }
}

/**
 * Déverrouillage d'une porte en utilisant une clé ou un outil
 * @param context {*}
 * @param direction {string}
 * @param item {MUDEntity}
 */
function unlockDoorUsingKeyOrTool (context, direction, item) {
    // déterminer la nature de l'item
    // Si c'est une clé ou regarde le tag de la clé et on vérifie qu'il corresponde au tag de l'issue
    // Si c'est un outil il va falloir demander à un système de règles externe
    const { engine, print, pc } = context;

    const { success, outcome } = engine.actionUnlock(pc, direction, item);
    const OUTCOMES = engine.CONSTS.UNLOCK_OUTCOMES;
    const name = pc.name;
    if (success) {
        print('unlock.action.doorUnlocked', { dir: direction, item: item.name });
        print.room('unlock.room.doorUnlocked', { name, dir: direction, item: item.name });
    } else {
        switch (outcome) {
        case OUTCOMES.EXIT_INVALID: {
            print('unlock.error.doorNotVisible');
            break;
        }

        case OUTCOMES.NOT_LOCKED: {
            print('unlock.error.doorNotLocked');
            break;
        }

        case OUTCOMES.WRONG_KEY: {
            print('unlock.failure.wrongKey');
            break;
        }

        case OUTCOMES.KEY_INVALID: {
            print('unlock.failure.wrongTool');
            break;
        }
        }
    }
}

module.exports = function main (context, aArguments) {
    // unlock object
    // permet d'ouvrir un contenant et de le déclarer comme "contenant actuellement ouvert"
    // certaines commandes utiliseront le "contenant actuellement ouvert" comme par exemple "get" ou "drop"
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.DIRECTION_WITH_ITEM]: p => unlockDoorUsingKeyOrTool(context, p.direction, p.item),
        [context.PARSER_PATTERNS.OBJECT_WITH_ITEM]: p => unlockContainerUsingKeyOrTool(context, p.entity, p.item),
        [context.PARSER_PATTERNS.NONE]: () => context.print('generic.error.needArgument'),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};

