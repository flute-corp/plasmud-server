/**
 * DROP
 * @author ralphy
 * @date 2022-03-08
 * Cette commande sert à déposer un objet au sol (ITEM uniquement)
 * ou dans un container spécifié.
 * Syntaxe : DROP <item>
 * Syntaxe : DROP <item> into <container>
 */

/**
 * Abandonne un objet et le place dans un contenant
 * @param context
 * @param oItem
 * @param oContainer
 * @param nCount
 */
function dropItemIntoContainer (context, oItem, nCount, oContainer) {
    const { engine, print, pc } = context;
    // vérifier la serrure du contenant
    if (oContainer.locked) {
        print('drop.failure.containerLocked');
    } else {
        engine.moveEntity(oItem, oContainer, nCount);
        print('drop.action.itemDroppedInContainer', { item: oItem.name, count: nCount, container: oContainer.name });
        print.room('drop.room.itemDroppedInContainer', { name: pc.name, item: oItem.name, count: nCount, container: oContainer.name });
    }
}

/**
 * Abandonne un objet et le place sur le sol de la pièce
 * @param context
 * @param oItem
 * @param nCount
 */
function dropItemOnRoom (context, oItem, nCount) {
    const { pc, engine, print } = context;
    engine.moveEntity(oItem, pc.location, nCount);
    print('drop.action.itemDropped', { item: oItem.name, count: nCount });
    print.room('drop.room.itemDropped', { name: pc.name, item: oItem.name, count: nCount });
}

module.exports = function main (context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.ITEM_TO_CONTAINER]: p => dropItemIntoContainer(context, p.item, p.count, p.container),
        [context.PARSER_PATTERNS.ITEM]: p => dropItemOnRoom(context, p.item, p.count),
        [context.PARSER_PATTERNS.NONE]: () => context.print('generic.error.argumentNeeded'),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};
