/**
 * GET
 * @author ralphy
 * @date 2022-03-03
 * Cette commande sert à récupérer un objet au sol (ITEM uniquement)
 * Ou un item contenu dans un contenant type coffre ou sac...
 * Syntaxe : GET <item>
 * Syntaxe : GET <item> from <container>
 */

/**
 * Rammasser l'item spécifié depuis le sol de la pièce
 * @param context
 * @param oItem {MUDEntity}
 * @param [oContainer] {MUDEntity}
 * @param nCount
 */
function takeItemFromAnywhere (context, oItem, oContainer, nCount) {
    const { print, pc, engine } = context;
    try {
        if (nCount === Infinity) {
            nCount = oItem.stack;
        }
        if (engine.isPlaceable(oItem)) {
            print('get.failure.cannotGetThis', { item: oItem.name });
            return;
        } else if (oContainer) {
            print('get.action.itemTakenFromContainer', { item: oItem.name, count: nCount, container: oContainer.name });
            print.room('get.room.itemTakenFromContainer', { name: pc.name, item: oItem.name, count: nCount, container: oContainer.name });
        } else {
            print('get.action.itemTaken', { item: oItem.name, count: nCount });
            print.room('get.room.itemTaken', { name: pc.name, item: oItem.name, count: nCount });
        }
        engine.moveEntity(oItem, pc, nCount);
    } catch (e) {
        print('get.failure.cannotGetThis', { item: oItem.name });
    }
}

module.exports = function main (context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.ITEM_FROM_CONTAINER]: p => takeItemFromAnywhere(context, p.item, p.container, p.count),
        [context.PARSER_PATTERNS.ENTITY]: p => takeItemFromAnywhere(context, p.entity, null, p.count),
        [context.PARSER_PATTERNS.NONE]: () => context.print('generic.error.argumentNeeded'),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};

