module.exports = function main ({ system, engine, entity, interlocutor, speech, volume }) {
    if (engine.isPlayer(interlocutor)) {
        const sDisplayName = engine.getEntityVisibility(entity, interlocutor) === engine.CONSTS.VISIBILITY_VISIBLE
            ? entity.name
            : engine.text('say.invisibleEntityName');
        switch (volume) {
        case engine.CONSTS.VOLUME_NOTIFY: {
            system.sendTextToUser(interlocutor.uid, 'speakstrings.notify', { name: sDisplayName, speech });
            break;
        }
        case engine.CONSTS.VOLUME_ECHO: {
            system.sendTextToUser(interlocutor.uid, 'speakstrings.echo', { name: sDisplayName, speech });
            break;
        }
        case engine.CONSTS.VOLUME_SHOUT: {
            system.sendTextToUser(interlocutor.uid, 'speakstrings.shout', { name: sDisplayName, speech });
            break;
        }
        default: {
            system.sendTextToUser(interlocutor.uid, 'speakstrings.normal', { name: sDisplayName, speech });
            break;
        }
        }
    }
};
