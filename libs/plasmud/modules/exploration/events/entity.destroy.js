module.exports = function main ({ system, engine, entity }) {
    if (engine.isPlayer(entity)) {
        system
            .players[entity.uid]
            .context
            .print
            .room('generic.room.playerDespawn', { name: entity.name });
    }
};
