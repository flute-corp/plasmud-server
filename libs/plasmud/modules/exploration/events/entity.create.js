module.exports = function main ({ system, engine, entity, location }) {
    if (engine.isPlayer(entity)) {
        system
            .players[entity.uid]
            .context
            .print
            .room(location, 'generic.room.playerSpawn', { name: entity.name });
    }
};
