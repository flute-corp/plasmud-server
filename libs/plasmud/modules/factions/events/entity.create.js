/**
 * FACTIONS entity.create event
 *
 * Ce script instancie la faction de l'actor
 *
 * @author ralphy
 * last update : 2024-09-03
 */
module.exports = function main ({ entity, system: { services: { factionManager } } }) {
    factionManager.registerActor(entity);
};
