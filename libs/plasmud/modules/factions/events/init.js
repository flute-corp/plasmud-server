const debug = require('debug');

const log = debug('mod:factions');

const FactionManager = require('../scripts/services/faction-manager');

module.exports = async function main ({ system, engine }) {
    const fm = new FactionManager({ system, engine });
    fm.init();
    system.addContextService('factionManager', fm);
    log('Faction management module.');
};
