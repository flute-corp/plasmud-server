const Faction = require('../classes/faction.class');

const FACTIONS_DATA_NS = 'faction';

/**
 * Les personnages son sensés avoir dans leur data, une entrée "faction"
 * c'est un identifiant de faction existant : la faction doit exister sinon : throw error
 */
class FactionManager {
    /**
     * @param system {System}
     * @param engine {Engine}
     */
    constructor ({ system, engine }) {
        this._system = system;
        this._engine = engine;
        this._factions = {};
    }

    init () {
        const oFactionDefinition = this._system.getAsset('data', 'factions');
        if (!oFactionDefinition || Object.keys(oFactionDefinition).length === 0) {
            throw new Error('Invalid faction definition');
        }
        this.createFactions(oFactionDefinition);
    }

    /**
     * @typedef FactionDefinition {object}
     * @property id {string}
     * @property parent {string} parent faction id
     * @property dispositions {Object<string, number>}
     *
     *
     */
    createFactions(elements) {
        // Création d'une map des éléments par id pour un accès rapide
        const elementMap = new Map();
        elements.forEach(element => {
            elementMap.set(element.id, element);
        });

        // Une map pour stocker les instances créées
        const instanceMap = new Map();

        /**
         * Fonction récursive pour créer les instances
         * @param element {FactionDefinition}
         * @returns {Faction}
         */
        const createInstance = (element) => {
            if (instanceMap.has(element.id)) {
                // Si l'instance a déjà été créée, on la retourne
                return instanceMap.get(element.id);
            }

            let parentInstance = null;
            if (element.parent && elementMap.has(element.parent)) {
                // On crée d'abord l'instance du parent si elle existe
                parentInstance = createInstance(elementMap.get(element.parent));
            }

            // On crée l'instance de l'élément courant
            const instance = this.addFaction(element.id, parentInstance);

            // On stocke l'instance dans la map pour éviter les doublons
            instanceMap.set(element.id, instance);
            return instance;
        };

        // On crée toutes les instances
        elements.forEach(element => createInstance(element));
        elements.forEach(element => {
            this.setFactionDispositions(element.id, element.dispositions); // Initialisation de l'instance (par exemple)
        });
    }

    /**
     * @param id {string}
     * @param oParentFaction {Faction|undefined}
     * @returns {Faction}
     */
    addFaction (id, oParentFaction = undefined) {
        return this._factions[id] = new Faction(id, oParentFaction);
    }

    setFactionDispositions (id, oDispositions) {
        const oFaction = this.getFaction(id);
        for (const idOtherFaction in oDispositions) {
            const oOtherFaction = this.getFaction(idOtherFaction);
            oFaction.setFactionDisposition(oOtherFaction, oDispositions[idOtherFaction]);
        }
    }

    /**
     *
     * @param id
     * @returns {Faction}
     */
    getFaction (id) {
        if (!(id in this._factions)) {
            const sFactions = Object.keys(this._factions).join(', ');
            throw new Error('invalid faction : ' + id + ' - allowed faction id are : ' + sFactions);
        }
        return this._factions[id];
    }

    registerActor (oMudEntity) {
        if (this._engine.isPlayer(oMudEntity)) {
            oMudEntity.data[FACTIONS_DATA_NS] = 'FACTION_PLAYER';
        }
        if (this._engine.isActor(oMudEntity) && (FACTIONS_DATA_NS in oMudEntity.data)) {
            this.getFaction(oMudEntity.data[FACTIONS_DATA_NS]); // check if faction exists ; throws if not
        }
    }

    getDisposition (oMUDSubject, oMUDTarget) {
        try {
            const oSubjectFaction = this.getFaction(oMUDSubject.data[FACTIONS_DATA_NS]);
            const oTargetFaction = this.getFaction(oMUDTarget.data[FACTIONS_DATA_NS]);
            return oSubjectFaction.getFactionDisposition(oTargetFaction);
        } catch (e) {
            return 0;
        }
    }

    isFriendly (oMUDSubject, oMUDTarget) {
        return this.getDisposition(oMUDSubject, oMUDTarget) > 0;
    }

    isHostile (oMUDSubject, oMUDTarget) {
        return this.getDisposition(oMUDSubject, oMUDTarget) < 0;
    }

    isNeutral (oMUDSubject, oMUDTarget) {
        return this.getDisposition(oMUDSubject, oMUDTarget) === 0;
    }
}

module.exports = FactionManager;
