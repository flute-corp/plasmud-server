/**
 * Faction de base
 * PC: Faction des joueurs
 * HOSTILE: Faction des PNJ hostiles aux autres faction
 * COMMONER: Faction des PNJ non combattant (gens du peuple)
 * MERCHANT: Faction des commerçants
 * DEFENDER: Faction des PNJ amicaux envers tous les autres
 */
class Faction {
    constructor (id, oParentFaction = null) {
        this._id = id;
        this._factionDispositions = {};
        this._parentFaction = oParentFaction;
        this.setFactionDisposition(this, 1);
    }

    get id () {
        return this._id;
    }

    /**
     * Défini la réputation d'une autre faction par rapport à celle-ci
     * HOSTILE : Provoque l'agression de la faction hostile envers l'autre
     * NEUTRE : Ne provoque aucune réaction
     * AMICAL : Si un membre M d'une faction est attaquée, les membres de la faction amicale attaqueront les agresseurs de M.
     * @param oFaction {Faction}
     * @param nDisposition {number} -1 hostile ; +1 amical ; 0 neutre
     */
    setFactionDisposition (oFaction, nDisposition) {
        this._factionDispositions[oFaction.id] = nDisposition;
    }

    getFactionDisposition (oFaction) {
        if (oFaction.id in this._factionDispositions) {
            return this._factionDispositions[oFaction.id];
        }
        if (this._parentFaction) {
            return this._parentFaction.getFactionDisposition(oFaction);
        }
        return 0;
    }
}

module.exports = Faction;
