const { Context: DlgContext } = require('../../../../../dialogs');
const debug = require('debug');
const EventEmitter = require('events');

const log = debug('mod:dialogs');

const DIALOG_DATA_NS = 'dialog';

/**
 * Pour info : les tokens de neverwinter nights
 *
 * <Alignment> Why would someone of <alignment> persuasion do this? Why would someone of Chaotic Neutral persuasion do this?
 * <alignment> Curse you and your <alignment> nature! Curse you and your chaotic neutral nature!
 * <Boy/Girl> <Boy/Girl> you will listen to me and sit down! Girl you will listen to me and sit down!
 * <boy/girl> What do you say, <boy/girl>? What do you say, girl?
 * <Brother/Sister> Welcome to our temple, <Brother/Sister>! Welcome to our temple, Sister!
 * <brother/sister> Well that'd make you kinda like a little <brother/sister>, wouldn't it? Well that'd make you kinda like a little sister, wouldn't it?
 * <Class> We need training staff. <Class> trainer would be your job, how about it? We need training staff. Wizard trainer would be your job, how about it? Uses the first class lot, so "unreliable" after level 1. Uses classes.2da tlk reference.
 * <class> A <class> eh? Welcome aboard! A wizard eh? Welcome aboard! Uses the first class slot, so "unreliable" after level 1. Uses classes.2da tlk reference made lowercase
 * <Day/Night> <Day/Night>times like this makes this all worthwhile. Daytimes like this make this all worthwhile.
 * <day/night> What a blasted <day/night>! What blasted day!
 * <Deity> For <Deity>'s sake! For Erevan Illesre's sake! The text-field "Deity" - also retrievable by GetDeity but otherwise unused by the engine (can be set with SetDeity)
 * <FirstName> May change based on the language
 * <FullName>
 * <GameMonth>
 * <GameTime>
 * <GameYear>
 * <Good/Evil>
 * <good/evil>
 * <He/She>
 * <he/she>
 * <Him/Her>
 * <him/her>
 * <His/Her>
 * <his/her>
 * <His/Hers>
 * <his/hers>
 * <Lad/Lass>
 * <lad/lass>
 * <LastName>
 * <Lawful/Chaotic>
 * <lawful/chaotic>
 * <Law/Chaos>
 * <law/chaos>
 * <Level> You're at level <Level>. You're at level 14. Equivalent to GetHitDice
 * <Lord/Lady>
 * <lord/lady>
 * <Male/Female>
 * <male/female>
 * <Man/Woman>
 * <man/woman>
 * <Master/Mistress>
 * <master/mistress>
 * <Mister/Missus>
 * <mister/missus>
 * <PlayerName>
 * <QuarterDay>
 * <quarterday>
 * <Race> Name from racialtypes.2da
 * <race> Name from racialtypes.2da, but made lowercase
 * <Sir/Madam>
 * <sir/madam>
 * <Subrace> Subrace field set by player or by script (GetSubRace/SetSubRace)
 * <bitch/bastard>
 */

/**
 * @typedef DialogEventContext {object}
 * @property speakerContext {MUDContext}
 * @property speakerEntity {MUDEntity}
 * @property locutorEntity {MUDEntity}
 */

class Dialogs {
    constructor () {
        this._events = new EventEmitter();
        this._dialogs = {}; // Registre des structures des dialogues
        this._contexts = {}; // Associe chaque entité à son instance de conversations
        // une conversation implique deux interlocuteurs et un contexte de dialogue
        this._system = null;
    }

    set system (value) {
        this._system = value;
    }

    get system () {
        return this._system;
    }

    set dialogs (value) {
        this._dialogs = value;
    }

    get dialogs () {
        return this._dialogs;
    }

    get events () {
        return this._events;
    }

    /**
   * Initie une conversation en associant des entités interlocutrices à un contexte de dialogue
   * @param oActionContext {MUDContext} contexte de l'entité (joueur) qui entame la conversation
   * @param oLocutor {MUDEntity|string} identifiant de l'entité NPC à qui le joueur parle
   * @param oVariables {object}
   */
    createDialogContext (oActionContext, oLocutor, oVariables = {}) {
        const engine = oActionContext.engine;
        const oPlayerEntity = oActionContext.pc;
        const oPlayerContext = oActionContext;
        const bLocutorString = typeof oLocutor === 'string';
        const bLocutorEntity = bLocutorString ? false : (engine.isActor(oLocutor) || engine.isPlaceable(oLocutor));
        const idDialog = bLocutorString
            ? oLocutor
            : bLocutorEntity
                ? oLocutor.data[DIALOG_DATA_NS]?.ref
                : '';
        if (idDialog) {
            if (bLocutorString) {
                log('%s is initiating dialog %s in room %s', oPlayerEntity.name, idDialog, oPlayerEntity.location.id);
            } else if (bLocutorEntity) {
                log('%s is initiating dialog %s with %s in room %s', oPlayerEntity.name, idDialog, oLocutor.name, oLocutor.location.id);
            }
            const ctx = new DlgContext();
            ctx.player = oPlayerContext;
            ctx.actor = bLocutorEntity ? oLocutor : null;
            /**
       * @typedef DialogScreenEventStruct {object}
       * @property action {string} nom de l'action
       * @property variables {object} objet contenant les variables de contexte pour les besoins du dialogue
       * @property player {MUDEntity} entité du joueur qui parle au PNJ
       * @property actor {MUDEntity} entité du PNJ qui propose le dialogue
       */
            ctx.events.on('dialog.screen', oEvent => this.doDialogScreenEvent(ctx, oEvent));
            ctx.events.on('dialog.action', oEvent => this.doDialogActionEvent(ctx, oEvent));
            ctx.events.on('dialog.condition', oEvent => this.doDialogConditionEvent(ctx, oEvent));
            ctx.load(this._dialogs[idDialog], oVariables);
            this._contexts[oActionContext.pc.id] = ctx;
            ctx.displayCurrentScreen();
            return ctx;
        } else {
            return null;
        }
    }

    setDialogCustomToken (entity, sToken, sValue) {
        const idEntity = entity.id;
        const oDlgContext = this.getDialogContext(idEntity);
        if (oDlgContext) {
            log('defining token %s as "%s"', sToken, sValue);
            const oVariables = oDlgContext.variables;
            oVariables[sToken] = sValue;
        }
    }

    getDialogCustomToken (entity, sToken) {
        const idEntity = entity.id;
        const oDlgContext = this.getDialogContext(idEntity);
        if (oDlgContext) {
            const oVariables = oDlgContext.variables;
            return oVariables[sToken];
        } else {
            return undefined;
        }
    }

    /**
   * Supprime le contexte de dialogue lié à l'entité.
   * N'a pas d'effet si l'entité n'a pas de dialogue.
   * @param entity {MUDEntity} identifiant de l'entité joueur pour qui on efface le contexte
   */
    clearDialogContext (entity) {
        const idEntity = entity.id;
        if (idEntity in this._contexts) {
            log('clearing dialog for entity');
            delete this._contexts[idEntity];
        }
    }

    getDialogContext (entity) {
        return this._contexts[entity.id];
    }

    doDialogScreenEvent (oDlgContext, { text, options }) {
        const engine = this._system.engine;
        const oVariables = oDlgContext.variables;
        const oNPC = oDlgContext.actor;
        const spkctx = oDlgContext.player;
        const s = spkctx.text(text, oVariables);
        if (oNPC) {
            engine.speakString(oNPC, spkctx.pc, s, engine.CONSTS.VOLUME_NORMAL);
        } else {
            spkctx.print(s);
        }
        for (let i = 0, l = options.length; i < l; ++i) {
            const o = options[i];
            const sOptionText = spkctx.text(o.text);
            const sOutput = '[#03f][' + (i + 1).toString() + '][#] ' + sOptionText;
            spkctx.print(sOutput);
        }
    }

    doDialogActionEvent (oDlgContext, oEvent) {
        this._events.emit('dialog.action', {
            ...oEvent,
            system: this._system,
            engine: this._system.engine,
            player: oEvent.player.pc,
            context: oEvent.player
        });
    }

    doDialogConditionEvent (oDlgContext, oEvent) {
        this._events.emit('dialog.condition', {
            ...oEvent,
            system: this._system,
            engine: this._system.engine,
            player: oEvent.player.pc,
            context: oEvent.player
        });
    }
}

module.exports = Dialogs;
