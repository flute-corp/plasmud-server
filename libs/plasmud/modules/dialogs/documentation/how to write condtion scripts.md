# Ecrire des scripts de condition pour dialog

## Script

```js
function main (oEvent) {
    //
}

main($event)
```

## Structure $event

Cette structure fait office de contexte et contient les propriétés suivantes :
- __engine__ : une instance du moteur de MUD
- __player__ : instance EntityMud du joueur impliqué dans le dialogue
- __actor__ : instance EntityMud du personnage non-joueur impliqué dans le dialogue
- __variables__ : objet contenant les variables du dialogue
- __result__ : fonction qu'il faut appeler avec un boolean pour indiquer si la condition est réalisé ou non
