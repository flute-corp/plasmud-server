function initDialog (context, oEntity) {
    const { pc, engine, print } = context;
    print('greet.action.youGreetSomeone', { name: oEntity.name });
    print.room.except([pc, oEntity], 'greet.room.playerGreetsSomeone', { name: pc.name, target: oEntity.name });
    print.to(oEntity, 'greet.action.someoneGreetsYou', { name: oEntity});
    context.services.dialogs.createDialogContext(context, oEntity, {
        player: {
            name: pc.name
        },
        actor: {
            name: oEntity.name
        }
    });
}

module.exports = function main (context, aArguments) {
    context.parse(aArguments, {
        [context.PARSER_PATTERNS.ACTOR]: p => initDialog(context, p.actor),
        [context.PARSER_PATTERNS.DEFAULT]: () => context.print('generic.error.argumentInaccurate')
    });
};
