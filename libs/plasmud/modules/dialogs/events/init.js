const Dialogs = require('../scripts/services/dialogs');
const debug = require('debug');

function doDialogAction (oSystem, oEvent) {
    const { action } = oEvent;
    oSystem.runScript(action, oEvent);
}

function doDialogCondition (oSystem, oEvent) {
    const { condition } = oEvent;
    oSystem.runScript(condition, oEvent);
}

module.exports = function main (evt) {
    const { system } = evt;
    const dialog = new Dialogs();
    dialog.system = system;
    dialog.events.on('dialog.action', oEvent => doDialogAction(evt.system, oEvent));
    dialog.events.on('dialog.condition', oEvent => doDialogCondition(evt.system, oEvent));
    const log = debug('mod:dialogs');
    log('Dialog module');
    log('An extension to the base system, providing dialog support.');
    dialog.dialogs = system.getAsset('dialogs');
    system.addContextService('dialogs', dialog);
};
