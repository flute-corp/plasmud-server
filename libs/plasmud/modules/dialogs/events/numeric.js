/**
 *
 * @param engine {Engine}
 * @param command {number}
 * @param $context {MUDContext}
 */
module.exports = function main ({ system: { services: { dialogs } }, command, $context }) {
    /**
   * @type {}
   */
    const pc = $context.pc;
    const dlgctx = dialogs.getDialogContext(pc);
    if (dlgctx) {
        try {
            // verifier que l'actor et le player sont dans la même pièce et que les deux soient vivants
            if (dlgctx.actor) {
                if (dlgctx.actor.location.id !== pc.location.id) {
                    $context.print('dialogModule.failure.actorNotSameRoom');
                    dialogs.clearDialogContext(pc);
                    return;
                }
            }
            dlgctx.selectOption(command - 1);
        } catch (e) {
            // L'option n'existe pas
            console.error(e);
            $context.print('dialogModule.failure.invalidOption');
        }
    } else {
    // Tenter de trouver le contexte de dialogue d'une commande système
        $context.print('dialogModule.failure.noActiveDialog');
    }
};
