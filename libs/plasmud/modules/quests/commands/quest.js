const TableRenderer = require('../../../../table-renderer');
const dd = require('../../../../date-decoration');

/**
 * Prédicat de tri par date
 * @param qa {QuestReport}
 * @param qb {QuestReport}
 * @returns {number}
 */
function sortByDate (qa, qb) {
    return qb.date - qa.date;
}

/**
 *
 * @param context
 * @param oPlayer
 * @param options
 */
function displayQuestList (context, oPlayer, options = {}) {
    // const { maxLines = 10, page = 1, sortBy = 'date' } = options
    const questHelper = context.services.quests;
    const quests = questHelper.getQuestList(oPlayer);
    if (quests.length === 0) {
        context.print('quests.noquest');
        return;
    }
    const aTable = quests
        .sort(sortByDate)
        .map(({ id, title, chapter }) => ([id, title + ' / ' + chapter]));
    aTable.unshift(['ref', 'title'].map(s => context.text('quests.listHeaders.' + s)));
    const tr = new TableRenderer();
    tr.theme = 'FILET_THIN';
    tr.render(aTable).forEach(s => {
        context.print(s);
    });
}

function displayOneQuest (context, oPlayer, idQuest) {
    const questHelper = context.services.quests;
    const q = questHelper.getQuestDetail(oPlayer, idQuest);
    if (q) {
        context.print('quests/details', {
            title: q.title,
            chapter: q.chapter.title,
            text: Array.isArray(q.chapter.text) ? q.chapter.text.join('\n') : q.chapter.text,
            date: dd.render(new Date(q.date), 'ymd hm'),
            duration: dd.renderDuration(Date.now() - q.date, questHelper.DURATION_DECORATIONS),
            state: q.state
        });
    } else {
        context.print('quests.error.badQuestId');
    }
}

function goDebug (context, oPlayer, aArguments) {
    const questHelper = context.services.quests;
    switch (aArguments[0]) {
    case 'add': {
        // ajouter une quete au joueur
        questHelper.acceptQuest(oPlayer, aArguments[1]);
        context.print('added quest ' + aArguments[1]);
        break;
    }

    case 'set': {
        context.print('setting quest ' + aArguments[1] + ' state variable ' + aArguments[2] + ' to value ' + aArguments[3]);
        const qs = questHelper.getQuestState(oPlayer, aArguments[1]);
        qs[aArguments[2]] = aArguments[3];
        break;
    }

    case 'advance': {
        const [dummy, sQuestId, ref = undefined] = aArguments;
        context.print('advancing quest ' + sQuestId + ' to ref : "' + ref + '"');
        questHelper.advanceQuest(oPlayer, sQuestId, ref);
        break;
    }

    case 'state': {

    }
    }
}

module.exports = function main (context, aArguments) {
    const { pc: oPlayer } = context;
    if (aArguments.length === 0) {
    // pas d'argument : liste des quêtes
        displayQuestList(context, oPlayer);
    } else if (aArguments[0] === 'debug') {
        goDebug(context, oPlayer, aArguments.slice(1));
    } else {
        displayOneQuest(context, oPlayer, aArguments[0]);
    }
};
