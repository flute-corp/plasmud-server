const QuestHelper = require('../scripts/services/quests');

const debug = require('debug');
const log = debug('mod:quests');

module.exports = function main (evt) {
    log('Quest module');
    log('An extension to the base system, providing quest and mission management.');
    const questHelper = new QuestHelper();
    questHelper.loadAssets(evt.system.getAsset('quests'));
    questHelper.initDecorations((k, v) => evt.system.textRenderer.renderSync(k, v, true));
    evt.system.addContextService('quests', questHelper);
};
