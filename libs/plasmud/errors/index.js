class KeyNotFoundError extends Error {
    constructor (sKey, sCollection) {
        super('Key "' + sKey + '" could not be found in collection "' + sCollection + '"');
        this.name = 'KeyNotFoundError';
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, KeyNotFoundError);
        }
    }
}

class InvalidSchemasError extends Error {
    constructor (id, sType, aErrors = []) {
        super('The "' + id + '" structure is invalid according to the ' + sType + ' schemas.\n' + aErrors.join('\n'));
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, InvalidSchemasError);
        }
    }
}

class AbortError extends Error {
    constructor (sWhy) {
        super(sWhy);
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, AbortError);
        }
    }
}

class UnlockExitError extends Error {
    constructor (sWhy) {
        super('unlock error - ' + sWhy);
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, UnlockExitError);
        }
    }
}

module.exports = {
    KeyNotFoundError,
    InvalidSchemasError,
    AbortError
};
