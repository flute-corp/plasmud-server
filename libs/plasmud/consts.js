module.exports = {
    ENTITY_TYPES: {
        ITEM: 'item',
        ACTOR: 'actor',
        PLAYER: 'player',
        PLACEABLE: 'placeable',
        ENVIRONMENT: 'environment'
    },

    ENVIRONMENT_TYPES: {
        NEUTRAL: 'neutral',
        DARKNESS: 'darkness',
        UNDERWATER: 'underwater',
        HAZARDOUS: 'hazardous',
        DIFFICULT_TERRAIN: 'difficult-terrain',
        WINDSTORM: 'windstorm'
    },

    INVENTORY_DISTURB_TYPE_REMOVED: 'remove',
    INVENTORY_DISTURB_TYPE_ADDED: 'added',
    INVENTORY_DISTURB_TYPE_STOLEN: 'stolen',

    VOLUME_SILENT: 0,
    VOLUME_NORMAL: 1,
    VOLUME_ECHO: 2,
    VOLUME_SHOUT: 3,
    VOLUME_NOTIFY: 4,

    UNLOCK_OUTCOMES: {
        SUCCESS: 'SUCCESS',
        WRONG_KEY: 'WRONG_KEY',
        NOT_LOCKED: 'EXIT_NOT_LOCKED',
        EXIT_INVALID: 'EXIT_INVALID',
        KEY_INVALID: 'KEY_INVALID',
        NOT_A_CONTAINER: 'NOT_A_CONTAINER',
        COOLDOWN: 'COOLDOWN',
        ATTEMPT_FAILED: 'ATTEMPT_FAILED'
    },

    LOCK_OUTCOMES: {
        SUCCESS: 'SUCCESS',
        WRONG_KEY: 'WRONG_KEY',
        ALREADY_LOCKED: 'ALREADY_LOCKED',
        INVALID_TARGET: 'INVALID_TARGET',
        INVALID_KEY: 'KEY_INVALID'
    },

    SEARCH_OUTCOMES: {
        COOLDOWN: 'COOLDOWN',
        NO_NEED_DETECTION: 'NO_NEED_DETECTION',
        NOT_FOUND: 'NOT_FOUND'
    },

    DISTURBANCE_ITEM_REMOVED: 'DISTURBANCE_ITEM_REMOVED',
    DISTURBANCE_ITEM_ADDED: 'DISTURBANCE_ITEM_ADDED',
    DISTURBANCE_ITEM_STOLEN: 'DISTURBANCE_ITEM_STOLEN',

    VISIBILITY_VISIBLE: 'VISIBILITY_VISIBLE', // Can see target
    VISIBILITY_DARKNESS: 'VISIBILITY_DARKNESS', // Can't see target : the room is dark
    VISIBILITY_INVISIBLE: 'VISIBILITY_INVISIBLE', // Can't see target : target is invisible by magic or advanced tech
    VISIBILITY_UNDETECTED: 'VISIBILITY_UNDETECTED',  // Can't see target : target is hidden, or undetected
    VISIBILITY_BLIND: 'VISIBILITY_BLIND',  // Can't see target : condition blindness

    MUD_SKILL_UNLOCK: 'MUD_SKILL_UNLOCK',
    MUD_SKILL_SEARCH: 'MUD_SKILL_SEARCH',
    MUD_SKILL_STEALTH: 'MUD_SKILL_STEALTH'
};
