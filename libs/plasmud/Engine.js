const Events = require('events');
const validateSchema = require('../schema-validation');
const Cooldown = require('./helpers/Cooldown');
const { deepClone } = require('@laboralphy/object-fusion');
const { suggest } = require('@laboralphy/did-you-mean'); // 2.2.1
const SCHEMAS = {
    blueprints: require('./schemas/blueprints.json'),
    rooms: require('./schemas/rooms.json'),
    sectors: require('./schemas/sectors.json')
};
const { deepMerge } = require('@laboralphy/object-fusion');
const {
    KeyNotFoundError,
    InvalidSchemasError
} = require('./errors');

const MUDRoom = require('./MUDRoom');
const MUDEntity = require('./MUDEntity');
const MUDActor = require('./MUDActor');
const MUDItem = require('./MUDItem');
const MUDPlayer = require('./MUDPlayer');
const MUDPlaceable = require('./MUDPlaceable');
const MUDEnvironment = require('./MUDEnvironment');
const MUDExit = require('./MUDExit');

const DIRECTION_CODES = ['n', 'e', 'w', 's', 'ne', 'nw', 'se', 'sw', 'u', 'd'];
const CONSTS = require('./consts');
const ID_SEPARATOR = '#';

const ENTITY_COOLDOWN_ACTION = 'ENTITY_COOLDOWN_ACTION';
const ROOM_DATA_TIME_SINCE_EMPTY = 'ROOM_DATA_TIME_SINCE_EMPTY';

const TASK_COOLDOWN_VALUE = 6; // nombre de secondes de cooldown avant réutilisation d'une compétence
const ROOM_TIME_BEFORE_RESET = 10; // nombre de fois ou une pièce a été vérifiée vide avant reset
const ROOM_INTERVAL_CHECK_EMPTY = 60; // période (en secondes) de vérification de l'occupation la pièce pour recyclage

const { generate: smalluuid } = require('../small-uuid');
const { getType, checkType} = require('../get-type');
const debug = require('debug');

const logMud = debug('serv:plasmud');

/**
 * @typedef MUDLockDef {object} objet de définition des serrures pour les blueprints
 * @property locked {boolean} état initial du verrou
 * @property difficulty {number} difficulté à crocheter la serrure
 * @property discard {boolean} supprimer la clé après usage
 * @property key {string} tag de la clé qui déverrouille la serrure
 *
 * @typedef MUDItemAcquiredEvent {object} Un item a été acquis
 * @property engine {Engine}
 * @property entity {string} Identifiant de l'item ayant été acquis
 * @property acquiredBy {string} Identifiant de l'entité qui a acquis l'item
 * @property acquiredFrom {string} Identifiant de l'entité qui possédait l'item avant. Peut-être vide si l'item est
 * directement créé dans l'inventaire de l'entité qui a acquis l'item.
 *
 * @typedef MUDItemLoseEvent {objet} Une entité a perdu un item
 * @property engine {Engine}
 * @property entity {string} Identifiant de l'item ayant été perdu
 * @property lostBy {string} Identifiant de l'entité qui a perdu l'item
 * @property acquiredBy {string} Identifiant de l'entité qui a acquis l'item perdu
 *
 * @typedef MUDItemDropEvent {objet} Une entité lache un item sur le sol de la pièce
 * @property engine {Engine}
 * @property entity {string} Identifiant de l'item ayant été abandonné
 * @property droppedBy {string} Identifiant de l'entité qui a laché l'item
 * @property location {string} Identifiant de la pièce dans laquelle a été posé l'item
 *
 * @typedef MUDEntityEvents {object}
 * @property blocked {function({ engine: Engine, self: string, room: string, direction: string })}
 * @property conversation {function({ engine: Engine, self: string, pattern: string, speaker: string, volume: string })}
 * @property disturbed {function({ engine: Engine, self: string, disturber: string, item: string, type: string })}
 * @property heartbeat {function({ engine: Engine, self: string })}
 * @property perception {function({ engine: Engine, self: string, entity: string, type: string })}
 * @property spawn {function({ engine: Engine, self: string })}
 * @property lock {function({ engine: Engine, self: string, lockedBy: string })}
 * @property open {function({ engine: Engine, self: string, openedBy: string })}
 * @property openFailure {function({ engine: Engine, self: string, inspectedBy: string })}
 * @property unlock {function({ engine: Engine, self: string, actor: string })}
 * @property enter {function({ engine: Engine, self: string, enterer: string })}
 * @property exit {function({ engine: Engine, self: string, exiter: string })}
 *
 * @typedef MUDBlueprint {object}
 * @property tag {string} tag permettant d'identifier les entités construite avec ce blueprints comme ayant certaine props
 * @property name {string} nom affichable de l'entité, lorsque identifiée (pour des items)
 * @property desc {string[]} description affichable de l'entité, lorsque identifié (pour des items)
 * @property type {string} type d'entité (actor, player, item, placeable)
 * @property weight {number} poids de l'entité
 * @property weight {number} ITEM : poids de l'item
 * @property stackable {boolean} ITEM : l'item peut-il être empilable
 * @property inventory {boolean} ITEM : l'item a-t-il un inventaire
 * @property lock {MUDLockDef} état du vérrou ---
 * @property events {MUDEntityEvents}
 * @property data {object} objet libre
 */

/**
 * @class Engine
 * @desc Moteur de gestion du MUD
 */
class Engine {
    constructor () {
        this._events = new Events();
        this._state = null;
        this._validObjects = new Set(); // les objets qui ont passé le json-validator sont stocké ici pour ne pas avoir à les revalider
        this._cooldown = new Cooldown();
        this._cooldown.registryName = ENTITY_COOLDOWN_ACTION;
        this._exits = {};
        this._clock = 0;
        /**
     * Correspondance entre identifiant entité et identifiant joueur
     * @type {Object.<string, string>}
     * @private
     */
        this._players = {};
        this._initCustomEventEmitter();
    }

    /**
   * Correspondance entre identifiant entité et identifiant joueur
   * @returns {Object<string, string>}
   */
    get players () {
        return this._players;
    }

    //               _     _ _           _      ____  ___
    //   _ __  _   _| |__ | (_) ___     / \    |  _ \|_ _|
    //  | '_ \| | | | '_ \| | |/ __|   / _ \   | |_) || |
    //  | |_) | |_| | |_) | | | (__   / ___ \ _|  __/ | | _
    //  | .__/ \__,_|_.__/|_|_|\___| /_/   \_(_)_| (_)___(_)
    //  |_|

    checkEntity (oEntity) {
        if (!this.isEntity(oEntity)) {
            throw new Error('ERR_NOT_AN_ENTITY');
        }
    }

    checkActor (oEntity) {
        if (!this.isActor(oEntity)) {
            throw new Error('ERR_NOT_AN_ACTOR');
        }
    }

    checkOptionalEntity (oEntity) {
        if (oEntity) {
            this.checkEntity(oEntity);
        }
    }

    checkRoom (oRoom) {
        if (!this.isRoom(oRoom)) {
            throw new Error('ERR_NOT_A_ROOM: ' + oRoom?.id);
        }
    }

    checkOptionalRoom (oRoom) {
        if (oRoom) {
            this.checkRoom(oRoom);
        }
    }

    checkItem (oItem) {
        if (!this.isItem(oItem)) {
            throw new Error('ERR_NOT_AN_ITEM: ' + oItem?.id);
        }
    }

    checkOptionalItem (oItem) {
        if (oItem) {
            this.checkItem(oItem);
        }
    }

    get CONSTS () {
        return CONSTS;
    }

    _pickEntityAutoIncId () {
        let sEntityId = '';
        do {
            sEntityId = ID_SEPARATOR + smalluuid();
        } while (this.isEntityExist(sEntityId));
        return sEntityId;
    }


    /**
   * Création d'une entité.
   * @param sBlueprint {string} référence du blueprints (modèle) d'après lequel on construit l'entité
   * @param roomOrEntity {null|MUDEntity|MUDRoom} localisation initiale, identifiant de la pièce ou du contenant où est crée l'entité
   * si la location est vide, l'entité sera créée mais ne sera pas intégrée au jeu, il faut utiliser la fonction
   * registerEntity()
   * @param count {number} nombre d'exemplaires dans la pile, pour le cas des objets empilables (flèche, or, potions...}
   * @param id {string} identifiant forcé
   * @param data {{}} données peronnalisées initiales
   * @returns {MUDEntity} instance de l'entité créée
   */
    createEntity (sBlueprint, roomOrEntity, { id = '', count = 1, data = null } = {}) {
        const idEntity = id === ''
            ? this._pickEntityAutoIncId()
            : id;
        const oBlueprint = this.getBlueprint(sBlueprint);
        let oEntity = null;
        switch (oBlueprint.type) {
        case CONSTS.ENTITY_TYPES.ITEM: {
            oEntity = new MUDItem({ engine: this, ref: sBlueprint, id: idEntity, count, data });
            break;
        }
        case CONSTS.ENTITY_TYPES.ACTOR: {
            oEntity = new MUDActor({ engine: this, ref: sBlueprint, id: idEntity, count, data });
            break;
        }
        case CONSTS.ENTITY_TYPES.PLACEABLE: {
            oEntity = new MUDPlaceable({ engine: this, ref: sBlueprint, id: idEntity, count, data });
            break;
        }
        case CONSTS.ENTITY_TYPES.ENVIRONMENT: {
            oEntity = new MUDEnvironment({ engine: this, ref: sBlueprint, id: idEntity, count, data });
            break;
        }
        default: {
            throw new Error('Unknown entity type ' + oBlueprint.type);
        }
        }
        if (roomOrEntity) {
            this._registerEntity(oEntity);
            this._setEntityLocation(oEntity, roomOrEntity);
        }
        return oEntity;
    }

    /**
   * Destruction d'une entité et de tous les objets qu'elle possède.
   * @param oEntity {MUDEntity} identifiant de l'entité à détruire
   */
    destroyEntity (oEntity) {
        this.checkEntity(oEntity);
        const idEntity = oEntity.id;
        const oEntities = this.getEntities();
        // détruire les objets qu'il transportait
        if (oEntity.inventory) {
            Object.keys(oEntity.inventory).forEach(id => {
                this.destroyEntity(this.getEntity(id));
            });
        }
        this._events.emit('entity.destroy', {
            engine: this,
            entity: oEntity
        });
        if (oEntity.location) {
            this._switchRoomEntity(oEntity.location, {
                room: room => {
                    this._removeRoomEntity(room, oEntity);
                },
                entity: entity => {
                    this._removeInventoryEntity(entity, oEntity);
                }
            });
            oEntity._location = null;
        }
        if (idEntity in oEntities) {
            delete oEntities[idEntity];
        }
    }

    /**
   * Création d'un nouveau personnage joueur.
   * @param uid {string} identifiant externe de l'utilisateur (désigné par l'appli cliente) qui sera transformé en identifiant interne
   * @param cid {string} identifiant externe du personnage (désigné par l'appli cliente) qui sera transformé en identifiant interne
   * @param sName {string} nom du joueur
   * @param oRoom {MUDRoom} pièce ou sera localisée l'entité
   * @returns {MUDEntity|null} instance de l'entité générée
   */
    createPlayerEntity (uid, cid, sName, oRoom = undefined) {
    // Vérifier si l'entité existe déja
        const idPlayer = cid;
        if (this.isEntityExist(idPlayer)) {
            const oPrevEntity = this.getEntity(idPlayer);
            this._players[uid] = idPlayer;
            this._events.emit('player.character.create', {
                id: idPlayer,
                entity: oPrevEntity,
                uid
            });
            return oPrevEntity;
        }
        // verifier si le nom est valide
        if (!sName.match(/^\S{2,20}$/)) {
            return null;
        }
        if (oRoom === undefined) {
            if ('startLocation' in this._state.data) {
                oRoom = this.getRoom(this._state.data.startLocation);
            }
        }
        // checking location
        if (!oRoom || !this.isRoom(oRoom)) {
            throw new Error('Entity "' + sName + '" (' + uid + ') starting location is invalid. Valid room object required (' + (typeof oRoom) + ' given)');
        }
        this._players[uid] = idPlayer;
        const oPlayer = this.getEntities()[idPlayer] = new MUDPlayer({
            engine: this,
            id: idPlayer,
            uid,
            name: sName
        });
        this._events.emit('player.character.create', {
            id: idPlayer,
            entity: oPlayer,
            uid
        });
        this.moveEntity(oPlayer, oRoom);
        return oPlayer;
    }

    /**
   * Renvoie une collection objet de description de secteurs
   * @returns {object} totalité des secteurs existant dans le plasmud
   */
    getSectors () {
        return this.state.sectors;
    }

    /**
   * Dictionnaire des entités
   * Dans le MUD les entités sont les objets mobiles qui ont des caractéristiques différentes selon leur type
   * Il y a 4 types d'entités : objet d'inventaire, créature, joueur, objet fixe.
   * @returns {object} totalité des entités présentes dans le plasmud
   */
    getEntities () {
        return this.state.entities;
    }

    /**
   * structure de stockage des données des pièces
   * @returns {object} totalité des pièces créées dans le plasmud
   */
    getRooms () {
        return this.state.rooms;
    }

    /**
   * objet blueprints correspondant à l'identifiant demandé
   * @param idBlueprint {string} identifiant du blueprints
   * @returns {object} instance du blueprints recherché
   */
    getBlueprint (idBlueprint) {
        return this._getValidObject(idBlueprint, 'blueprints', true);
    }

    /**
   * objet sector coorespondant à l'identifiant demandé
   * @param idSector {string} identifiant du secteur recherché
   * @returns {object} instance du secteur recherché
   */
    getSector (idSector) {
        return this._getValidObject(idSector, 'sectors', true);
    }

    /**
   * entité dont l'identifiant est spécifié en paramètre
   * @param idEntity {string} identifiant de l'entité recherchée
   * @returns {MUDEntity} instance de l'entité recherchée
   */
    getEntity (idEntity) {
        const oEntities = this.getEntities();
        checkType(idEntity, 'string');
        if (idEntity in oEntities) {
            return oEntities[idEntity];
        } else {
            throw new KeyNotFoundError(idEntity, 'entities');
        }
    }

    /**
   * pièce dont l'identifiant est spécifié en paramètre
   * @param idRoom {string} identifiant de la pièce recherchée
   * @returns {MUDRoom} instance de la pièce recherchée
   */
    getRoom (idRoom) {
        if (idRoom in this._state.rooms) {
            return this._state.rooms[idRoom];
        } else {
            throw new KeyNotFoundError(idRoom, 'rooms');
        }
    }

    isRoom (oThing) {
        return !!oThing &&
        (oThing instanceof MUDRoom) &&
        (oThing.id in this.getRooms());
    }

    isEntity (oThing) {
        return oThing instanceof MUDEntity;
    }

    /**
   * vrai si l'identifiant spécifié correspond à une entité instanciée dans le jeu
   * @param idEntity {string} identifiant entité presumée
   * @returns {boolean} vrai si l'entité existe
   */
    isEntityExist (idEntity) {
        return idEntity in this.getEntities();
    }

    /**
   * vrai si l'identifiant spécifié identifie une entité de type Placeable qui possède un inventaire
   * @param oEntity {MUDEntity} identifiant
   * @return {boolean}
   */
    isContainer (oEntity) {
        return this.isEntity(oEntity) &&
        oEntity.blueprint.inventory && (
            this.isPlaceable(oEntity) ||
          this.isItem(oEntity)
        );
    }

    isPlaceable (oEntity) {
        return this.isEntity(oEntity) && oEntity.blueprint.type === CONSTS.ENTITY_TYPES.PLACEABLE;
    }

    isItem (oEntity) {
        return this.isEntity(oEntity) && oEntity.blueprint.type === CONSTS.ENTITY_TYPES.ITEM;
    }

    /**
   * Renvoie true s'il l'item est stackable
   * @param oEntity {MUDEntity}
   * @return {boolean}
   */
    isItemStackable (oEntity) {
        return this.isItem(oEntity) && Boolean(oEntity.blueprint.stackable);
    }

    /**
   * Renvoie true si l'entité contient le tag spécifié (les tags peuvent être séparés par des espaces)
   * @param oEntity {MUDEntity}
   * @param tag {string}
   * @returns {boolean}
   */
    isEntityTagged (oEntity, tag) {
        const t = oEntity.tag || '';
        return t === tag ||
        t.startsWith(tag + ' ') ||
        t.endsWith(' ' + tag) ||
        t.includes(' ' + tag + ' ');
    }

    /**
   * Vrai si l'entité spécifiée est un joueur
   * @param oEntity {MUDEntity} identifiant entité
   * @returns {boolean}
   */
    isPlayer (oEntity) {
        return this.isEntity(oEntity) && oEntity.blueprint.type === CONSTS.ENTITY_TYPES.PLAYER;
    }

    /**
   * Vrai si l'entité spécifiée est un actor
   * @param oEntity {MUDEntity} identifiant entité
   * @returns {boolean}
   */
    isActor (oEntity) {
        if (!this.isEntity(oEntity)) {
            return false;
        }
        const sType = oEntity.blueprint.type;
        return sType === CONSTS.ENTITY_TYPES.ACTOR || sType === CONSTS.ENTITY_TYPES.PLAYER;
    }

    /**
   * Renvoie vrai si le paramètre spécifié est un code valide pour une direction
   * @param sDirection {string} identifiant à vérifier
   * @return {boolean} vrai si l'identifiant spécifié est une direction valide
   */
    isDirectionValid (sDirection) {
        return DIRECTION_CODES.includes(sDirection);
    }

    /**
   * Renvoie la liste des acteurs joueurs dans la room
   * Ne renvoie rien si la pièce est taggée 'LIMBO'
   * @param room {MUDRoom}
   * @returns {MUDEntity[]}
   */
    getRoomPlayers (room) {
        this.checkRoom(room);
        return this
            .getLocalEntities(room) // getLocalEntities: ok
            .filter(entity => this.isPlayer(entity));
    }


    /**
   * À partir d'un inventaire, extrait les entité avec le localId
   * @param oInventory {*}
   * @returns {MUDEntity[]}
   * @private
   */
    _extractInventoryEntities (oInventory) {
        return Object
            .keys(oInventory)
            .map(idEntity => this.getEntity(idEntity));
    }

    /**
   * Renvoie la liste des objets d'environnement d'une pièce
   * @param oRoom {MUDRoom}
   * @returns {*[]}
   */
    getRoomEnvironment (oRoom) {
        this.checkRoom(oRoom);
        const ENVIRONMENT = CONSTS.ENTITY_TYPES.ENVIRONMENT;
        return this
            ._extractInventoryEntities(
                this._getRoomEntityStorage(oRoom)
            )
            .filter(entity => entity.blueprint.type === ENVIRONMENT);
    }

    /**
   * Renvoie la liste des entités présentes dans un conteneur ou une pièce.
   * @param roomOrEntity {MUDRoom|MUDEntity} identifiant du conteneur ou de la pièce
   * @returns {MUDEntity[]} liste des entités présentes dans le contenant spécifié
   */
    getLocalEntities (roomOrEntity) {
        const oInventory = this._switchRoomEntity(roomOrEntity, {
            room: room => this._getRoomEntityStorage(room),
            entity: entity => entity.inventory || {}
        });
        const ENVIRONMENT = CONSTS.ENTITY_TYPES.ENVIRONMENT;
        return this
            ._extractInventoryEntities(oInventory)
            .filter(entity => entity.blueprint.type !== ENVIRONMENT);
    }

    /**
   * Déplacement d'une entité vers une nouvelle destination, cela peut être un inventaire (pour les items uniquement)
   * ou une nouvelle pièce (pour les personnages).
   * @param oEntity {MUDEntity} identifiant de l'objet à ramasser
   * @param roomOrEntity {MUDEntity|MUDRoom} identifiant de l'entité qui effectue l'action
   * @param [count=Infinity] {number} nombre d'exemplaire à prendre
   * @return {object|null} instance de l'objet transféré à titre purement informatif (parfois c'est un clone)
   */
    moveEntity (oEntity, roomOrEntity, count = Infinity) {
        this.checkEntity(oEntity);
        // vérifier la nature de l'entité
        if (count < 1) {
            return null;
        }
        const locationSwitcher = {
            room: room => ({
                location: room,
                isRoom: true,
                isEntity: false
            }),
            entity: entity => ({
                location: entity,
                isRoom: false,
                isEntity: true
            })
        };

        // déterminer nouvelle location
        const {
            location: oNewLocation,
            isRoom: bNewLocationIsRoom,
            isEntity: bNewLocationIsEntity
        } = this._switchRoomEntity(roomOrEntity, locationSwitcher);

        const oNewInventory = bNewLocationIsRoom
            ? this._getRoomEntityStorage(oNewLocation)
            : bNewLocationIsEntity
                ? (oNewLocation.inventory || null)
                : null;

        // déplacement d'un placeable ou d'un acteur d'un pièce à une autre
        if (!this.isItem(oEntity)) {
            // l'entité n'est pas un objet stockable dans un inventaire
            // donc la destination doit etre une pièce : un simple setEntityLocation suffit
            if (bNewLocationIsRoom) {
                this._setEntityLocation(oEntity, oNewLocation);
                return oEntity;
            }
            // La destination n'était pas une pièce... on ne peut pas transférer
            throw new Error('ERR_INVALID_ENTITY_LOCATION: cannot move ' + oEntity.blueprint.type + ' in ' + oNewLocation.id);
        }

        // L'entité à déplacé est un item
        const oItem = oEntity;
        // l'entité bénéficiaire a bien un inventaire,
        // on ne dépasse pas les bornes, on corrige le paramètre
        count = Math.max(1, Math.min(oItem.stack, count));
        if (this.isItemStackable(oItem)) {
            // cas où l'élément convoité contient davantage d'exemplaires que ce qu'on veut prendre
            // dans ce cas, on va laisser l'élément dans son ancien inventaire, on réduit sa pile,
            // on le clone dans le nouvel inventaire
            const oReturn = this._cloneEntity(oItem, false);
            oReturn.stack = count;
            // l'item qu'on veut déposer est stackable
            // recherche une éventuelle pile d'objet du meme blueprints
            const oInvItem = this._findEntityStack(oItem, oNewInventory);
            oItem.stack -= count;
            if (oItem.stack <= 0) {
                this.destroyEntity(oItem);
            }
            if (oInvItem) {
                // une pile du même type d'objet existe déja dans l'inventaire
                // additionner les piles
                // on ne veut qu'une partie de la pile
                oInvItem.stack += count;
            } else {
                // aucune pile du même type déja dans l'inv.
                // il faut créer notre propre pile
                // créer un clone de la pile d'origin
                const oClone = this._cloneEntity(oItem, false);
                oClone.stack = count;
                this._registerEntity(oClone);
                if (!this._setEntityLocation(oClone, roomOrEntity)) {
                    return null;
                }
            }
            return oReturn;
        } else {
            if (this._setEntityLocation(oItem, roomOrEntity)) {
                return oItem;
            }
        }
        return null;
    }

    /**
   *
   * @param oRoom {MUDRoom}
   * @param sDirection {string}
   * @param oExplorer {MUDActor}
   * @returns {MUDExit}
   * @private
   */
    _getRoomExit (oRoom, sDirection, oExplorer = null) {
        this.checkOptionalEntity(oExplorer);
        this.checkRoom(oRoom);
        const rid = oRoom.id;
        const oExitRegistry = this._exits;
        if (!(rid in oExitRegistry)) {
            oExitRegistry[rid] = {};
        }
        const oERRoom = oExitRegistry[rid];
        if (sDirection in oExitRegistry[rid]) {
            return oERRoom[sDirection];
        }
        const oNav = oRoom.nav[sDirection];
        const oExit = new MUDExit({
            room: oRoom,
            direction: sDirection,
            destination: oNav ? this.getRoom(oNav.to) : null,
            nav: oNav
        });
        oERRoom[sDirection] = oExit;
        return oExit;
    }

    /**
   * Renvoie les renseignements concernant une porte.
   * @param roomOrEntity {MUDEntity|string} identifiant pièce ou d'une entité dans la pièce
   * @param sDirection {string} direction de la porte
   * @returns {MUDExit} état de la porte
   */
    getExit (roomOrEntity, sDirection) {
        return this._switchRoomEntity(roomOrEntity, {
            room: room => this._getRoomExit(room, sDirection),
            entity: entity => this._getRoomExit(entity.location, sDirection, entity)
        });
    }

    /**
   * Renvoie la liste des portes valides pour une pièce donnée
   * @param oRoom {MUDRoom} identifiant pièce
   * @return {string[]} liste des directions valides
   */
    getRoomValidExits (oRoom) {
        this.checkRoom(oRoom);
        return Object.keys(oRoom.nav);
    }

    /**
   * Ajoute l'id d'une porte cachée à la liste des portes cachées repérées par le joueur.
   * Cette opération rend cette porte effectivement visible par le joueur.
   * @param oEntity {MUDEntity} joueur
   * @param sDirection {string} une direction valide
   * @param value {boolean} true = repéré ; false = non-repéré
   */
    setEntityExitSpotted (oEntity, sDirection, value) {
        this.checkEntity(oEntity);
        if (!this.isDirectionValid(sDirection)) {
            throw new KeyNotFoundError(sDirection, 'directions');
        }
        this.getExit(oEntity, sDirection).setDetectedBy(oEntity, value);
    }

    /**
   * cherche et renvoie un tableau contenant tous les objets possédant un tag particulier et se trouvant
   * dans l'inventaire de l'entité spécifiée.
   * Cette fonction est récursive, elle va cherche dans la location spécifiée, ainsi que dans les contenant de cette
   * location.
   * @param sTag {string}
   * @param roomOrEntity {MUDEntity|string}
   * @param [bRecursive] {boolean} si vrai alors la fonction fouille également dans les contenants recursivement
   * defaut : faux
   * @returns {object[]} liste des entités correspondant au tag, dans la pièce spécifiée
   */
    findEntitiesByTag (sTag, roomOrEntity, bRecursive = false) {
        const aContainer = this.getLocalEntities(roomOrEntity); // getLocalEntities: ok
        const aItems = [];
        const aInventories = [];
        aContainer.forEach(entity => {
            if (this.isEntityTagged(entity, sTag)) {
                aItems.push(entity);
            }
            if (bRecursive && entity.inventory) {
                aInventories.push(entity);
            }
        });
        while (aInventories.length > 0) {
            aItems.push(...this.findEntitiesByTag(sTag, aInventories.shift(), bRecursive));
        }
        return aItems;
    }

    /**
   * Affiche le nom d'une entité au travers de la perception d'un acteur
   * @param oEntity {MUDEntity}
   * @param oPerceiver {MUDEntity}
   */
    getEntityVisibility (oEntity, oPerceiver) {
        if (oEntity === oPerceiver) {
            return CONSTS.VISIBILITY_VISIBLE;
        }
        if (this.isActor(oPerceiver) && this.isActor(oEntity)) {
            const oPayload = {
                watcher: oPerceiver,
                target: oEntity,
                visibility: CONSTS.VISIBILITY_VISIBLE
            };
            this._events.emit('resolve.entity.visibility', oPayload);
            const sRoomVisibility = this.getRoomVisibility(oPerceiver.location, oPerceiver);
            return sRoomVisibility === CONSTS.VISIBILITY_VISIBLE
                ? oPayload.visibility
                : sRoomVisibility;
        } else {
            return CONSTS.VISIBILITY_VISIBLE;
        }
    }

    /**
   * Renvoie un indice de luminosité d'une pièce
   * @param oRoom {MUDRoom}
   * @param oWatcher {MUDEntity}
   * @return {string}
   */
    getRoomVisibility (oRoom, oWatcher) {
        const oEnvSet = new Set(this
            .getRoomEnvironment(oRoom)
            .map(e => e.environment));
        /**
     * @type {{ perception: string, room: MUDRoom, watcher: MUDEntity }}
     */
        const oEvent = {
            room: oRoom,
            watcher: oWatcher,
            visibility: oEnvSet.has(CONSTS.ENVIRONMENT_TYPES.DARKNESS) ? CONSTS.VISIBILITY_DARKNESS : CONSTS.VISIBILITY_VISIBLE
        };
        this._events.emit('resolve.room.visibility', oEvent);
        return oEvent.visibility;
    }

    /**
   * Renvoie la liste des parents de l'entité, cela peut être utile pour obtenir le parent direct (shift)
   * ou bien connaitre la pièce dans laquelle se trouve l'objet, même si cet objet est dans un conteneur
   * @param oEntity {MUDEntity}
   * @param [aChain] {array} utilisée par la récursivité
   * @return {string[]} liste des identifiant des entités parentes
   * @throws Error renvoie une erreur avec le message recursive inventory si un objet A est contenu dans une entité contenu dans A
   */
    getParentEntities (oEntity, aChain = []) {
        this.checkEntity(oEntity);
        const oLocation = oEntity.location;
        if (!oLocation) {
            return aChain;
        }
        if (aChain.includes(oLocation)) {
            throw new Error('Recursive inventory : ' + oLocation.id);
        }
        aChain.push(oLocation);
        return this._switchRoomEntity(oLocation, {
            room: () => aChain,
            entity: entity => this.getParentEntities(entity, aChain)
        });
    }

    /**
   * Renvoie l'instance de l'entité (actor ou placeable) qui possède l'objet
   * @param oEntity {MUDEntity}
   * @return {MUDEntity|null}
   */
    getItemOwner (oEntity) {
        if (!this.isItem(oEntity)) {
            return null;
        }
        const aParents = this.getParentEntities(oEntity);
        switch (aParents.length) {
        case 0: {
            return null;
        }

        case 1: {
            const oParent = aParents[0];
            if (this.isEntityExist(oParent.id) && (this.isActor(oParent) || this.isPlaceable(oParent))) {
                return oParent;
            } else {
                return null;
            }
        }

        default: {
            for (let i = aParents.length - 1; i >= 0; --i) {
                const oCrt = aParents[i];
                if (this.isActor(oCrt) || this.isPlaceable(oCrt)) {
                    return oCrt;
                }
            }
            return null;
        }
        }
    }

    getCooldown (oEntity, sKey, nValue) {
        const data = oEntity;
        if (!this._cooldown.isCooldown(data, sKey, nValue)) {
            const cd = this._cooldown.getCooldownRegistry(data, sKey);
            const time = Math.ceil((cd.ts + (nValue * 1000) - this._cooldown.now) / 1000);
            return {
                ready: false,
                time
            };
        } else {
            return {
                ready: true,
                time: 0
            };
        }
    }

    /**
   * Détermine si une tache est réussie
   * @param oEntity {MUDEntity} identifiant du joueur effectuant la tâche
   * @param sSkill {string} nom du skill testé
   * @param nDifficulty {number} difficulté à battre
   * @param oTool {MUDEntity|null} parfois un outil est utilisé dans la rzsolution de la tâche
   * @returns {{info: {cooldown: number}, success: boolean, reason: string}} vrai : la tâche est surmontée
   */
    resolveTaskOutcome (oEntity, sSkill, nDifficulty, oTool = null) {
        const CD_KEY_NAME = 'task';
        this.checkEntity(oEntity);
        const cd = this.getCooldown(oEntity, CD_KEY_NAME, TASK_COOLDOWN_VALUE);
        if (!cd.ready) {
            return {
                success: false,
                reason: 'cooldown',
                info: {
                    cooldown: cd.time
                }
            };
        }
        this.checkOptionalItem(oTool);
        let bSuccess = false;
        const oEvt = {
            engine: this,
            entity: oEntity,
            skill: sSkill,
            difficulty: nDifficulty,
            success: (b = true) => {
                bSuccess = b;
            },
            value: 0,
            tool: oTool
        };
        this._events.emit('entity.task.attempt', oEvt);
        return {
            success: bSuccess,
            reason: bSuccess ? '' : 'fail',
            info: {
                cooldown: 0
            }
        };
    }

    /**
   * Fait parler l'entité spécifiée. Le message est envoyer à l'interlocuteur désigné ce qui peut être
   * Une pièce, une autre entité. L'entité parleuse, recoit sont propre message également
   * @param oEntity {MUDEntity} entité qui parle
   * @param roomOrInterlocutor {MUDEntity|MUDRoom} entité ou pièce qui recoit le message
   * @param speech {string} contenu du message
   * @param nVolume {number} 0 = silent, 1 = normal, 2 = shout : intervient lorsque l'on parle à la pièce entière
   */
    speakString (oEntity, roomOrInterlocutor, speech, nVolume = CONSTS.VOLUME_NORMAL) {
        this.checkEntity(oEntity);
        this._switchRoomEntity(roomOrInterlocutor, {
            room: room => {
                this
                    .getLocalEntities(room) // getLocalEntities: ok
                    .filter(entity =>
                        entity.blueprint.type === CONSTS.ENTITY_TYPES.ACTOR ||
              entity.blueprint.type === CONSTS.ENTITY_TYPES.PLACEABLE ||
              entity.blueprint.type === CONSTS.ENTITY_TYPES.PLAYER
                    )
                    .forEach(entity => {
                        this.speakString(oEntity, entity, speech, nVolume);
                    });
                if (nVolume === CONSTS.VOLUME_SHOUT) {
                    this.getRoomValidExits(room).forEach(dir => {
                        const oExit = this.getExit(room, dir); // ok getExit
                        this.speakString(oEntity, oExit.destination, speech, CONSTS.VOLUME_ECHO);
                    });
                }
            },
            entity: entity => {
                this._events.emit('entity.speak', {
                    engine: this,
                    entity: oEntity,
                    interlocutor: entity,
                    speech: speech,
                    volume: nVolume
                });
            }
        });
    }

    text (textref, context = {}) {
        const oEvent = { textref, context, text: textref };
        this._events.emit('resolve.textref', oEvent);
        return oEvent.text;
    }



    //         _   _ _ _ _   _
    //   _   _| |_(_) (_) |_(_) ___  ___
    //  | | | | __| | | | __| |/ _ \/ __|
    //  | |_| | |_| | | | |_| |  __/\__ \
    //   \__,_|\__|_|_|_|\__|_|\___||___/

    _flatDeep(aArray, d = 1) {
        if (!Array.isArray(aArray)) {
            return aArray;
        }
        return d > 0
            ? aArray.reduce((acc, val) => acc.concat(this._flatDeep(val, d - 1)), [])
            : aArray.slice();
    }

    buildItemState (oItem) {
        const oLocation = oItem.location;
        const state = {
            _state: true,
            type: 'item',
            id: oItem.id,
            ref: oItem.ref,
            name: oItem._name,
            location: oLocation.id,
            stack: oItem.stack,
            remark: oItem.remark,
            locked: oItem.locked,
            data: oItem.data,
            inventory: this.isContainer(oItem)
                ? this.getLocalEntities(oItem).map(entity => entity.id) //getLocalEntities: ok
                : null
        };
        const save = payload => {
            deepMerge(state.data, payload);
        };
        this.events.emit('entity.save', { entity: oItem, save });
        return state;
    }

    /**
   * Construction de state d'un inventaire complet
   * @param oEntity {MUDEntity}
   * @returns {string[]}
   */
    buildInventoryState (oEntity) {
        return (this.isActor(oEntity) || this.isContainer(oEntity))
            ? this.getLocalEntities(oEntity).map(({ id }) => id) //getLocalEntities: ok
            : null;
    }

    buildItemFlatListState (oEntity) {
        return this.getLocalEntities(oEntity)  //getLocalEntities: ok
            .reduce((prev, entity) => {
                if (this.isContainer(entity)) {
                    prev.push(...this.buildItemFlatListState(entity));
                }
                prev.push(this.buildItemState(entity));
                return prev;
            }, []);
    }

    /**
   * Sauvegarde du personnage joueur spécifié
   */
    buildPlayerState (oEntity) {
        const state = {
            _state: true,
            type: 'player',
            id: oEntity.id, // C'est aussi l'identifiant client, normalement il commence par #
            uid: oEntity.uid, // identifiant utilisateur, il ne commence pas par # car ce n'est pas une entité du jeu
            name: oEntity._name,
            location: oEntity.location.id,
            remark: oEntity.remark,
            creation: oEntity.creation,
            data: {},
            // Tous les items sont mis à plat pour pouvoir tous les sauver facilement
            // Mais cela ne tient plus compte de l'arborescence des items entre eux
            inventory: this.buildInventoryState(oEntity),
            items: this.buildItemFlatListState(oEntity)
        };
        const save = payload => {
            deepMerge(state.data, payload);
        };
        this.events.emit('entity.save', { entity: oEntity, save });
        return state;
    }

    restoreItemState (state) {
        try {
            if (state._state && state.type === 'item') {
                const oItem = this.createEntity(
                    state.ref,
                    this.getEntity(state.location),
                    { id: state.id, count: state.stack }
                );
                oItem.name = state.name;
                oItem.remark = state.remark;
                oItem.locked = state.locked;
                const restore = payload => {
                    deepMerge(oItem.data, payload);
                };
                this.events.emit('entity.restore', { entity: oItem, restore });
                return true;
            }
            return false;
        } catch (e) {
            console.error(e);
            console.warn(state.ref, 'is not defined in blueprints.');
            return false;
        }
    }

    /**
   * Restore un ensemble d'items qui peuvent avoir des liens hierarchiques de contenant-contenu
   * @param oPlayerEntity {MUDEntity}
   * @param items {{}[]}
   */
    restoreItemStateRepository (oPlayerEntity, items) {
        let iItem = 0;
        // maximum de repoussement possible
        // cette valeur est égale à la somme de la suite arithmétique 1..items.length
        // car dans une liste totalement dans le désordre,
        // à chaque itération de rang I, il faudra repousser length-I élément avant de trouver le bon contenant
        let wd = items.length * (items.length + 1) / 2;
        while (iItem < items.length) {
            if (--wd < 0) {
                // constituer le dossier
                const d = items.map(it => ({
                    id: it.id,
                    need: it.location
                }));
                console.error(items);
                console.error(d);
                throw new Error('watchdog error when looping thru item state repository');
            }
            const oItem = items[iItem];
            // déterminer si la location existe
            if (this.isEntityExist(oItem.location)) {
                this.restoreItemState(oItem);
                ++iItem;
            } else {
                // repousser la création de l'objet
                // cet objet n'a jamais été repoussé en fin de liste, peut etre que son contenant et lui sont mal classés
                items.splice(iItem, 1);
                items.push(oItem);
            }
        }
    }

    restorePlayerState (state) {
        if (state._state && state.type === 'player') {
            const oPlayerEntity = this.createPlayerEntity(
                state.uid,
                state.id,
                state.name,
                this.getRoom(state.location)
            );
            oPlayerEntity.remark = state.remark;
            const restore = payload => {
                deepMerge(oPlayerEntity.data, payload);
            };
            this.restoreItemStateRepository(oPlayerEntity, state.items);
            restore(state.data);
            this.events.emit('entity.restore', { entity: oPlayerEntity, restore });
            return oPlayerEntity;
        } else {
            return null;
        }
    }

    /**
   * utilitaire de freezage d'objet. Permet de rendre un objet totalement immutable
   * @param o {object} objet à freezer
   * @returns {object} objet freezé
   * @private
   */
    static _deepFreeze (o) {
        if (Object.isFrozen(o)) {
            return o;
        }
        Object.freeze(o);
        if (o === undefined) {
            return o;
        }

        Object.getOwnPropertyNames(o).forEach(function (prop) {
            switch (getType(o[prop])) {
            case 'object':
            case 'function': {
                Engine._deepFreeze(o[prop]);
                break;
            }
            }
        });

        return o;
    }

    /**
   * Par défaut les entités qui sont définies dans les pièces ne sont pas créées
   * tant qu'un joueur n'y mets pas les pieds.
   * Cette fonction permet des les créer toutes
   * @private
   */
    _createAllEntities () {
        Object.values(this.getRooms()).forEach(room => {
            this.getLocalEntities(room); //getLocalEntities: ok
        });
    }

    /**
   * fabrique un identifiant par concaténation de composant
   * @param args {string} liste des composant à concaténé
   * @return {string} valeur de l'identifiant
   */
    static composeId (...args) {
        if (args[0] === 'room') {
            throw new Error('ERR_FORBIDDEN');
        }
        return args.join(ID_SEPARATOR);
    }

    static decomposeId (sId) {
        return sId.split(ID_SEPARATOR);
    }

    //  _                      _                                                                                 _
    // (_)_ ____   _____ _ __ | |_ ___  _ __ _   _   _ __ ___   __ _ _ __   __ _  __ _  ___ _ __ ___   ___ _ __ | |_
    // | | '_ \ \ / / _ \ '_ \| __/ _ \| '__| | | | | '_ ` _ \ / _` | '_ \ / _` |/ _` |/ _ \ '_ ` _ \ / _ \ '_ \| __|
    // | | | | \ V /  __/ | | | || (_) | |  | |_| | | | | | | | (_| | | | | (_| | (_| |  __/ | | | | |  __/ | | | |_
    // |_|_| |_|\_/ \___|_| |_|\__\___/|_|   \__, | |_| |_| |_|\__,_|_| |_|\__,_|\__, |\___|_| |_| |_|\___|_| |_|\__|
    //                                       |___/                               |___/

    /**
   * Clonage d'une entité
   * @param oEntity {MUDEntity} instance de l'entité source
   * @param [bRegister] {boolean} si true alors enregsttre l'objet et lui attribue un id
   * @returns {MUDEntity} objet cloné
   * @private
   */
    _cloneEntity (oEntity, bRegister = true) {
        this.checkEntity(oEntity);
        const oClone = this.createEntity(
            oEntity.ref,
            bRegister ? oEntity : null,
            {
                count: this.isItemStackable(oEntity) ? oEntity.stack : 1,
                data: deepClone(oEntity.data)
            }
        );
        oClone.tag = oEntity.tag;
        if ('locked' in oEntity) {
            oClone.locked = oEntity.locked;
        }
        oClone._data = deepClone(oEntity.data);
        return oClone;
    }

    /**
   * Enregistre une entité dans le MUD. Cela convien particulièrement aux entité clonées qui n'ont pas encore
   * été intégrée au MUD parce qu'il fallait appliquer des modification au clone auparavent.
   * @param oEntity {*}
   * @private
   */
    _registerEntity (oEntity) {
        this.checkEntity(oEntity);
        const idEntity = oEntity.id;
        this.getEntities()[idEntity] = oEntity;
        this._events.emit('entity.register', {
            engine: this,
            entity: oEntity
        });
    }

    //             _   _
    //   __ _  ___| |_| |_ ___ _ __ ___
    //  / _` |/ _ \ __| __/ _ \ '__/ __|
    // | (_| |  __/ |_| ||  __/ |  \__ \
    //  \__, |\___|\__|\__\___|_|  |___/
    //  |___/

    /**
   * état du monde
   * @returns {object} état du monde
   */
    get state () {
        return this._state;
    }

    /**
   * renvoie le gestionnaire d'évènements
   * @returns {module:events.EventEmitter} instance du gestionnaire d'évènement
   */
    get events () {
        return this._events;
    }

    // ▗▄▖  ▗▘   ▄▖ ▗▖      ▗▖  ▗▖  ▗▖               ▗▖                             ▗▖
    // ▐▌▜▖▗▛▜▖ ▟▙▖ ▄▖ ▐▛▜▖ ▄▖ ▝▜▛▘ ▄▖ ▗▛▜▖▐▛▜▖     ▄▟▌▗▛▜▖▗▛▀▘     ▀▜▖▗▛▀▘▗▛▀▘▗▛▜▖▝▜▛▘▗▛▀▘
    // ▐▌▟▘▐▛▀▘ ▐▌  ▐▌ ▐▌▐▌ ▐▌  ▐▌  ▐▌ ▐▌▐▌▐▌▐▌    ▐▌▐▌▐▛▀▘ ▀▜▖    ▗▛▜▌ ▀▜▖ ▀▜▖▐▛▀▘ ▐▌  ▀▜▖
    // ▝▀▘  ▀▀  ▝▘  ▀▀ ▝▘▝▘ ▀▀   ▀▘ ▀▀  ▀▀ ▝▘▝▘     ▀▀▘ ▀▀ ▝▀▀      ▀▀▘▝▀▀ ▝▀▀  ▀▀   ▀▘▝▀▀

    /**
   * vrai si l'objet est valide selon les schemas de validation json
   * @param oObject
   * @param sType {string}
   * @param id {string}
   * @private
   */
    static _checkBlueprintValid (oObject, sType, id) {
        if (!(sType in SCHEMAS)) {
            throw new KeyNotFoundError(sType, 'schemas');
        }
        const aMessages = [];
        const options = {
            throwError: false,
            output: s => {
                aMessages.push(s);
            }
        };
        if (!validateSchema(oObject, SCHEMAS[sType], options)) {
            throw new InvalidSchemasError(id, sType, aMessages);
        }
    }

    /**
   * Renvoie l'objet (room, blueprints, sector...) spécifié après vérification
   * il ne s'agit pas d'objets manipulables.
   * pour ces "objets" il s'agit principalement d'un objet abstrait comme une pièce ou un blueprints
   * @param id {string} identifiant
   * @param sTypes {string} fammile de types auquel appartient l'objet
   * @param freeze {boolean|function} après vérif, l'objet est gelé
   * @returns {object}
   * @private
   */
    _getValidObject (id, sTypes, freeze = false) {
        const oState = this.state;
        if (!(sTypes in oState)) {
            throw new KeyNotFoundError(sTypes, 'state');
        }
        const oTypes = oState[sTypes];
        if (!(id in oTypes)) {
            throw new KeyNotFoundError(id, sTypes);
        }
        const oObject = oTypes[id];
        if (!this._validObjects.has(id)) {
            if (!(sTypes in SCHEMAS)) {
                throw new KeyNotFoundError(sTypes, 'schemas');
            }
            Engine._checkBlueprintValid(oObject, sTypes, id);
            // blueprints valide
            this._validObjects.add(id);
            let bFreeze;
            if (getType(freeze) === 'function') {
                bFreeze = freeze(id, oObject);
            } else {
                bFreeze = !!freeze;
            }
            if (bFreeze) {
                // Lorsqu'une asset est utilisée pour la première fois, il est transformé en objet immutable
                // Avant cela un évent est déclenché pour permettre à l'application d'apporter des corrections.
                // à l'asset.
                const sType = sTypes.substring(0, sTypes.length - 1);
                // Cet event à toute lattitude pour modifier l'objet avant sa mise en immutabilité.
                this._events.emit('asset.' + sType + '.hook', {
                    engine: this,
                    resref: id,
                    data: oObject
                });
                Engine._deepFreeze(oObject);
            }
        }
        return oObject;
    }

    /**
   * Chargement des données initiales d'une aventure
   * @param value
   */
    setAssets (value) {
        const rooms = {};
        // vérification des pièces et des blueprints
        if ('rooms' in value) {
            Object.entries(value.rooms).forEach(([idRoom, room]) => {
                Engine._checkBlueprintValid(room, 'rooms', idRoom);
                rooms[idRoom] = new MUDRoom({
                    id: idRoom,
                    ...room
                });
            });
        }
        if ('blueprints' in value) {
            Object.entries(value.blueprints).forEach(([idBlueprint, blueprint]) => {
                Engine._checkBlueprintValid(blueprint, 'blueprints', idBlueprint);
            });
        }
        this._state = {
            ...value,
            rooms,
            entities: {},
            roomEntityStorage: {}
        };
    }

    // ▗▄▄▖     ▗▖  ▗▖  ▗▖          ▄▖              ▗▖  ▗▖                       ▗▖     ▗▖                   ▄▖
    // ▐▙▄ ▐▛▜▖▝▜▛▘ ▄▖ ▝▜▛▘▐▌▐▌     ▐▌ ▗▛▜▖▗▛▀  ▀▜▖▝▜▛▘ ▄▖ ▗▛▜▖▐▛▜▖     ▀▜▖▐▛▜▖ ▄▟▌    ▝▜▛▘▐▛▜▖ ▀▜▖▐▛▜▖▗▛▀▘ ▟▙▖▗▛▜▖▐▛▜▖
    // ▐▌  ▐▌▐▌ ▐▌  ▐▌  ▐▌ ▝▙▟▌     ▐▌ ▐▌▐▌▐▌  ▗▛▜▌ ▐▌  ▐▌ ▐▌▐▌▐▌▐▌    ▗▛▜▌▐▌▐▌▐▌▐▌     ▐▌ ▐▌  ▗▛▜▌▐▌▐▌ ▀▜▖ ▐▌ ▐▛▀▘▐▌
    // ▝▀▀▘▝▘▝▘  ▀▘ ▀▀   ▀▘▗▄▛      ▀▀  ▀▀  ▀▀  ▀▀▘  ▀▘ ▀▀  ▀▀ ▝▘▝▘     ▀▀▘▝▘▝▘ ▀▀▘      ▀▘▝▘   ▀▀▘▝▘▝▘▝▀▀  ▝▘  ▀▀ ▝▘

    // La pluspart des methodes ici font se charger de transferer des items d'un contenu à un autre
    // - Examiner le contenu d'un inventaire
    // - Supprimer, ajouter des entités dans les pièces ou les contenant
    // - Déplacer des entités d'une pièce à une autre, ou d'un contenant à un autre

    /**
   * retourne la zone de stockage des entités appartenant à une pièce
   * @param oRoom {MUDRoom} pièce
   * @returns {*}
   * @private
   *
   * Fonctions dépendantes de celle-ci :
   * - getLocalEntities
   * - moveEntity
   * - _removeRoomEntity
   * - _addRoomEntity
   */
    _getRoomEntityStorage (oRoom) {
        this.checkRoom(oRoom);
        const idRoom = oRoom.id;
        const oStorage = this.state.roomEntityStorage;
        if (!(oStorage[idRoom])) {
            // construction du registre d'identifiants locaux
            // construction du stockage des entités
            oStorage[idRoom] = {};
            // on a profité pour coller tous les objets par default
            const aContent = oRoom.content;
            if (aContent) {
                aContent.forEach(oContentDef => {
                    const {
                        ref,
                        tag = null,
                        remark = null,
                        locked = null,
                        stack = 1,
                        events = null,
                        content = []
                    } = oContentDef;
                    const oEntity = this.createEntity(ref, oRoom, { count: stack });
                    if (tag) {
                        oEntity.tag = tag;
                    }
                    if (remark) {
                        oEntity.remark = remark;
                    }
                    if (locked !== null) {
                        oEntity.locked = locked;
                        oEntity.locked = locked;
                    }
                    if (events) {
                        oEntity.events = events;
                    }
                    if (oEntity.inventory) {
                        this._setEntityContent(oEntity, content);
                    }
                });
            }
        }
        return oStorage[idRoom];
    }

    /**
   * Supprime le storage d'une room ; cela permet de  :
   * - Libérer des ressources
   * - Réinitialiser le contenu d'une pièce
   * @param oRoom {MUDRoom}
   * @private
   */
    resetRoom (oRoom) {
        this.checkRoom(oRoom);
        const idRoom = oRoom.id;
        const oStorage = this.state.roomEntityStorage;
        if (oStorage[idRoom]) {
            // destruction de toutes les entités
            const aEntities = this.getLocalEntities(oRoom);
            if (aEntities.some(entity => this.isPlayer(entity))) {
                // pas de reset s'il y a un joueur dans la pièce
                return false;
            }
            aEntities.forEach(entity => {
                this.destroyEntity(entity);
            });
            delete oStorage[idRoom];
        }
        delete this._exits[oRoom.id];
        oRoom.reset();
        return true;
    }

    /**
   * Recherche, dans l'inventaire donné, une pile du même accabi que l'objet transmis en paramètre,
   * c'est à dire, même tag et même ref
   * @param oItem {MUDEntity}
   * @param oInventory {object|null}
   * @private
   */
    _findEntityStack (oItem, oInventory) {
        this.checkEntity(oItem);
        for (const id of Object.keys(oInventory)) {
            const oOtherItem = this.getEntity(id);
            if (oOtherItem.ref === oItem.ref &&
          oOtherItem.tag === oItem.tag
            ) {
                return oOtherItem;
            }
        }
        return null;
    }

    /**
   * Supprime une entité d'un container
   * @param oContainerOwner {MUDEntity}
   * @param oItem {MUDEntity}
   * @private
   */
    _removeInventoryEntity (oContainerOwner, oItem) {
        this.checkEntity(oContainerOwner);
        this.checkEntity(oItem);
        const idItem = oItem.id;
        const oContainer = oContainerOwner.inventory;
        if (oContainer) {
            delete oContainer[idItem];
        }
    }

    /**
   * Ajoute une entité à l'inventaire, et lui associe un identifiant local
   * @param oContainerOwner {MUDEntity}
   * @param oItem {MUDEntity}
   * @private
   */
    _addInventoryEntity (oContainerOwner, oItem) {
        this.checkEntity(oContainerOwner);
        this.checkEntity(oItem);
        const idItem = oItem.id;
        const oContainer = oContainerOwner.inventory;
        if (oContainer) {
            oContainer[idItem] = true;
        }
    }

    /**
   * Suprime une entity du registre des entités de la pièce
   * @param oRoom {MUDRoom} identifiant pièce
   * @param oEntity {MUDEntity} identifiant entité
   * @private
   */
    _removeRoomEntity (oRoom, oEntity) {
        this.checkEntity(oEntity);
        const res = this._getRoomEntityStorage(oRoom);
        const idEntity = oEntity.id;
        delete res[idEntity];
    }

    /**
   * Ajoute une entity au registre des entités de la pièce.
   * attribue un numéro d'identification local.
   * Selon le type d'objet, la lettre associé au lid sera différente
   * @param oRoom {MUDEntity} pièce
   * @param oEntity {MUDEntity} identifiant entité
   * @private
   */
    _addRoomEntity (oRoom, oEntity) {
        this.checkEntity(oEntity);
        this.checkRoom(oRoom);
        // déterminer le type d'une entité
        const idEntity = oEntity.id;
        const oRES = this._getRoomEntityStorage(oRoom);
        oRES[idEntity] = true;
    }

    /**
   * Execute l'une ou l'autre des callback en fonction de la nature du paramètre passé
   * @param roomOrEntity {MUDEntity|MUDRoom}
   * @param room {function} appelée si roomOrEntity est une room
   * @param entity {function} appelée si roomOrEntity est une entité
   * @param other {null|function} appelée si roomOrEntity est une exit
   * @return {*}
   * @private
   */
    _switchRoomEntity (roomOrEntity, { room = null, entity = null, other = null }) {
        try {
            if (this.isRoom(roomOrEntity)) {
                return room ? room(roomOrEntity) : null;
            }
            if (this.isEntity(roomOrEntity)) {
                return entity ? entity(roomOrEntity) : null;
            }
            if (other) {
                return other(roomOrEntity);
            }
        } catch (e) {
            if (other) {
                return other(roomOrEntity);
            }
            console.error(e);
            throw new Error('ERR_INVALID_MUD_ROOM_OR_ENTITY: ' + roomOrEntity.id);
        }
        throw new Error('ERR_TYPE_MISMATCH: expected MUDEntity or MUDRoom ; got ' + getType(roomOrEntity) + ' - value: ' + roomOrEntity);
    }

    /**
   * Change la localisation d'une entité
   * @param roomOrEntity {MUDRoom|MUDEntity} identifiant pièce
   * @param oEntity {MUDEntity} identifiant entité
   * @private
   */
    _setEntityLocation (oEntity, roomOrEntity) {
        this.checkEntity(oEntity);

        const ET = CONSTS.ENTITY_TYPES;

        const getEntityType = e => {
            const sType = e.blueprint
                ? e.blueprint.type
                : 'r';
            switch (sType) {
            case ET.ITEM: {
                return 'i';
            }

            case ET.PLAYER:
            case ET.ACTOR: {
                return 'a';
            }

            case ET.PLACEABLE: {
                return 'p';
            }

            case ET.ENVIRONMENT: {
                return 'e';
            }
            }
        };

        const locationSwitcher = {
            room: room => ({
                location: room,
                isRoom: true,
                isEntity: false,
                locationType: 'r'
            }),
            entity: entity => ({
                location: entity,
                isRoom: false,
                isEntity: true,
                locationType: getEntityType(entity)
            }),
            other: other => ({
                location: null,
                isRoom: false,
                isEntity: false,
                locationType: '*'
            })
        };


        // déterminer précédente location
        const {
            location: oPrevLocation,
            locationType: sPrevLocationType
        } = this._switchRoomEntity(oEntity.location, locationSwitcher);

        // déterminer nouvelle location
        const {
            location: oNewLocation,
            isRoom: bNewLocationIsRoom,
            isEntity: bNewLocationIsEntity,
            locationType: sNewLocationType
        } = this._switchRoomEntity(roomOrEntity, locationSwitcher);

        const oNewInventory = bNewLocationIsRoom
            ? this._getRoomEntityStorage(oNewLocation)
            : bNewLocationIsEntity
                ? (oNewLocation.inventory || null)
                : null;

        if (oNewInventory === null) {
            // Pas de nouvel inventaire
            return false;
        }

        const sFromTo = getEntityType(oEntity) + sPrevLocationType + sNewLocationType;
        switch (sFromTo) {
        // création d'une entité (item, placeable, actor) dans une pièce
        case 'e*r':
        case 'i*r':
        case 'p*r': {
            this._addRoomEntity(oNewLocation, oEntity);
            this._events.emit('entity.create', {
                engine: this,
                entity: oEntity,
                location: oNewLocation,
                owner: null
            });
            break;
        }

        // special case for actors who trigger more event
        // actor must be created before entering room !
        case 'a*r': {
            this._addRoomEntity(oNewLocation, oEntity);
            this._events.emit('entity.create', {
                engine: this,
                entity: oEntity,
                location: oNewLocation,
                owner: null
            });
            this._events.emit('entity.room.enter', {
                engine: this,
                entity: oEntity,
                from: null,
                to: oNewLocation
            });
            break;
        }

        // Suppression d'une entité (item, placeable, actor) dans une pièce
        case 'er*':
        case 'ir*':
        case 'pr*': {
            this._removeRoomEntity(oPrevLocation, oEntity);
            break;
        }

        // Destruction d'un actor
        case 'ar*': {
            this._events.emit('entity.room.exit', {
                engine: this,
                entity: oEntity,
                from: oPrevLocation,
                to: oNewLocation
            });
            break;
        }

        // Déplacement d'une entité (item, placeable) d'une pièce à une autre
        case 'irr':
        case 'prr': {
            this._removeRoomEntity(oPrevLocation, oEntity);
            this._addRoomEntity(oNewLocation, oEntity);
            break;
        }

        // case spécial de l'actor de room à room
        case 'arr': {
            const oPayload = {
                engine: this,
                entity: oEntity,
                from: oPrevLocation,
                to: oNewLocation
            };
            this._events.emit('entity.room.exit', oPayload);
            this._removeRoomEntity(oPrevLocation, oEntity);
            this._addRoomEntity(oPayload.to, oEntity);
            this._events.emit('entity.room.enter', oPayload);
            break;
        }

        // L'item est créé dans l'inventaire d'un actor ou d'un placeable
        case 'i*a':
        case 'i*p':
        {
            this._addInventoryEntity(oNewLocation, oEntity);
            this._events.emit('entity.create', {
                engine: this,
                entity: oEntity,
                location: oEntity.location,
                owner: oNewLocation
            });
            this._events.emit('item.acquire', {
                engine: this,
                entity: oEntity,
                acquiredBy: oNewLocation,
                acquiredFrom: null
            });
            break;
        }

        // l'item est créé dans l'inventaire d'un autre item
        case 'i*i': {
        // déterminer le propriétaire du sac
            const oOwner = this.getItemOwner(oNewLocation);
            this._addInventoryEntity(oNewLocation, oEntity);
            // S'il n'y a pas de propriétaire on ne déclenche pas l'évent
            if (oOwner) {
                this._events.emit('item.acquire', {
                    engine: this,
                    entity: oEntity,
                    acquiredBy: oOwner,
                    acquiredFrom: null
                });
            }
            break;
        }

        // l'item est retiré de l'inventaire d'un autre item
        case 'ii*': {
        // déterminer le propriétaire du sac (il est possible que l'item soit dans un sous-sous-sac...)
            const oOwner = this.getItemOwner(oPrevLocation);
            this._removeInventoryEntity(oPrevLocation, oEntity);
            // S'il n'y a pas de propriétaire on ne déclenche pas l'évent
            if (oOwner) {
                this._events.emit('item.lose', {
                    engine: this,
                    entity: oEntity,
                    lostBy: oOwner
                });
            }
            break;
        }

        // l'item est supprimé depuis l'inventaire d'un actor ou d'un placeable
        case 'ia*':
        case 'ip*': {
            this._removeInventoryEntity(oPrevLocation, oEntity);
            this._events.emit('item.lose', {
                engine: this,
                entity: oEntity,
                lostBy: oPrevLocation
            });
            break;
        }

        // L'item est transféré depuis un inventaire vers une pièce
        case 'iar':
        case 'ipr': {
            this._removeInventoryEntity(oPrevLocation, oEntity);
            this._addRoomEntity(oNewLocation, oEntity);
            this._events.emit('item.drop', {
                engine: this,
                entity: oEntity,
                droppedBy: oPrevLocation,
                location: oNewLocation
            });
            break;
        }

        // L'item est transféré depuis une pièce vers un inventaire
        case 'ira':
        case 'irp': {
            this._removeRoomEntity(oPrevLocation, oEntity);
            this._addInventoryEntity(oNewLocation, oEntity);
            this._events.emit('item.acquire', {
                engine: this,
                entity: oEntity,
                acquiredBy: oNewLocation,
                acquiredFrom: null
            });
            break;
        }

        // L'item est transféré d'un actor à un autre actor ou un placable
        case 'iap':
        case 'ipa':
        case 'iaa': {
            this._removeInventoryEntity(oPrevLocation, oEntity);
            this._addInventoryEntity(oNewLocation, oEntity);
            this._events.emit('item.lose', {
                engine: this,
                entity: oEntity,
                lostBy: oPrevLocation,
                acquiredBy: oNewLocation
            });
            this._events.emit('item.acquire', {
                engine: this,
                entity: oEntity,
                acquiredBy: oNewLocation,
                acquiredFrom: oPrevLocation
            });
            break;
        }

        // une item est transfere depuis une sacovhe vers l'inventaire d'un acteur
        case 'iip':
        case 'iia': {
            const oOwnerFrom = this.getItemOwner(oPrevLocation);
            this._removeInventoryEntity(oPrevLocation, oEntity);
            this._addInventoryEntity(oNewLocation, oEntity);
            // ne déclencher les évènements que s'il y a changement de propriétaire
            if (oOwnerFrom !== oNewLocation) {
                if (oOwnerFrom) {
                    this._events.emit('item.lose', {
                        engine: this,
                        entity: oEntity,
                        lostBy: oPrevLocation,
                        acquiredBy: oNewLocation
                    });
                }
                this._events.emit('item.acquire', {
                    engine: this,
                    entity: oEntity,
                    acquiredBy: oNewLocation,
                    acquiredFrom: oOwnerFrom
                });
            }
            break;
        }

        // ou bien de l'inventaire d'un acteur ou d'un placable vers une sacoche
        case 'ipi':
        case 'iai': {
            const oOwnerTo = this.getItemOwner(oNewLocation);
            this._removeInventoryEntity(oPrevLocation, oEntity);
            this._addInventoryEntity(oNewLocation, oEntity);
            // ne déclencher les évènements que s'il y a changement de propriétaire
            if (oPrevLocation !== oOwnerTo) {
                this._events.emit('item.lose', {
                    engine: this,
                    entity: oEntity,
                    lostBy: oPrevLocation,
                    acquiredBy: oOwnerTo
                });
                if (oOwnerTo) {
                    this._events.emit('item.acquire', {
                        engine: this,
                        entity: oEntity,
                        acquiredBy: oOwnerTo,
                        acquiredFrom: oPrevLocation
                    });
                }
            }
            break;
        }

        // L'item est transféré d'une sacoche à une autre
        case 'iii': {
        // vérifier paradox : ITEM ne doit pas être placé DEST si DEST est déjà contenu dans ITEM
            if (this.getParentEntities(oNewLocation).includes(oEntity)) {
                console.error('ERR_BOXES_PARADOX');
                return false;
            }
            const oOwnerFrom = this.getItemOwner(oPrevLocation);
            const oOwnerTo = this.getItemOwner(oNewLocation);
            this._removeInventoryEntity(oPrevLocation, oEntity);
            this._addInventoryEntity(oNewLocation, oEntity);
            // ne déclencher les évènements que s'il y a changement de propriétaire
            if (oOwnerFrom !== oOwnerTo) {
                if (oOwnerFrom) {
                    this._events.emit('item.lose', {
                        engine: this,
                        entity: oEntity,
                        lostBy: oOwnerFrom,
                        acquiredBy: oOwnerTo
                    });
                }
                if (oOwnerTo) {
                    this._events.emit('item.acquire', {
                        engine: this,
                        entity: oEntity,
                        acquiredBy: oOwnerTo,
                        acquiredFrom: oOwnerFrom
                    });
                }
            }
            break;
        }

        default: {
            throw new Error('ERR_INVALID_ENTITY_TRANSFER_CASE: ' + sFromTo);
        }
        }
        oEntity._location = oNewLocation;
        return true;
    }

    /**
   * @typedef MUDContentDefinitionItem {object}
   * @property ref {string} référence de l'item
   * @property tag {string} tag à associer à l'entité-item
   * @property stack {number} nombre d'exemplaires de l'objet (si il est stackable)
   * @property content {MUDContentDefinitionItem[]} contenu de l'obket si c'est un contenant
   *
   * Définition du contenu d'un objet pourvu d'un inventaire
   * @param oEntity {MUDEntity}
   * @param aContent {MUDContentDefinitionItem[]}
   */
    _setEntityContent (oEntity, aContent) {
        this.checkEntity(oEntity);
        aContent.forEach(({
            ref,
            tag = null,
            stack = 1,
            content = []
        }) => {
            const oSubEntity = this.createEntity(ref, oEntity, { count: stack });
            if (tag !== null) {
                oSubEntity.tag = tag;
            }
            if (Array.isArray(content) && oSubEntity.inventory) {
                this._setEntityContent(oSubEntity, content);
            }
        });
    }

    // ▗▖          ▗▖    ▗▖         ▄▖         ▗▖       ▗▖ ▗▖   ▗▖
    // ▐▌  ▗▛▜▖▗▛▀ ▐▌▄  ▗▛ ▐▌▐▌▐▛▜▖ ▐▌ ▗▛▜▖▗▛▀ ▐▌▄     ▝▜▛▘▐▙▄  ▄▖ ▐▛▜▖▗▛▜▌▗▛▀▘
    // ▐▌  ▐▌▐▌▐▌  ▐▛▙ ▗▛  ▐▌▐▌▐▌▐▌ ▐▌ ▐▌▐▌▐▌  ▐▛▙      ▐▌ ▐▌▐▌ ▐▌ ▐▌▐▌▝▙▟▌ ▀▜▖
    // ▝▀▀▘ ▀▀  ▀▀ ▝▘▝▘▝    ▀▀▘▝▘▝▘ ▀▀  ▀▀  ▀▀ ▝▘▝▘      ▀▘▝▘▝▘ ▀▀ ▝▘▝▘▗▄▟▘▝▀▀

    /**
   * Ouverture d'un contenant à l'aide d'une clé (ou d'un outil)
   * @param oActor {MUDEntity}
   * @param oContainer {MUDPlaceable|MUDItem}
   * @param oKey {MUDEntity}
   * @returns {{success: boolean, outcome: string}}
   */
    _unlockContainer (oActor, oContainer, oKey = undefined) {
        this.checkEntity(oActor);
        if (!this.isContainer(oContainer)) {
            return {
                success: false,
                outcome: CONSTS.UNLOCK_OUTCOMES.NOT_A_CONTAINER
            };
        }
        if (!oContainer.locked) {
            return {
                success: false,
                outcome: CONSTS.UNLOCK_OUTCOMES.NOT_LOCKED
            };
        }
        if (
            oKey &&
      oKey.blueprint.type === CONSTS.ENTITY_TYPES.ITEM
        ) {
            const oLock = oContainer.blueprint.lock;
            if (this.isEntityTagged(oKey, oLock.key)) {
                oContainer.locked = false; // Ouvrir la porte
                if (oLock.discardKey) { // tester discardKey
                    // virer la clé de l'inventaire
                    this.destroyEntity(oKey);
                }
                this._events.emit('container.unlock', {
                    container: oContainer,
                    actor: oActor
                });
                return {
                    success: true,
                    outcome: ''
                };
            } else {
                this._events.emit('container.unlock.failure', {
                    container: oContainer,
                    actor: oActor
                });
                return {
                    success: false,
                    outcome: CONSTS.UNLOCK_OUTCOMES.WRONG_KEY
                };
            }
        } else {
            // Ce n'est pas une vrai clé, voir même pas un item valide
            // Vérifier par évènement si un sous-système prévoi de considéré l'item comme un outil pouvant aider
            // à déverrouiller la porte
            const { success: bSuccess, reason } = this.resolveTaskOutcome(oActor, CONSTS.MUD_SKILL_UNLOCK, oContainer.blueprint.lock.difficulty, oKey);
            if (bSuccess) {
                oContainer.locked = false;
                this._events.emit('container.unlock', {
                    container: oContainer,
                    actor: oActor
                });
                return {
                    success: true,
                    outcome: CONSTS.UNLOCK_OUTCOMES.SUCCESS
                };
            } else {
                this._events.emit('container.unlock.failure', {
                    container: oContainer,
                    actor: oActor
                });
                // La tentative a échoué
                return {
                    success: false,
                    outcome: reason === 'cooldown' ? CONSTS.UNLOCK_OUTCOMES.COOLDOWN : CONSTS.UNLOCK_OUTCOMES.KEY_INVALID
                };
            }
        }
    }

    /**
   * Verrouillage d'une issue à l'aide d'une clé.
   * L'issue à verrouillée doit avoir été déclarée avec un verrou et une clé
   * @param oActor {MUDEntity} identifiant de l'acteur qui verrouille
   * @param sDirection {string} direction de la porte
   * @param oKey {MUDEntity}
   * @return {{success: boolean, outcome: string}}
   *
   *     SUCCESS: 'SUCCESS',
   *     WRONG_KEY: 'WRONG_KEY',
   *     ALREADY_LOCKED: 'ALREADY_LOCKED',
   *     INVALID_TARGET: 'INVALID_TARGET',
   *     INVALID_KEY: 'KEY_INVALID'
   * @private
   */
    _lockExit (oActor, sDirection, oKey) {
        if (!this.isActor(oActor)) {
            throw new Error('Only actors may lock doors or containers');
        }
        const idRoom = oActor.location;
        const oExit = this.getExit(idRoom, sDirection); // ok getExit
        if (!oExit.lockable || !oExit.valid || !oExit.isDetectedBy(oActor)) {
            return {
                success: false,
                outcome: CONSTS.LOCK_OUTCOMES.INVALID_TARGET
            };
        }
        if (oExit.locked) {
            return {
                success: false,
                outcome: CONSTS.LOCK_OUTCOMES.ALREADY_LOCKED
            };
        }
        if (oExit.key !== oKey.tag) {
            return {
                success: false,
                outcome: CONSTS.LOCK_OUTCOMES.WRONG_KEY
            };
        }
        oExit.locked = true;
        this._events.emit('exit.lock', { room: idRoom, direction: sDirection, actor: oActor });
        return {
            success: true,
            outcome: CONSTS.LOCK_OUTCOMES.SUCCESS
        };
    }

    _lockContainer (oActor, oContainer, oKey) {
        if (!this.isActor(oActor)) {
            throw new Error('Only actors may lock doors or containers');
        }
        if (!oContainer.blueprint.lock) {
            return {
                success: false,
                outcome: CONSTS.LOCK_OUTCOMES.INVALID_TARGET
            };
        }
        if (oContainer.locked) {
            return {
                success: false,
                outcome: CONSTS.LOCK_OUTCOMES.ALREADY_LOCKED
            };
        }
        if (oKey.tag !== oContainer.blueprint.lock?.key) {
            return {
                success: false,
                outcome: CONSTS.LOCK_OUTCOMES.WRONG_KEY
            };
        }
        oContainer.locked = true;
        this._events.emit('container.lock', { container: oContainer, actor: oActor });
        return {
            success: true,
            outcome: CONSTS.LOCK_OUTCOMES.SUCCESS
        };
    }

    /**
   * Ouverture d'une issue par un actor à l'aide d'une clé
   * @param oActor {MUDEntity}
   * @param [oKey] {MUDEntity}
   * @param sDirection {string}
   * @returns {{success: boolean, outcome: string}}
   */
    _unlockExit (oActor, sDirection, oKey = undefined) {
        this.checkEntity(oActor);
        this.checkOptionalItem(oKey);
        const oExit = this.getExit(oActor.location, sDirection); // ok getExit
        if (!oExit.valid || !oExit.isDetectedBy(oActor)) {
            return {
                success: false,
                outcome: CONSTS.UNLOCK_OUTCOMES.EXIT_INVALID
            };
        }
        if (!oExit.locked) {
            return {
                success: false,
                outcome: CONSTS.UNLOCK_OUTCOMES.NOT_LOCKED
            };
        }
        if (oKey) {
            // c'est bon : c'est une clé. Vérifier son tag
            if (this.isEntityTagged(oKey, oExit.key)) {
                oExit.locked = false; // Ouvrir la porte
                if (oExit.discardKey) { // tester discardKey
                    // virer la clé de l'inventaire
                    this.destroyEntity(oKey);
                }
                this._events.emit('exit.unlock', {
                    room: oActor.location,
                    direction: sDirection,
                    actor: oActor
                });
                return {
                    success: true,
                    outcome: ''
                };
            } else {
                this._events.emit('exit.unlock.failure', {
                    room: oActor.location,
                    direction: sDirection,
                    actor: oActor
                });
                // Pas la bonne clé
                return {
                    success: false,
                    outcome: CONSTS.UNLOCK_OUTCOMES.WRONG_KEY
                };
            }
        } else {
            // Ce n'est pas une vrai clé, voir même pas un item valide
            // Vérifier par évènement si un sous-système prévoi de considéré l'item comme un outil pouvant aider
            // à déverrouiller la porte
            const { success: bSuccess, reason } = this.resolveTaskOutcome(oActor, CONSTS.MUD_SKILL_UNLOCK, oExit.dcLockpick, oKey);
            if (bSuccess) {
                oExit.locked = false;
                this._events.emit('exit.unlock', {
                    room: oActor.location,
                    direction: sDirection,
                    actor: oActor
                });
                return {
                    success: true,
                    outcome: ''
                };
            } else {
                // La tentative à finalement échoué
                this._events.emit('exit.unlock.failure', {
                    room: oActor.location,
                    direction: sDirection,
                    actor: oActor
                });
                return {
                    success: false,
                    outcome: reason === 'cooldown' ? CONSTS.UNLOCK_OUTCOMES.COOLDOWN : CONSTS.UNLOCK_OUTCOMES.KEY_INVALID
                };
            }
        }
    }

    // ▗▄▄  ▄▖                              ▗▖  ▗▖
    // ▐▌▐▌ ▐▌  ▀▜▖▐▌▐▌▗▛▜▖▐▛▜▖     ▀▜▖▗▛▀ ▝▜▛▘ ▄▖ ▗▛▜▖▐▛▜▖▗▛▀▘
    // ▐▛▀  ▐▌ ▗▛▜▌▝▙▟▌▐▛▀▘▐▌      ▗▛▜▌▐▌   ▐▌  ▐▌ ▐▌▐▌▐▌▐▌ ▀▜▖
    // ▝▘   ▀▀  ▀▀▘▗▄▛  ▀▀ ▝▘       ▀▀▘ ▀▀   ▀▘ ▀▀  ▀▀ ▝▘▝▘▝▀▀

    /**
   * @typedef ActionOpenDoorStruct {object}
   * @property valid {boolean} existance de l'issue
   * @property visible {boolean} visibilité de l'issue (a été détectée par l'acteur)
   * @property locked {boolean} issue fermée et verrouillée
   * @property destination {string} identifiant de la pièce de destination
   * @property success {boolean} l'acteur a réussi à passer la porte
   *
   * Lance une action d'ouverture de porte. L'action échoue si la porte fermant l'issue est verrouilée
   * @param oActor {MUDEntity} identigiant de l'acteur qui se déplace
   * @param sDirection {string} direction de l'issue que veut emprunter l'acteur
   * @return {ActionOpenDoorStruct}
   */
    actionPassThruExit (oActor, sDirection) {
        this.checkEntity(oActor);
        const oExit = this.getExit(oActor, sDirection); // ok getExit
        const from = oActor.location;
        const valid = oExit.valid;
        const visible = oExit.isDetectedBy(oActor);
        const locked = oExit.locked;
        const destination = oExit.destination;
        if (valid && visible && !locked) {
            this.moveEntity(oActor, destination);
            return {
                success: true,
                valid,
                visible,
                locked,
                from,
                destination
            };
        } else {
            this.triggerEntityEvent(oActor, 'blocked', {
                room: oActor.location,
                direction: sDirection
            });
            return {
                success: false,
                valid,
                visible,
                locked,
                from,
                destination
            };
        }
    }

    /**
   * Lance une action de déverrouillage du container ou de l'issue spécifiée
   * L'action est effectuée par un acteur de sorte que la résolution dépendant de la qualité des compétence de l'acteur.
   * @param oActor {MUDEntity} identifiant de l'actor qui effectue l'action
   * @param containerOrDirection {string|MUDPlaceable|MUDItem} identifiant du container ou de la direction
   * @param oKey {MUDEntity} identifiant de la clé ou de l'outil
   * @returns {{success: boolean, outcome: string}}
   */
    actionUnlock (oActor, containerOrDirection, oKey = null) {
        this.checkEntity(oActor);
        this.checkOptionalEntity(oKey);
        if ((getType(containerOrDirection) === 'string') && this.isDirectionValid(containerOrDirection)) {
            return this._unlockExit(oActor, containerOrDirection, oKey);
        } else {
            this.checkEntity(containerOrDirection);
            return this._unlockContainer(oActor, containerOrDirection, oKey);
        }
    }

    /**
   * L'action de verrouillage n'est possible qu'à l'aide de la clé correspondant à la serrure.
   * @param oActor {MUDEntity}
   * @param containerOrDirection {MUDEntity|string}
   * @param oKey {MUDEntity}
   */
    actionLock (oActor, containerOrDirection, oKey) {
        this.checkEntity(oActor);
        this.checkEntity(oKey);
        if ((getType(containerOrDirection) === 'string') && this.isDirectionValid(containerOrDirection)) {
            return this._lockExit(oActor, containerOrDirection, oKey);
        } else {
            this.checkEntity(containerOrDirection);
            return this._lockContainer(oActor, containerOrDirection, oKey);
        }
    }

    /**
   * Lance une action de recherche d'issues cachées
   * L'action est effectuée par un acteur de sorte que la résolution dépendant de la qualité des compétence de l'acteur.
   * @param oPlayer {MUDEntity} identifiant de l'acteur qui tente de rechercher l'issue cachée
   * @param sDirection {string} direction dans laquelle la recherche est effectuée
   * @param oTool {MUDEntity} identifiant optionnel de l'outil qui aide à l'action
   * @returns {{success: boolean, outcome: string}}
   */
    actionSearchSecret (oPlayer, sDirection, oTool = null) {
        this.checkEntity(oPlayer);
        this.checkOptionalItem(oTool);
        const oExit = this.getExit(oPlayer, sDirection); // ok getExit
        const valid = oExit.valid;
        const secret = oExit.secret;
        const visible = oExit.isDetectedBy(oPlayer);
        const dcSearch = oExit.dcSearch;
        if (valid && visible) {
            return {
                success: false,
                outcome: CONSTS.SEARCH_OUTCOMES.NO_NEED_DETECTION
            };
        }
        const { success: bCouldFind, reason } = this.resolveTaskOutcome(oPlayer, CONSTS.MUD_SKILL_SEARCH, dcSearch, oTool);
        if (!bCouldFind) {
            return {
                success: false,
                outcome: reason === 'cooldown'
                    ? CONSTS.SEARCH_OUTCOMES.COOLDOWN
                    : CONSTS.SEARCH_OUTCOMES.NOT_FOUND
            };
        }
        if (!valid || !secret) {
            return {
                success: false,
                outcome: CONSTS.SEARCH_OUTCOMES.NOT_FOUND
            };
        }
        this.setEntityExitSpotted(oPlayer, sDirection, true);
        return {
            success: true,
            outcome: ''
        };
    }

    /**
   * Use inventory item (healing kit, magic device) or placeable item (lever, press plate, button)
   */
    actionUse (oActor, oItemOrPlaceable, oTool = null) {
        this.checkActor(oActor);
        this.checkEntity(oItemOrPlaceable);
        this.checkOptionalItem(oTool);
        this.triggerEntityEvent(oItemOrPlaceable, 'used', {
            interactor: oActor,
            tool: oTool
        });
    }

    //  ▄▄          ▗▖                              ▗▖              ▗▖  ▗▖  ▗▖
    // ▐▌▝▘▐▌▐▌▗▛▀▘▝▜▛▘▗▛▜▖▐▙▟▙    ▗▛▜▖▐▌▐▌▗▛▜▖▐▛▜▖▝▜▛▘    ▗▛▜▖▐▙▟▙ ▄▖ ▝▜▛▘▝▜▛▘▗▛▜▖▐▛▜▖
    // ▐▌▗▖▐▌▐▌ ▀▜▖ ▐▌ ▐▌▐▌▐▛▛█    ▐▛▀▘▝▙▟▘▐▛▀▘▐▌▐▌ ▐▌     ▐▛▀▘▐▛▛█ ▐▌  ▐▌  ▐▌ ▐▛▀▘▐▌
    //  ▀▀  ▀▀▘▝▀▀   ▀▘ ▀▀ ▝▘ ▀     ▀▀  ▝▘  ▀▀ ▝▘▝▘  ▀▘     ▀▀ ▝▘ ▀ ▀▀   ▀▘  ▀▘ ▀▀ ▝▘

    triggerEntityEvent (roomOrEntity, event, payload) {
        const events = this._switchRoomEntity(roomOrEntity, {
            room: room => room.events,
            entity: entity => entity.events
        });
        if (events && getType(events[event]) === 'string') {
            this._events.emit('entity.event.script', {
                script: events[event],
                payload: {
                    engine: this,
                    self: roomOrEntity,
                    ...payload
                }
            });
        }
    }

    _initCustomEventEmitter () {
    // blocked            ? (unable to open door)
    // conversation       ? say
    // disturbed          ? inventory changed
        this._events.on('item.acquire', ({
            entity: oItem,
            acquiredFrom,
            acquiredBy
        }) => {
            // il faut que entity soit item
            if (oItem.blueprint.type !== CONSTS.ENTITY_TYPES.ITEM) {
                return;
            }
            let sTransfer = '';
            if (!acquiredFrom) {
                sTransfer += 'X';
            } else if (this.isActor(acquiredFrom) || this.isPlayer(acquiredFrom)) {
                sTransfer += 'A';
            } else if (this.isContainer(acquiredFrom)) {
                sTransfer += 'C';
            } else if (this.isRoom(acquiredFrom)) {
                sTransfer += 'R';
            } else {
                sTransfer += 'X';
            }
            if (!acquiredBy) {
                sTransfer += 'X';
            } else if (this.isActor(acquiredBy) || this.isPlayer(acquiredBy)) {
                sTransfer += 'A';
            } else if (this.isContainer(acquiredBy)) {
                sTransfer += 'C';
            } else if (this.isRoom(acquiredBy)) {
                sTransfer += 'R';
            } else {
                sTransfer += 'X';
            }
            switch (sTransfer) {
            case 'AR':
            case 'AC': {
                // l'objet est transféré depuis un actor vers un contenant
                // l'actor dépose donc l'objet dans un contenant ou la pièce
                this.triggerEntityEvent(acquiredBy, 'disturbed', {
                    interactor: acquiredFrom,
                    type: CONSTS.DISTURBANCE_ITEM_ADDED,
                    item: oItem
                });
                this.triggerEntityEvent(oItem, 'dropped', { interactor: acquiredFrom });
                break;
            }

            case 'RA':
            case 'CA': {
                // l'objet est transféré depuis un contenant vers un actor
                // l'actor a donc pris l'objet
                this.triggerEntityEvent(acquiredFrom, 'disturbed', {
                    interactor: acquiredBy,
                    type: CONSTS.DISTURBANCE_ITEM_REMOVED,
                    item: oItem
                });
                this.triggerEntityEvent(oItem, 'acquired', { interactor: acquiredBy });
                break;
            }

            case 'AA': {
                // Un actor donne un objet à un autre actor
                this.triggerEntityEvent(acquiredFrom, 'disturbed', {
                    interactor: acquiredBy,
                    type: CONSTS.DISTURBANCE_ITEM_REMOVED,
                    item: oItem
                });
                this.triggerEntityEvent(acquiredBy, 'disturbed', {
                    interactor: acquiredFrom,
                    type: CONSTS.DISTURBANCE_ITEM_ADDED,
                    item: oItem
                });
                this.triggerEntityEvent(oItem, 'dropped', { interactor: acquiredFrom });
                this.triggerEntityEvent(oItem, 'acquired', { interactor: acquiredBy });
                break;
            }

            default: {
                // Les autres cas sont ignorés
                break;
            }
            }
        });

        // heartbeat ? indexer les entités qui ont un blueprints contenant cet event
        // perception
        // spawn & enter
        this._events.on('entity.create', ({ entity }) => {
            this.triggerEntityEvent(entity, 'spawn', {});
        });
        // lock
        // open
        // openFailure
        this._events.on('container.unlock.failure', ({ container, actor }) => {
            this.triggerEntityEvent(container, 'openFailure', { interactor: actor });
        });
        this._events.on('exit.unlock.failure', ({ room, direction, actor }) => {
            this.triggerEntityEvent(room, 'openFailure', { direction, interactor: actor });
        });
        // unlock
        this._events.on('container.unlock', ({ container, actor }) => {
            this.triggerEntityEvent(container, 'unlocked', { interactor: actor });
        });
        this._events.on('exit.unlock', ({ room, direction, actor }) => {
            this.triggerEntityEvent(room, 'unlocked', { direction, interactor: actor });
        });
        // lock
        this._events.on('container.lock', ({ container, actor }) => {
            this.triggerEntityEvent(container, 'locked', { interactor: actor });
        });
        this._events.on('exit.lock', ({ room, direction, actor }) => {
            this.triggerEntityEvent(room, 'locked', { direction, interactor: actor });
        });

        // enter
        // exit
        // acquired
        // dropped
        this._events.on('entity.room.enter', ({ entity, from, to }) => {
            // il faut que entity soit actor ou player
            if (this.isActor(entity)) {
                this.triggerEntityEvent(to, 'enter', { interactor: entity, from });
            }
        });
        // exit
        this._events.on('entity.room.exit', ({ entity, from, to }) => {
            // il faut que entity soit actor ou player
            if (this.isActor(entity)) {
                this.triggerEntityEvent(from, 'exit', { interactor: entity, to });
            }
        });
        this._events.on('entity.destroy', ({ entity: oDestroyedEntity }) => {
            const oRoom = oDestroyedEntity.location;
            if (oRoom) {
                this.triggerEntityEvent(oRoom, 'exit', { interactor: oDestroyedEntity });
            }
            this.triggerEntityEvent(oDestroyedEntity, 'despawn', { location: oRoom });
        });
    }

    /**
   * Pour chaque pièce active : verifier que la pièce est vide de tout joueur
   * Si oui alors augmenter le data.noPlayerDuring
   */
    _garbageCollectRooms () {
        const aActiveRooms = Object.keys(this.state.roomEntityStorage);
        let nActiveRoomCount = aActiveRooms.length;
        aActiveRooms
            .map(idRoom => this.getRoom(idRoom)) // transformer la collection en room
            .forEach(room => { // pour chaque pièce vide,
                // filtrer les pièces vides de tout joueur
                const bHasPlayers = this.getRoomPlayers(room).length > 0;
                const bHasTimer = ROOM_DATA_TIME_SINCE_EMPTY in room.data;
                switch (true) {
                case bHasPlayers && !bHasTimer: {
                    // Cette pièce est occupée par des joueurs et ne possède pas de timer
                    // C'est une pièce régulièrement occupée
                    break;
                }
                case bHasPlayers && bHasTimer: {
                    // Cette pièce est occupée par des joueurs et possède un timer
                    // Le timer est mis en pause et reprendra quand la pièce sera inoccupée
                    break;
                }
                case !bHasPlayers && bHasTimer: {
                    // Cette pièce est inoccupée, le timer continue
                    // Si le timer atteind la limite, la pièce est reset
                    if (++room.data[ROOM_DATA_TIME_SINCE_EMPTY] > ROOM_TIME_BEFORE_RESET) {
                        const nTotalRoomCount = Object.keys(this.state.rooms).length;
                        this.resetRoom(room);
                        --nActiveRoomCount;
                        const nRoomActive100 = Math.round(nActiveRoomCount * 100 / nTotalRoomCount);
                        logMud('reset room %s. active rooms / total rooms : %d/%d (%d%%)', room.id, nActiveRoomCount, nTotalRoomCount, nRoomActive100);
                    }
                    break;
                }
                case !bHasPlayers && !bHasTimer: {
                    // Pièce inoccupée, et sans timer : on va créer le timer
                    room.data[ROOM_DATA_TIME_SINCE_EMPTY] = 0;
                    break;
                }
                }
            });
    }

    /**
   * idéalement appelée chaque seconde
   * Effectuer un reset des pièces inoccupées depuis longtemps (5 min)
   */
    handleClock () {
        ++this._clock;
        if ((this._clock % ROOM_INTERVAL_CHECK_EMPTY) === 0) {
            this._garbageCollectRooms();
        }
    }
}

module.exports = Engine;
