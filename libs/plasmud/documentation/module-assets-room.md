# Module definition

## Rooms

### name
The room display name.
- Type : string
- Required : yes

### sector
The id of the sector where this room belongs to. Must reference one of the sectors defined in the "sectors" section.
- Type : string
- Required : yes

### desc
A detailed description of the room. Each item of the array is rendered as a paragraph.
- Type : array of strings
- Required : yes

### nav
A KeyVal collection of exit definition.
Define all exit (one for each direction). {dir} is a direction, an item of this list : "n, e, w, s, nw, ne, sw, se, u, d"
- Type : complex object
- Required : yes

#### nav.{dir}
