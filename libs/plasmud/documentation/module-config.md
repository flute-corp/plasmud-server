#### Contenu de config.json

```json
{
  "startingLocation": "xxxx",
  "defaultLanguage": "fr",
  "languages": ["fr", "en"]
}
```

- startingLocation :
- languages : liste des langues supportées
- defaultLanguage : nom de la langue supportée en cas de non spécification

Si defaultLanguage n'est aps spécifié, alors le système considère que c'est le premier
de la liste qui sera le defaultLanguage.
