# Wizard commandes

## @teleport

Téléporte un joueur ou un objet à un autre emplacement.

```
@teleport <entity> <id-room>
```

## @summon

Appelle un joueur ou un objet à votre position actuelle.

```
@summon <entity>
```

## @shutdown

Arrête le serveur de jeu ou redémarre le jeu.

```
@shutdown
```

## @who

Affiche la liste des joueurs actuellement connectés, souvent avec des détails supplémentaires.

```
@who
```

Affiche une liste de tous les utilisateurs connectés.
Pour chaque utilisateur on affiche :
- Nom d'utilisateur
- Nom du personnage utilisé
- Zone dans laquelle se trouve le personnage

## @ban

Bannit un joueur, souvent par IP ou par nom de compte.

```
@ban <user>
```

@kick : Expulse un joueur du jeu sans le bannir.
@create : Crée un nouvel objet, personnage ou salle.
@edit : Modifie les propriétés d’un objet, d’une salle, ou d’un personnage.
@destroy : Supprime un objet, une salle ou un personnage du jeu.
@give : Donne un objet ou une somme d'argent à un joueur.
@inventory : Affiche l'inventaire d'un joueur.
@mute : Réduit au silence un joueur, l'empêchant d'envoyer des messages ou de parler.
@unmute : Enlève la restriction de silence sur un joueur.
@move : Déplace un joueur ou un objet à un autre endroit.
@freeze : Gèle un joueur, l'empêchant d'agir.
@unfreeze : Dégèle un joueur.
@load : Charge un objet, personnage ou salle à partir des données du jeu.
@save : Sauvegarde l'état actuel du jeu ou d’un joueur.
@announce : Envoie un message global à tous les joueurs.
@zone : Crée, modifie ou supprime une zone dans le jeu.
@grant : Accorde des privilèges ou des droits spécifiques à un joueur.
@revoke : Révoque des privilèges ou des droits précédemment accordés à un joueur.
@force : Force un joueur à exécuter une commande spécifique.
@stat : Affiche les statistiques ou les détails sur un joueur, un objet ou une salle.
@link : Crée une connexion entre deux salles ou objets.
@unlink : Supprime une connexion entre deux salles ou objets.
@clone : Crée une copie d’un objet, d’un personnage, ou d’une salle.
@heal : Rétablit la santé d’un joueur ou d’un personnage.
@damage : Inflige des dégâts à un joueur ou à un personnage.
@reload : Recharge un script, un module ou une zone du jeu sans redémarrer complètement le serveur.