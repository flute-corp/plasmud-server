# Mud entity types

## item
Objet que l'on peut inclure dans l'inventaire.

## actor
Créature animée NPC.

## player
Personnage dirigé par un joueur.

## placeable
Objet posé dans la pièce.
