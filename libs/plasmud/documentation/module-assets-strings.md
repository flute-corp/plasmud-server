## Section i18n dédiée

### Section "room"

Cette section sera colorée pour indiquer les messages d'évènement qui se passe dans la pièce,
mais qui ne sont pas du fait du joueur. Typiquement les évènements déclenchés par d'autres joueurs de
la même pièce.

### Section "error"

Cette section colorera le message pour refléter le caractère important du message,
qui met en évidence une erreur.

### Section "failure"

Cette section colorera le message pour indiquer qu'une précédente action a échoué.

### Section "action"

Cette section est dédiés aux messages qui décrivent les actions effectuées, et qui ont
rencontré un succès (les actions en echec son décrite dans une autre section).

### Section "info"

Cette section est dédiés aux messages qui donne davantage de renseignements.

## Internationalisation des chaînes de caractère

Tout texte qui peut être internationalisé doit être placé dans le répertoire __assets__.

- blueprints : Les noms, les descriptions
- rooms : Les noms, les descriptions
- sectors : Les noms, les descriptions
- strings : Les chaînes de caractères affichées à destination des joueurs
- templates : Les templates affichés à destination des joueurs

### Exemple d'internationalisation

#### Structure du module internationnalisé :

```
├─ assets
│  ├─ fr
│  │  ├─ blueprints
│  │  ├─ rooms
│  │  ├─ sectors
│  │  ├─ strings
│  │  ├─ dialogs
│  │  ├─ templates
│  │  └─ quests
│  └─ en
│     ├─ blueprints
│     ├─ rooms
│     ├─ sectors
│     ├─ strings
│     ├─ dialogs
│     ├─ templates
│     └─ quests
├─ scripts
├─ commands
└─ config.json
```

#### Contenu de config.json

```json
{
  "startingLocation": "xxxx",
  "defaultLanguage": "fr",
  "languages": ["fr", "en"]
}
```

- startingLocation :
- languages : liste des langues supportées
- defaultLanguage : nom de la langue supportée en cas de non spécification

Si defaultLanguage n'est aps spécifié, alors le système considère que c'est le premier
de la liste qui sera le defaultLanguage.
