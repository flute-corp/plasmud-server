# Evènements déclenchés par l'engine

## Interception des évènements par script

Il est possible d'intercepter les évènements suivants en
crééant un script du même nom dans le dossier des scripts.

## Tableaux récapitulatifs

### Engine

| Evènement                 | Paramètres                                        | Testé |
|---------------------------|---------------------------------------------------|-------|
 | entity.create             | entity, location                                  | ☑     |
 | entity.destroy            | entity                                            | ☑     |
| entity.room.enter         | entity, from, to                                  | ☑     |
| entity.room.exit          | entity, from, to                                  | ☑     |
 | entity.speak              | entity, interlocutor, speech, volume              | ☑     |
 | entity.task.attempt       | entity, skill, difficulty, success(), value, tool | ☑     |
| resolve.entity.visibility | watcher, target, visibility                       |       |
| resolve.textref           | context, textref, text                            |       |
 | resolve.room.visibility   | room, watcher, visibility                         |       |
 | item.acquire              | entity, acquiredBy, acquiredFrom                  | ☑     |
 | item.lose                 | entity, lostBy, acquiredBy                        | ☑     |
 | item.drop                 | entity, droppedBy, location                       | ☑     |
| container.lock            | container, actor                                  | ☑     |
| container.unlock          | container, actor                                  | ☑     |
 | container.unlock.failure  | container, actor                                  | ☑     |
| exit.lock                 | room, direction, actor                            | ☑     |
| exit.unlock               | room, direction, actor                            | ☑     |
 | exit.unlock.failure       | room, direction, actor                            | ☑     |

### System

| Evènement | Paramètres         |
|-----------|--------------------|
 | init      |                    |
 | output    | id, content        |
 | command   | command, deny, uid |



## Détails des évènements

### entity.create
Déclenché lorsqu'une entité apparait dans une pièce.
#### Paramètres
- entity : référence de l'entité
- location : pièce dans laquelle l'entité apparait

### entity.destroy
Déclenché lorsqu'une entité disparait du jeu.
#### Paramètres
- entity : référence de l'entité qui disparait. Il est conseiller de ne
  pas exploiter l'instance de l'entité dans un traitement asynchrone
  car l'objet est détruit juste après l'évènement.

### entity.room.enter
Déclenché lorsque une entité change de localisation. Cela peut etre un
acteur qui change de pièce où bien un item qui passe d'un coffre vers
l'inventaire d'un joueur
#### Paramètres
- entity : référence de l'entité qui bouge
- from : référence de la pièce/inventaire que l'entité quitte
- to : référence de la pièce dans laquelle le joueur arrive

### entity.speak
Déclenché lorsqu'une entité émet un message de communication à destination
d'une autre entité.
#### Paramètres
- entity : référence de l'entité qui parle
- interlocutor : référence de l'entité qui reçois le message
- speech : contenu du message
- volume : volume du message (0: silence, 1: normal, 2: echo, 3: shout)

### entity.task.attempt
Déclenché lorsqu'une entité tente d'effectuer une action qui fait intervenir un talent
#### Paramètres
- entity : référence de l'entité qui tente une action.
- skill : nom du talent sollicité
- difficulty : difficulté de la tâche à effectuer.
- success : le gestionnaire d'évènement devra écrire true ou false dans cette variable
  pour déterminer l'issue de l'action (réussite ou echec)
- value : valeur du talent, peut être réécrite par le gestionnaire d'évènement
  à titre informatif.
- tool : instance d'une éventuelle entité-outil servant à la résolution de la tâche.

### resolve.entity.visibility
Déclenché lorsque l'engine souhaite obtenir la capacité d'un actor d'en percevoir un autre.
#### Paramètre
- watcher : référence de l'acteur qui tente de percevoir la cible
- target : acteur cible dont on cherche à savoir s'il est percu par le watcher
- visibility : constante à renseigner pour déterminer la capacité du watcher à détecter target

### resolve.textref
Déclenché lorsque l'engine réclame la transcription d'une référence de texte.
Etant donné que l'engine ne connais pas le véritable contenu des chaïnes de caractères
(à cause de l'internationnalisation), il compte sur un service extérieur pour pouvoir afficher
des information dans ses templates etc...
####
- textref: référence du texte
- text: chaîne à compléter
- context: contexte de substitution i18n ou Twig. Contient des propriétés dont les valeurs aideront à composer un texte final.

### resolve.room.visibility
Déclenché lorsque l'engine réclame la luminosité d'une pièce. Par défaut, il y a une luminosité associée
à chaque pièce à cause de la présence d'une entité environnement.
####
- room: référence de la pièce
- watcher : acteur qui est les point de vue de la scène
- perception: variable à renseigner pour déterminer la luminosité de la pièce.

### item.acquire
Déclenché lorsqu'une entité acquiert un item.
#### Paramètres
- entity : référence de l'item
- acquiredBy: référence de l'entité qui acquiert l'item
- acquiredFrom: référence de l'entité qui cède l'item

### item.lose
Déclenché lorsqu'une entité perd un item au profit de l'inventaire d'une autre entité.
#### Paramètres
- entity : référence de l'item
- lostBy : référence de l'entité qui perd l'objet
- location : référence de la pièce

### item.drop
Déclenché lorsqu'une entité lache un item sur le sol d'une pièce
#### Paramètres
- entity : référence de l'item
- droppedBy : référence de l'inventaire de l'entité qui abandonne l'objet
- location : référence de la pièce dans laquelle l'item est laché

### container.unlock
Déclenché lorsqu'un contenant est déverrouillé avec success
#### Paramètres
- container : identifiant du contenant qui vient d'être déverrouillé.
- actor : référence de l'actor qui a effectué l'action.

### container.unlock.failure
Déclenché lorsqu'une action de déverrouillage d'un contenant verrouillé, échoue.
#### Paramètres
- container : identifiant du contenant qui vient d'être déverrouillé.
- actor: référence de l'actor qui a tenté le deverrouillage.

### exit.unlock
Déclenché lorsqu'un contenant est déverrouillé avec success
#### Paramètres
- room : référence de la pièce.
- direction : issue qui vient d'être déverrouillée.
- actor : référence de l'actor qui a effectué l'action.

### exit.unlock.failure
Déclenché lorsqu'une action de déverrouillage d'un contenant verrouillé, échoue.
#### Paramètres
- room : référence de la pièce.
- direction : issue qui n'a pas pu être déverrouillée.
- actor: référence de l'actor qui a tenté le déverrouillage.



## Evènements déclenchés par le système

### init
Déclenché lorsque le système s'initialise

### output
Déclenché lorsque le système a besoin d'informer un client sur ce qu'il se passe dans le jeu.
Généralement lorsqu'un client entre une commande, le système répond par une série d'évènements "output".
#### Paramètres
- id : identifiant du client auquel il faut délivrer le message
- content : contenu du message (texte)

### command
Déclenché lorsque le client a entré une commande. Cet évènement est utilisé afin de déterminer si
le joueur a le droit d'utiliser une commande.
Par exemple les joueur dont le personnage est KO ne peuvent plus utiliser la plupart des commandes nécessitant une
action.
#### Paramètres
- command : nom de la commande
- deny : révoque l'autorisation du client d'utiliser la commande. On peut fournir la raison en paramètre (string) de ce callback
- uid : identifiant du client qui tape la commande

