# Evènements déclenchés par les entités

## Quelles entités déclenchent quels évènements ?

| Evènement      | actor | player | placeable | item | room | door | Testé |
|----------------|-------|--------|-----------|------|------|------|-------|
 | blocked        | x     | x      |           |      |      |      | x     |
 | conversation * | x     |        | x         |      |      |      |       |
 | disturbed      | x     |        | x         |      |      |      | x     |
 | heartbeat *    | x     |        |           |      |      |      |       |
| perception *   | x     |        |           |      |      |      |       |
| think          | x     |        |           |      |      |      |       |
| spawn          | x     | x      | x         | x    |      |      | x     |
| despawn        | x     | x      | x         | x    |      |      | x     |
 | locked         |       |        | x         |      |      | x    |       |
 | openFailure    |       |        | x         | x    |      | x    | x     |
 | unlocked       |       |        | x         | x    |      | x    | x     |
 | enter          |       |        |           |      | x    |      | x     |
 | exit           |       |        |           |      | x    |      | x     |
 | command        |       |        |           |      | x    |      |       |
 | acquired       |       |        |           | x    |      |      | x     |
 | dropped        |       |        |           | x    |      |      | x     |
 | equipped *     |       |        |           | x    |      |      |       |
 | unequipped *   |       |        |           | x    |      |      |       |
 | used *         |       |        |           | x    |      |      |       |

* Cet évènement n'est pas codé et pourrait dépendre d'un module externe

## Propriété communes des objet fournis aux évènement

Lorsqu'un évènement est déclenché, il lance un script qui reçois en
paramètre un objet contenant diverses propriété selon la nature de l'évènement,
il y a cependant plusieurs propriétés communes à tous les évènement.
- __engine__: instance du moteur du jeu, permettant d'obtenir des information sur la nature des entité (porte, pièce, acteurs...)
- __services__: plugins aditionels proposés par certains modules. Par exemple le module ADDV propose des fonctionnalité pour gérer la partie "règles du jeu", les combats...
- __self__: instance de l'entité sur qui s'applique l'évènement.


## Liste des évènements

### blocked
Déclenché lorsqu'une entité ne peut pas se déplacer d'une pièce à une autre
à cause d'une porte verrouillée.
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: instance de l'entité bloquée
- __room__: instance de la pièce dans laquelle se trouve l'acteur.
- __direction__: direction de la porte bloquée.

### conversation
Déclenché lorsqu'une entité entend une phrase prononcée par une autre entité.
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: instance de l'entité qui réagit à la phrase.
- __speech__: contenu de la phrase prononcée
- __interactor__: instance de l'entité qui à prononcé la phrase
- __volume__: volume de la phrase prononcée (0 = signal silencieux, 1 = chuchotement, 2 = volume normal, 3 = cri, 4 = cri distant (d'une autre pièce))

### disturbed
Déclenché lorsque l'inventaire de l'entité est modifié (on ajoute ou on retire un objet du contenant).
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: entité dont l'inventaire est modifié
- __interactor__: identité de l'entité qui effectue l'action de modifiication de l'inventaire.
- __item__: identité de l'élément de l'inventaire qui a été déplacé
- __type__: 1: élément supprimé, 2: élément ajouté, 3: élément volé.

### heartbeat
Déclenché de manière régulière tous les rounds.
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: entité à laquelle s'applique l'évènement

### perception
Déclenché lorsque la perception d'une entité enver une autre change.
Permet de déterminer si une entité aperçois visuellement ou auditivement
une autre entité.
Permet aussi de déterminer si un entité A perd de vue une autre entité B, si B
fait usage d'invisibilité ou si B quitte la pièce.
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: id entité qui ressent une modification de perception
- __interactor__: autre entité qui __self__ percois, olù dont elle pert la perception.
- __heard__: si vrai alors l'interactor est repéré auditivement
- __seen__: si vrai alors l'interactor est repéré visuellement
- __inaudible__: si vrai alors l'interactor n'est plsu détecté auditivement
- __vanished__: si vrai alors l'interactor disparait aux yeux de l'observateur

### spawn
Déclenché au moment ou l'entité est créée et rejoind le jeu.
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: entité qui a spawné.

### despawn
Déclenché au moment où l'entité est détruite.
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: entité qui a déspawn.
- __location__: dernière localisation connue de l'entité qui a été détruite

### locked
Déclenché lorsqu'un contenant ou une porte est verrouillé.
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: identifiant du contenant.
- __interactor__: instance de l'entité qui a verrouillé _self_.

### openFailure
Déclenché lorsqu'un contenant ou une porte n'a pas pu être ouvert(e).

Pour déterminer si c'est une porte : Engine.getExit(self).valid

Pour déterminer si c'est une contenant : Engine.isContainer(self)

- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: identifiant du contenant ou de la porte qui n'a pas pu être ouverte.
- __interactor__: instance de l'acteur qui n'a pas réussi à ouvrir le contenant

### unlocked
Déclenché lorsqu'un contenant ou une porte initialement verrouillé(e) est déverrouillé(e).
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: identifiant du contenant ou de la porte deverrouillé(e).
- __interactor__: instance de l'entité ayant déverrouillé le contenant.

### enter
Déclenché lorsqu'une pièce accueille une nouvelle entité.
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: instance de la pièce
- __interactor__: instance de l'entité qui entre dans la pièce

### exit
Déclenché lorsqu'une entité quitte la pièce.
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: instance de la pièce
- __interactor__: instance de l'entité qui quitte dans la pièce

### acquired
Déclenché par un objet qui vien d'être acquis par un acteur.
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: instance de l'objet acquis
- __interactor__: instance de l'acteur qui a acquis l'objet

### dropped
Déclenché par un objet qui vien d'être abandonné par un acteur, au profit d'un autre.
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: instance de l'objet abandonné
- __interactor__: instance de l'acteur qui a abandonné l'objet

## Listes des évènements du système de combat et d'aventure

### think
Déclenché lorsqu'une créature NPC est impliquée dans un combat et qu'un tour
de combat vient de se terminer. Cet évènement lui permet de prendre des
décisions concernant l'utilisation de ses compétence de combat. 
Les combats sont effectivement diviser en tours afin de pouvoir appliquer les
règle du tour par tour dynamique (des tours automtiquement joué à intervalle
courts de 6 secondes)
- __engine__: instance du moteur du jeu
- __services__: plugins aditionels proposés par certains modules
- __self__: instance de l'acteur qui combat
- __interactor__: instance de l'adversaire

