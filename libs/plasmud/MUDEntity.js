const { deepClone} = require('@laboralphy/object-fusion');
const { checkType } = require('../get-type');

class MUDEntity {
    /**
     *
     * @param engine {Engine}
     * @param ref {string}
     * @param blueprint {MUDBlueprint}
     * @param id {string}
     * @param data {{}}
     * @param events {{}} event personnalisé lors de la création de la pièce
     */
    constructor ({
        engine,
        ref = '',
        blueprint = null,
        id,
        data = null,
        events = {}
    }) {
        // private & weakref
        this._engine = new WeakRef(engine);
        // readonly
        if (ref) {
            blueprint = engine.getBlueprint(ref);
        }
        this._id = id;
        this._ref = ref;
        this._blueprint = blueprint;
        this._data = data
            ? data
            : 'data' in blueprint
                ? deepClone(blueprint.data)
                : {};
        this._name = blueprint.name;
        this._desc = [...blueprint.desc];
        this._inventory = blueprint.inventory ? {} : null;
        this._environment = blueprint.environment ? blueprint.environment : null;
        // writeable
        this._tag = blueprint.tag;
        this._remark = '';
        this._location = null;
        this.events = events;
    }

    /**
     * @returns {string}
     */
    get id () {
        return this._id;
    }

    /**
     * @returns {string}
     */
    get ref () {
        return this._ref;
    }

    set events (oEvents) {
        this._events = {
            ...this._blueprint.events,
            ...oEvents
        };
    }

    get events () {
        return this._events;
    }

    /**
     * @returns {Engine}
     */
    get engine () {
        const engine = this._engine.deref();
        if (engine) {
            return engine;
        } else {
            throw new Error('Engine is no longer available');
        }
    }

    /**
     * @returns {MUDBlueprint}
     */
    get blueprint () {
        return this._blueprint;
    }

    get data () {
        return this._data;
    }

    get desc () {
        return this._desc;
    }

    get inventory () {
        return this._inventory;
    }

    get environment () {
        return this._environment;
    }

    // ▗▖ ▄     ▗▖  ▗▖         ▗▖   ▄▖
    // ▐▌▖█▐▛▜▖ ▄▖ ▝▜▛▘▗▛▜▖ ▀▜▖▐▙▄  ▐▌ ▗▛▜▖
    // ▐█▜█▐▌   ▐▌  ▐▌ ▐▛▀▘▗▛▜▌▐▌▐▌ ▐▌ ▐▛▀▘
    // ▝▘ ▀▝▘   ▀▀   ▀▘ ▀▀  ▀▀▘▝▀▀  ▀▀  ▀▀

    /**
     * @returns {string}
     */
    get name () {
        return this._name;
    }

    set name (value) {
        checkType(value, 'string');
        this._name = value;
    }

    get remark () {
        return this._remark;
    }

    set remark (value) {
        checkType(value, 'string');
        this._remark = value;
    }

    get location () {
        return this._location;
    }

    get tag () {
        return this._tag;
    }

    set tag (value) {
        checkType(value, 'string');
        this._tag = value;
    }

    get weight () {
        return 0;
    }

    // ▗▄▄▖         ▗▖
    // ▐▙▄ ▐▛▜▖▗▛▜▌ ▄▖ ▐▛▜▖▗▛▜▖    ▐▛▜▖▐▛▜▖▗▛▜▖▝▙▟▘▐▌▐▌
    // ▐▌  ▐▌▐▌▝▙▟▌ ▐▌ ▐▌▐▌▐▛▀▘    ▐▙▟▘▐▌  ▐▌▐▌ ▟▙ ▝▙▟▌
    // ▝▀▀▘▝▘▝▘▗▄▟▘ ▀▀ ▝▘▝▘ ▀▀     ▐▌  ▝▘   ▀▀ ▝▘▝▘▗▄▛

}

module.exports = MUDEntity;
