const path = require('path');
const os = require('os');
const fs = require('fs');

function hasWriteAccess (sDirectory) {
    try {
        fs.accessSync(sDirectory, fs.constants.R_OK | fs.constants.W_OK | fs.constants.X_OK);
        return true;
    } catch (e) {
        return false;
    }
}

function hasAccess (sDirectory) {
    try {
        fs.accessSync(sDirectory, fs.constants.R_OK | fs.constants.X_OK);
        return true;
    } catch (e) {
        return false;
    }
}

function fileExist (s) {
    try {
        fs.statSync(s);
        return true;
    } catch (e) {
        return false;
    }
}

function resolve (...aPaths) {
    const sHomeDir = os.homedir();
    const sCurrentDir = process.cwd();
    const sFolder = path.join(...aPaths);
    if (sFolder.length > 0) {
        switch (sFolder.charAt(0)) {
        case '~': {
            return path.resolve(sHomeDir, sFolder.substring(2));
        }

        case '.': {
            return path.resolve(sCurrentDir, sFolder);
        }

        default: {
            return path.resolve(sFolder);
        }
        }
    } else {
        return path.resolve(sCurrentDir);
    }
}

function main (...aPaths) {
    const sPath = resolve(...aPaths);
    return {
        path: sPath,
        read: hasAccess(sPath),
        write: hasWriteAccess(sPath)
    };
}

module.exports = main;
