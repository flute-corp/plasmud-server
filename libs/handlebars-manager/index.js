const path = require('path');
const Handlebars = require('handlebars');
const PromFS = require('../prom-fs');

class HandlebarsManager {
    constructor () {
        this._templates = {};
    }

    init () {
    }

    loadTemplate (sBasePath, filename) {
        return PromFS.read(path.resolve(sBasePath, filename)).then(sTemplate => {
            this.defineTemplate(filename, sTemplate);
        });
    }

    defineTemplate (filename, template) {
        try {
            const dir = path.dirname(filename);
            const name = path.basename(filename, '.hb');
            const id = path.posix.join(dir, name).replace(/\\/g, '/');
            this._templates[id] = Handlebars.compile(template);
        } catch (e) {
            console.error('error while parsing template file', filename);
            throw e;
        }
    }

    get count () {
        return Object.keys(this._templates).length;
    }

    hasTemplate (key) {
        return key in this._templates;
    }

    render (sKey, oVariables) {
        if (this.hasTemplate(sKey)) {
            return this._templates[sKey](oVariables, {
                partials: this._templates
            });
        } else {
            return sKey;
        }
    }
}

module.exports = HandlebarsManager;
