const aValidation = [
    /^[a-z][-a-z0-9]{2,}$/i,  // allowed characters
    /^[^-]/,  // no starting dash
    /[^-]$/,  // no ending dash
    /^(?!.*--).+$/,  // no double dash
    /^(?!false$)(?!true$)(?!null$)(?!undefined$).+$/
];

function isSafeName (sName) {
    return aValidation.every(r => !!sName.match(r));
}

module.exports = {
    isSafeName
};
