const { digObjectPath, getObjectPath, getObjectPathEntry } = require('./index');

describe('test object path', function () {
    describe('when object is { alpha: 1, beta: 2, gamma: { delta: 3, epsilon: [5, 6, 7], iota: null }', function () {
        const OBJ = {
            alpha: 1,
            beta: 2,
            gamma: {
                delta: 3,
                epsilon: [5, 6, 7],
                iota: null
            }
        };
        it('should return 1 when entry is alpha', function () {
            expect(getObjectPath(OBJ, 'alpha')).toBe(1);
        });
        it('should return 2 when entry is beta', function () {
            expect(getObjectPath(OBJ, 'beta')).toBe(2);
        });
        it('should return 3 when entry is gamma.delta', function () {
            expect(getObjectPath(OBJ, 'gamma.delta')).toBe(3);
        });
        it('should return [5, 6, 7] when entry is gamma.epsilon', function () {
            expect(getObjectPath(OBJ, 'gamma.epsilon')).toEqual([5, 6, 7]);
        });
        it('should return undefined when entry is not found', function () {
            expect(getObjectPath(OBJ, 'foo')).toBeUndefined();
        });
        it('should return undefined when entry is composed and not found', function () {
            expect(getObjectPath(OBJ, 'foo.bar')).toBeUndefined();
        });
        it('should return undefined when first entry is alpha and other entries are not found', function () {
            expect(getObjectPath(OBJ, 'alpha.foo.bar')).toBeUndefined();
        });
    });
});

describe('getObjectPathEntry', function () {
    describe('when object is { alpha: 1, beta: 2, gamma: { delta: 3, epsilon: [5, 6, 7], iota: null, kappa: { lambda: { gordon: freeman } }', function () {
        const OBJ = {
            alpha: 1,
            beta: 2,
            gamma: {
                delta: 3,
                epsilon: [5, 6, 7],
                iota: null,
                kappa: {
                    lambda: {
                        gordon: 'freeman'
                    }
                }
            }
        };
        it('should return OBJ, alpha', function () {
            expect(getObjectPathEntry(OBJ, 'alpha')).toEqual([OBJ, 'alpha']);
        });
        it('should return OBJ, beta', function () {
            expect(getObjectPathEntry(OBJ, 'beta')).toEqual([OBJ, 'beta']);
        });
        it('should return OBJ.gamma, delta', function () {
            expect(getObjectPathEntry(OBJ, 'gamma.delta')).toEqual([OBJ.gamma, 'delta']);
        });
        it('should return OBJ.gamma.kappa.lambda, gordon', function () {
            expect(getObjectPathEntry(OBJ, 'gamma.kappa.lambda.gordon')).toEqual([OBJ.gamma.kappa.lambda, 'gordon']);
        });
        it('should return null when asking for invalid key', function () {
            expect(getObjectPathEntry(OBJ, 'x')).toEqual([null, '']);
        });
        it('should return null when asking for invalid sub key', function () {
            expect(getObjectPathEntry(OBJ, 'gamma.x')).toEqual([null, '']);
        });
        it('should return null when asking for invalid key over scalar value', function () {
            expect(getObjectPathEntry(OBJ, 'alpha.x')).toEqual([null, '']);
        });
    });
});

describe('digObjectPath', function () {
    it('should return { alpha: 1 } when digging alpha, 1', function () {
        const o = {};
        digObjectPath(o, 'alpha', 1);
        expect(o).toEqual({ alpha: 1 });
    });
    it('should return { beta: { gamma: 2 } } when digging alpha, 1', function () {
        const o = {};
        digObjectPath(o, 'beta.gamma', 2);
        expect(o).toEqual({ beta: { gamma: 2 } });
    });
    it('should return { beta: { gamma: { delta : 3 }, epsilon: { iota: { kappa : 7 } } } } when digging alpha, 1', function () {
        const o = {};
        digObjectPath(o, 'beta.gamma.delta', 3);
        digObjectPath(o, 'beta.epsilon.iota.kappa', 7);
        expect(o).toEqual({ beta: { gamma: { delta: 3 }, epsilon: { iota: { kappa: 7 } } } });
    });
    it('should return error when diggin through number', function () {
        const o = { alpha: { beta: 7 }};
        expect(() => digObjectPath(o, 'alpha.beta.gamma', 3)).toThrow(new Error('cannot dig object path through non object property : alpha.beta[number]'));
    });
});
