const { getType } = require('../get-type');

function getObjectPathEntry (oObject, aProperties) {
    if (getType(aProperties) === 'string') {
        aProperties = aProperties.split('.');
    }
    while (getType(oObject) === 'object' && aProperties.length > 0) {
        const sEntry = aProperties.shift();
        if (aProperties.length === 0) {
            return (sEntry in oObject)
                ? [oObject, sEntry]
                : [null, ''];
        } else {
            oObject = oObject[sEntry];
        }
    }
    return [null, ''];
}

function digObjectPath (oObject, aProperties, value) {
    if (getType(aProperties) === 'string') {
        aProperties = aProperties.split('.');
    }
    const aDug = [];
    for (let i = 0, l = aProperties.length - 1; i < l; ++i) {
        const sEntry = aProperties[i];
        if (getType(oObject) === 'object') {
            aDug.push(sEntry);
            if (!(sEntry in oObject)) {
                oObject[sEntry] = {};
            }
        } else {
            throw new Error('cannot dig object path through non object property : ' + aDug.join('.') + '[' + getType(oObject) + ']');
        }
        oObject = oObject[sEntry];
    }
    if (getType(oObject) === 'object') {
        oObject[aProperties[aProperties.length - 1]] = value;
    } else {
        throw new Error('cannot dig object path through non object property : ' + aDug.join('.') + '[' + getType(oObject) + ']');
    }
}

function getObjectPath (oObject, aProperties) {
    const [o, s] = getObjectPathEntry(oObject, aProperties);
    return o ? o[s] : undefined;
}

module.exports = {
    getObjectPath, getObjectPathEntry, digObjectPath
};
