const BracketTokenizer = require('../bracket-tokenizer');
const c = require('./escape-codes');
const ansiParser = require('ansi-parser');
const ANSI_REVERSED_CODE = [
    '#000000', // 000
    '#800000',
    '#008000',
    '#808000',
    '#000080',
    '#800080',
    '#008080',
    '#c0c0c0',
    '#808080',
    '#ff0000',
    '#00ff00', // 010
    '#ffff00',
    '#0000ff',
    '#ff00ff',
    '#00ffff',
    '#ffffff',
    '#000000',
    '#00005f',
    '#000087',
    '#0000af',
    '#0000d7', // 020
    '#0000ff',
    '#005f00',
    '#005f5f',
    '#005f87',
    '#005faf',
    '#005fd7',
    '#005fff',
    '#008700',
    '#00875f',
    '#008787', // 030
    '#0087af',
    '#0087d7',
    '#0087ff',
    '#00af00',
    '#00af5f',
    '#00af87',
    '#00afaf',
    '#00afd7',
    '#00afff',
    '#00d700', // 040
    '#00d75f',
    '#00d787',
    '#00d7af',
    '#00d7d7',
    '#00d7ff',
    '#00ff00',
    '#00ff5f',
    '#00ff87',
    '#00ffaf',
    '#00ffd7', // 050
    '#00ffff',
    '#5f0000',
    '#5f005f',
    '#5f0087',
    '#5f00af',
    '#5f00d7',
    '#5f00ff',
    '#5f5f00',
    '#5f5f5f',
    '#5f5f87', // 060
    '#5f5faf',
    '#5f5fd7',
    '#5f5fff',
    '#5f8700',
    '#5f875f',
    '#5f8787',
    '#5f87af',
    '#5f87d7',
    '#5f87ff',
    '#5faf00', // 070
    '#5faf5f',
    '#5faf87',
    '#5fafaf',
    '#5fafd7',
    '#5fafff',
    '#5fd700',
    '#5fd75f',
    '#5fd787',
    '#5fd7af',
    '#5fd7d7', // 080
    '#5fd7ff',
    '#5fff00',
    '#5fff5f',
    '#5fff87',
    '#5fffaf',
    '#5fffd7',
    '#5fffff', // 087
    '#870000', // 088
    '#87005f', // 089
    '#870087', // 090
    '#8700af',
    '#8700d7',
    '#8700ff',
    '#875f00',
    '#875f5f',
    '#875f87',
    '#875faf',
    '#875fd7',
    '#875fff',
    '#878700', // 100
    '#87875f',
    '#878787',
    '#8787af',
    '#8787d7',
    '#8787ff',
    '#87af00',
    '#87af5f',
    '#87af87',
    '#87afaf',
    '#87afd7', // 110
    '#87afff',
    '#87d700',
    '#87d75f',
    '#87d787',
    '#87d7af',
    '#87d7d7',
    '#87d7ff',
    '#87ff00',
    '#87ff5f',
    '#87ff87', // 120
    '#87ffaf',
    '#87ffd7',
    '#87ffff',
    '#af0000',
    '#af005f',
    '#af0087',
    '#af00af',
    '#af00d7',
    '#af00ff',
    '#af5f00', // 130
    '#af5f5f',
    '#af5f87',
    '#af5faf',
    '#af5fd7',
    '#af5fff',
    '#af8700',
    '#af875f',
    '#af8787',
    '#af87af',
    '#af87d7', // 140
    '#af87ff',
    '#afaf00',
    '#afaf5f',
    '#afaf87',
    '#afafaf',
    '#afafd7',
    '#afafff',
    '#afd700',
    '#afd75f',
    '#afd787', // 150
    '#afd7af',
    '#afd7d7',
    '#afd7ff',
    '#afff00',
    '#afff5f',
    '#afff87',
    '#afffaf',
    '#afffd7',
    '#afffff',
    '#d70000', // 160
    '#d7005f',
    '#d70087',
    '#d700af',
    '#d700d7',
    '#d700ff',
    '#d75f00',
    '#d75f5f',
    '#d75f87',
    '#d75faf',
    '#d75fd7', // 170
    '#d75fff',
    '#d78700',
    '#d7875f',
    '#d78787',
    '#d787af',
    '#d787d7',
    '#d787ff',
    '#d7af00',
    '#d7af5f',
    '#d7af87', // 180
    '#d7afaf',
    '#d7afd7',
    '#d7afff',
    '#d7d700',
    '#d7d75f',
    '#d7d787',
    '#d7d7af',
    '#d7d7d7',
    '#d7d7ff',
    '#d7ff00', // 190
    '#d7ff5f',
    '#d7ff87',
    '#d7ffaf',
    '#d7ffd7',
    '#d7ffff',
    '#ff0000',
    '#ff005f',
    '#ff0087',
    '#ff00af',
    '#ff00d7', // 200
    '#ff00ff',
    '#ff5f00',
    '#ff5f5f',
    '#ff5f87',
    '#ff5faf',
    '#ff5fd7',
    '#ff5fff',
    '#ff8700',
    '#ff875f',
    '#ff8787', // 210
    '#ff87af',
    '#ff87d7',
    '#ff87ff',
    '#ffaf00',
    '#ffaf5f',
    '#ffaf87',
    '#ffafaf',
    '#ffafd7',
    '#ffafff',
    '#ffd700', // 220
    '#ffd75f',
    '#ffd787',
    '#ffd7af',
    '#ffd7d7',
    '#ffd7ff',
    '#ffff00',
    '#ffff5f',
    '#ffff87',
    '#ffffaf',
    '#ffffd7', // 230
    '#ffffff',
    '#080808',
    '#121212',
    '#1c1c1c',
    '#262626',
    '#303030',
    '#3a3a3a',
    '#444444',
    '#4e4e4e',
    '#585858', // 240
    '#626262',
    '#6c6c6c',
    '#767676',
    '#808080',
    '#8a8a8a',
    '#949494',
    '#9e9e9e',
    '#a8a8a8',
    '#b2b2b2',
    '#bcbcbc', // 250
    '#c6c6c6',
    '#d0d0d0',
    '#dadada',
    '#e4e4e4',
    '#eeeeee'
];


const R_STYLE_SPLIT = /(\u001b\[[;m0-9]+)/g;

const R_STYLE_PARSE = /^\u001b\[([34][89]);5;(\d+)m/;
const R_STYLE_COMP = /(\d+)[;m]/g;

const R_DOUBLE_RESET = /(\u001b\[0m){2,}/g;

class A256 {
    constructor () {
        this._bt = new BracketTokenizer();
    }

    get ansiCodes () {
        return c;
    }

    parseAnsiStyle (sStyle) {
        const aParsed = sStyle.match(R_STYLE_SPLIT);
        let fg = '';
        let bg = '';
        let attrs = [];
        if (aParsed) {
            aParsed.forEach(p => {
                const pm = p.match(R_STYLE_COMP);
                if (pm) {
                    const pm2 = pm.map(x => parseInt(x));
                    const prev = [0, 0, 0];
                    for (const n of pm2) {
                        const [n2, n1] = prev.slice(-2);
                        if (n1 === 5) {
                            // Cas des couleurs 256 bits
                            switch (n2) {
                            case 38: {
                                fg = n.toString();
                                break;
                            }
                            case 39: {
                                fg = '';
                                break;
                            }
                            case 48: {
                                bg = n.toString();
                                break;
                            }
                            case 49: {
                                bg = '';
                                break;
                            }
                            }
                        } else {
                            switch (n) {
                            case 0: {
                                // reset
                                fg = '';
                                bg = '';
                                attrs = [];
                                break;
                            }
                            case c.ST_BOLD: {
                                // bold
                                attrs.push('b');
                                break;
                            }
                            case c.ST_DIM: {
                                // bold
                                attrs.push('d');
                                break;
                            }
                            case c.ST_ITALIC: {
                                // italic
                                attrs.push('i');
                                break;
                            }
                            case c.ST_UNDERLINE: {
                                // underline
                                attrs.push('u');
                                break;
                            }
                            case c.ST_STRIKE: {
                                // strike
                                attrs.push('s');
                                break;
                            }
                            case c.ST_HIDDEN: {
                                // strike
                                attrs.push('h');
                                break;
                            }
                            case 30:
                            case 31:
                            case 32:
                            case 33:
                            case 34:
                            case 35:
                            case 36:
                            case 37: {
                                // fg color
                                fg = (n - 30).toString();
                                break;
                            }
                            case 40:
                            case 41:
                            case 42:
                            case 43:
                            case 44:
                            case 45:
                            case 46:
                            case 47: {
                                // bg color
                                bg = (n - 40).toString();
                                break;
                            }
                            }
                        }
                        prev.push(n);
                    }
                }
            });
        }
        return {
            fg,
            bg,
            attrs: attrs.sort((a, b) => a.localeCompare(b)).join('')
        };
    }

    parse (sAnsiString) {
        const a = ansiParser.parse(sAnsiString);
        const output = [];
        let sLastStyle = '';
        let sContent = '';
        const commitStyle = style => {
            const { fg, bg, attrs } = this.parseAnsiStyle(sLastStyle);
            const fghex = fg ? ANSI_REVERSED_CODE[parseInt(fg)] : '';
            const bghex = bg ? ANSI_REVERSED_CODE[parseInt(bg)] : '';
            output.push({ fg: fghex, bg: bghex, text: sContent, attrs });
            sLastStyle = style;
            sContent = '';
        };
        a.forEach(({ style, content }) => {
            if (style !== sLastStyle) {
                commitStyle(style);
            }
            sContent += content;
        });
        commitStyle('');
        return output;
    }

    render (sString) {
        const aTokens = this._bt.tokenize(sString);
        if (aTokens.length === 1) {
            return aTokens[0].text;
        }
        const f = [], b = [];

        const pushFGColor = aCodes => {
            if (f.length > 0) {
                const flast = f[f.length - 1];
                aCodes.push(38, 5, c.getAnsi256Code('#' + flast));
            } else {
                aCodes.push(0);
            }
        };

        const pushBGColor = aCodes => {
            if (b.length > 0) {
                const blast = b[b.length - 1];
                aCodes.push(48, 5, c.getAnsi256Code('#' + blast));
            } else {
                aCodes.push(0);
            }
        };

        const oLets = {
            bold: false,
            italic: false,
            underline: false,
            strike: false,
            dim: false,
            hidden: false,
            blink: false
        };

        return (aTokens.map(({ tokens, text }) => {
            const aAnsiCodes = [];
            if ('#' in tokens) {
                const aColorCodes = tokens['#'];
                const sMask = aColorCodes.map(cc => cc === '' ? '.' : 'c').join('');
                switch (sMask) {
                case '.': {
                    // exemple : [#]
                    // on veut réinitialiser la couleur du texte
                    // Déplier la couleur f et lire celle d'en dessous
                    f.pop();
                    pushFGColor(aAnsiCodes);
                    break;
                }

                case '..': {
                    // exemple : [##]
                    // on veut réinitialiser la couleur du fond
                    b.pop();
                    pushBGColor(aAnsiCodes);
                    break;
                }

                case '...': {
                    // exemple : [###]
                    // on veut réinitialiser la couleur du fond et celle du texte
                    f.pop();
                    b.pop();
                    pushFGColor(aAnsiCodes);
                    pushBGColor(aAnsiCodes);
                    break;
                }

                case 'c': {
                    // exemple : [#123]
                    // on veut définir la couleur du texte
                    f.push(aColorCodes[0]);
                    pushFGColor(aAnsiCodes);
                    break;
                }

                case 'cc': {
                    // exemple : [#123#456]
                    // on veut définir la couleur du fond et celle du texte
                    f.push(aColorCodes[0]);
                    b.push(aColorCodes[1]);
                    pushFGColor(aAnsiCodes);
                    pushBGColor(aAnsiCodes);
                    break;
                }

                case '.c': {
                    // exemple : [##456]
                    // on veut définir la couleur du fond
                    b.push(aColorCodes[1]);
                    pushBGColor(aAnsiCodes);
                    break;
                }

                case 'c.': {
                    // exemple : [#123#]
                    // on veut définir la couleur du texte et réinitialiser le fond
                    b.pop();
                    f.push(aColorCodes[0]);
                    pushFGColor(aAnsiCodes);
                    pushBGColor(aAnsiCodes);
                    break;
                }
                }
            }
            if (':' in tokens) {
                const t = new Set(tokens[':']);
                if (t.size === 1 && t.has('')) {
                    if (oLets.bold) {
                        oLets.bold = false;
                        aAnsiCodes.push(c.ST_NORMAL_INTENSITY);
                    }
                    if (oLets.italic) {
                        oLets.italic = false;
                        aAnsiCodes.push(c.ST_ITALIC_OFF);
                    }
                    if (oLets.underline) {
                        oLets.underline = false;
                        aAnsiCodes.push(c.ST_UNDERLINE_OFF);
                    }
                    if (oLets.dim) {
                        oLets.dim = false;
                        aAnsiCodes.push(c.ST_NORMAL_INTENSITY);
                    }
                    if (oLets.blink) {
                        oLets.blink = false;
                        aAnsiCodes.push(c.ST_BLINK_OFF);
                    }
                    if (oLets.hidden) {
                        oLets.hidden = false;
                        aAnsiCodes.push(c.ST_REVEAL);
                    }
                    if (oLets.strike) {
                        oLets.strike = false;
                        aAnsiCodes.push(c.ST_STRIKE_OFF);
                    }
                }
                if (t.has('b')) {
                    aAnsiCodes.push(c.ST_BOLD);
                    oLets.bold = true;
                }
                if (t.has('i')) {
                    aAnsiCodes.push(c.ST_ITALIC);
                    oLets.italic = true;
                }
                if (t.has('u')) {
                    aAnsiCodes.push(c.ST_UNDERLINE);
                    oLets.underline = true;
                }
                if (t.has('s')) {
                    aAnsiCodes.push(c.ST_STRIKE);
                    oLets.strike = true;
                }
                if (t.has('h')) {
                    aAnsiCodes.push(c.ST_HIDDEN);
                    oLets.hidden = true;
                }
                if (t.has('x')) {
                    aAnsiCodes.push(c.ST_BLINK);
                    oLets.blink = true;
                }
                if (t.has('d')) {
                    aAnsiCodes.push(c.ST_DIM);
                    oLets.dim = true;
                }
            }
            const sOutput = aAnsiCodes.length > 0
                ? c.CSI + aAnsiCodes.join(';') + 'm'
                : '';
            return sOutput + text;
        }).join('')).replace(R_DOUBLE_RESET, '\u001b[0m');
    }
}

module.exports = A256;
