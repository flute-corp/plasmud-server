const { getType, checkType } = require('../get-type');

class JsonMutationManager {
    constructor (data = {}) {
        this._data = {};
        this.data = data;
        this._index = null;
        this._separator = '.';
    }

    set data (data) {
        checkType(data, 'object');
        this._data = data;
    }

    get data () {
        return this._data;
    }

    /**
     * Returns a list of objet containing the given property
     * @param oObject {object}
     * @param sProperty {string}
     * @return {object[]}
     */
    searchProperty (sProperty, oObject) {
        const aResult = [];
        checkType(oObject, 'object');
        if (sProperty in oObject) {
            aResult.push(oObject);
        }
        for (const sKey in oObject) {
            const oSubObject = oObject[sKey];
            if (getType(oSubObject) === 'object') {
                aResult.push(...this.searchProperty(sProperty, oSubObject));
            }
        }
        return aResult;
    }

    concatKeys(sKey1, sKey2) {
        if (sKey1 === '') {
            return sKey2;
        }
        return sKey1 + this._separator + sKey2;
    }

    indexProperties (oObject, sPrefix = '', aRecurseWatcher = []) {
        let aResult = {};
        checkType(oObject, 'object');
        if (aRecurseWatcher.includes(oObject)) {
            throw new Error('ERR_RECURSION');
        }
        aRecurseWatcher.push(oObject);
        for (const sKey in oObject) {
            const sNewKey = this.concatKeys(sPrefix, sKey);
            const oSubObject = oObject[sKey];
            if (getType(oSubObject) === 'object') {
                aResult = {
                    ...aResult,
                    ...this.indexProperties(oSubObject, sNewKey, aRecurseWatcher)
                };
            } else {
                aResult[sNewKey] = oObject;
            }
        }
        aRecurseWatcher.pop();
        return aResult;
    }

    compareKeys (sKeyFull, sKeyPartial) {
        if (sKeyFull === sKeyPartial) {
            return true;
        }
        if (sKeyFull === '' || sKeyPartial === '') {
            return false;
        }
        const nFragLength = sKeyFull.length - sKeyPartial.length;
        return sKeyFull.endsWith(sKeyPartial) &&
            sKeyFull.substring(nFragLength - this._separator.length, nFragLength) === this._separator;
    }

    changeProperty (sProperty, value) {
        if (this._index === null) {
            this._index = this.indexProperties(this.data);
        }
        const aBranches = Object
            .entries(this._index)
            .filter(([key]) => this.compareKeys(key, sProperty));
        switch (aBranches.length) {
        case 0: {
            throw new Error('ERR_KEY_NOT_FOUND: ' + sProperty);
        }

        case 1: {
            const sLastKeyPart = sProperty.split(this._separator).pop();
            aBranches[0][1][sLastKeyPart] = value;
            break;
        }

        default: {
            throw new Error('ERR_AMBIGUOUS_KEY: ' + sProperty);
        }
        }
    }
}

module.exports = JsonMutationManager;
