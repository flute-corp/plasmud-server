const JsonMutationManager = require('./index');

describe('constructor', function () {
    it('should not throw error when constructing instance', function () {
        expect(() => new JsonMutationManager()).not.toThrow();
    });
});

describe('set data', () => {
    it('should throw an error when assigning non pure-object', function () {
        expect(() => new JsonMutationManager(1)).toThrow(new TypeError('Type mismatch - expected type : object - given type : number'));
        expect(() => new JsonMutationManager('1')).toThrow(new TypeError('Type mismatch - expected type : object - given type : string'));
        expect(() => new JsonMutationManager(new Date())).toThrow(new TypeError('Type mismatch - expected type : object - given type : date'));
        expect(() => new JsonMutationManager([1, 2, 3])).toThrow(new TypeError('Type mismatch - expected type : object - given type : array'));
        expect(() => new JsonMutationManager(new Set())).toThrow(new TypeError('Type mismatch - expected type : object - given type : set'));
        expect(() => new JsonMutationManager(null)).toThrow(new TypeError('Type mismatch - expected type : object - given type : null'));
    });

    it('should not throw an error when assigning pure-object', function () {
        expect(() => new JsonMutationManager(undefined)).not.toThrow();
        expect(() => new JsonMutationManager({})).not.toThrow();
        expect(() => new JsonMutationManager({ alpha: 1 })).not.toThrow();
    });
});

describe('get data', function () {
    it('should return the exact same instance when setting data', function () {
        const a = {};
        const jmm = new JsonMutationManager(a);
        expect(a).toBe(jmm.data);
    });
});

describe('searchProperty', function () {
    it('should return "jmm.data" when searching alpha in {alpha: 1}', function () {
        const jmm = new JsonMutationManager({ alpha: 1 });
        const r = jmm.searchProperty('alpha', jmm.data);
        expect(jmm.data).toEqual({alpha: 1});
        expect('alpha' in jmm.data).toBeTruthy();
        expect(r).toHaveLength(1);
        expect(r[0]).toBe(jmm.data);
    });

    it('should return jmm.data.beta when searching gamma in {alpha: 1, beta: { gamma: 2 }}', function () {
        const jmm = new JsonMutationManager({alpha: 1, beta: { gamma: 2 }});
        expect(jmm.searchProperty('gamma', jmm.data))
            .toEqual([jmm.data.beta]);
    });

    it('should return jmm.data.beta and jmm.data.delta when searching gamma in {alpha: 1, beta: { gamma: 2 }, delta: {gamma: 3 }}', function () {
        const jmm = new JsonMutationManager({alpha: 1, beta: { gamma: 2 }, delta: {gamma: 3 }});
        const r = jmm.searchProperty('gamma', jmm.data);
        expect(r).toHaveLength(2);
        expect(r[0]).toBe(jmm.data.beta);
        expect(r[1]).toBe(jmm.data.delta);
    });

    it('should return jmm.data.beta.epsilon and jmm.data.delta.iota when searching gamma in {alpha: 1, beta: { epsilon: { gamma: 2} }, delta: { iota: { gamma: 3} }}', function () {
        const jmm = new JsonMutationManager({alpha: 1, beta: { epsilon: { gamma: 2} }, delta: { iota: { gamma: 3} }});
        const r = jmm.searchProperty('gamma', jmm.data);
        expect(r).toHaveLength(2);
        expect(r[0]).toBe(jmm.data.beta.epsilon);
        expect(r[1]).toBe(jmm.data.delta.iota);
    });
});

describe('indexProperties', function () {
    it('should return { alpha: data } when indexing { alpha: 1 }', function () {
        const j = new JsonMutationManager({ alpha: 1 });
        const r = j.indexProperties(j.data);
        expect(r).toEqual({
            alpha: j.data
        });
    });
    it('should return { alpha: data, beta:data } when indexing { alpha: 1; beta: 2 }', function () {
        const j = new JsonMutationManager({ alpha: 1, beta: 2 });
        const r = j.indexProperties(j.data);
        expect(r).toEqual({
            alpha: j.data,
            beta: j.data
        });
    });
    it('should return { alpha: data, beta.gamma: data.beta, when indexing { alpha: 1, beta: { gamma: 2 }}', function () {
        const j = new JsonMutationManager({ alpha: 1, beta: { gamma: 2 }});
        const r = j.indexProperties(j.data);
        expect(r).toEqual({
            'alpha': j.data,
            'beta.gamma': j.data.beta
        });
    });
    it('should throw error when object has recursivity', function () {
        const x1 = { alpha: 1 };
        const x2 = { gamma: x1 };
        x1.beta = x2;
        // x1 = { alpha: 1, beta: { gamma: { alpha: 1, beta: { gamma: { alpha: 1, beta: { gamma: ....
        const j = new JsonMutationManager(x1);
        expect(() => j.indexProperties(j.data)).toThrow(new Error('ERR_RECURSION'));
    });
    it('should not throw error when object has non recursive references', function () {
        const x1 = { alpha: 1 };
        const x2 = { gamma: x1 };
        const x0 = {
            delta: { x1, x2 },
            epsilon: { x1, x2}
        };
        const j = new JsonMutationManager(x0);
        expect(() => j.indexProperties(j.data)).not.toThrow(new Error('ERR_RECURSION'));
        const r = j.indexProperties(j.data);
        expect(Object.keys(r)).toEqual([
            'delta.x1.alpha',
            'delta.x2.gamma.alpha',
            'epsilon.x1.alpha',
            'epsilon.x2.gamma.alpha'
        ]);
    });
});

describe('compareKeys', function () {
    it('should return true when keys are identical', function () {
        const j = new JsonMutationManager({});
        expect(j.compareKeys('a', 'a')).toBeTruthy();
        expect(j.compareKeys('a', '')).toBeFalsy();
        expect(j.compareKeys('', 'a')).toBeFalsy();
        expect(j.compareKeys('', '')).toBeTruthy();
    });
    it('should return true when partial keys is at end of full key', function () {
        const j = new JsonMutationManager({});
        expect(j.compareKeys('abc.defgh.thuil', 'thuil')).toBeTruthy();
        expect(j.compareKeys('abc.defgh.thuil', 'defgh.thuil')).toBeTruthy();
    });
    it('should return false when partial keys got incomplete key', function () {
        const j = new JsonMutationManager({});
        expect(j.compareKeys('abc.defgh.thuil', 'gh.thuil')).toBeFalsy();
        expect(j.compareKeys('abc.defgh.thuil', 'efgh.thuil')).toBeFalsy();
        expect(j.compareKeys('abc.defgh.thuil', 'uil')).toBeFalsy();
        expect(j.compareKeys('abc.defgh.thuil', 'huil')).toBeFalsy();
    });
});

describe('changeProperty', function () {
    it('should change X134 property', function () {
        const j = new JsonMutationManager({
            X1: {
                X11: {
                    X111: 542,
                    X112: 96456,
                    X113: 8794,
                    X114: 4523
                },
                X12: {
                    X121: 456
                },
                X13: {
                    X131: 485648,
                    X132: {
                        X1321: 78915,
                        X1322: 43213
                    },
                    X134: 0
                },
            },
            X2: {
                X21: {
                    X211: 456,
                    X222: 48942
                }
            }
        });
        expect(j.data.X1.X13.X134).toBe(0);
        j.changeProperty('X134', 666);
        expect(j.data.X1.X13.X134).toBe(666);
    });
    it('should determine right property to change when having several same property names', function () {
        const j = new JsonMutationManager({
            alpha: {
                beta: 0,
                gamma: 0,
            },
            beta: {
                beta: 0,
                gamma: 0,
                delta: 0,
            }
            ,
            gamma: {
                beta: 0,
                gamma: 0,
            }
        });
        j.changeProperty('alpha.beta', 1);
        j.changeProperty('alpha.gamma', 2);
        j.changeProperty('beta.beta', 3);
        j.changeProperty('beta.gamma', 4);
        j.changeProperty('gamma.beta', 5);
        j.changeProperty('gamma.gamma', 6);
        j.changeProperty('delta', 7);
        expect(j.data).toEqual({
            alpha: {
                beta: 1,
                gamma: 2
            },
            beta: {
                beta: 3,
                gamma: 4,
                delta: 7
            },
            gamma: {
                beta: 5,
                gamma: 6
            }
        });
    });
});
