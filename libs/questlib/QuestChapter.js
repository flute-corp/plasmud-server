const CONSTS = require('./consts');
const HandleBars = require('handlebars');

class QuestChapter {
    constructor ({ ref = '', title = '', text, watchers, status = CONSTS.QUEST_STATUS.ACCEPTED }) {
        this._ref = ref;
        /**
     * titre du chapitre
     * @type {string}
     * @private
     */
        this._title = title;
        this._text = Array.isArray(text) ? text.join('\n') : text;
        this._template = HandleBars.compile(this._text);
        this._watchers = watchers;
        this._status = status;
    }

    render (oVariable) {
        return this._template(oVariable);
    }

    get title () {
        return this._title;
    }

    get text () {
        return this._text;
    }

    get ref () {
        return this._ref;
    }

    get status () {
        return this._status;
    }

    setWatcher (sEvent, sScript) {
        this._watchers[sEvent] = sScript;
    }

    get watchers () {
        return this._watchers;
    }
}

module.exports = QuestChapter;
