const { deepClone } = require('@laboralphy/object-fusion');
const QuestChapter = require('./QuestChapter');
const { validate } = require('./validator');

class QuestBlueprint {
    constructor (oDef) {
    /**
     * Identifiant de la quête
     * @type {string}
     * @private
     */
        this._id = '';
        /**
     * Titre de la quête
     * @type {string}
     * @private
     */
        this._title = '';
        /**
     * objet state
     * @type {{}}
     * @private
     */
        this._state = {};
        /**
     * Liste des chapitres
     * @type {QuestChapter[]}
     * @private
     */
        this._chapters = [];

        this._data = {}; // données supplémentaires (zone géo, dates, difficulté, etc...)

        this._chapterIndices = {};

        if (oDef) {
            this.load(oDef);
        }
    }

    get data () {
        return this._data;
    }

    get title () {
        return this._title;
    }

    get chapterIndices () {
        return this._chapterIndices;
    }

    /**
   * @typedef QuestChapterDefinition {object}
   * @property ref {string} reference du chapitre
   * @property text {string} contenu du chapitre (texte ou strref)
   * @property watchers {Object.<string, string>} registre des watchers
   *
   * @typedef QuestDefinition {object}
   * @property id {string} identifiant de la quete
   * @property state {object} état initial de la quete
   * @property chapters {QuestChapterDefinition[]}
   *
   * @param oDefinition
   */
    load (oDefinition) {
        validate(oDefinition);
        this._id = oDefinition.id;
        this._title = oDefinition.title || '';
        this._state = 'state' in oDefinition ? deepClone(oDefinition.state) : {};
        this._chapters = oDefinition.chapters.map(cd => new QuestChapter(cd));
        this._chapterIndices = this.getChapterIndices();
        this._data = oDefinition.data || {};
    }

    getStateClone () {
        return deepClone(this._state);
    }

    /**
   * Identifiant de la quete
   * @returns {string}
   */
    get id () {
        return this._id;
    }

    /**
   * Liste des chapitre composant la quete
   * @returns {QuestChapter[]}
   */
    get chapters () {
        return this._chapters;
    }

    getChapterStatus () {
        return this.chapters.map((qc, i) => ({ index: i, status: qc.status }));
    }

    getChapterIndices () {
        const oRegistry = {};
        this._chapters.forEach((c, i) => {
            if (c.ref) {
                oRegistry[c.ref] = i;
            }
        });
        return oRegistry;
    }
}

module.exports = QuestBlueprint;
