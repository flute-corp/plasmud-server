const EventEmitter = require('events');
const QuestBlueprint = require('./QuestBlueprint');

/**
 * @typedef QuestProgress {object}
 * @property id {string}
 * @property chapter {number}
 * @property state {object}
 * @property date {number}
 */

class QuestManager {
    constructor () {
    /**
     * @type {Object.<string, QuestBlueprint>}
     * @private
     */
        this._quests = {};
        this._events = new EventEmitter();
    }

    get events () {
        return this._events;
    }

    get quests () {
        return this._quests;
    }

    /**
   * definition et enregistrment d'une nouvelle quête
   * @param oQuestDefinition {QuestDefinition}
   */
    defineQuest (oQuestDefinition) {
        this._quests[oQuestDefinition.id] = new QuestBlueprint(oQuestDefinition);
    }

    /**
   *
   * Ajoute une quest progress à un joueur
   * @param oPlayer {MUDEntity}
   * @param idQuest {string}
   */
    addQuest (oPlayer, idQuest) {
        const qbp = this._quests[idQuest];
        const qp = this.getEntityQuestProgresses(oPlayer);
        qp[idQuest] = {
            id: idQuest,
            chapter: 0,
            state: qbp.getStateClone(),
            date: 0
        };
        this._events.emit('quest.accepted', { player: oPlayer, quest: idQuest });
    }

    /**
   * Réagit a un évènement item.acquire de l'engine pour lancer les scripts de quête abonnés à cet évènement.
   * @param oPlayer {MUDEntity} entité de joueur
   * @param oItem {MUDEntity} objet acquis
   * @param nStack {number} nombre d'exemplaire de l'item (si stackable)
   */
    eventItemAcquired (oPlayer, oItem, nStack = 1) {
    // L'entité a-t-elle une quête ou il faut acquérir un objet ?
        const aQuestProgresses = Object.values(this.getEntityQuestProgresses(oPlayer));
        aQuestProgresses.forEach(qp => {
            const qbp = this._quests[qp.id];
            const qch = qbp.chapters[qp.chapter];
            const watchers = qch.watchers;
            if ('item.acquire' in watchers) {
                this._events.emit('quest.script', {
                    player: oPlayer,
                    parameters: {
                        item: oItem,
                        stack: nStack
                    },
                    script: watchers['item.acquire'],
                    state: this.getEntityQuestProgress(oPlayer, qp.id).state,
                    advance: ref => this.advanceEntityQuest(oPlayer, qbp.id, ref)
                });
            }
        });
    }

    /**
   * Renvoie la liste des quetes associées au joueur spécifié
   * @param oPlayer {MUDEntity}
   * @returns {QuestReport[]}
   */
    getEntityQuestReportList (oPlayer) {
        return Object.values(this.getEntityQuestProgresses(oPlayer)).map(qp => this.getEntityQuestReport(oPlayer, qp.id));
    }

    /**
   * @typedef QuestReportChapter {object}
   * @property title {string}
   * @property text {string}
   *
   * @typedef QuestReport {object}
   * @property qid {string}
   * @property title {string}
   * @property chapter {QuestReportChapter}
   * @property state {object}
   * @property status {string}
   * @property data {Object.<string, *>}
   *
   * @param oPlayer
   * @param idQuest
   * @returns {QuestReport|null}
   */
    getEntityQuestReport (oPlayer, idQuest) {
        const qp = this.getEntityQuestProgress(oPlayer, idQuest);
        if (!qp) {
            return null;
        }
        const qbp = this._quests[qp.id];
        const qch = qbp.chapters[qp.chapter];
        return {
            qid: qp.id,
            title: qbp.title,
            chapter: {
                title: qch.title,
                text: qch.render(qp.state)
            },
            state: qp.state,
            status: qch.status,
            date: qp.date
        };
    }

    /**
   * Renvoie le chapitre courant d'une quête prise par un joueur
   * @param oPlayer {MUDEntity} entité de joueur
   * @param idQuest {string} identifiant de la quête
   * @returns {QuestChapter} Structure de chapitre de quête
   */
    getEntityQuestCurrentChapter (oPlayer, idQuest) {
        const qp = this.getEntityQuestProgress(oPlayer, idQuest);
        const idChapter = qp.chapter;
        const oQuestBlueprint = this._quests[idQuest];
        return oQuestBlueprint.chapters[idChapter];
    }

    /**
   * Renvoie la progression d'une quête
   * @param oEntity {MUDEntity}
   * @param idQuest {string}
   * @returns {QuestProgress}
   */
    getEntityQuestProgress (oEntity, idQuest) {
        return this.getEntityQuestProgresses(oEntity)[idQuest];
    }

    /**
   * Renvoie le registre des progressions de quêtes
   * @param oEntity {MUDEntity}
   * @returns {Object.<string, QuestProgress>}
   */
    getEntityQuestProgresses (oEntity) {
        if (!('quests' in oEntity.data)) {
            oEntity.data.quests = {};
        }
        return oEntity.data.quests;
    }

    advanceEntityQuest (oEntity, idQuest, ref = undefined) {
        const qp = this.getEntityQuestProgress(oEntity, idQuest);
        const qbp = this._quests[idQuest];
        if (ref) {
            if (ref in qbp.chapterIndices) {
                qp.chapter = qbp.chapterIndices[ref];
            } else {
                throw new RangeError('This reference is not in quest data : "' + ref + '"');
            }
        } else if ((qp.chapter + 1) < qbp.chapters.length) {
            ++qp.chapter;
        }
        qp.date = 0;
        const qch = this.getEntityQuestCurrentChapter(oEntity, idQuest);
        this._events.emit('quest.advance', { player: oEntity, quest: idQuest, chapter: qp.chapter, status: qch.status });
    }

    abortEntityQuest (oEntity, idQuest) {
        const qp = this.getEntityQuestProgress(oEntity, idQuest);
        if (qp) {
            this._events.emit('quest.abort', { player: oEntity, quest: idQuest });
            const qps = this.getEntityQuestProgresses(oEntity);
            delete qps[idQuest];
        }
    }
}

module.exports = QuestManager;
