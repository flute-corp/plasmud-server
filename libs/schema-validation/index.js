const { Validator } = require('jsonschema');
const v = new Validator();

function validate (d, s, { throwError = true, output = console.error } = {}) {
    const result = v.validate(d, s);
    if (result.valid) {
        return true;
    } else {
        if (output) {
            result
                .errors
                .map(err => err.stack)
                .forEach(output);
        }
        if (throwError) {
            throw new Error('ERR_SCHEMA_VALIDATION');
        }
        return false;
    }
}

module.exports = validate;
