const TextProducerInterface = require('./TextProducerInterface');

class TextProducerDummy extends TextProducerInterface {
    get count () {
        return 0;
    }

    /**
     * Permet de définir si le producer peut rendre du texte en synchrone
     */
    get isSync () {
        return true;
    }

    /**
     * Permet de définir si le producer peut rendre du texte en asynchrone
     */
    get isAsync () {
        return true;
    }

    /**
     * Définition d'une ressource identifiée.
     * @param sId {string}
     * @param oResource {string|object}
     * @return {*}
     */
    defineResource (sId, oResource) {
        return sId;
    }

    /**
     * Renvoie true si la ressource existe
     * @param sId {string}
     * @return {boolean}
     */
    isResourceDefined (sId) {
        return true;
    }

    /**
     * Définit un chemin de localisation des resources. La classe finale doit s'en servir pour chager les fichiers
     * situé dans ce dossier en tant que resources
     * @param sPath {string}
     */
    defineResourcePath (sPath) {
    }

    /**
     * Initialisation du text producer
     * @returns {Promise<never>}
     */
    init (config) {
        return Promise.resolve();
    }

    /**
     * Terminer l'initialisation du text producer
     * Cette méthode est appelée dans un contexte ou les ressources sont ajoutée petit à petit
     * lors de la phase d'initialisation (comme lorsqu'on emploi des module ou addon)
     * La methode doit renvoyée une promise qui lorsqu'elle est résolue
     * indique que toutes les ressources ajoutée on bienété chargées et sont immédiatement invoquable
     * @returns {Promise<never>}
     */
    initDone () {
        return Promise.resolve();
    }

    /**
     * Rendu synchrone d'un texte. Si le producteur ne gère pas le rendu synchrone, il doit renvoyer null
     * @param sId {string} identifiant du texte
     * @param oContext {object} contexte de completion du texte
     * @return {string|null}
     */
    renderSync (sId, oContext) {
        return sId + ' - ' + JSON.stringify(oContext);
    }

    /**
     * Rendu asynchrone du texte. Certains moteurs de templates font du rendu asynchrone car ils gère des
     * système de cache async.
     * @param sId {string}
     * @param oContext {object}
     * @returns {Promise<string>}
     */
    renderAsync (sId, oContext) {
        return Promise.resolve(this.renderSync(sId, oContext));
    }
}

module.exports = TextProducerDummy;
