const { deepMerge } = require('@laboralphy/object-fusion');
const path = require('path');
const promfs = require('../prom-fs');
const i18next = require('i18next');
const TextProducerInterface = require('./TextProducerInterface');
const { getType } = require('../get-type');
const A256 = require('../ansi-256-colors');
const a256 = new A256();

class TextProducerI18next extends TextProducerInterface {
    constructor () {
        super();
        this._resources = {};
        this._t = null;
        this._i18n = null;
        this._promfs = promfs;
        this._lang = '';
        this._styles = {};
        this._a256 = a256;
    }

    _getObjectPropertyCount (obj) {
        switch (getType(obj)) {
        case 'string': {
            return 1;
        }

        case 'object': {
            return this._getObjectPropertyCount(Object.values(obj));
        }

        case 'array': {
            return obj
                .reduce((prev, curr) => prev + this._getObjectPropertyCount(curr), 0);
        }
        }
    }

    get count () {
        return this._getObjectPropertyCount(this._resources);
    }

    get isSync () {
        return true;
    }

    get isAsync () {
        return true;
    }

    /**
     * Défini des ressources pour un langage donné
     * @param sLang {string} code langage (fr, en etc...)
     * @param oResource {*}
     */
    defineResource (sLang, oResource) {
        if (sLang in this._resources) {
            return deepMerge(this._resources[sLang], oResource);
        } else {
            return this._resources[sLang] = oResource;
        }
    }

    async defineResourcePath(sPath) {
        const aList = await this._promfs.tree(sPath);
        const aJSONList = aList.filter(s => s.endsWith('.json'));
        for (const xres of aJSONList) {
            const sContent = await this._promfs.read(path.join(sPath, xres));
            const oContent = JSON.parse(sContent.toString());
            this.defineResource(this._lang, oContent);
        }
    }

    /**
     * Renvoie true si l'identifiant spécifié correspond à une ressource définie
     * @param sId {string}
     * @returns {boolean}
     */
    isResourceDefined(sId) {
        if (!this._i18n) {
            return false;
        }
        if (typeof sId !== 'string') {
            throw new Error('TextProducer.isResourceDefined : Resource must be string - got ' + sId);
        }
        return this._i18n.exists(sId) ||
            this._i18n.exists(sId + '_one') ||
            this._i18n.exists(sId + '_other') ||
            this._i18n.exists(sId + '_female');
    }

    /**
     * Initialisation de i18n
     * @param config {object}
     * @returns {Promise}
     */
    async init (config = {}) {
        const { lang, strings = {}, styles } = config;
        this._styles = styles;
        this._lang = lang;
        this._resources = strings;
    }

    async initDone () {
        const resources = {};
        for (const [lang, res] of Object.entries(this._resources)) {
            resources[lang] = {
                translation: res
            };
        }
        const oI18n = i18next.createInstance();
        this._i18n = oI18n;
        this._t = await oI18n
            .init({
                lng: Object.keys(this._resources).shift(),
                debug: false,
                resources
            });
    }

    /**
     * @param sId {string}
     * @param oContext {object}
     * @returns {string}
     */
    renderSync(sId, oContext) {
        if (sId.indexOf('\u00a0') >= 0) {
            return this._a256.render(sId);
        }
        const sOutput = this._t(sId, {
            ...oContext,
            style: this._styles,
            interpolation: { escapeValue: false }
        });
        return this._a256.render(sOutput);
    }

    /**
     *
     * @param sId {string}
     * @param oContext {object}
     * @returns {Promise<string>}
     */
    renderAsync(sId, oContext) {
        return Promise.resolve(this.renderSync(sId, oContext));
    }
}

module.exports = TextProducerI18next;
