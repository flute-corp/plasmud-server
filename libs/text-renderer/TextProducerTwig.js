const TextProducerInterface = require('./TextProducerInterface');
const promfs = require('../prom-fs');
const path = require('path');
const TWIG = require('twig');
const TableRenderer = require('../table-renderer');
const { parseCSVLine } = require('../parse-csv');
const A256 = require('../ansi-256-colors');
const BT = require('../bracket-tokenizer');
const ICellString = require('../table-renderer/ICellString');
const Abbr = require('../abbr');

const a256 = new A256();
const bt = new BT();
const abbr = new Abbr();

class StylizedString extends ICellString {
    constructor (s) {
        super();
        this._text = s;
        this._outputText = a256.render(s);
        const tokens = bt.tokenize(s);
        this._printableText = tokens.reduce((prev, curr) => prev + curr.text, '');
        this._length = this._printableText.length;
    }

    get isStylized () {
        return this._printableText !== this._text;
    }

    toString () {
        return this._outputText;
    }

    get length () {
        return this._length;
    }
}

class TextProducerTwig extends TextProducerInterface {
    constructor() {
        super();
        this._ids = new Set();
        this._promfs = promfs;
        this._styles = {};
        this._a256 = a256;
        this._resetEachLine = false;
    }

    /**
     *
     * @returns {number}
     */
    get count () {
        return this._ids.size;
    }

    get isSync () {
        return true;
    }

    get isAsync () {
        return true;
    }

    /**
     *
     * @param filename {string}
     * @param sTemplate {string}
     * @return {{id: string, template}}
     */
    defineResource(filename, sTemplate) {
        sTemplate = sTemplate
            .trim()
            .split('\n')
            .map(s => s !== ' ' ? s.trim() : s)
            .filter(s => s !== '')
            .join('\n')
            .replace(/%} *\n/g, '%}\n\n');
        const dir = path.dirname(filename);
        const name = path.basename(filename, '.twig');
        const id = path.posix.join(dir, name).replace(/\\/g, '/');
        const template = TWIG.twig({
            id,
            allowInlineIncludes: true,
            data: sTemplate
        });
        if (!template.tokens) {
            throw new Error('twig template ' + template.id + ' could not be parsed');
        }
        this._ids.add(id);
        return {
            id,
            template
        };
    }

    isResourceDefined(sId) {
        return this._ids.has(sId);
    }

    async defineResourcePath(sPath) {
        const aList = await this._promfs.tree(sPath);
        const aTWIGList = aList
            .filter(s => s.endsWith('.twig'));
        for (const xres of aTWIGList) {
            const bufContent = await this._promfs.read(path.join(sPath, xres));
            const sContent = bufContent.toString();
            this.defineResource(xres, sContent);
        }
    }


    getFunctions () {
        return {
            attribute: function (oObject, sKey) {
                return sKey in oObject ? oObject[sKey] : '';
            },

            table: function (aTable, options = {}) {
                const tr = new TableRenderer();
                tr.theme = 'FILET_THIN';
                return tr.render(aTable, options).join('\n');
            }
        };
    }

    getFilters () {
        const STYLES = this._styles;
        return {
            /**
             * Padding à droite : la chaine de caractère est complétée par le caractère à droite
             * si la chaine du pipe est plus longue que la longueur finale spécifiée, la chaine d'origine est renvoyée non-modifiée
             * @param value {string} chaine du pipe
             * @param length {number} longueur finale voulue de la chaine
             * @param char {string} caractètre servant de remplissage
             * @returns {string} chaine à afficher
             */
            rpad: function (value, aParams) {
                const [length, char = ' '] = Array.isArray(aParams) ? aParams : [];
                return value.toString().replace(/ /g, ' ').padEnd(length, char);
            },

            /**
             * Padding à gauche : la chaine de caractère est complétée par le caractère à gauche
             * si la chaine du pipe est plus longue que la longueur finale spécifiée, la chaine d'origine est renvoyée non-modifiée
             * @param value {string} chaine du pipe
             * @param length {number} longueur finale voulue de la chaine
             * @param char {string} caractètre servant de remplissage
             * @returns {string} chaine à afficher
             */
            lpad: function (value, aParams) {
                const [length, char = ' '] = Array.isArray(aParams) ? aParams : [];
                return value.toString().replace(/ /g, ' ').padStart(length, char);
            },

            /**
             * Padding des deux cotés : la chaine de caractère est complétée par le caractère à droite et à gauche
             * si la chaine du pipe est plus longue que la longueur finale spécifiée, la chaine d'origine est renvoyée non-modifiée
             * @param value {string} chaine du pipe
             * @param length {number} longueur finale voulue de la chaine
             * @param char {string} caractètre servant de remplissage
             * @param aParams
             * @returns {string} chaine à afficher
             */
            pad: function (value, aParams) {
                const [length, char = ' '] = Array.isArray(aParams) ? aParams : [];
                const sInput = value.toString().replace(/ /g, ' ');
                const nLenInput = sInput.length;
                const nLenLeft = length - nLenInput;
                const nr = nLenLeft >> 1;
                const nl = nLenLeft - nr;
                const sr = char.toString().repeat(nr);
                const sl = char.toString().repeat(nl);
                return sl + sInput + sr;
            },

            /**
             * Remplace tous les espaces par des espaces insécables
             * @param value {string} chaine d'entrée
             * @returns {string} chaine de sortie
             */
            pre: function (value) {
                return value.toString().replace(/ /g, ' ');
            },

            /**
             * Ce filtre permet de transformer un fragment de template sur plusieurs lignes avec indentations,
             * en une seule ligne compacte. Cela permet de garder une certaine clarté dans la conception des templates
             * tout en s'assurant que les espace d'indentation ne soient pas visible dans le rendu final de la ligne.
             *
             * Remplace tous les sauts de ligne par des espaces, trim toutes les lignes avant concaténation
             * Supprimer tous les doubles espaces
             * @param value
             */
            packline: function (value) {
                return value
                    .toString()
                    .trim()
                    .split('\n')
                    .map(s => s.trimStart())
                    .join(' ')
                    .replace(/\s+/g, ' ');
            },

            /**
             * Abbreviationne une chaine si celle-ci dépasse un certain nombre de caractères
             * @param value {string}
             * @param aParams {string[]}
             * @returns {string}
             */
            abbr: function (value, aParams) {
                const [length = 0] = Array.isArray(aParams) ? aParams : [];
                return value.length > length
                    ? abbr.make(value)
                    : value;
            },

            /**
             * Compose un style ansi-bracket
             * Si la chaine est composée de plusieurs lignes, applique le style à chaque ligne.
             * @param value {string} texte à rendre
             * @param sStyle {string} code du style
             * @return {string}
             */
            style: function (value, sStyle) {
                if (!sStyle) {
                    return value;
                }
                const sx = sStyle in STYLES ? STYLES[sStyle] : '[' + sStyle + ']';
                const se = STYLES.END;
                return value
                    .toString()
                    .split('\n')
                    .map(s => {
                        const st = s.trim();
                        return st === '' ? '' : sx + st + se;
                    })
                    .join('\n');
            },

            /**
             * Fabrique une table à partir de donnée CSV
             * @param value {string}
             * @param options {object}
             */
            table: function (value, options) {
                const aTable = value
                    .trim()
                    .split('\n')
                    .map(s => parseCSVLine(s.trim())
                        .map(s => {
                            const ic = new StylizedString(s);
                            return ic.isStylized
                                ? ic
                                : s;
                        })
                    );
                const tr = new TableRenderer();
                tr.theme = 'COMPACT';
                return tr.render(aTable).join('\n');
            }
        };
    }

    /**
     *
     * @param config {{ resetEachLine: boolean, styles: {} }}
     */
    init (config) {
        if ('styles' in config) {
            this._styles = config.styles;
            if (!this._styles) {
                console.error(config);
                throw new Error('TextProducer : what the fuck are these styles ?');
            }
        } else {
            throw new Error('TextProducer : where are my styles ?');
        }
        this._resetEachLine = Boolean(config.resetEachLine);
        TWIG.extend(function(Twig) {
            // Déclarer la balise
            Twig.logic.extend({
                type: 'break',
                regex: /^break$/,
                next: [],
                open: true,
                compile: function(token) {
                    return token;
                },
                parse: function(token, context, chain) {
                    return {
                        chain: false,
                        output: ' \n', // Ici, on retourne un saut de ligne
                    };
                }
            });
        });
        const oFilters = this.getFilters();
        for (const [sFilter, fFilter] of Object.entries(oFilters)) {
            TWIG.extendFilter(sFilter, fFilter);
        }
        const oFunctions = this.getFunctions();
        for (const [sFunction, fFunction] of Object.entries(oFunctions)) {
            TWIG.extendFunction(sFunction, fFunction);
        }
    }

    initDone() {
        return Promise.resolve();
    }

    renderSync (id, locals) {
        const oTmpl = TWIG.twig({ ref: id });
        const sReset = this._resetEachLine ? this._a256.ansiCodes.RESET : '';
        if (oTmpl) {
            return oTmpl
                .render({
                    ...locals,
                    style: this._styles
                })
                .split('\n') // diviser par sauts de ligne
                .filter(s => s === '\u00A0' || s.trim() !== '') // ne prendre que les lignes non vides (sauf espace insécable)
                .map(s => s === ' ' ? '' : s)
                .map(s => sReset + this._a256.render(s)) // terminer chaque ligne par une fin de style
                .join('\n') // recoller les lignes
                .trimStart(); // virer les espaces et sauts de lignes initiaux
        } else {
            throw new Error('twig template not found : "' + id + '"');
        }
    }

    renderAsync(sId, oContext) {
        try {
            return Promise.resolve(this.renderSync(sId, oContext));
        } catch (e) {
            return Promise.reject(e);
        }
    }
}

module.exports = TextProducerTwig;
