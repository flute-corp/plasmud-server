class TextRenderer {
    constructor () {
        this._producers = new Map();
        this._styles = null;
    }

    get styles () {
        return this._styles;
    }

    set styles (value) {
        this._styles = value;
    }

    get producers () {
        return this._producers;
    }

    get count () {
        const c = {};
        for (const [sId, p] of this._producers) {
            c[sId] = p.count;
        }
        return c;
    }

    /**
     * @param id {string}
     * @param p {TextProducerInterface}
     */
    addProducer (id, p) {
        this._producers.set(id, p);
    }

    async init (config) {
        const styles = this.styles = config.styles || this.styles;
        if (!styles) {
            throw new Error('TextRenderer : my styles are not initialized !!!');
        }
        for (const [sId, p] of this._producers) {
            const cfg = config[sId] || {};
            await p.init({
                ...cfg,
                styles
            });
        }
    }

    async initDone () {
        for (const [sId, p] of this._producers) {
            await p.initDone();
        }
    }

    /**
     *
     * @param sId
     * @param oContext
     * @returns {string|null}
     */
    renderSync (sId, oContext) {
        for (const [, p] of this._producers) {
            if (p.isSync && p.isResourceDefined(sId)) {
                return p.renderSync(sId, oContext);
            }
        }
        return sId;
    }

    /**
     * rendu d'un texte asynchrone
     * @param sId {string} identifiant de la resource text
     * @param oContext {object} contexte
     * @returns {Promise<string>}
     */
    async renderAsync (sId, oContext) {
        for (const [, p] of this._producers) {
            if (p.isAsync && p.isResourceDefined(sId)) {
                return p.renderAsync(sId, oContext);
            }
        }
        return sId;
    }
}

module.exports = TextRenderer;
