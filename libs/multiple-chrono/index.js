const oChronos = {};
let iChrono = 0;

function getTime () {
    return Date.now();
}

function start () {
    const sChrono = (iChrono++).toString(36);
    oChronos[sChrono] = getTime();
    return sChrono;
}

function stop (sChrono) {
    if (sChrono in oChronos) {
        const t = getTime() - oChronos[sChrono];
        delete oChronos[sChrono];
        return t;
    } else {
        return 0;
    }
}

module.exports = {
    start,
    stop
};
