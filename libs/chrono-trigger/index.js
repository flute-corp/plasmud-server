/**
 * Effectue des mesures sur le temps
 * Permet d'echantilloner des intervalles de temps afin de mesurer le temps moyen de la durée de ces échantillons
 */
class ChronoTrigger {
    constructor () {
        this._stats = [];
        this._currStat = {};
        this._statCount = 20;
        this._timeFunc = function () {
            return ChronoTrigger._getTimeHRMS();
        };
    }

    /**
     * Défini le nombre d'échantillons max
     * @param value {number}
     */
    set statCount (value) {
        this._statCount = value;
    }

    /**
     * renvoie le nombre d'échantillons
     * @returns {number}
     */
    get statCount () {
        return this._statCount;
    }

    /**
     * Efface les stats
     */
    clearStats () {
        this._stats.splice(0, this._stats.length);
    }

    /**
     * Renvoie la liste des échantillons
     * @returns {[]}
     */
    get stats () {
        return this._stats;
    }

    /**
     * Permet de définir la fonction de mesure du temps
     * La fonction doit renvoyer un number (milliseconde)
     * @param value {function () : number}
     */
    set timeFunc (value) {
        if (typeof value === 'function') {
            this._timeFunc = value;
        } else {
            throw TypeError('function() : number expected');
        }
    }

    /**
     * Renvoie le nombre de milliseconde calculé par processHRTime
     * @returns {number}
     * @private
     */
    static _getTimeHRMS () {
        const hrTime = process.hrtime();
        return hrTime[0] * 1000 + hrTime[1] / 1000000;
    }

    /**
     * Renvoie le nombre de milliseconde calculé par Date.now
     * @returns {number}
     * @private
     */
    static _getTimeMS () {
        return Date.now();
    }

    /**
     * Commence un echantillonage.
     * Si aucun paramètre n'est spécifié, utilise la fonction timeFunc
     * @param [t] {number}
     */
    begin (t = undefined) {
        const a = this._currStat = {
            start: t === undefined ? this._timeFunc() : t,
            end: 0,
        };
    }

    /**
     * Termine un echantillonage.
     * Si aucun paramètre n'est spécifié, utilise la fonction timeFunc
     * @param [t] {number}
     */
    commit (t) {
        const a = this._currStat;
        const s = this._stats;
        a.end = t === undefined ? this._timeFunc() : t;
        s.push(a);
        const sc = this._statCount;
        while (s.length > sc) {
            s.shift();
        }
    }

    /**
     * Calcule la durée moyenne des echantillonages précédemment enregistré
     * avec begin et commit
     * @returns {number}
     */
    get time () {
        const s = this._stats;
        const l = s.length;
        return l > 0
            ? s.reduce((prev, { start, end }) => prev + end - start, 0) / l
            : 0;
    }
}

module.exports = ChronoTrigger;
