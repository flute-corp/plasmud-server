import Events from 'events';

const WS_READY_STATES = {
    NONE: -1,
    CONNECTING: 0,
    CONNECTED: 1,
    CLOSING: 2,
    CLOSED: 3
};

class WSConnector {
    constructor () {
        this._socket = null;
        this._events = new Events();
        this._path = '';
    }

    info (...args) {
        console.info(...args);
    }

    get path () {
        return this._path;
    }

    set path (value) {
        this._path = value;
    }

    get events () {
        return this._events;
    }

    get connected () {
        return !!this._socket && this._socket.readyState === 1;
    }

    get connecting () {
        return !!this._socket && this._socket.readyState === 0;
    }

    get remoteAddress () {
        const sSocketProtocol = (window.location.protocol === 'https:' ? 'wss:' : 'ws:');
        const sPath = this.path || '';
        return sSocketProtocol + '//' + window.location.hostname + ':' + window.location.port + sPath;
    }

    get socket () {
        return this._socket;
    }

    send (sMessage) {
        this._socket.send(sMessage);
    }

    connect (socket = null) {
        return new Promise((resolve, reject) => {
            const cnxErr = () => {
                reject(new Error('Could not establish connection to server.'));
            };

            // ne peut se connecter si déja connecté
            if (this.connected) {
                resolve(true);
                return;
            }

            this.info('Connecting to', this.remoteAddress);

            // tenter de se connecter
            if (!socket) {
                try {
                    socket = new WebSocket(this.remoteAddress);
                    // connexion réussie
                    socket.onopen = () => {
                        this.info('connected');
                        resolve(true);
                    };

                    socket.onclose = () => {
                        this.info('disconnected');
                        this._events.emit('disconnect');
                        this._socket.close();
                        this._socket = null;
                    };

                    socket.onerror = cnxErr;

                    socket.onmessage = message => {
                        this._events.emit('message', { content: message.data });
                    };
                } catch (e) {
                    // en cas d'erreur, rejeter la promesse
                    cnxErr();
                    return;
                }
            }

            // garder la référence de la socket
            this._socket = socket;
        });
    }
}

export default WSConnector;
