/**
 * Description : Bannissement d'un utilisateur
 * Syntaxe : user-ban <nom-d-utilisateur> <duration> <reason>
 */
const DURATION_VALIDATOR = /^(p|perm|permanent|forever)$|^([.0-9]+)(h|hr|hrs|hour|hours|min|d|day|days)$/i;

function convertMinToMillisecs (s) {
    return parseFloat(s) * 60 * 1000;
}

function convertHrsToMillisecs (s) {
    return parseFloat(s) * 60 * 60 * 1000;
}

function convertDayToMillisecs (s) {
    return parseFloat(s) * 24 * 60 * 60 * 1000;
}

function parseDurationMs (duration) {
    const [s, perm, dur, unit] = duration.match(DURATION_VALIDATOR);
    if (s !== undefined) {
        if (perm !== undefined) {
            return Infinity;
        }
        switch (unit.toLowerCase()) {
        case 'min':
            return convertMinToMillisecs(dur);

        case 'h':
        case 'hr':
        case 'hrs':
        case 'hour':
        case 'hours':
            return convertHrsToMillisecs(dur);

        case 'd':
        case 'day':
        case 'days':
            return convertDayToMillisecs(dur);
        }
    } else {
        return undefined;
    }
}

module.exports = { parseDurationMs };
