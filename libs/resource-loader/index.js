const path = require('path');
const PromFS = require('../prom-fs');

const CODE_EXTENSIONS = ['.js'];
const IGNORED_RESOURCE_PATTERN = /\.(test|spec)\.js$/;

/**
 * bibliothèque basée sur ARCHIVER
 * Extraction complete d'un ZIP, en mémoire
 * Produit un tableau d'entrées :
 * {
 *   path: string,
 *   content: string
 * }
 * possibilité d'évaluer le contenu des entrées en tant que texte brut, JSON, ou script JS
 */

class ResourceLoader {
    constructor () {
        this._resources = {};
    }

    get resources () {
        return this._resources;
    }

    /**
   * supprime la totalité des ressources enregistrées
   */
    clearResources () {
        this._resources = {};
    }

    /**
   * Ajoute une ressource
   * Selon le type de ressource, un traitement supplémentaire est effectué
   * (compilation, parsing etc...)
   * @param sEntryName {string} nom de fichier de la ressource (avec extension)
   * @param data {string}
   */
    addResourceEntry (sEntryName, data) {
        sEntryName = sEntryName.replace(/\\/g, '/');
        const sExt = path.extname(sEntryName);
        switch (sExt) {
        case '.json': {
            try {
                this._resources[sEntryName] = JSON.parse(data);
            } catch (e) {
                throw new Error('ERR_JSON_PARSE: resource "' + sEntryName + '" is invalid : ' + e.message);
            }
            break;
        }
        case '.js': {
        // if (sEntryName.startsWith('events')) {
        //   if (!(sEntryName in this._resources)) {
        //     this._resources[sEntryName] = []
        //   }
        //   this._resources[sEntryName].push(data)
        // } else {
        //   this._resources[sEntryName] = data
        // }
            this._resources[sEntryName] = data;
            break;
        }
        default: {
            this._resources[sEntryName] = data;
            break;
        }
        }
    }

    /**
   * Chargement de l'intégralité du fichier ZIP en mémoire
   * @param sFile {string} nom du fichier ZIP
   */
    loadFromArchive (sFile) {
        const zip = new AdmZip(sFile, {});
        for (const oZipEntry of zip.getEntries()) {
            if (!oZipEntry.isDirectory) {
                this.addResourceEntry(
                    oZipEntry.entryName,
                    oZipEntry.getData().toString()
                );
            }
        }
    }

    /**
   * Loads and returns a single resource
   * @param sFullName {string} full name resource location
   * @returns {Promise<function|string>}
   */
    async loadSpecificResource (sFullName) {
        if (CODE_EXTENSIONS.includes(path.extname(sFullName))) {
            return require(sFullName);
        } else {
            const data = await PromFS.read(sFullName);
            return data.toString();
        }
    }

    /**
   * Charge les ressources à partir d'un dossier
   * @param sPath {string}
   * @returns {Promise<Awaited<Function | string>[]>}
   */
    async loadFromFolder (sPath) {
        const aTree = await PromFS.tree(sPath);
        const aResources = aTree
            .filter(sFile => !sFile.match(IGNORED_RESOURCE_PATTERN))
            .map(sFile => this
                .loadSpecificResource(path.resolve(sPath, sFile))
                .then(r => {
                    this.addResourceEntry(sFile, r);
                    return r;
                })
            );
        return Promise.all(aResources);
    }

    /**
   * Chargement des ressources contenues dans un zip ou dans un dossier
   * @param sLocation
   * @returns {Promise<object>}
   */
    async load (sLocation) {
        const oStat = await PromFS.stat(sLocation);
        if (!oStat) {
            console.error('could not reach module location', sLocation);
            throw new Error('ERR_MODULE_NOT_FOUND: ' + sLocation);
        }
        if (oStat.dir) {
            await this.loadFromFolder(sLocation);
        } else {
            throw new Error('Zip files are no longer supported');
        }
        return Promise.resolve(this._resources);
    }
}

module.exports = ResourceLoader;
