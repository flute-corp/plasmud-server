const PromFS = require('../prom-fs');
const path = require('path');

const JAVASCRIPT_EXTENSION = '.js';
const JSON_EXTENSION = '.json';

/**
 * Charge tous les scripts contenu dans un dossier ainsi que ses sous dossiers
 * L'identifiant correspond au chemin de fichier relatif au chemin transmis en paramètre.
 * @param sBasePath {string} chemin de base à partir duquel on recharche les scripts
 * @returns {Promise<{}|FlatArray<Awaited<unknown>[], 1>[]>}
 */
async function main (sBasePath) {
    if (Array.isArray(sBasePath)) {
        return Promise
            .all(sBasePath.map(s => main(s)))
            .then(aAll => aAll.flat());
    }
    const t1 = await PromFS.tree(sBasePath);
    const t2prom = t1
        .filter(x => x.endsWith(JAVASCRIPT_EXTENSION) || x.endsWith(JSON_EXTENSION))
        .map(async x => {
            const filename = x;
            const dir = path.dirname(x);
            const oModule = await import(path.resolve(sBasePath, filename));
            const name = x.endsWith(JAVASCRIPT_EXTENSION)
                ? path.basename(filename, JAVASCRIPT_EXTENSION)
                : path.basename(filename, JSON_EXTENSION);
            const id = path.posix.join(dir, name);
            return {
                id,
                module: oModule.default
            };
        });
    const t2 = await Promise.all(t2prom);
    const t3 = {};
    t2.forEach(({ id, route, module: m }) => {
        t3[id] = m;
    });
    return t3;
}

module.exports = main;
