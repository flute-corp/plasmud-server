/**
 * @author Copilot
 * @param line {string}
 * @returns {string[]}
 */
function parseCSVLine(line) {
    const result = [];
    let insideQuotes = false;
    let currentField = '';

    for (let i = 0; i < line.length; i++) {
        const char = line[i];

        if (char === '"' && insideQuotes && line[i + 1] === '"') {
            // Handle escaped quotes
            currentField += '"';
            i++; // Skip the next quote
        } else if (char === '"') {
            // Toggle the insideQuotes flag
            insideQuotes = !insideQuotes;
        } else if (char === ',' && !insideQuotes) {
            // If we encounter a comma and we're not inside quotes, it's the end of a field
            result.push(currentField.trim());
            currentField = '';
        } else {
            // Append the current character to the current field
            currentField += char;
        }
    }

    // Add the last field
    result.push(currentField.trim());

    return result;
}

module.exports = {
    parseCSVLine
};
