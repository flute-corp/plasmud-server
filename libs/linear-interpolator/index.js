/**
 * computes a linear interpolation
 * yb                     ......
 * -                ......
 * y          ..X...
 * ya   ......
 * -
 * ----xa-------x--------------xb
 * @param x {number} input value
 * @param xa {number} minimum input value
 * @param ya {number} output value matching the minimum input value (xa)
 * @param xb {number} maximum value
 * @param yb {number} output value matching the maximum input value (xb)
 * @returns {number} output value matching the input value
 */
function interpolateSegment(x, xa, ya, xb, yb) {
    return x * (ya - yb) / (xa - xb) + (xa * yb - xb * ya) / (xa - xb);
}

/**
 * Find out of a set of contiguous segment, which segment contains the value x
 * @param x {number} searched value
 * @param aSegments {{x: number, y: number}[]}
 * @param i0 {number} recursive searching segment registry i0
 * @param i1 {number} recursive searching segment registry i1
 * @returns {*|boolean|boolean|null}
 */
function findSegment(x, aSegments, i0 = null, i1 = null) {
    const nLen = aSegments.length;
    if (i0 === null) {
        return findSegment(x, aSegments, 0, nLen -1);
    }
    const p0 = aSegments[i0];
    const p1 = aSegments[i1];
    if (x >= p0.x && x <= p1.x) {
        if (Math.abs(i0 - i1) > 1) {
            const iMid = (i0 + i1) >> 1;
            let r = findSegment(x, aSegments, i0, iMid);
            if (r === false) {
                r = findSegment(x, aSegments, iMid, i1);
            }
            return r;
        } else {
            // il ne reste plus qu'un point
            return i0;
        }
    } else {
        return false;
    }
}

function interpolate(x, aSegments) {
    const i0 = findSegment(x, aSegments);
    if (i0 === false) {
        return false;
    }
    const p0 = aSegments[i0];
    const p1 = aSegments[i0 + 1];
    return interpolateSegment(x, p0.x, p0.y, p1.x, p1.y);
}

function makeSegments(...coords) {
    const a = [];
    for (let i = 0, l = coords.length; i < l; i += 2) {
        const x = coords[i];
        const y = coords[i + 1];
        a.push({x, y});
    }
    return a;
}

module.exports = {
    interpolate,
    findSegment,
    interpolateSegment,
    makeSegments
};
