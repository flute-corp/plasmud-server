class BracketTokenizer {
    constructor () {
        this.TOKENS = [':', '#'];
        this.init();
    }

    get tokens () {
        return this.TOKENS.join('');
    }

    set tokens (value) {
        this.TOKENS = value.split('');
        this.init();
    }

    init () {
        const j = this.TOKENS.join('');
        this.REGEXP_TOKENS = new RegExp('([' + j + '])');
        this.REGEXP_TOKENS_EXTER = new RegExp('(\\[[' + j + '][' + j + '[\\da-z]*])', 'i');
    }

    /**
   * Evalue le contenue du token, fournit une structure indiquant le nombre de paramètre # et de paramètre :
   * @param sToken
   * @returns {{}}
   */
    evaluateToken (sToken) {
        const sSubTokenString = sToken.substring(1, sToken.length - 1);
        const aSubTokens = sSubTokenString.split(this.REGEXP_TOKENS);
        const oTokenRegistry = {};
        let sCurrentToken = '';
        aSubTokens.forEach(t => {
            if (this.TOKENS.includes(t)) {
                sCurrentToken = t;
                if (!(t in oTokenRegistry)) {
                    oTokenRegistry[sCurrentToken] = [];
                }
            } else {
                if (sCurrentToken !== '' || t !== '') {
                    oTokenRegistry[sCurrentToken].push(t);
                }
            }
        });
        return oTokenRegistry;
    }

    tokenize (sInput) {
        const REG_EXP_TOKENIZER = this.REGEXP_TOKENS_EXTER;
        const aParts = sInput.split(REG_EXP_TOKENIZER);
        let tokens = {};
        return aParts.map(s => {
            if (s.match(REG_EXP_TOKENIZER)) {
                tokens = this.evaluateToken(s);
                return null;
            } else {
                return {
                    tokens, text: s
                };
            }
        }).filter(s => !!s);
    }
}

module.exports = BracketTokenizer;
