function getType (x) {
    const tox = typeof x;
    switch (tox) {
    case 'undefined': {
        return 'undefined';
    }
    case 'object': {
        if (x === null) {
            return 'null';
        }
        return x.constructor.name.toLowerCase();
    }
    default: {
        return tox;
    }
    }
}

function checkType (x, sExpectedType, sMessage = 'Type mismatch') {
    const s = getType(x);
    if (s !== sExpectedType) {
        throw new TypeError(sMessage + ' - expected type : ' + sExpectedType + ' - given type : ' + s);
    }
}

function switchType (x, oSwitcher) {
    const sType = getType(x);
    if (sType in oSwitcher) {
        return oSwitcher[sType](x);
    }
    for (const [sEntry, f] of Object.entries(oSwitcher)) {
        if (sEntry.includes('|')) {
            const aTypes = sEntry.split('|').map(s => s.trim());
            if (aTypes.includes(sType)) {
                return f(x);
            }
        }
    }
    throw new TypeError('switchType has encountered unexpected type "' + sType + '"');
}

module.exports = {
    getType,
    checkType,
    switchType
};
