const SPELL_SLOT_MANAGER_NS = 'bfcm-spell-slot-manager';

/**
 * @typedef SSMCharacter {object}
 * @property data {{}}
 */

/**
 * @class
 */
class SpellSlotManager {
    /**
     * @param timers {number[]}
     */
    constructor ({ timers = null } = {}) {
        if (timers && !(Array.isArray(timers) && timers.every(x => typeof x === 'number'))) {
            throw new TypeError('timer property should be number[]');
        }
        this._timePerLevel = timers ? timers : [
            10,
            12,
            15,
            19,
            24,
            30
        ];
    }

    get timers () {
        return this._timePerLevel;
    }

    static get NAMESPACE () {
        return SPELL_SLOT_MANAGER_NS;
    }

    /**
     * Renvoie les données concernant tous les emplacements de sorts
     * @param oCharacter {SSMCharacter}
     * @return {{ slots: number[][] }}
     */
    getSpellSlotData (oCharacter) {
        const data = oCharacter.data;
        if (!(SPELL_SLOT_MANAGER_NS in data)) {
            data[SPELL_SLOT_MANAGER_NS] = {
                slots: this._timePerLevel.map(() => [])
            };
        }
        return data[SPELL_SLOT_MANAGER_NS];
    }

    /**
     * Renvoie les données concernant les emplacements de sorts d'un niveau de sort spécifié
     * @param oCharacter {SSMCharacter}
     * @param nSlotLevel {number}
     * @returns {number[]}
     */
    getSpellSlotLevelData (oCharacter, nSlotLevel) {
        const iLevel = nSlotLevel - 1;
        const { slots } = this.getSpellSlotData(oCharacter);
        if (!slots[iLevel]) {
            throw new Error('Spell slot level invalid : ' + nSlotLevel + ' - max spell slot level is ' + slots.length);
        }
        return slots[iLevel];
    }

    /**
     * Consomme un emplacement de sort
     * @param oCharacter {SSMCharacter}
     * @param nSlotLevel {number}
     */
    consumeSpellSlot (oCharacter, nSlotLevel) {
        const slotData = this.getSpellSlotLevelData(oCharacter, nSlotLevel);
        slotData.push(this._timePerLevel[nSlotLevel - 1]);
    }

    /**
     * Recharge un emplacement de sort
     * @param oCharacter {SSMCharacter}
     * @param nSlotLevel {number}
     */
    rechargeSpellSlot (oCharacter, nSlotLevel) {
        const slotData = this.getSpellSlotLevelData(oCharacter, nSlotLevel);
        slotData.shift();
    }

    /**
     * Recharge tous les emplacements de sort d'un niveau donné
     * @param oCharacter {SSMCharacter}
     * @param nSlotLevel {number}
     */
    rechargeSpellSlotLevel (oCharacter, nSlotLevel) {
        const slotData = this.getSpellSlotLevelData(oCharacter, nSlotLevel);
        slotData.splice(0, slotData.length);
    }

    /**
     * Recharge tous les emplacements de sort du personnage
     * @param oCharacter {SSMCharacter}
     */
    rechargeAllSpellSlots (oCharacter) {
        this.getSpellSlotData(oCharacter).slots.forEach(slots => {
            slots.splice(0, slots.length);
        });
    }

    /**
     * Renvoie le nombre d'emplacements de sort consommés pour un niveau donné
     * @param oCharacter {SSMCharacter}
     * @param nSlotLevel {number}
     * @returns {number}
     */
    getConsumedSpellSlotLevelCount (oCharacter, nSlotLevel) {
        const slotData = this.getSpellSlotLevelData(oCharacter, nSlotLevel);
        return slotData.length;
    }

    /**
     * Renvoie la liste des nombres d'emplacements de sort consommés
     * @param oCharacter {SSMCharacter}
     * @returns {number[]}
     */
    getConsumedSpellSlots (oCharacter) {
        return this._timePerLevel.map((x, i) => this.getConsumedSpellSlotLevelCount(oCharacter, i + 1));
    }

    processLevel (oCharacter, nSlotLevel) {
        const slotData = this.getSpellSlotLevelData(oCharacter, nSlotLevel);
        let i = slotData.length - 1;
        while (i >= 0) {
            if (--slotData[i] <= 0) {
                slotData.splice(i, 1);
            }
            --i;
        }
    }

    restore (oCharacter) {
        for (let i = 0, l = this._timePerLevel.length; i < l; ++i) {
            this.processLevel(oCharacter, i + 1);
        }
    }
}

module.exports = SpellSlotManager;
