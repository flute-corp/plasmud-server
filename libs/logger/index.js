const A256 = require('../ansi-256-colors');
const { render } = require('../date-decoration');

const a256 = new A256();

/**
 * simple logging class that writes on stdout
 */
class Logger {
    /**
     * Date formatting utility
     * @param date {Date}
     */
    dateFormat (date) {
        return render(date, 'ymd hms');
    }

    /**
     * Transforms level and date into a prefix displayable string
     * @param level {string} log, warn, error, info
     * @param date {Date} now
     * @returns {string}
     */
    formatString (level, date) {
        const packet = [
            '[' + this.dateFormat(date) + ']',
            '[' + level + ']'
        ];
        return packet.join(' ');
    }

    log (...args) {
        console.log(this.formatString(a256.render('[#088]log'), new Date()), ...args);
    }

    error (...args) {
        console.error(this.formatString(a256.render('[#800]error'), new Date()), ...args);
    }
}

module.exports = new Logger();
