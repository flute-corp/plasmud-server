const Month = require('./Month');
const Events = require('events');


const FACTORY_DEF = {
    months: [
        {
            id: 1,
            displayShort: 'bla',
            displayLong: 'Blanchêne',
            displayNumeric: '01',
            dayCount: 30
        },
        {
            id: 2,
            displayShort: 'bri',
            displayLong: 'Brisefrost',
            displayNumeric: '02',
            dayCount: 30
        },
        {
            id: 3,
            displayShort: 'flo',
            displayLong: 'Florécume',
            displayNumeric: '03',
            dayCount: 30
        },
        {
            id: 4,
            displayShort: 'ver',
            displayLong: 'Verthée',
            displayNumeric: '04',
            dayCount: 30
        },
        {
            id: 5,
            displayShort: 'ecl',
            displayLong: 'Éclatbrise',
            displayNumeric: '05',
            dayCount: 30
        },
        {
            id: 6,
            displayShort: 'sol',
            displayLong: 'Solclair',
            displayNumeric: '06',
            dayCount: 30
        },
        {
            id: 7,
            displayShort: 'cha',
            displayLong: 'Chantfeu',
            displayNumeric: '07',
            dayCount: 30
        },
        {
            id: 8,
            displayShort: 'fla',
            displayLong: 'Flambeciel',
            displayNumeric: '08',
            dayCount: 30
        },
        {
            id: 9,
            displayShort: 'dor',
            displayLong: 'Doréclair',
            displayNumeric: '09',
            dayCount: 30
        },
        {
            id: 10,
            displayShort: 'emb',
            displayLong: 'Embrumes',
            displayNumeric: '10',
            dayCount: 30
        },
        {
            id: 11,
            displayShort: 'som',
            displayLong: 'Sombresonge',
            displayNumeric: '11',
            dayCount: 30
        },
        {
            id: 12,
            displayShort: 'giv',
            displayLong: 'Givrelune',
            displayNumeric: '12',
            dayCount: 30
        }
    ]
};

class GameTime {
    constructor () {
        this._time = {
            h: 0,
            m: 0,
        };
        this._date = {
            day: 0,
            month: 0,
            year: 0
        };
        this._months = FACTORY_DEF.months.map(m => new Month(m));
        this._events = new Events;
    }

    get events () {
        return this._events;
    }

    get time () {
        return {
            hour: this._time.h,
            minute: this._time.m
        };
    }

    /**
     *
     * @returns {Month}
     */
    get month () {
        this.advanceMonth(0);
        return this._months[this._date.month];
    }

    advanceYear (n = 1) {
        this._date.year += n;
    }

    advanceMonth (n = 1) {
        this._date.month += n;
        while (this._date.month >= this._months.length) {
            this._date.month -= this._months.length;
            this.advanceYear();
        }
    }

    advanceDay (n = 1) {
        this._date.day += n;
        while (this._date.day >= this.month.dayCount) {
            this._date.day -= this.month.dayCount;
            this.advanceMonth();
        }
    }

    advanceHour (n = 1) {
        this._time.h += n;
        while (this._time.h >= 24) {
            this._time.h -= 24;
            this.advanceDay();
        }
    }

    /**
     * Avance l'horloge d'une minute
     */
    advanceMinute (n = 1) {
        this._time.m += n;
        while (this._time.m >= 60) {
            this._time.m -= 60;
            this.advanceHour();
        }
    }

    triggerEvent () {
        this._events.emit('time', { time: this });
    }

    _padz (n, nz) {
        return n.toString().padStart(nz, '0');
    }

    toShortString () {
        return this._date.year.toString() + '-' +
            this.month.display.numeric + '-' +
            this._padz(this._date.day + 1, 2) +
            ' ' + this._padz(this._time.h, 2) + ':' + this._padz(this._time.m, 2);
    }

    normalize () {
        this.advanceMinute(0);
        this.advanceHour(0);
        this.advanceDay(0);
        this.advanceMonth(0);
        this.advanceYear(0);
    }

    setDate (y, m, d) {
        this._date.year = y;
        this._date.month = m - 1;
        this._date.day = d - 1;
        this.normalize();
    }

    setTime (h, m) {
        this._time.h = h;
        this._time.m = m;
        this.normalize();
    }
}

module.exports = GameTime;
