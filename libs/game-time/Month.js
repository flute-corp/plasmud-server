class Month {
    constructor ({
        id,
        displayShort,
        displayLong,
        displayNumeric,
        dayCount = 30
    }) {
        this._id = id;
        this._displayShort = displayShort;
        this._displayLong = displayLong;
        this._displayNumeric = displayNumeric;
        this._dayCount = 30;
    }

    get id () {
        return this._id;
    }

    get dayCount () {
        return this._dayCount;
    }

    get display () {
        return {
            short: this._displayShort,
            long: this._displayLong,
            numeric: this._displayNumeric
        };
    }
}

module.exports = Month;
