const { webFetcher: wb } = require('../../admin-libs/web-fetcher');
const { print } = require('../../admin-libs/print');

module.exports = function (yargs) {
    return yargs
        .command(
            'close',
            'Disconnect all clients, and close the service. This command is used to prevent clients from connecting during maintenance operations.',
            yargs => {
                yargs
                    .option('delay', {
                        alias: 'd',
                        default: 5,
                        describe: 'Add a delay (in minutes) before shutting down the service.'
                    })
                    .option('message', {
                        alias: 'm',
                        describe: 'Send a message to all connected clients every minute until shutdown.'
                    })
                    .option('cancel', {
                        alias: 'c',
                        describe: 'Cancel a delayed shutting down process.'
                    });
            },
            async argv => {
                if (argv.cancel) {
                    print('Cancelling service shutdown...');
                    const { result } = await wb.delete('/admin/shutdown');
                    print(result);
                } else {
                    const nDelay = parseInt('' + argv.delay);
                    print('Shutting down service.');
                    if (nDelay > 0) {
                        print('All clients will be disconnected in', nDelay, 'min.');
                    }
                    const { result } = await wb.put('/admin/shutdown', {
                        message: argv.message,
                        delay: nDelay
                    });
                    print(result);
                }
            }
        );
};
