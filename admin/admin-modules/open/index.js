const { webFetcher: wb } = require('../../admin-libs/web-fetcher');
const { print } = require('../../admin-libs/print');

module.exports = function (yargs) {
    return yargs
        .command(
            'open',
            'Open the service to incoming client connections.',
            {},
            async () => {
                const { result } = await wb.put('/admin/open');
                print(result);
            }
        );
};
