const { webFetcher: wb } = require('../../admin-libs/web-fetcher');
const { print } = require('../../admin-libs/print');
const TableRenderer = require('../../../libs/table-renderer');

module.exports = function (yargs) {
    return yargs
        .command(
            'status',
            'Display service status.',
            yargs => {
                yargs.option('table', {
                    alias: 't',
                    describe: 'Print status as a table, instead of a list.'
                });
            },
            async argv => {
                const toTable = aData => {
                    const tr = new TableRenderer();
                    tr.theme = tr.THEMES.FILET_THIN_ROUNDED;
                    const a = Object.entries(aData);
                    a.unshift(['Variables', 'Value']);
                    tr.render(a).forEach(s => print(s));
                };
                const toList = aData => {
                    print(aData);
                };
                const { result } = await wb.get('/admin/info');
                if (argv.table) {
                    print('switches:');
                    toTable(result.switches);
                    print('modules:');
                    toTable(result.game.modules);
                } else {
                    toList({
                        switches: result.switches,
                        modules: result.game.modules
                    });
                }
            }
        );
};
