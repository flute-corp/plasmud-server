const { webFetcher: wb } = require('../../admin-libs/web-fetcher');
const { print } = require('../../admin-libs/print');
const {parseDurationMs} = require('../../../libs/parse-duration');

module.exports = function (yargs) {
    return yargs
        .command(
            'unban <user>',
            'Cancel user banishment.',
            yargs => {
            },
            async argv => {
                print(await wb.delete('/admin/user/ban/' + argv.user));
            }
        );
};
