const { webFetcher: wb } = require('../../admin-libs/web-fetcher');
const { print } = require('../../admin-libs/print');


module.exports = function (yargs) {
    return yargs
        .command(
            'save <user>',
            'Save a user.',
            () => {
            },
            async argv => {
                const sUser = argv.user;
                print('saving', argv.user);
                print(await wb.put('/admin/user/save/' + sUser, {}));
            }
        );
};
