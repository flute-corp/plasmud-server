const { webFetcher: wb } = require('../../admin-libs/web-fetcher');
const { print } = require('../../admin-libs/print');
const { imbueYargs } = require('../../admin-libs/imbue-yargs');

module.exports = function (yargs) {
    return yargs
        .command(
            'user <user>',
            'Perform operations on a user or display information about a user.',
            yargs => {
                imbueYargs(yargs, [
                    require('./ban'),
                    require('./create'),
                    require('./unban'),
                    require('./role'),
                    require('./save')                ]);
            },
            async argv => {
                // sans option : afficher la liste des utilisateurs
                // avec option filter : afficher les utilisateurs répondant au filtre
                // avec option count : afficher les quelques premiers utilisateurs
                // avec option -v : afficher avec davantage de verbosité
                if (argv.user) {
                    const { result: user } = await wb.get('/admin/user/' + argv.user);
                    print(user);
                }
            }
        );
};
