const { webFetcher: wb } = require('../../admin-libs/web-fetcher');
const { print } = require('../../admin-libs/print');

module.exports = function (yargs) {
    return yargs
        .command(
            'role <user>',
            'Modify user roles.',
            yargs => {
                yargs
                    .option('add', {
                        alias: 'a',
                        choices: ['a', 'g', 'm'],
                        describe: 'Add a role.'
                    })
                    .option('remove', {
                        alias: ['rm', 'r'],
                        choices: ['a', 'g', 'm'],
                        describe: 'Remove a role.'
                    });
            },
            async argv => {
                if (argv.add) {
                    const sAdd = argv.add.toLowerCase().charAt(0);
                    switch (sAdd) {
                    case 'a': {
                        print(await wb.put('/admin/user/role/' + argv.user + '/USER_ROLE_ADMIN'));
                        break;
                    }
                    case 'g': {
                        print(await wb.put('/admin/user/role/' + argv.user + '/USER_ROLE_DUNGEON_MASTER'));
                        break;
                    }
                    case 'm': {
                        print(await wb.put('/admin/user/role/' + argv.user + '/USER_ROLE_MODERATOR'));
                        break;
                    }
                    }
                }
                if (argv.remove) {
                    const sRemove = argv.remove.toLowerCase().charAt(0);
                    switch (sRemove) {
                    case 'a': {
                        print(await wb.delete('/admin/user/role/' + argv.user + '/USER_ROLE_ADMIN'));
                        break;
                    }
                    case 'g': {
                        print(await wb.delete('/admin/user/role/' + argv.user + '/USER_ROLE_DUNGEON_MASTER'));
                        break;
                    }
                    case 'm': {
                        print(await wb.delete('/admin/user/role/' + argv.user + '/USER_ROLE_MODERATOR'));
                        break;
                    }
                    }
                }
            }
        );
};
