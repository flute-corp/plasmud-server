const { webFetcher: wb } = require('../../admin-libs/web-fetcher');
const { print } = require('../../admin-libs/print');
const { parseDurationMs } = require('../../../libs/parse-duration');

module.exports = function (yargs) {
    return yargs
        .command(
            'ban <user>',
            'Bannish a user.',
            yargs => {
                yargs
                    .option('duration', {
                        alias: 'd',
                        describe: 'Specify banishment duration in days (d), hours (h) or minutes (min).',
                        default: 'forever'
                    })
                    .option('reason', {
                        alias: 'r',
                        describe: 'Specify banishment reason.',
                        default: ''
                    });
            },
            async argv => {
                const sUser = argv.user;
                const nDuration = parseDurationMs(argv.duration);
                const sReason = argv.reason;
                print(await wb.put('/admin/user/ban/' + sUser, {
                    duration: nDuration,
                    reason: sReason
                }));
            }
        );
};
