const { webFetcher: wb } = require('../../admin-libs/web-fetcher');
const { print } = require('../../admin-libs/print');

module.exports = function (yargs) {
    return yargs
        .command(
            'create <user>',
            'Save a user.',
            yargs => {
                yargs
                    .option('password', {
                        alias: 'p',
                        describe: 'Specify user password.',
                        type: 'string',
                        demandOption: true
                    })
                    .option('email', {
                        alias: 'm',
                        describe: 'Specify user email.',
                        type: 'string',
                        demandOption: true
                    });
            },
            async argv => {
                const sUser = argv.user;
                print(await wb.post('/admin/user/create', {
                    name: sUser,
                    password: argv.password || null,
                    email: argv.email
                }));
            }
        );
};
