const { webFetcher: wb } = require('../../admin-libs/web-fetcher');
const { print } = require('../../admin-libs/print');

/**
 * Sets a new value to a character property
 * admin char xxxx set race RACE_ELF
 */
module.exports = function (yargs) {
    return yargs
        .command(
            'modify <charname> <property> <value>',
            'Sets a new value to a character state property.',
            yargs => {
            },
            async argv => {
                print(await wb.put('/admin/char/' + argv.charname, {
                    property: argv.property,
                    value: argv.value
                }));
            });
};
