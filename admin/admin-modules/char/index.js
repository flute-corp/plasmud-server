const { webFetcher: wb } = require('../../admin-libs/web-fetcher');
const { print } = require('../../admin-libs/print');
const { imbueYargs } = require('../../admin-libs/imbue-yargs');

module.exports = function (yargs) {
    return yargs
        .command(
            'char <charname>',
            'Perform operations on a user\'s character or display information about user characters.',
            yargs => {
                imbueYargs(yargs, [
                    require('./modify')
                ]);
            },
            async argv => {
                // sans option : afficher les données du personnage
                if (argv.charname) {
                    print(await wb.get('/admin/char/' + argv.charname));
                }
            }
        );
};
