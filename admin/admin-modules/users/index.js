const { roleConstToName } = require('../../../srv/infrastructure/frameworks/common/role-strings');
const { webFetcher: wb } = require('../../admin-libs/web-fetcher');
const { print } = require('../../admin-libs/print');
const TableRenderer = require('../../../libs/table-renderer');

async function getAdminQuery (sQuery, ...aArgs) {
    const { result } = await wb.get('/admin/' + sQuery + '/' + aArgs.join('/'));
    return result;
}

function r2n (s) {
    switch (roleConstToName(s)) {
    case 'admin': {
        return 'ADMIN';
    }
    case 'dungeon_master': {
        return 'GM';
    }
    case 'moderator': {
        return 'MOD';
    }

    default: {
        return s;
    }
    }
}



async function getAdminUser (sFilter = '', nCount = 0) {
    let prom;
    if (sFilter && nCount > 0) {
        prom = getAdminQuery('users', 'find', sFilter, nCount.toString());
    } else if (sFilter) {
        prom = getAdminQuery('users', 'find', sFilter);
    } else if (nCount > 0) {
        prom = getAdminQuery('users', nCount.toString());
    } else {
        prom = getAdminQuery('users');
    }
    prom = prom || Promise.resolve({ users: [] });
    const { users } = await prom;
    const aTable = users.map(aData => {
        aData.roles = aData.roles.map(r2n).sort().join(' ');
        const sState = aData.banned
            ? 'BANNED'
            : aData
                .state
                .substring(('CLIENT_STATE_').length)
                .replace(/_/g, ' ')
                .toLowerCase();
        return [
            aData.id,
            aData.name,
            aData.date,
            aData.roles,
            aData.type,
            sState
        ];
    });
    const aHeader = ['Id', 'Name', 'Connected on', 'Roles', 'Client', 'State'];
    aTable.unshift(aHeader);
    const tr = new TableRenderer();
    tr.theme = tr.THEMES.FILET_THIN_ROUNDED;
    tr.render(aTable).forEach(s => print(s));
}

module.exports = function (yargs) {
    return yargs
        .command(
            'users',
            'Provide user listing and management functionalities.',
            yargs => {
                yargs
                    .option('filter', {
                        alias: 'f',
                        describe: 'Specify filter on user name. Only display users whose name matches the filter.',
                        default: '',
                        type: 'string'
                    })
                    .option('count', {
                        alias: 'c',
                        describe: 'Limit the maximum displayed users.',
                        default: 0,
                        type: 'number'
                    });
            },
            async argv => {
                // sans option : afficher la liste des utilisateurs
                // avec option filter : afficher les utilisateurs répondant au filtre
                // avec option count : afficher les quelques premiers utilisateurs
                // avec option -v : afficher avec davantage de verbosité
                const sFilter = argv.filter || '';
                const nCount = argv.count || 0;
                await getAdminUser(sFilter, nCount);
            }
        );
};
