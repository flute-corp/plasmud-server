function imbueYargs (yargs, aCommands) {
    return aCommands.reduce((prev, curr) => curr(prev), yargs);
}

module.exports = { imbueYargs };
