const { getType } = require('../../../libs/get-type');
const YAML = require('yaml');
const { highlight, defineTheme } = require('cli-highlight');

const myTheme =  {
    keyword: s => `\x1b[1;31m${s}\x1b[0m`, // Rouge et gras
    built_in: s => `\x1b[1;33m${s}\x1b[0m`, // Jaune et gras
    string: s => `\x1b[32m${s}\x1b[0m`, // Vert
    number: s => `\x1b[1;34m${s}\x1b[0m`, // Bleu
    literal: s => {
        switch (s) {
        case 'true': {
            return '\x1b[1;32mtrue\x1b[0m';
        }
        case 'false': {
            return '\x1b[1;31mfalse\x1b[0m';
        }
        case 'null': {
            return '\x1b[1;30mnull\x1b[0m';
        }
        default: {
            return s;
        }
        }
    },
    attr: s => s,
    comment: s => `\x1b[90m${s}\x1b[0m`, // Gris
    punctuation: s => `\x1b[1;37m${s}\x1b[0m`, // Blanc et gras
    function: s => `\x1b[35m${s}\x1b[0m`, // Magenta
    variable: s => `\x1b[36m${s}\x1b[0m`, // Cyan
};

function highlightedYaml(jsonObject) {
    try {
        // Convert JSON object to YAML string
        const yamlStr = YAML.stringify(jsonObject, {
            indent: 2 // Set indentation level (2 spaces by default)
        });
        // Highlight the YAML string for terminal display
        return highlight(yamlStr, { language: 'yaml', theme: myTheme });
    } catch (e) {
        console.error('Error converting JSON to highlighted YAML:', e);
        return null;
    }
}

function transformDisplay (aArgs) {
    return aArgs.map(a => {
        if (getType(a) === 'object') {
            return highlightedYaml(a);
        } else {
            return a;
        }
    });
}

function print (...aArgs) {
    console.log(...transformDisplay(aArgs));
}

print.error = (...aArgs) => {
    console.error(...transformDisplay(aArgs));
};

module.exports = {
    print
};
