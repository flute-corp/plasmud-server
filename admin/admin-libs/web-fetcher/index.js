const WebFetcher = require('../../../libs/o876-web-fetcher');
const { getEnv } = require('../../../srv/env-parser');

const wb = new WebFetcher({
    host: 'http://localhost',
    port: parseInt(getEnv().PLASMUD_SERVER_PORT_ADMIN)
});

module.exports = {
    webFetcher: wb
};
